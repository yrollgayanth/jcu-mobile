import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native'
import UIUtils from '../../Utils/UIUtils'
import {COLOR, EVENT} from '../../Constants/Constants'
import EventEmitter from "react-native-eventemitter"

export default class TopMenuBar extends Component<{}> {

  constructor(prop){
    super(prop);
    this.state={
      orderChanged:true
    }
    this.OrderChanged = this.OrderChanged.bind(this)
  }

  clickedOnItem(itemKey){
    EventEmitter.emit(EVENT.HOME_TOP_BAR, {selected_tab:itemKey})
  }

  componentWillMount(){
    EventEmitter.addListener(EVENT.QUICKLINK_ORDER_CHANGED, this.OrderChanged)
  }

  componentWillUnmount(){
    EventEmitter.removeListener(EVENT.QUICKLINK_ORDER_CHANGED, this.OrderChanged)
  }

  OrderChanged(){
    alert("order changed")
  }

  renderMenuItem(itemModel){

    return(
        <TouchableHighlight style={styles.menuItemWrapper}
                            key={itemModel.Name}
                            underlayColor="transparent"
                            onPress={() => {this.clickedOnItem(itemModel.Name)}}>
          <View style={styles.itemContent}>
            <Image style={styles.itemIcon} source={{uri:itemModel.Icon}}></Image>
            <Text style={styles.itemName}>{itemModel.Name}</Text>
          </View>
        </TouchableHighlight>
    );
  }

  renderMenuItems(){
    var menuItemViews = [];
    var menuItemModels = [];
    menuItemModels.push({"Name":"Email","Icon":require('../../../assets/img/base64/ic_mail_blue.json')})
    menuItemModels.push({"Name":"Ask Us","Icon":require('../../../assets/img/base64/ic_online_faq.json')})
    menuItemModels.push({"Name":"Campus Safety","Icon":require('../../../assets/img/base64/ic_campus_safety.json')})

    for(var itemModel of menuItemModels){
      menuItemViews.push(this.renderMenuItem(itemModel));
    }
    return menuItemViews;
  }

  render() {
    return(
      <View style={styles.container}>
        {this.renderMenuItems()}
      </View>);
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
    alignItems:'center',
    flexDirection:'row',
  },
  menuItemWrapper:{
    flex:1
  },
  itemContent:{
    flex:1,
    alignItems:'center',
    paddingTop:UIUtils.size(5),
    paddingBottom:UIUtils.size(5)
  },
  itemName:{
    fontSize:UIUtils.size(12),
    color:"#969696"
  },
  itemIcon:{
    width:UIUtils.size(20),
    height:UIUtils.size(20),
    resizeMode:'contain'
  }
});
