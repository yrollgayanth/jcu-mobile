/**
* Created by Yrol on 27/03/18.
*/

import React, { Component } from 'react'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, ScrollView } from 'react-native';
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import {COLOR,LAYOUT_SPACE,FONT_SIZE, EVENT} from '../../Constants/Constants'
import LinearGradient from 'react-native-linear-gradient'
import EventEmitter from "react-native-eventemitter"

export default class TestProgress extends Component<{}> {

  constructor(prop){
    super(prop)
    this.state={
      isHorizontal:UIUtils.isDeviceInHorizontal()
    }
    this.onLayout = this.onLayout.bind(this)
  }

  componentWillMount(){
    
  }

  //layout changes
  onLayout(){

  }


  render(){
    return(
      <View ></View>
    )
  }

}
