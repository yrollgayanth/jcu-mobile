/**
* Created by Yrol on 21/02/18.
*/

import React, { Component } from 'react'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, ScrollView } from 'react-native';
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import {COLOR,LAYOUT_SPACE,FONT_SIZE, EVENT} from '../../Constants/Constants'
import LinearGradient from 'react-native-linear-gradient'
import EventEmitter from "react-native-eventemitter"
import Moment from 'moment'

export default class EventsWidget extends Component<{}> {
  constructor(prop){
    super(prop);

    this.state = {
      dotCount:this.props.countItems,
      activeDot:0,
      isHorizontal:UIUtils.isDeviceInHorizontal(),
      eventData:this.props.eventData,
      expanded:true
    }
    this.onScrollEnd = this.onScrollEnd.bind(this)
    this.onLayout = this.onLayout.bind(this)
    this.openNews = this.openNews.bind(this)
  }

  renderDotBar(){
    var dots = [];
    for(var i=0;i<this.state.dotCount;i++){
      if(i==this.state.activeDot){
        dots.push(
          <View style={styles.dotWrapper} key={i}>
          <Image style={styles.dotImage} source={require('../../../assets/img/dot_active.png')}></Image>
          </View>
        );
      }else{
        dots.push(
          <View style={styles.dotWrapper} key={i}>
          <Image style={styles.dotImage} source={require('../../../assets/img/dot_inactive.png')}></Image>
          </View>
        );
      }
    }
    return dots;
  }

  //when cleck on see all news
  seeAllNews(){
    EventEmitter.emit(EVENT.SHOW_ALL_NEWS)
  }

  openNews(nid,link,title){
    EventEmitter.emit(EVENT.OPEN_NEWS_ITEM,{news_id:nid, news_link:link, news_title:title})
  }

  processEventItems(){

    var eventData = this.state.eventData
    var eventModelCollection = []
    var eventItems = []

    if(eventData.length > 0){
      //create event objects
      for(var i=0; i<eventData.length; i++){
        var eventID = eventData[i].id
        var eventTitle = eventData[i].title
        var eventText = eventData[i].text
        var eventUrl = eventData[i].url
        var eventCreationDate = eventData[i].creation_date
        var eventStartDate = eventData[i].start_date
        var eventImage = eventData[i].image
        var eventItem = {eventID:eventID, eventTitle:eventTitle, eventImage:eventImage, eventUrl:eventUrl, creationDate:eventCreationDate, startDate:eventStartDate};
        eventModelCollection.push(eventItem)
      }
    }


    /*
    * <View style={eventItemStyle.containerInfo}>
     <Image source={{uri: eventModelCollection[i].eventImage}} style={eventItemStyle.imageBlock} />
     <View>
     <Text style={eventItemStyle.mainInfo}>{eventModelCollection[i].eventTitle}</Text>
     <Text style={eventItemStyle.subInfo}></Text>
     <Text style={eventItemStyle.subInfo}>{eventModelCollection[i].creationDate}</Text>
     </View>
     </View>
    * */

    // var eventItem1 = {eventTitle:"TestOne TestOne TestOne TestOne TestOne TestOne TestOne TestOne TestOne TestOne ", eventImage:"https://i.imgur.com/LVHVZUk.jpg", creationDate:"09/07/18", startDate:"08/09/18"}
    // eventModelCollection.push(eventItem1)
    //
    // var eventItem2 = {eventTitle:"TestTwo", eventImage:"https://i.imgur.com/LVHVZUk.jpg", creationDate:"09/07/18", startDate:"08/09/18"};
    // eventModelCollection.push(eventItem2)


    //create output
    if(eventModelCollection.length == 0){
      var i = 0
      eventItems.push(
        <View key={i} style={{ flex: 1, flexDirection:'row', height:UIUtils.size(90), alignItems:"center", justifyContent: 'center', width:UIUtils.getScreenWidth()
        }}>
        <Text>{"No news items found"}</Text>
        </View>
      )
    }else{
      for(var i=0;i<eventModelCollection.length;i++){
        let currentIndex = i
        let eventID = eventModelCollection[currentIndex].eventID
        let eventNewsLink = eventModelCollection[currentIndex].eventUrl
        let eventTitle = eventModelCollection[currentIndex].eventTitle
        let creationDate = eventModelCollection[currentIndex].creationDate

        eventItems.push(
          <TouchableHighlight onPress={()=>this.openNews(eventID, eventNewsLink, eventTitle)} key={currentIndex} style={[styles.scrollContent]}>
            <View style={{flexDirection: 'row'}}>
              <Image source={{uri: eventModelCollection[currentIndex].eventImage}} style={eventItemStyle.imageBlock} />
              <View style={{flex: 1}}>
                <Text style={eventItemStyle.mainInfo}>{eventModelCollection[currentIndex].eventTitle}</Text>
                <Text style={eventItemStyle.subInfo}></Text>
                <Text style={eventItemStyle.subInfo}>{Moment(creationDate).format('ddd, DD MMM YYYY')}</Text>
              </View>
            </View>
          </TouchableHighlight>)
      }
    }
    return eventItems
  }

  onScrollEnd(event){
    var width = UIUtils.getScreenWidth();
    var xOffset = event.nativeEvent.contentOffset.x;
    var scrolledToIndex = xOffset/width;
    this.setState({activeDot:scrolledToIndex});
  }

  onLayout(){
    if(this.state.isHorizontal!=UIUtils.isDeviceInHorizontal()){
      this.setState({isHorizontal:UIUtils.isDeviceInHorizontal()});
      this.refs.scrollView.scrollTo({x:0, y:0, animated:false});
      this.setState({activeDot:0});
    }
  }

  //render see all button
  renderSeeAllButton(){
    if(this.state.eventData.length > 0){
      return (
        <TouchableHighlight style={styles.seeAllButton}
        onPress={()=>this.seeAllNews()}>
        <Text style={styles.buttonText}>SEE ALL</Text>
        </TouchableHighlight>
      )
    }

    return(
      <View></View>
    )
  }

  renderArrow(){
    if(this.state.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }

  renderExpandableSection(){
    var branding = Branding.sharedInstance
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {this.setState({expanded:!this.state.expanded})}}>
      <LinearGradient colors={branding.getHeaderGradient()}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={[styles.sectionTitle, {color:branding.getHeaderFontColor()}]}>{this.props.title}</Text>
      {this.renderArrow()}
      </LinearGradient>
      </TouchableHighlight>
    );
  }

  //render all content
  renderAllContent(){
    if(this.state.expanded){
      return(
        <View>
          <View style={styles.scrollViewWrapper}>
            <ScrollView ref="scrollView"
            style={styles.scrollView}
            onMomentumScrollEnd={this.onScrollEnd}
            showsHorizontalScrollIndicator={false}
            pagingEnabled={true}
            horizontal={true}>
            {this.processEventItems()}
            </ScrollView>
          </View>
          <View style={styles.dotBar}>
            <View style={styles.dotsWrapper}>{this.renderDotBar()}</View>
          </View>
          {this.renderSeeAllButton()}
        </View>
      )
    }
  }

  render() {
    return (
      <View style={styles.container} onLayout={this.onLayout}>
        {this.renderExpandableSection()}
        {this.renderAllContent()}
      </View>
    );
  }
}

const eventItemStyle = StyleSheet.create({
  containerProducts:{
  paddingTop: 40,
  paddingLeft: 15,
  flexDirection: 'row',
 },
productName: {
  alignSelf: 'flex-start',
},
minus:{
  width: 20,
  height: 20,
  borderRadius: 20/2,
  backgroundColor: 'gray',
},
containerInfo:{
  paddingTop:15,
  flexDirection:'row',
  paddingLeft: 15
},
mainInfo:{
  fontWeight: 'bold',
  paddingLeft: 15,
  paddingRight: 20
},
subInfo:{
  color: 'gray',
  paddingLeft: 15,
},
imageBlock: {
  width: 60,
  height: 60,
  marginLeft:10,
  resizeMode:'cover',
  backgroundColor: 'gray',
  justifyContent: 'flex-end',
},
})

const styles = StyleSheet.create({
  container: {
    flexDirection:'column',
  },
  scrollViewWrapper:{
    // height:100
  },
  scrollView:{
    backgroundColor:'white'
  },
  scrollContent:{
    flex: 1,
    flexDirection:'row',
    height:UIUtils.size(90),
    alignItems:"center",
    width:UIUtils.getScreenWidth()
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  dotBar:{
    flexDirection:'row',
    height:UIUtils.size(35),
    backgroundColor:COLOR.NAV_GREY,
    justifyContent:"space-around",
    alignItems:'center'
  },
  dotsWrapper:{
    flexDirection:'row',
  },
  dotWrapper:{
    // backgroundColor:'white',
    alignItems:'center',
    justifyContent:'space-around',
    flexDirection:'row',
    width:UIUtils.size(15),
    height:UIUtils.size(15),
  },
  dotImage:{
    width:UIUtils.size(8),
    height:UIUtils.size(8),
    resizeMode:'contain'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  seeAllButton:{
    backgroundColor:'white',
    width:UIUtils.size(70),
    height:UIUtils.size(30),
    borderRadius: UIUtils.size(6),
    borderColor:COLOR.APP_THEME_BLUE,
    borderWidth:UIUtils.size(1),
    alignItems:'center',
    justifyContent:'space-around',
    position:'absolute',
    right:UIUtils.size(10),
    bottom:UIUtils.size(2.5),
  },
  buttonText:{
    fontSize:UIUtils.size(14),
    color:COLOR.APP_THEME_BLUE,
    fontWeight:'bold'
  }
});
