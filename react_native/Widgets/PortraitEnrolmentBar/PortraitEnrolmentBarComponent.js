/**
* Created by Yrol on 05/04/18.
*/

import React, { Component } from 'react'
import {Platform, Dimensions, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, ScrollView } from 'react-native';
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import {COLOR,LAYOUT_SPACE,FONT_SIZE, EVENT} from '../../Constants/Constants'
import LinearGradient from 'react-native-linear-gradient'
import EventEmitter from "react-native-eventemitter"
import EnrolmentObject from "../../Objects/EnrolmentObject"
import StepObject from "../../Objects/StepObject"
import Triangle from 'react-native-triangle'
import Carousel from 'react-native-snap-carousel'

const { height, width } = Dimensions.get('window')

export default class PortraitEnrolmentBarComponent extends Component<{}> {

  constructor(prop){
    super(prop)
    this.state={
      isHorizontal:UIUtils.isDeviceInHorizontal(),
      expanded:true,
      enrolmentData:this.props.enrolmentData
    }
    this.onLayout = this.onLayout.bind(this)
  }

  onLayout(){
    if(this.state.isHorizontal!=UIUtils.isDeviceInHorizontal()){
      this.setState({isHorizontal:UIUtils.isDeviceInHorizontal()});
    }
  }

  renderExpandableSection(){
    var branding = Branding.sharedInstance
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {this.setState({expanded:!this.state.expanded})}}>
      <LinearGradient colors={branding.getHeaderGradient()}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={[styles.sectionTitle, {color:branding.getHeaderFontColor()}]}>{this.props.title}</Text>
      {this.renderArrow()}
      </LinearGradient>
      </TouchableHighlight>
    );
  }

  renderArrow(){
    if(this.state.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }

  openEnrolmentStep(courseCode, courseName, courseID, stepName, stepKey){
    this.props.tapCallback({
      "stepName":stepName,
      "courseId":courseID,
      "courseName":courseName,
      "courseCode":courseCode,
      "stepKey":stepKey
    })
  }

  renderItem = ({item}) => {
    if(item.current_status === true){//current step
      return (
        <TouchableHighlight onPress={()=>this.openEnrolmentStep(item.courseCode, item.courseName, item.courseID, item.step_name, item.step_key)} style={[styles.slide, {flex:1}]}>
          <View>
              <LinearGradient colors={['#66cc00','#cef7a5']}
                              start={{ x: 0, y: 1 }}
                              end={{ x: 1, y: 1 }}
                              style={[styles.currentBlock, {flexDirection: 'row'}]} >
              <View style={[styles.currentBlockTitle]}>
                <Image style={[styles.iconBlock]}  source={this.getEnrolmentIcon(item.step_key, true)} />
              </View>
              <View style={[styles.currentBlockTitle]}>
                <Text style={[styles.currentText]}>{item.step_name}</Text>
              </View>
                  <View style={styles.triangle}>
                      <Triangle
                          width={20}
                          height={UIUtils.size(40)/2}
                          color={this.state.bgColor}
                          direction={'up-right'}
                      />
                      <Triangle
                          width={20}
                          height={UIUtils.size(40)/2}
                          color={this.state.bgColor}
                          direction={'down-right'}
                      />
                  </View>
              </LinearGradient>
          </View>
        </TouchableHighlight>
      )
    }else if(item.complete_status === true){ //completed step
      return (
        <TouchableHighlight onPress={()=>this.openEnrolmentStep(item.courseCode, item.courseName, item.courseID, item.step_name, item.step_key)} style={[styles.slide, {flex:1}]}>
          <View style={[styles.completeBlock, {flexDirection: 'row'}]}>
              <Image style={[styles.iconBlock]}  source={this.getEnrolmentIcon(item.step_key, true)} />
          </View>
        </TouchableHighlight>
      )
    }else{//not completed step
      return (
        <TouchableHighlight onPress={()=>this.openEnrolmentStep(item.courseCode, item.courseName, item.courseID, item.step_name, item.step_key)} style={[styles.slide, {flex:1}]}>
          <View style={[styles.incompleteBlock, {flexDirection: 'row'}]}>
              <Image style={[styles.iconBlock]}  source={this.getEnrolmentIcon(item.step_key, false)} />
          </View>
        </TouchableHighlight>
      )
    }
  }

  //render body content
  renderBodyContent(){
    var enrolmentData = this.state.enrolmentData
    var enrolmentItems = []
    var enrolmentObjects = []

    if( (typeof enrolmentData === 'undefined' || enrolmentData === null || enrolmentData.length <=0) && this.state.expanded === true ){
      return(
        <View key={i} style={[styles.nodataFoundContainer, {flexDirection:'row'}]}><Text>No enrolment data found</Text></View>
      )
    }

    //map enrolment data
    for(var i=0; i<enrolmentData.length; i++){
      var enrolmentObj = new EnrolmentObject(enrolmentData[i].course_code, enrolmentData[i].course_name, enrolmentData[i].courseId)

      Object.keys(enrolmentData[i].steps).map(function (key) {
        var completeStatus = enrolmentData[i].steps[key].complete
        var availableStatus = enrolmentData[i].steps[key].available
        var requirements = enrolmentData[i].steps[key].requires
        var base64Image = enrolmentData[i].steps[key].icon
        // var content = enrolmentData[i].steps[key].content
        var stepName = enrolmentData[i].steps[key].stepname
        var currentStatus = enrolmentData[i].steps[key].current_step
        var courseCode = enrolmentData[i].course_code
        var courseName = enrolmentData[i].course_name
        var courseID = enrolmentData[i].courseId
        var stepObj = new StepObject(key, stepName, completeStatus, availableStatus, requirements, base64Image, currentStatus, courseCode, courseName, courseID)
        enrolmentObj.addStep(stepObj)
      })
      enrolmentObjects.push(enrolmentObj)
    }

    //creating enrolment object
    for(var i=0; i<enrolmentObjects.length; i++){
      var firstItem = 0
      let enrolData = enrolmentObjects[i].getSteps()

      //get the current step
      for(var j = 0; j<enrolData.length; j++){
        let isCurrentStep = enrolData[j].current_status
        if(isCurrentStep === true){
          firstItem = j
        }
      }

      //determine first item
      if(firstItem === 0 || firstItem === 1){
        firstItem = 0
      }else if(firstItem === 2){
        firstItem = 1
      }else if(firstItem === 3){
        firstItem = 2
      }else if(firstItem === 4){
        firstItem = 3
      }else{
        firstItem = 3
      }

      enrolmentItems.push(
        <View key={i}>
          <View style={[styles.courseNameBlock]}><Text style={{color:"black", fontWeight:"bold"}}>{enrolmentObjects[i].course_code + " - " +enrolmentObjects[i].course_name}</Text></View>
          <View style={{backgroundColor:"#ffffff"}}>
            <Carousel
              firstItem={firstItem}
              layout={'default'}
              sliderWidth={width}
              itemWidth={width/3}
              activeSlideAlignment={'start'}
              inactiveSlideScale={1}
              inactiveSlideOpacity={1}
              data={enrolmentObjects[i].getSteps()}
              renderItem={this.renderItem}
            />
          </View>
        </View>
      )
    }

    if(this.state.expanded){
      return enrolmentItems
    }
  }

  render () {
      return (
        <View style={styles.container} onLayout={this.onLayout}>
          {this.renderExpandableSection()}
          {this.renderBodyContent()}
        </View>
      );
  }

  //get enrolment icon
  getEnrolmentIcon(stepKey, status){

    var enrolIconsWhiteMap = {
      offer:require('../../../assets/img/en_offer_white.png'),
      details:require('../../../assets/img/en_details_white.png'),
      ecaf:require('../../../assets/img/en_ecaf_white.png'),
      plan:require('../../../assets/img/en_plan_white.png'),
      subject:require('../../../assets/img/en_subjects_white.png'),
      classes:require('../../../assets/img/en_classes_white.png')
    }


    var enrolIconsGreyMap = {
      offer:require('../../../assets/img/en_offer_grey.png'),
      details:require('../../../assets/img/en_details_grey.png'),
      ecaf:require('../../../assets/img/en_ecaf_grey.png'),
      plan:require('../../../assets/img/en_plan_grey.png'),
      subject:require('../../../assets/img/en_subjects_grey.png'),
      classes:require('../../../assets/img/en_classes_grey.png')
    }

    //*************OFFER**********//
    if(stepKey === "ACPT-OFF" && status === true){
      return enrolIconsWhiteMap.offer
    }

    if(stepKey === "ACPT-OFF" && status === false){
      return enrolIconsGreyMap.offer
    }

    //*************CLASSES REG**********//
    if(stepKey === "CLS-REG" && status === true){
      return enrolIconsWhiteMap.classes
    }

    if(stepKey === "CLS-REG" && status === false){
      return enrolIconsGreyMap.classes
    }

    //*************CLASSES REG**********//
    if(stepKey === "CNF-DTLS" && status === true){
      return enrolIconsWhiteMap.details
    }

    if(stepKey === "CNF-DTLS" && status === false){
      return enrolIconsGreyMap.details
    }

    //*************ECAF**********//
    if(stepKey === "ECAF" && status === true){
      return enrolIconsWhiteMap.ecaf
    }

    if(stepKey === "ECAF" && status === false){
      return enrolIconsGreyMap.ecaf
    }

    //*************PLAN YOUR S**********//
    if(stepKey === "PLAN-YOUR-STUDY" && status === true){
      return enrolIconsWhiteMap.plan
    }

    if(stepKey === "PLAN-YOUR-STUDY" && status === false){
      return enrolIconsGreyMap.plan
    }

    //*************ENROLMENT**********//
    if(stepKey === "ENR" && status === true){
      return enrolIconsWhiteMap.subject
    }

    if(stepKey === "ENR" && status === false){
      return enrolIconsGreyMap.subject
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'column',
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  logoStyle: {
      width: width / 3,
      height: 60,
      borderColor: 'white',
      borderWidth:1,
      backgroundColor:"green",
      paddingHorizontal:0
  },
  nodataFoundContainer:{
    paddingTop:10,
    paddingLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    height:40,
    backgroundColor:"white"
  },
  completeBlock:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"#66cc00",
    height:UIUtils.size(40),
    width:width / 3
  },
  incompleteBlock:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"#FFFFFF",
    height:UIUtils.size(40),
    width:UIUtils.getScreenWidth()/3
  },
  currentBlock:{
    // backgroundColor:"#66cc00",
    // height:UIUtils.size(40),
    //   flex:1
    position:'relative',
    width:UIUtils.getScreenWidth()/3
  },
  courseNameBlock:{
    flexDirection:'row',
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    height:'auto',
    backgroundColor:'white'
  },
  currentBlockTitle:{
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:"#66cc00",
    height:UIUtils.size(40),
    width:'auto'
  },
  iconBlock:{
    width:30,
    height:30
  },
  currentBlockIcon:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"yellow",
    height:UIUtils.size(40),
    width:'auto'
  },
  currentText:{
    color:"#ffffff",
    padding:10
  },
  triangle:{
      position:'absolute',
      right:0,
      width:20,
      height:UIUtils.size(40)
  }
});
