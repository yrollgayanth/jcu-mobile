import React, { Component } from 'react';
import {Linking, StyleSheet, Modal, Text, TouchableHighlight, View, ScrollView } from 'react-native';
import UIUtils from '../../Utils/UIUtils';
import {COLOR} from '../../Constants/Constants';
import HTMLView from 'react-native-htmlview';


export default class AdvancedPopupWidget extends Component {


    /*
    popup:  this.refs.alertDialog.setVisible(true);


      Single ok button

     <AdvancedPopupWidget ref='alertDialog'
     message={"!!!!Message from My Study page,a this message is veeeeeeeeeeery  lonnnnnnnnnnnnnnnnnnnnnnnng."}
     okButtonCallback={()=>{}}
     ></AdvancedPopupWidget>
    *
    *
    *
    * */

    constructor(props) {
        super(props);
        this.state = {modalVisible: false};
        this.okButtonTapped = this.okButtonTapped.bind(this);
        this.renderTitle = this.renderTitle.bind(this);
        this.sendEmail = this.sendEmail.bind(this);
        this.makePhoneCall = this.makePhoneCall.bind(this);
    }

    setVisible(visible) {
        this.setState({modalVisible: visible});
    }


    okButtonTapped(){
        this.setVisible(!this.state.modalVisible);
        this.props.singleButtonCallback();

    }

    renderButtonBar(){

        return(
            <View style={styles.singleButtonBar}>
                <TouchableHighlight style={styles.okButton}
                                    onPress={this.okButtonTapped}>
                    <Text style={styles.buttonText}>{this.props.singleButtonLabel}</Text>
                </TouchableHighlight>
            </View>

        );

    }

    renderTitle(){

        if(this.props.title==null||this.props.title===""){
            return (
                <View></View>
            )
        }else{
            return(
                <Text style={styles.title}>
                    {this.props.title}
                </Text>
            );

        }
    }

    sendEmail(emailAddress){
        // alert(emailAddress);
        Linking.openURL('mailto:'+emailAddress)
    }

    makePhoneCall(phoneNumber){
        Linking.openURL('tel:'+phoneNumber)
        // alert(phoneNumber);
    }

    openLinkInDescription(url){

        if(url.indexOf('mailto:')>-1){
            this.sendEmail(url.split(":")[1])
        }else if(url.indexOf('tel:')>-1){
            this.makePhoneCall(url.split(":")[1])
        }

    }

    render() {

        let htmlContent = '<p>Congratulations on your offer!We are currently processing your offer and will email you shortly with instructions on how to activate your account details.'
            +'In the meantime, we encourage you to explore our GetReady4Uni page with lots of useful information to help you get prepared for study at JCU.'
            +'If you have any questions, or you have already received your email from JCU and are still seeing this message,'
        +'then please contact <a href="mailto:enquiries@jcu.edu.au" target="_top">enquiries@jcu.edu.au</a> or phone <a href="tel:1800246446" target="_top">1800 246 446</a> so that we can assist you.</p>'

        return (
            <View style={styles.container}>

                <Modal
                        animationType={"fade"}
                        transparent={true}
                        visible={this.state.modalVisible}
                       onRequestClose={() => {
                           // alert("Modal has been closed.")
                        }
                       }
                >
                    <View style={styles.modalWrapper}>
                        <View style={styles.modalContent}>
                            {this.renderTitle()}
                            <ScrollView style={styles.htmlViewWrapper}>
                                <HTMLView
                                    stylesheet={htmlViewStyles}
                                    value={this.props.message}
                                    onLinkPress={(url) => this.openLinkInDescription(url)}
                                />
                            </ScrollView>
                            {this.renderButtonBar()}

                        </View>

                    </View>

                </Modal>

            </View>
        );
    }
}

const htmlViewStyles = StyleSheet.create({
    div: {
        marginTop:UIUtils.size(5),
        paddingTop:UIUtils.size(5),
        fontSize:UIUtils.defaultMenuTextFontSize()
    },
    p: {
        fontSize:UIUtils.defaultMenuTextFontSize(),
        lineHeight:UIUtils.defaultMenuTextFontSize()+2
    },
    a:{
        color:'blue',
        fontSize:UIUtils.defaultMenuTextFontSize(),
        lineHeight:UIUtils.defaultMenuTextFontSize()+2
    },
    ul:{
        lineHeight:UIUtils.defaultMenuTextFontSize()+2
    }
});

const styles = StyleSheet.create({

    htmlViewWrapper:{
        padding:UIUtils.size(10)
    },
    modalWrapper:{
        flex:1,
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:'#00000055'
    },
    modalContent: {
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        minHeight: UIUtils.size(120),
        width: UIUtils.size(300),
        borderRadius: UIUtils.size(6)

    },
    title:{
        color:COLOR.APP_THEME_BLUE,
        fontWeight:"bold",
        paddingLeft:UIUtils.size(20),
        paddingRight:UIUtils.size(20),
        paddingTop:UIUtils.size(20),
        fontSize:UIUtils.size(15),
        flexWrap:'wrap',
        textAlign:'center'
    },
    messageContent:{
        padding:UIUtils.size(20),
        flexWrap:'wrap',
        fontSize:UIUtils.size(15)
    },
    buttonBar:{
        padding:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    singleButtonBar:{
        padding:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around'
    },
    button:{
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:COLOR.APP_THEME_BLUE,
        width:UIUtils.size(80),
        height:UIUtils.size(35),
        borderRadius: UIUtils.size(3)
    },
    leftButton:{
        marginLeft:UIUtils.size(50),
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:COLOR.APP_THEME_BLUE,
        width:UIUtils.size(80),
        height:UIUtils.size(35),
        borderRadius: UIUtils.size(6)
    },
    rightButton:{
        marginRight:UIUtils.size(50),
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:COLOR.APP_THEME_BLUE,
        width:UIUtils.size(80),
        height:UIUtils.size(35),
        borderRadius: UIUtils.size(6)
    },
    okButton:{
            alignItems:'center',
            justifyContent:'space-around',
            backgroundColor:COLOR.APP_THEME_BLUE,
            width:UIUtils.size(80),
            height:UIUtils.size(35),
            borderRadius: UIUtils.size(6)
    },
    buttonText: {
        fontSize:UIUtils.size(14),
        color:'white',
        fontWeight:'bold'
    }

});