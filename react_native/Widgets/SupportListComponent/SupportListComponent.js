import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableHighlight, TouchableOpacity, Linking, DeviceEventEmitter } from 'react-native'
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import AsyncImageLoader from '../../Utils/AsyncImageLoader'
import EventEmitter from "react-native-eventemitter"
import {COLOR,LAYOUT_SPACE,FONT_SIZE, SUPPORT_EVENTS} from '../../Constants/Constants'
import LinearGradient from 'react-native-linear-gradient'
import CommonListView from '../../SharedComponents/CommonListView'
import MenuOptionModel from '../../SharedComponents/MenuItem/models/MenuItemModel'
import MenuItemComponent from '../../SharedComponents/MenuItem/views/MenuItemComponent'
import GreyDividerComponent from '../../SharedComponents/GreyDividerComponent';
import SupportObject from '../../Objects/SupportObject'
import VerticalMarginComponent from '../../SharedComponents/VerticalMarginComponent';

export default class SupportListComponent extends Component<{}> {

  _branding = Branding.sharedInstance

  constructor(prop){
    super(prop);
    this.state = {
      supportItems:this.props.supportItems,
      sectionIndex:this.props.sectionIndex
    };

  }

  componentWillMount(){
    //console.log(this.state.supportItems)
  }

  renderImageIcon(icon){
    if(icon!=null&&icon!=""){
      return(<Image style={styles.menuIcon}
                    source={{
                      uri:icon
                    }}
                    resizeMode="contain"/>)
    }else{
      return (<View style={{width:UIUtils.size(10),height:UIUtils.size(10)}}></View>)
    }

  }


  renderHeaderSection(){
    if(this.state.supportItems.section === null || this.state.supportItems.section == ""){
      if(this.state.sectionIndex > 0){//if not the very first item
        return(
          <View style={[styles.sectionItem, {backgroundColor:COLOR.COLOR.EMPTY_HEADER_COLOR}]}></View>
        )
      }

      //if the item is the very first on the list
      return(
        <View></View>
      )
    }

    return(
        <View>
          <VerticalMarginComponent height={15}></VerticalMarginComponent>
          <LinearGradient colors={this._branding.getHeaderGradient()}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          style={styles.sectionItem} >
          <Text style={[styles.sectionTitle, {color:this._branding.getHeaderFontColor()}]}>{this.state.supportItems.section}</Text>
          </LinearGradient>
        </View>
    );
  }

  internalLinkTouched(internalLink){
    EventEmitter.emit(SUPPORT_EVENTS.OPEN_INTERNAL);
  }

  externalLinkTouched(externalLink, pageTitle){
    EventEmitter.emit(SUPPORT_EVENTS.OPEN_EXTERNAL, {webLink:externalLink, pageTitle:pageTitle})
  }

  phoneNumberTouched(phoneNumber){
    alert(phoneNumber)
  }

  widgetTouched(widgetitle){
    alert(widgetitle)
  }

  textTouched(text){
    alert(text)
  }


  //render components (passing support components)
  renderComponent(supportObject){

    if(supportObject.button_type === "Margin"){
      return(<VerticalMarginComponent height={15}></VerticalMarginComponent>)
    }

    if(supportObject.button_type === "Internal link"){
      return(<TouchableOpacity onPress={() => this.internalLinkTouched(supportObject.internal_link)}>
      <View style={styles.mainItemWrapper}>
        <View style={styles.intExtLinkWrapper}>
          {this.renderImageIcon(supportObject.icon)}
          <Text style={styles.extIntTextStyles}>{supportObject.title}</Text>
          <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_right_grey.png')} ></Image>
        </View>
      </View>
      <GreyDividerComponent></GreyDividerComponent>
      </TouchableOpacity>)
    }

    if(supportObject.button_type === "External link"){
      return(
      <TouchableOpacity onPress={() => this.externalLinkTouched(supportObject.external_link, supportObject.title)}>
      <View style={styles.mainItemWrapper}>
        <View style={styles.intExtLinkWrapper}>
          {this.renderImageIcon(supportObject.icon)}
          <Text style={styles.extIntTextStyles}>{supportObject.title}</Text>
          <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_right_grey.png')} ></Image>
        </View>
      </View>
      <GreyDividerComponent></GreyDividerComponent>
      </TouchableOpacity>)
    }

    if(supportObject.button_type === "Phone"){
      return(<TouchableOpacity onPress={() => this.phoneNumberTouched(supportObject.phone_number)}>
      <View style={styles.mainItemWrapper}>
      <View style={styles.phoneWrapper}>
        {this.renderImageIcon(supportObject.icon)}
      <Text style={styles.extIntTextStyles}>{supportObject.title}</Text>
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_right_grey.png')} ></Image>
      </View>
      </View>
      <GreyDividerComponent></GreyDividerComponent>
      </TouchableOpacity>)
    }

    if(supportObject.button_type === "Widget"){
      return(<TouchableOpacity onPress={() => this.widgetTouched(supportObject.text)}>
      <View style={styles.mainItemWrapper}>
      <View style={styles.widgetWrapper}>
      <Text>{supportObject.title}</Text>
      </View>
      </View>
      <GreyDividerComponent></GreyDividerComponent>
      </TouchableOpacity>)
    }

    if(supportObject.button_type === "Text"){
      return(<TouchableOpacity onPress={() => this.textTouched(supportObject.text)}>
      <View style={styles.mainItemWrapper}>
      <View style={styles.textWrapper}>
      <Text>{supportObject.title}</Text>
      </View>
      </View>
      <GreyDividerComponent></GreyDividerComponent>
      </TouchableOpacity>)
    }
  }

  renderContent(){

    var supportModelCollection = []

    if( typeof this.state.supportItems.items !== 'undefined' && this.state.supportItems.items !== null && this.state.supportItems.items.length > 0 ){
      //items belongs to this section
      var items = this.state.supportItems.items

      for(var i = 0; i<items.length; i++){
        var id = items[i].id
        var title = items[i].title
        var button_type = items[i].button_type
        var text = ""
        var internalLink = ""
        var externalLink = ""
        var country_code = ""
        var phone_number = ""
        var icon = ""

        //type widget
        if(button_type === "Widget"){
          var suppportObject = new SupportObject()
          suppportObject.initiateWidget(id, title, button_type)
          supportModelCollection.push(suppportObject)
        }

        //type text
        if(button_type === "Text"){
          text = items[i].text
          var suppportObject = new SupportObject()
          suppportObject.initiateText(id, title, button_type, text)
          supportModelCollection.push(suppportObject)
        }

        //type internal link
        if(button_type === "Internal link"){
          internalLink = items[i].internalLink
          icon = items[i].icon
          var suppportObject = new SupportObject()
          suppportObject.initiateInternalLink(id, title, button_type, internalLink, icon)
          supportModelCollection.push(suppportObject)
        }

        //type external link
        if(button_type === "External link"){
          externalLink = items[i].externalLink
          icon = items[i].icon
          console.log(externalLink)
          var suppportObject = new SupportObject()
          suppportObject.initiateExternalLink(id, title, button_type, externalLink, icon)
          supportModelCollection.push(suppportObject)
        }

        //type phone
        if(button_type === "Phone"){
          country_code = items[i].country_code
          phone_number = items[i].phone_number
          icon = items[i].icon
          var suppportObject = new SupportObject()
          suppportObject.initiatePhone(id, title, button_type, country_code, phone_number, icon)
          supportModelCollection.push(suppportObject)
        }

        //margin type
        if(button_type === "Margin"){
          var suppportObject = new SupportObject()
          suppportObject.initiateMargin(id, title, button_type)
          supportModelCollection.push(suppportObject)
        }
      }
    }

    //if no data found
    if(supportModelCollection.length <=0 ){
      return(
        <View style={styles.nodataFoundContainer}><Text>No data found</Text></View>
      )
    }

    return(
      <View>
      {supportModelCollection.map((prop, key) => {
        return (
          <View supportItems={items[key]} key={key}>{this.renderComponent(supportModelCollection[key])}</View>
        );
      })}
      </View>)
}

render() {
  return(<View style={styles.container}>
    {this.renderHeaderSection()}
    {this.renderContent()}
    </View>);
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: 'transparent',
    // alignItems:'center',
    // flexDirection:'row',
  },
  nodataFoundContainer:{
    padding:10
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between',
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold',
    fontSize:UIUtils.defaultMenuTextFontSize()
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  editButton:{
    fontSize:UIUtils.size(10),
    color:'white',
    borderColor:'white',
    borderWidth:1,
    borderRadius:UIUtils.size(3),
    padding:UIUtils.size(4),
    paddingLeft:UIUtils.size(8),
    paddingRight:UIUtils.size(8)
  },
  editButtonWrapper:{
    position:'absolute',
    left:UIUtils.size(110),
  },

  mainItemWrapper:{
    // marginBottom:5
    height:UIUtils.size(35)
  },

  intExtLinkWrapper:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
  },

  phoneWrapper:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
    margin:10
  },

  textWrapper:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
    margin:10
  },

  widgetWrapper:{
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
    margin:10
  },

  extIntTextStyles:{
    paddingLeft:UIUtils.size(10),
    fontSize:UIUtils.defaultMenuTextFontSize()
  },
  arrowIcon:{
    position: 'absolute',
    right: UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    width:UIUtils.size(LAYOUT_SPACE.STANDARD_ARROR_SIZE_WIDTH),
    height:UIUtils.size(LAYOUT_SPACE.STANDARD_ARROR_SIZE_HEIGHT)
  },
  menuIcon:{
    height:UIUtils.size(25),
    width:UIUtils.size(25),
    marginLeft:UIUtils.size(15)
  }

});
