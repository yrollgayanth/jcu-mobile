/**
* Created by Yrol on 21/03/18.
*/
import React, { Component } from 'react'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, TouchableOpacity, ActivityIndicator, Animated, Easing, Dimensions } from 'react-native'
import Branding from "../../Utils/Branding"
import EventEmitter from "react-native-eventemitter"
import {EVENT} from '../../Constants/Constants'
import Orientation from 'react-native-orientation'

export default class QuicklinkRowComponent extends Component<{}> {

  _branding = Branding.sharedInstance
  _screenWidth = Dimensions.get('window').width

  constructor(props) {
    super(props)
    this._active = new Animated.Value(0)

    this.state={
      showEditMode:false,
      isHorizontal:false
    }

    this._style = {
      ...Platform.select({
        ios: {
          transform: [{
            scale: this._active.interpolate({
              inputRange: [0, 1],
              outputRange: [1, 1.1],
            }),
          }],
          shadowRadius: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 10],
          }),
        },

        android: {
          transform: [{
            scale: this._active.interpolate({
              inputRange: [0, 1],
              outputRange: [1, 1.07],
            }),
          }],
          elevation: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 6],
          }),
        },
      })
    }

    //orientation change
    this._orientationDidChange = this._orientationDidChange.bind(this)
  }

  _orientationDidChange = (orientation) => {
    if(Platform.OS !== 'android'){
      if (orientation === 'LANDSCAPE') {
        this._screenWidth = Dimensions.get('window').width
        this.setState({isHorizontal: true})
      }else{
        this._screenWidth = Dimensions.get('window').width
        this.setState({isHorizontal: false})
      }
    }
  }

  setEditStatus = data => {
    this.setState({showEditMode:data})
  }

  componentWillMount(){
    EventEmitter.addListener(EVENT.SHOW_QUICKLINK_EDIT_BUTTONS, this.setEditStatus)

    //get initial orientation
    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      this.state.isHorizontal = false
    } else {
      this.state.isHorizontal = true
    }
  }

  componentWillUnmount(){
    EventEmitter.removeListener(EVENT.SHOW_QUICKLINK_EDIT_BUTTONS, this.setEditStatus)
    Orientation.removeOrientationListener(this._orientationDidChange)
  }

  componentDidMount(){
    //remove orientation listener
    Orientation.addOrientationListener(this._orientationDidChange);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }

  editLink(id, title, url){
    EventEmitter.emit(EVENT.QUICKLINK_EDIT_ROW, {editID:id, editTitle:title, editUrl:url})
  }

  deleteLink(id, title, url){
    EventEmitter.emit(EVENT.QUICKLINK_DELETE_ROW, {deleteID:id, deleteTitle:title, deleteUrl:url})
  }



  renderContent(data){
    var contentBlockWidthNonEdit = "90%"
    var contentBlockWidthEdit = "68%"

    if(this.state.isHorizontal){
      contentBlockWidthNonEdit = "95%"
      contentBlockWidthEdit = "83%"
    }

    if(!this._branding.getQuicklinksEditMode()){
      let dragReorderIcon = require('../../../assets/img/drag_reorder_grey.png')
      return(
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View  style={{height: 40, width:contentBlockWidthNonEdit}}><Text style={styles.text}>{data.title}</Text></View>
          <View style={{width: 40, height: 40, justifyContent:'center', alignItems:'center'}} onPress={() => {}} >
            <Image style={[styles.iconBlock]} source={dragReorderIcon} />
          </View>
        </View>
      )
    }else{
      let editImage = require('../../../assets/img/edit_link_grey.png')
      let deleteImage = require('../../../assets/img/delete_icon_grey.png')
      let dragReorderIcon = require('../../../assets/img/drag_reorder_grey.png')
      return(
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{width:contentBlockWidthEdit, height: 30}}><Text style={styles.text}>{data.title}</Text></View>
            <TouchableOpacity style={{width: 40, height: 40, justifyContent:'center', alignItems:'center'}} onPress={() => this.editLink(data.id, data.title, data.url)}>
              <Image style={[styles.iconBlock]} source={editImage} />
            </TouchableOpacity>
            <TouchableOpacity style={{width: 40, height: 40, justifyContent:'center', alignItems:'center'}} onPress={() => this.deleteLink(data.id, data.title, data.url)} >
              <Image style={[styles.iconBlock]} source={deleteImage} />
            </TouchableOpacity>
            <View style={{width: 40, height: 40, justifyContent:'center', alignItems:'center'}} onPress={() => {}} >
              <Image style={[styles.iconBlock]} source={dragReorderIcon} />
            </View>
          </View>
      )
    }
  }

  render() {
   const {data, active} = this.props
    return (
      <Animated.View style={[this._style, styles.row, {width:this._screenWidth}]}>
        {this.renderContent(data)}
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 5,
    height: 'auto',
    flex: 1,
    marginTop: 7,
    marginBottom: 5,
    borderRadius: 0,


    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOpacity: 1,
        shadowOffset: {height: 2, width: 2},
        shadowRadius: 2,
      }
    })
  },

  iconBlock:{
    width:30,
    height:30
  },

  text: {
    fontSize: 16,
    color: 'black',
  },
});
