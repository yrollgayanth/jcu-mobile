import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native';
import UIUtils from '../../Utils/UIUtils';
import Branding from "../../Utils/Branding";
import {COLOR,LAYOUT_SPACE,FONT_SIZE} from '../../Constants/Constants';
import LinearGradient from 'react-native-linear-gradient';

export default class CommingUpClassWidget extends Component<{}> {

  _branding = Branding.sharedInstance

  constructor(prop){
    super(prop);
    this.state = {
      expanded:true,
      nextClassModel:{
        date:"12 Dec 2016",
        courseCode:"COSC-1201",
        time:"10:30 am",
        duration:"2h",
        location:"Rm 401, Building 12"
      }
    };
  }

  render() {
    return(<View style={styles.container}>
      {this.renderExpandableSection()}
      {this.renderContent()}
      </View>
    );
  }

  renderTimetableBar(){
    return(
      <TouchableHighlight underlayColor="transparent"
      onPress={() => {alert("tapped on my timatable")}}>
      <View>
      <View style={styles.blueLine}></View>
      <View style={styles.timetableBar}>
      <Image style={styles.timetableImage}
      source={require('../../../assets/img/ic_timetable.png')}
      ></Image>
      <Text style={styles.timetableText}>MY TIMETABLE</Text>
      </View>
      <View style={styles.blueLine}></View>
      </View>
      </TouchableHighlight>
    );
  }

  renderContent(){
    return(
      <View>
        <View style={{padding:5}}><Text style={{color: 'black', fontWeight:"bold"}} >Bachelor of Nursing</Text></View>
        <Image source={require('../../../assets/img/sample_nursing_progress.png')} />
        <View style={{padding:5}}><Text style={{color: 'black', fontWeight:"bold"}} >Bachelor of Dentistry</Text></View>
        <Image source={require('../../../assets/img/sample_dentistry_progress.png')} />
      </View>
    )
  }

  // renderContent(){
  //   if(this.state.expanded){
  //     return(
  //       <View>
  //       <View style={styles.contentWrapper}>
  //       <Text style={styles.courseCode}>{this.state.nextClassModel.courseCode}</Text>
  //       <View style={styles.courseDetailsItem}>
  //       <Image
  //       source={require('../../../assets/img/ic_clock.png')}
  //       style={styles.courseDetailsIcon}></Image>
  //       <Text style={styles.courseDetailsText}>{this.state.nextClassModel.time}</Text>
  //       <Image
  //       source={require('../../../assets/img/ic_duration.png')}
  //       style={styles.courseDetailsIcon}></Image>
  //       <Text style={styles.courseDetailsText}>{this.state.nextClassModel.duration}</Text>
  //       </View>
  //
  //       <View style={styles.courseDetailsItem}>
  //       <Image
  //       source={require('../../../assets/img/ic_location.png')}
  //       style={styles.courseDetailsIcon}></Image>
  //       <Text style={styles.courseDetailsText}>{this.state.nextClassModel.location}</Text>
  //       </View>
  //
  //       <TouchableHighlight
  //       underlayColor="transparent"
  //       onPress={() => {alert("tapped on navigate")}}
  //       >
  //       <Image
  //       source={require('../../../assets/img/ic_navigate.png')}
  //       style={styles.navigateIcon}></Image>
  //       </TouchableHighlight>
  //
  //       </View>
  //       {this.renderTimetableBar()}
  //       </View>
  //     );
  //   }
  // }


  renderExpandableSection(){
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => { this.setState({expanded:!this.state.expanded});}}
      >

      <LinearGradient colors={this._branding.getHeaderGradient()}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={[styles.sectionTitle, {color:this._branding.getHeaderFontColor()}]}>Enrolment status</Text>
      <Text style={[styles.nextDate, {color:this._branding.getHeaderFontColor()}]}>{this.state.nextClassModel.date}</Text>
      {this.renderArrow()}
      </LinearGradient>
      </TouchableHighlight>
    );

  }

  renderArrow(){
    if(this.state.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }



}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  nextDate:{
    color:"white",
    fontSize:UIUtils.size(11),
    fontWeight: 'bold',
    position:'absolute',
    right:UIUtils.size(55)
  },

  contentWrapper:{

    marginBottom:UIUtils.size(15)

  },

  navigateIcon:{
    resizeMode:'contain',
    position:'absolute',
    width:UIUtils.size(25),
    height:UIUtils.size(25),
    right:UIUtils.size(15),
    bottom:UIUtils.size(0)

  },
  courseCode:{
    fontWeight: 'bold',
    fontSize:UIUtils.size(16),
    marginLeft:UIUtils.size(20),
    marginTop:UIUtils.size(10)
  },
  courseDetailsItem:{
    flexDirection:'row',
    marginTop:UIUtils.size(10),
    marginLeft:UIUtils.size(20)

  },
  courseDetailsText:{
    fontSize:UIUtils.size(13),
    marginLeft:UIUtils.size(5),
    marginRight:UIUtils.size(30)
  },
  courseDetailsIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain'
  },

  timetableBar:{
    height:UIUtils.size(35),
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  },
  timetableImage:{
    resizeMode:'contain',
    width:UIUtils.size(20),
    height:UIUtils.size(20),

  },
  timetableText:{
    marginLeft:UIUtils.size(10),
    color:COLOR.APP_THEME_BLUE,
    fontSize:UIUtils.size(14)
  },
  blueLine:{
    backgroundColor:COLOR.APP_THEME_BLUE,
    height:0.5
  }


});
