import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native'
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import {COLOR,LAYOUT_SPACE,FONT_SIZE, EVENT} from '../../Constants/Constants'
import LinearGradient from 'react-native-linear-gradient'
import GreyDividerComponent from '../../SharedComponents/GreyDividerComponent'
import EventEmitter from "react-native-eventemitter"

export default class QuickLInksWidget extends Component<{}> {

_branding = Branding.sharedInstance

constructor(prop){
  super(prop);
  this.state = {
    expanded:true,
    quicklinkData:this.props.quicklinkData
  }
}

openQuicklink(title, link){
  EventEmitter.emit(EVENT.OPEN_QUICK_LINK, {quicklink:link, linkTitle:title})
}

openAllQuicklink(){
  EventEmitter.emit(EVENT.OPEN_ALL_QUICK_LINK)
}

renderExpandableSection(){

  return(
    <TouchableHighlight style={styles.container}
    underlayColor="transparent"
    onPress={() => {this.setState({expanded:!this.state.expanded})}}
    >

    <LinearGradient colors={this._branding.getHeaderGradient()}
    start={{ x: 0, y: 1 }}
    end={{ x: 1, y: 1 }}
    style={styles.sectionItem} >
    <Text style={[styles.sectionTitle, {color:this._branding.getHeaderFontColor()}]}>Quick links</Text>
    {this.renderArrow()}
    <TouchableHighlight
    style={styles.editButtonWrapper}
    underlayColor="transparent"
    onPress={() => this.openAllQuicklink() }
    ><Text style={[styles.editButton, {color:this._branding.getHeaderFontColor()}]}>EDIT</Text></TouchableHighlight>
    </LinearGradient>
    </TouchableHighlight>
  );
}

renderArrow(){
  if(this.state.expanded){
    return(
      <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
    );
  }else{
    return(
      <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
    );
  }
}

//render quicklink content
renderQuicklinkContent(){
  if( typeof this.state.quicklinkData === 'undefined' || this.state.quicklinkData === null || this.state.quicklinkData.length <=0 ){
    return(
      <View style={styles.nodataFoundContainer}><Text>{"No quicklinks found"}</Text></View>
    )
  }
  if(this.state.expanded){
    var blocks = []
    let quicklink = this.state.quicklinkData
    for(var i=0; i<quicklink.length; i++){
      let currentIndex = i
      blocks.push(
        <TouchableHighlight onPress={()=>this.openQuicklink(quicklink[currentIndex].title, quicklink[currentIndex].url)} key={currentIndex}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.quicklinksContainer}><Text>{quicklink[currentIndex].title}</Text></View>
            <GreyDividerComponent></GreyDividerComponent>
          </View>
        </TouchableHighlight>)
    }
    return blocks
  }
}

render() {
  return(<View style={styles.container}>
    {this.renderExpandableSection()}
    {this.renderQuicklinkContent()}
    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    // alignItems:'center',
    // flexDirection:'row',
  },
  nodataFoundContainer:{
    padding:10,
    height:40,
    backgroundColor:"white"
  },
  quicklinksContainer:{
    paddingTop:10,
    paddingRight:10,
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    height:40,
    backgroundColor:"white"
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  editButton:{
    fontSize:UIUtils.size(10),
    color:'white',
    borderColor:'white',
    borderWidth:1,
    borderRadius:UIUtils.size(3),
    padding:UIUtils.size(4),
    paddingLeft:UIUtils.size(8),
    paddingRight:UIUtils.size(8)
  },
  editButtonWrapper:{
    position:'absolute',
    left:UIUtils.size(110),
  }
})
