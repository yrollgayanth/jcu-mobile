/**
* Created by Yrol on 05/02/18.
*/

import React, { Component } from 'react'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, ScrollView } from 'react-native'
import UIUtils from '../../Utils/UIUtils'
import Branding from "../../Utils/Branding"
import {COLOR,LAYOUT_SPACE,FONT_SIZE} from '../../Constants/Constants'
import MenuSectionComponent from '../../SharedComponents/MenuSectionComponent'
import LinearGradient from 'react-native-linear-gradient'
import EnrolmentObject from "../../Objects/EnrolmentObject"
import StepObject from "../../Objects/StepObject"
import EventEmitter from "react-native-eventemitter"
import Triangle from 'react-native-triangle'
import Orientation from 'react-native-orientation'

export default class EnrolmentStatusWidget extends Component<{}> {

  _branding = Branding.sharedInstance

  constructor(prop){
    super(prop)
    this.state = {
      isHorizontal:false,
      enrolmentData:this.props.enrolmentData,
      shouldShowCourseTitle:this.props.showCourseTitle,
      expanded:true,
      fromEnrolmentComponent:this.props.isEnrolmentComponent
    }
  }

  componentDidMount(){
    //remove orientation listener
    Orientation.addOrientationListener(this._orientationDidChange);
  }

  componentWillMount(){
    const initial = Orientation.getInitialOrientation()
    if (initial === 'PORTRAIT') {
      this.state.isHorizontal = false
    } else {
      this.state.isHorizontal = true
    }
  }

  componentWillReceiveProps(newProps){ //here
    this.setState({enrolmentData:newProps.enrolmentData})
  }

  componentWillUnmount(){
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

  //device orientation
  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      this.setState({isHorizontal: true})
    }else{
      this.setState({isHorizontal: false})
    }
  }

  //shpw course title
  showCourseTitle(courseCode, courseName){
    if(this.state.shouldShowCourseTitle){
      return(
        <View style={[styles.courseNameBlock]}><Text style={{color:"black", fontWeight:"bold"}}>{courseCode + " - " + courseName}</Text></View>
      )
    }
  }

  //validate items
  validateMadatoryElements(element){
    if( typeof element === 'undefined' || element === null || element === '' ){
      return false
    }
    return true
  }

  processEnrolmentItems(){
    var enrolmentData = this.state.enrolmentData
    var enrolmentItems = []
    var enrolmentObjects = []

    if( typeof enrolmentData === 'undefined' || enrolmentData === null || enrolmentData.length <=0 ){
      return(
        <View key={i} style={[styles.nodataFoundContainer, {flexDirection:'row'}]}><Text>No enrolment data found</Text></View>
      )
    }

    //map enrolment data
    for(var i=0; i<enrolmentData.length; i++){
      var enrolmentObj = new EnrolmentObject(enrolmentData[i].course_code, enrolmentData[i].course_name, enrolmentData[i].courseId)

      Object.keys(enrolmentData[i].steps).map(function (key) {
        var completeStatus = enrolmentData[i].steps[key].complete
        var availableStatus = enrolmentData[i].steps[key].available
        var requirements = enrolmentData[i].steps[key].requires
        var base64Image = enrolmentData[i].steps[key].icon
        // var content = enrolmentData[i].steps[key].content
        var stepName = enrolmentData[i].steps[key].stepname
        var currentStatus = enrolmentData[i].steps[key].current_step
        var courseCode = enrolmentData[i].course_code
        var courseName = enrolmentData[i].course_name
        var courseID = enrolmentData[i].courseId
        var stepObj = new StepObject(key, stepName, completeStatus, availableStatus, requirements, base64Image, currentStatus, courseCode, courseName, courseID)
        enrolmentObj.addStep(stepObj)
      })
      enrolmentObjects.push(enrolmentObj)
    }

    for(var i=0; i<enrolmentObjects.length; i++){
      enrolmentItems.push(
        <View key={i}>
          {this.showCourseTitle(enrolmentObjects[i].course_code, enrolmentObjects[i].course_name)}
          <View style={{backgroundColor:"#ffffff"}}>
            <View style={{flexDirection:'row'}}>
            <ScrollView ref="scrollView"
            showsHorizontalScrollIndicator={false}
            pagingEnabled={true}
            horizontal={true}>
            {this.addEnrolmentBlocks(enrolmentObjects[i].getSteps(), enrolmentObjects[i].course_code, enrolmentObjects[i].course_id, enrolmentObjects[i].course_name)}
            </ScrollView>
            </View>
          </View>
        </View>)
    }
    if(this.state.expanded){ //if only expanded return view
       return enrolmentItems
    }
  }

  //adding each view block to enrolment progress
  addEnrolmentBlocks(enrolData, courseCode, courseID, courseName){
    var blocks = []
    var currentStepIndex = -1

    // get completed block
    for(var i = 0; i<enrolData.length; i++){
      let currentIndex = i
      let stepName = enrolData[currentIndex].step_name
      let isCurrentStep = enrolData[currentIndex].current_status
      if(isCurrentStep === true){
        currentStepIndex = currentIndex
      }
    }

    for(var i = 0; i<enrolData.length; i++){
      let currentIndex = i
      let completeStatus =  enrolData[currentIndex].complete_status
      let currentStatus = enrolData[currentIndex].current_status
      let stepKey = enrolData[currentIndex].step_key
      let stepName = enrolData[currentIndex].step_name

      if(this.state.fromEnrolmentComponent === false){//from home
        if(currentStatus === true){//current step
          let enrolmentIcon = this.getEnrolmentIcon(stepKey, true)
          blocks.push(<TouchableHighlight onPress={()=>this.openEnrolmentStep(courseCode, courseName, courseID, stepName,enrolData[currentIndex].step_key)} key={currentIndex}>

                        <LinearGradient colors={['#66cc00','#cef7a5']}
                                        start={{ x: 0, y: 1 }}
                                        end={{ x: 1, y: 1 }}
                                        style={[styles.currentBlock, {flexDirection: 'row', width:UIUtils.getScreenWidth()/6}]} >
                          <View style={[styles.currentBlockTitle]}>
                            <Image style={[styles.iconBlock]}  source={enrolmentIcon} />
                          </View>
                          <View style={[styles.currentBlockTitle]}>
                            <Text style={[styles.currentText]}>{stepName}</Text>
                          </View>
                          <View style={styles.triangle}>
                            <Triangle
                                width={UIUtils.size(20)}
                                height={UIUtils.size(40)/2}
                                color={this.state.bgColor}
                                direction={'up-right'}
                            />
                            <Triangle
                                width={UIUtils.size(20)}
                                height={UIUtils.size(40)/2}
                                color={this.state.bgColor}
                                direction={'down-right'}
                            />
                          </View>
                        </LinearGradient>
                      </TouchableHighlight>)
        }else if(completeStatus === true){//step completed
          let enrolmentIcon = this.getEnrolmentIcon(stepKey, completeStatus)
          blocks.push(<TouchableHighlight onPress={()=>this.openEnrolmentStep(courseCode, courseName, courseID, stepName,enrolData[currentIndex].step_key)} key={currentIndex}><View style={[styles.completeBlock, {flexDirection: 'row', width:UIUtils.getScreenWidth()/6}]}>
                      <Image style={[styles.iconBlock]} source={enrolmentIcon} />
                      </View></TouchableHighlight>)
        }else{//not completed
          var enrolmentIcon = this.getEnrolmentIcon(stepKey, completeStatus)
          blocks.push(<TouchableHighlight onPress={()=>this.openEnrolmentStep(courseCode, courseName, courseID, stepName,enrolData[currentIndex].step_key)} key={currentIndex}><View style={[styles.incompleteBlock, {flexDirection:'row', width:UIUtils.getScreenWidth()/6}]}>
                      <Image style={[styles.iconBlock]}  source={enrolmentIcon} />
                      </View></TouchableHighlight>)
        }

      }else{
        if(currentStatus === true){//current step
          let enrolmentIcon = this.getEnrolmentIcon(stepKey, true)
          blocks.push(<TouchableHighlight onPress={()=>this.openEnrolmentStep(courseCode, courseName, courseID, stepName,enrolData[currentIndex].step_key)} key={currentIndex}>

                        <LinearGradient colors={['#66cc00','#cef7a5']}
                                        start={{ x: 0, y: 1 }}
                                        end={{ x: 1, y: 1 }}
                                        style={[styles.currentBlock, {flexDirection: 'row', width:'auto'}]} >
                          <View style={[styles.currentBlockTitle]}>
                            <Image style={[styles.iconBlock]}  source={enrolmentIcon} />
                          </View>
                          <View style={[styles.currentBlockTitle]}>
                            <Text style={[styles.currentText]}>{stepName}</Text>
                          </View>
                          <View style={styles.triangle}>
                            <Triangle
                                width={UIUtils.size(20)}
                                height={UIUtils.size(40)/2}
                                color={this.state.bgColor}
                                direction={'up-right'}
                            />
                            <Triangle
                                width={UIUtils.size(20)}
                                height={UIUtils.size(40)/2}
                                color={this.state.bgColor}
                                direction={'down-right'}
                            />
                          </View>
                        </LinearGradient>
                      </TouchableHighlight>)
        }else if(completeStatus === true){//step completed
          let enrolmentIcon = this.getEnrolmentIcon(stepKey, completeStatus)
          blocks.push(<TouchableHighlight onPress={()=>this.openEnrolmentStep(courseCode, courseName, courseID, stepName,enrolData[currentIndex].step_key)} key={currentIndex}><View style={[styles.completeBlock, {flexDirection: 'row', width:UIUtils.getScreenWidth()/6.6}]}>
                      <Image style={[styles.iconBlock]} source={enrolmentIcon} />
                      </View></TouchableHighlight>)
        }else{//not completed
          var enrolmentIcon = this.getEnrolmentIcon(stepKey, completeStatus)
          blocks.push(<TouchableHighlight onPress={()=>this.openEnrolmentStep(courseCode, courseName, courseID, stepName,enrolData[currentIndex].step_key)} key={currentIndex}><View style={[styles.incompleteBlock, {flexDirection:'row', width:UIUtils.getScreenWidth()/6.6}]}>
                      <Image style={[styles.iconBlock]}  source={enrolmentIcon} />
                      </View></TouchableHighlight>)
        }
      }
    }
    return blocks
  }

  //get enrolment icon
  getEnrolmentIcon(stepKey, status){

    var enrolIconsWhiteMap = {
      offer:require('../../../assets/img/en_offer_white.png'),
      details:require('../../../assets/img/en_details_white.png'),
      ecaf:require('../../../assets/img/en_ecaf_white.png'),
      plan:require('../../../assets/img/en_plan_white.png'),
      subject:require('../../../assets/img/en_subjects_white.png'),
      classes:require('../../../assets/img/en_classes_white.png')
    }


    var enrolIconsGreyMap = {
      offer:require('../../../assets/img/en_offer_grey.png'),
      details:require('../../../assets/img/en_details_grey.png'),
      ecaf:require('../../../assets/img/en_ecaf_grey.png'),
      plan:require('../../../assets/img/en_plan_grey.png'),
      subject:require('../../../assets/img/en_subjects_grey.png'),
      classes:require('../../../assets/img/en_classes_grey.png')
    }

    //*************OFFER**********//
    if(stepKey === "ACPT-OFF" && status === true){
      return enrolIconsWhiteMap.offer
    }

    if(stepKey === "ACPT-OFF" && status === false){
      return enrolIconsGreyMap.offer
    }

    //*************CLASSES REG**********//
    if(stepKey === "CLS-REG" && status === true){
      return enrolIconsWhiteMap.classes
    }

    if(stepKey === "CLS-REG" && status === false){
      return enrolIconsGreyMap.classes
    }

    //*************CLASSES REG**********//
    if(stepKey === "CNF-DTLS" && status === true){
      return enrolIconsWhiteMap.details
    }

    if(stepKey === "CNF-DTLS" && status === false){
      return enrolIconsGreyMap.details
    }

    //*************ECAF**********//
    if(stepKey === "ECAF" && status === true){
      return enrolIconsWhiteMap.ecaf
    }

    if(stepKey === "ECAF" && status === false){
      return enrolIconsGreyMap.ecaf
    }

    //*************PLAN YOUR S**********//
    if(stepKey === "PLAN-YOUR-STUDY" && status === true){
      return enrolIconsWhiteMap.plan
    }

    if(stepKey === "PLAN-YOUR-STUDY" && status === false){
      return enrolIconsGreyMap.plan
    }

    //*************ENROLMENT**********//
    if(stepKey === "ENR" && status === true){
      return enrolIconsWhiteMap.subject
    }

    if(stepKey === "ENR" && status === false){
      return enrolIconsGreyMap.subject
    }
  }

  openEnrolmentStep(courseCode, courseName, courseID, stepName,stepKey){
    this.props.tapCallback({
      "stepName":stepName,
      "courseId":courseID,
      "courseName":courseName,
      "courseCode":courseCode,
      "stepKey":stepKey
    })
  }

  renderExpandableSection(){

    if(this.props.hideSection===true){
      return
    }else{
      return(
          <TouchableHighlight style={styles.container}
                              underlayColor="transparent"
                              onPress={() => {this.setState({expanded:!this.state.expanded})}}>
            <LinearGradient colors={this._branding.getHeaderGradient()}
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}
                            style={styles.sectionItem} >
              <Text style={[styles.sectionTitle, {color:this._branding.getHeaderFontColor()}]}>{"Enrolment status"}</Text>
              {this.renderArrow()}
            </LinearGradient>
          </TouchableHighlight>
      );

    }
  }

  renderArrow(){
    if(this.state.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }

  render() {
    return(<View style={styles.container}>
      {this.renderExpandableSection()}
      {this.processEnrolmentItems()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  nodataFoundContainer:{
    padding:10
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  nextDate:{
    color:"white",
    fontSize:UIUtils.size(11),
    fontWeight: 'bold',
    position:'absolute',
    right:UIUtils.size(55)
  },

  completeBlock:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"#66cc00",
    height:UIUtils.size(40),
    width:40
  },
  incompleteBlock:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"#FFFFFF",
    height:UIUtils.size(40),
    width:40
  },
  currentBlock:{
    backgroundColor:"white",
    // height:UIUtils.size(40),
    width:'auto'
  },
  currentBlockTitle:{
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor:"#66cc00",
    height:UIUtils.size(40),
    width:'auto'
  },
  iconBlock:{
    width:30,
    height:30
  },
  currentBlockIcon:{
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"yellow",
    height:UIUtils.size(40),
    width:'auto'
  },
  currentText:{
    color:"#ffffff",
    padding:10
  },
  navigateIcon:{
    resizeMode:'contain',
    position:'absolute',
    width:UIUtils.size(25),
    height:UIUtils.size(25),
    right:UIUtils.size(15),
    bottom:UIUtils.size(0)

  },
  courseCode:{
    fontWeight: 'bold',
    fontSize:UIUtils.size(16),
    marginLeft:UIUtils.size(20),
    marginTop:UIUtils.size(10)
  },
  courseDetailsItem:{
    flexDirection:'row',
    marginTop:UIUtils.size(10),
    marginLeft:UIUtils.size(20)

  },
  courseNameBlock:{
    flexDirection:'row',
    paddingTop:5,
    paddingBottom:5,
    paddingLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    height:'auto',
    backgroundColor:'white'
  },
  courseDetailsText:{
    fontSize:UIUtils.size(13),
    marginLeft:UIUtils.size(5),
    marginRight:UIUtils.size(30)
  },
  courseDetailsIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain'
  },

  timetableBar:{
    height:UIUtils.size(35),
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
  },
  timetableImage:{
    resizeMode:'contain',
    width:UIUtils.size(20),
    height:UIUtils.size(20),

  },
  timetableText:{
    marginLeft:UIUtils.size(10),
    color:COLOR.APP_THEME_BLUE,
    fontSize:UIUtils.size(14)
  },
  blueLine:{
    backgroundColor:COLOR.APP_THEME_BLUE,
    height:0.5
  },
  triangle:{
    position:'absolute',
    right:0,
    width:UIUtils.size(20),
    height:UIUtils.size(40)
  }
})
