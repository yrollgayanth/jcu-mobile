/**
* Created by Yrol on 18/03/18.
*/

import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableHighlight } from 'react-native'
import UIUtils from '../../Utils/UIUtils'
import {COLOR, EVENT, TOP_BAR_OPTIONS} from '../../Constants/Constants'
import EventEmitter from "react-native-eventemitter"

export default class TopMenuBar extends Component<{}> {

  constructor(prop){
    super(prop);
  }

  clickedOnItem(itemKey){
    EventEmitter.emit(EVENT.QUICKLINK_TOPBAR, {selected_tab:itemKey})
  }

  renderMenuItem(itemModel){

    return(
        <TouchableHighlight style={styles.menuItemWrapper}
                            key={itemModel.Name}
                            underlayColor="transparent"
                            onPress={() => {this.clickedOnItem(itemModel.Name)}}>
          <View style={styles.itemContent}>
            <Image style={styles.itemIcon} source={{uri:itemModel.Icon}}></Image>
            <Text style={styles.itemName}>{itemModel.Name}</Text>
          </View>
        </TouchableHighlight>
    );
  }

  renderMenuItems(){
    var menuItemViews = [];
    var menuItemModels = [];
        menuItemModels.push({"Name":TOP_BAR_OPTIONS.QUICKLINK_EDIT,"Icon":require('../../../assets/img/base64/edit_link_grey.json')})
    menuItemModels.push({"Name":TOP_BAR_OPTIONS.QUICKLINK_ADD,"Icon":require('../../../assets/img/base64/add_link_grey.json')})
    for(var itemModel of menuItemModels){
      menuItemViews.push(this.renderMenuItem(itemModel));
    }
    return menuItemViews;
  }

  render() {
    return(<View style={styles.container}>
        {this.renderMenuItems()}
        </View>);
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
    alignItems:'center',
    flexDirection:'row',
  },
  menuItemWrapper:{
    flex:1
  },
  itemContent:{
    flex:1,
    alignItems:'center',
    paddingTop:UIUtils.size(5),
    paddingBottom:UIUtils.size(5)
  },
  itemName:{
    fontSize:UIUtils.size(12),
    color:"#969696"
  },
  itemIcon:{
    width:UIUtils.size(20),
    height:UIUtils.size(20),
    resizeMode:'contain'
  }
});
