/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component, PropTypes } from 'react'
import { StyleSheet, Text, View, ScrollView, RefreshControl, ActivityIndicator, Image, Button, AsyncStorage, Linking, Platform, NativeEventEmitter, NativeModules } from 'react-native'
import UIUtils from "../../Utils/UIUtils"
import Branding from "../../Utils/Branding"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils';
import WebEngine from '../../Utils/WebEngine'
import BrandingEngine from '../../Utils/BrandingEngine'
import BrowserUtils from '../../Utils/BrowserUtils'
import BaseViewComponent from '../../SharedComponents/BaseViewComponent'
import EventEmitter from "react-native-eventemitter"
import {DRAWER_MENU, EVENT, DEFAULT_FILES} from '../../Constants/Constants'
import TopMenuBar from '../../Widgets/TopMenuBar/TopMenuBar'
import CommingUpClassWidget from '../../Widgets/ComingUpClass/CommingUpClassWidget'
import VerticalMarginComponent from '../../SharedComponents/VerticalMarginComponent'
import QuickLInksWidget from '../../Widgets/QuickLinks/QuickLInksWidget'
import EventsWidget from '../../Widgets/Events/EventsWidget'
import CommonSwiperComponent from '../../SharedComponents/CommonSwiperComponent'
import EnrolmentStatusWidget from '../../Widgets/EnrolmentStatus/EnrolmentStatusWidget'
import RNFS from'react-native-fs'
import RNFetchBlob from 'react-native-fetch-blob'
import PortraitEnrolmentBarComponent from '../../Widgets/PortraitEnrolmentBar/PortraitEnrolmentBarComponent'

export default class HomeComponent extends BaseViewComponent {

  _mounted = true
  iosEventSubscription;

  constructor(props){
    super(props);
    this.state = {
      loaded:false,
      loadingAttempted:false,
      topBottomSafeArea:false,
      announcements:[],
      enrolmentDetails:[],
      quicklinks:[],
      campusSecurityTel:"",
      needsReload:false,
      isRefreshing:false,
      enableNavigationBar: true,
      enrolmentCourseCount:0,
      navigationBarConfig: {
        showLogo: true,
        showProfile:true,
        showAlert: true,
        showEmail:false,
        showMenu: true
      }
    }

    this.topBarAction = this.topBarAction.bind(this)
    this.openNewsItem = this.openNewsItem.bind(this)
    this.openQuicklink = this.openQuicklink.bind(this)
    this.showAllNews = this.showAllNews.bind(this)
    this.showAllQuicklinks = this.showAllQuicklinks.bind(this)
    this.refreshQuickLinks = this.refreshQuickLinks.bind(this)

    if(Platform.OS=='ios'){
      iosEventSubscription = new NativeEventEmitter(NativeModules.IOSEventManager)
    }
    this.quiteWebPage = this.quiteWebPage.bind(this)
    this.onRefresh = this.onRefresh.bind(this)
  }

  async componentWillMount(){
    EventEmitter.addListener(EVENT.SHOW_ALL_NEWS, this.showAllNews)
    EventEmitter.addListener(EVENT.HOME_TOP_BAR, this.topBarAction)
    EventEmitter.addListener(EVENT.OPEN_NEWS_ITEM, this.openNewsItem)
    EventEmitter.addListener(EVENT.OPEN_QUICK_LINK, this.openQuicklink)
    EventEmitter.addListener(EVENT.OPEN_ALL_QUICK_LINK, this.showAllQuicklinks)
    EventEmitter.addListener(EVENT.QUICKLINK_REFRESH_HOME, this.refreshQuickLinks)

    if(Platform.OS=='ios'){
      iosEventSubscription.addListener('QuitIOSBrowser', () => this.quiteWebPage())
    }else{
      EventEmitter.addListener(EVENT.QUIT_WEBPAGE, this.quiteWebPage);
    }

    //get analytics
    UIUtils.sendAnalytics("Home")

    this._mounted = true // set mounted to true
    //fetch homepage content
    this.fetchHomePageContent()
  }

  componentWillUnmount(){
    this._mounted = false
    EventEmitter.removeListener(EVENT.SHOW_ALL_NEWS, this.showAllNews)
    EventEmitter.removeListener(EVENT.HOME_TOP_BAR, this.topBarAction)
    EventEmitter.removeListener(EVENT.OPEN_NEWS_ITEM, this.openNewsItem)
    EventEmitter.removeListener(EVENT.OPEN_QUICK_LINK, this.openQuicklink)
    EventEmitter.removeListener(EVENT.OPEN_ALL_QUICK_LINK, this.showAllQuicklinks)
    EventEmitter.removeListener(EVENT.QUICKLINK_REFRESH_HOME, this.refreshQuickLinks)

    if(Platform.OS=='ios'){
      iosEventSubscription.removeListener('QuitIOSBrowser',this.quiteWebPage);
    }else{
      EventEmitter.removeListener(EVENT.QUIT_WEBPAGE,this.quiteWebPage);
    }
  }

  quiteWebPage(){
    if(this.state.needsReload===true){
      this.setState({needsReload:false})
    }
  }

  //open news
  openNewsItem(e){
    BrowserUtils.openBrowser(e.news_title, e.news_link)
  }

  //open quick link
  openQuicklink(e){
    BrowserUtils.openBrowser(e.linkTitle, e.quicklink)
  }

  //show all news
  showAllNews(){
    this.props.navigate("AllNews")
  }

  showAllQuicklinks(){
    this.props.navigate("EditQuicklinks", {showCustomNavigation:false})
  }

  //refresh page
  refreshQuickLinks(){
    this.fetchHomePageContent()
  }

  topBarAction(e){
    if(e.selected_tab === "Email"){
      //Linking.openURL('mailto:support@domain.com?subject=Inquiry&body=')
      BrowserUtils.openBrowser(e.selected_tab, "http://my.jcu.edu.au")
    }

    if(e.selected_tab === "Ask Us"){
      BrowserUtils.openBrowser(e.selected_tab, "https://jcu.custhelp.com/")
    }

    if(e.selected_tab === "Campus Safety"){
      if(typeof this.state.campusSecurityTel !== 'undefined' && this.state.campusSecurityTel !== null && this.state.campusSecurityTel !== ''){
        Linking.openURL(this.state.campusSecurityTel)
      }
    }
  }

  //setting fetch status (only if the view is the current view - Mounted)
  setFetchStatus(loadedStatus, attemptedStatus, announcementData, enrolmentData, quicklinkData, campusSafetyTel, courseCount, scrollRefreshStatus){
    console.log("course count:" + courseCount)
    if(this._mounted){
      this.setState({
        loaded     : loadedStatus,
        loadingAttempted : attemptedStatus,
        announcements: announcementData,
        enrolmentDetails: enrolmentData,
        quicklinks:quicklinkData,
        isRefreshing: scrollRefreshStatus,
        campusSecurityTel: campusSafetyTel,
        enrolmentCourseCount:courseCount
      })
    }
  }

  //fetch homepage content
  async fetchHomePageContent(){

    //this.setState({loaded:false, loadingAttempted:false})
    var announcmentsArr = []
    var enrolmentDetailsArr = []
    var quicklinksArr = []
    var campusSafetyNo = ""
    var courseCount = 0
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var brandingShared = Branding.sharedInstance

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    //check if token needs to be replaced
    if(shouldUpdateToken){
      await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
        if(values[0] === false){
          this.setFetchStatus(false, true, announcmentsArr, enrolmentDetailsArr, quicklinksArr, campusSafetyNo, courseCount, false)
        }else{
          newUserToken = values[0]
          LocalStorageManager.saveAuthTokens(newUserToken,()=>{
            LocalStorageManager.getSavedAuthTokens((savedValues)=>{
              ConfigUtils.id_token = savedValues.id_token
              this.fetchData()
            },(error)=>{
              this.setFetchStatus(false, true, announcmentsArr, enrolmentDetailsArr, quicklinksArr, campusSafetyNo, courseCount, false)
            })
          }),(error)=>{
            this.setFetchStatus(false, true, announcmentsArr, enrolmentDetailsArr, quicklinksArr, campusSafetyNo, courseCount, false)
          }
        }
      })
    }else{
      this.fetchData()
    }
  }

  async fetchData(){
    var announcmentsArr = []
    var enrolmentDetailsArr = []
    var quicklinksArr = []
    var campusSafetyNo = ""
    var courseCount = 0
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var brandingShared = Branding.sharedInstance

    this.setFetchStatus(false, false, announcmentsArr, enrolmentDetailsArr, quicklinksArr, campusSafetyNo, courseCount, false)
    var fetchComplete = false
    var brandingVersion = null
    var jsonOutput = null
    var base64ProfileImage =  null
    //add enrolment data also to the promise call
    await Promise.all([webEngine.fetchHomeContent()]).then(values => {
      jsonOutput = values[0]
      if(jsonOutput === false){
        //this.setState({loaded:false, loadingAttempted:true})
        this.setFetchStatus(false, true, announcmentsArr, enrolmentDetailsArr, quicklinksArr, campusSafetyNo, courseCount, false)
      }else{
        fetchComplete = true
        brandingVersion = jsonOutput.brandingVersion
        base64ProfileImage = jsonOutput.profileImage
      }
    });

    if(fetchComplete){
      //get branding version
      await Promise.all([branding.updateOnlineBranding(brandingVersion), this.saveProifleImageLocally(base64ProfileImage)]).then(values => {
        if(values.indexOf(false) === 0){
          console.log("error in branding / image save")
        }else{
          var profileImageLocation = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
          brandingShared.setProfileImage(profileImageLocation)
          EventEmitter.emit(EVENT.PROFILE_PIC_UPDATED)
        }
      })
    }

    announcmentsArr = jsonOutput.announcements
    enrolmentDetailsArr = jsonOutput.enrolment
    courseCount = jsonOutput.enrolment.length
    quicklinksArr = jsonOutput.quicklinks
    campusSafetyNo = jsonOutput.campusSecurityTel

    this.setFetchStatus(true, false, announcmentsArr, enrolmentDetailsArr, quicklinksArr, campusSafetyNo, courseCount, false)
  }

  componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.HOME_ITEM});
  }

  //callback func tapped on a particular enrolment step
  tapCallback(data){
    this.props.goEnrolmentProgress(data.courseId);
  }

  //save profile image
  async saveProifleImageLocally(imageData){
    var base64ImageData = imageData
    if(typeof imageData === 'undefined' || imageData === null || imageData === '' || imageData === '/assets/images/no-image.jpg'){
      var defaultImageAssets = require("../../../assets/img/base64/default_profile_base64.json")
      defaultImageAssets = defaultImageAssets.split('data:image/png;base64,')
      base64ImageData = defaultImageAssets[1]
    }
    base64ImageData =  base64ImageData.split('data:image/png;base64,')//split base64 string
    if(base64ImageData.length === 1){
      base64ImageData = base64ImageData[0]
    }else{
      base64ImageData = base64ImageData[1]
    }
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    return RNFS.writeFile(filePath, base64ImageData, 'base64')
    .then((success) => true)
    .catch((err) => false)
  }


  enrolmentStatusWidget() {
    return (<View>
      <VerticalMarginComponent height={15}></VerticalMarginComponent>
      <EnrolmentStatusWidget tapCallback={this.tapCallback.bind(this)} showCourseTitle={true}  enrolmentData={this.state.enrolmentDetails} isEnrolmentComponent={false}/>
      </View>);
  }

  enrolmentPortraitWidget(){
    return(
      <View>
        <PortraitEnrolmentBarComponent tapCallback={this.tapCallback.bind(this)} title={"Enrolment status"} enrolmentData={this.state.enrolmentDetails}/>
      </View>
    )
  }

  getEnrolmentWidget(){
    if(this.state.enrolmentCourseCount > 0){
      if(this.state.isHorizontal){
        return this.enrolmentStatusWidget()
      }
      return this.enrolmentPortraitWidget()
    }
  }

  renderQuickLinksWidget(){
    return(
      <View>
      <VerticalMarginComponent height={15}></VerticalMarginComponent>
      <QuickLInksWidget maxAllowed={this.state.quicklinks.max_links} quicklinkData={this.state.quicklinks.links} />
      </View>
    );
  }

  //scroll to refresh
  onRefresh(){
    this.setState({isRefreshing: true})
    this.fetchHomePageContent()
  }

  renderEventsWidget(){
    // if(typeof this.state.announcements !== 'undefined' && this.state.announcements !== null && this.state.announcements !== ''){
    //   countItems = this.state.announcements.length
    // }


    return(<View>
      <VerticalMarginComponent height={15}></VerticalMarginComponent>
      <EventsWidget
      title={"News"}
      countItems={this.state.announcements.length}
      eventData={this.state.announcements}
      ></EventsWidget>
      </View>)
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }
  //failed view
  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.fetchHomePageContent()}
      title="Try again">
      </Button>
      </View>
    )
  }

  renderView() {

    //load failed
    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    return (
      <View style={[{flex:1}]}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }>
        <TopMenuBar></TopMenuBar>
        {this.getEnrolmentWidget()}
        {this.renderQuickLinksWidget()}
        {this.renderEventsWidget()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorImage:{
    height:150,
    width:150
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
});
