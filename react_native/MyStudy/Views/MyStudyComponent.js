/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import { Platform, StyleSheet, RefreshControl, Text, View, ActivityIndicator, Image, ScrollView, Button, DeviceEventEmitter } from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent'
import EventEmitter from "react-native-eventemitter"
import {DRAWER_MENU,EVENT, MY_STUDY_EVENTS, TAB_MANAGER_PREFS} from '../../Constants/Constants'
import UIUtils from "../../Utils/UIUtils"
import BrandingEngine from '../../Utils/BrandingEngine'
import Branding from "../../Utils/Branding"
import WebEngine from "../../Utils/WebEngine"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils'
import MyStudyListComponent from '../../Widgets/MyStudyListComponent/MyStudyListComponent'
import BrowserUtils from '../../Utils/BrowserUtils'

export default class MyStudyComponent extends BaseViewComponent {

  _mounted = true

  constructor(props){
    super(props);
    this.state= {
      loaded:false,
      loadingAttempted:false,
      myStudyData:null,
      topBottomSafeArea:false,
      enableNavigationBar: true,
      isRefreshing:false,
      navigationBarConfig: {
        title: "My Study",
        showEmail:false,
        showHome: true,
        showAlert: true,
        showMenu: true
      }
    }

    //binding  "openExternal" and "openInternal" functions
    this.openExternal = this.openExternal.bind(this)
    this.openInternal = this.openInternal.bind(this)

    //on refresh method
    this.onRefresh = this.onRefresh.bind(this)
  }

  //initiate study fetch
  async initiateStudyFetch(){
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""
    var myStudyArr = []
    this.setState({loaded:false, loadingAttempted:false})

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    //verify if the token needs to be updated
    if(shouldUpdateToken){
        await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
          if(values[0] === false){
            this.setFetchStatus(false, true, myStudyArr, false)
          }else{
            newUserToken = values[0]
            LocalStorageManager.saveAuthTokens(newUserToken,()=>{
              LocalStorageManager.getSavedAuthTokens((savedValues)=>{
                ConfigUtils.id_token = savedValues.id_token
                this.fetchStudyData()
              },(error)=>{
                this.setFetchStatus(false, true, myStudyArr, false)
              })
            }),(error)=>{
              this.setFetchStatus(false, true, myStudyArr, false)
            }
          }
        })
    }else{
      this.fetchStudyData()
    }
  }

  //fetching study data
  async fetchStudyData(){
    var myStudyArr = []
    this.setState({loaded:false, loadingAttempted:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.fetchMyStudy()]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setFetchStatus(false, true, myStudyArr, false)
      }else{
        myStudyArr = jsonContent
        this.setFetchStatus(true, false, myStudyArr, false)
      }
    })
  }

  //refresh data
  onRefresh(){
    this.setState({isRefreshing: true})
    this.initiateStudyFetch()
  }

  async componentWillMount(){
    this._mounted = true // set mounted to true
    this.initiateStudyFetch()
    EventEmitter.addListener(MY_STUDY_EVENTS.OPEN_INTERNAL, this.openInternal);
    EventEmitter.addListener(MY_STUDY_EVENTS.OPEN_EXTERNAL,this.openExternal);
  }


  openExternal(e){
    BrowserUtils.openBrowser(e.pageTitle,e.webLink);

    // if(Platform.OS =='ios'){
    //     BrowserUtils.openBrowser(e.pageTitle,e.webLink);
    // }else{
    //     this.props.navigate('InAppWeb', {webLink: e.webLink, pageTitle:e.pageTitle})
    // }


  }

  openInternal(e){

    if(e.webLink === "Enrolment"){
      EventEmitter.emit(EVENT.OPEN_ENROLMENT)
    }

  }

  componentWillUnmount(){
    this._mounted = false // set mounted to true
    EventEmitter.removeListener(MY_STUDY_EVENTS.OPEN_EXTERNAL,this.openExternal)
    EventEmitter.removeListener(MY_STUDY_EVENTS.OPEN_INTERNAL,this.openInternal)
  }

  setFetchStatus(loadedStatus, attemptedStatus, studyData, refreshStatus){
    if(this._mounted){
      this.setState({
        loaded     : loadedStatus,
        loadingAttempted : attemptedStatus,
        myStudyData: studyData,
        isRefreshing: refreshStatus
      })
    }
  }

  async componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.MY_STUDY_ITEM});
  }

  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.initiateStudyFetch()}
      title="Try again">
      </Button>
      </View>
    )
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  renderMyStudyList(){
    if( typeof this.state.myStudyData === 'undefined' || this.state.myStudyData === null || this.state.myStudyData.length <=0 ){
      return(
        <View style={styles.loadFailedContainer}>
        <Text>No study data found</Text>
        <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
        <Button onPress={() => this.initiateStudyFetch()}
        title="Try again">
        </Button>
        </View>
      )
    }

    return(
      <View style={[{flex:1}]}>
      <ScrollView
        refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
        }>
      {this.state.myStudyData.map((prop, key) => {
          return (
            <MyStudyListComponent myStudyItems={this.state.myStudyData[key]} sectionIndex={key} key={key}/>
          );
      })}
      </ScrollView>
      </View>
    )
  }

  renderView() {
    //load failed
    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    //success load
    return (
      this.renderMyStudyList()
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorImage:{
      height:150,
      width:150
  }
});
