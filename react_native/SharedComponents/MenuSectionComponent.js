/**
* Created by Sylvia on 29/11/17.
*/
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import RNFS from'react-native-fs';
import UIUtils from "../Utils/UIUtils";
import Branding from "../Utils/Branding";
import {FONT_SIZE,LAYOUT_SPACE,COLOR, DEFAULT_FILES} from "../Constants/Constants";
import LinearGradient from 'react-native-linear-gradient';

export default class MenuSectionComponent extends Component<{}> {

  constructor(prop){
    super(prop);
  }

  renderSectionComponent(){
    var branding = Branding.sharedInstance
    return (
      <View style={styles.container}>
      <LinearGradient colors={branding.getHeaderGradient()}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={[styles.sectionTitle, {color:branding.getHeaderFontColor()}]}>{this.props.title}</Text>
      </LinearGradient>
      </View>
    );
  }

  render() {
    return this.renderSectionComponent()
  }
}

const styles = StyleSheet.create({
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  }
});
