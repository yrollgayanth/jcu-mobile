/**
* Created by Sylvia on 29/11/17.
*/

import React, { Component } from 'react';
import { Platform, Alert, StyleSheet, Text, View, Image, TouchableHighlight,DeviceEventEmitter, StatusBar } from 'react-native';
import {COLOR,LAYOUT_SPACE,EVENT,DEFAULT_FILES} from '../Constants/Constants';
import UIUtils from '../Utils/UIUtils';
import Branding from '../Utils/Branding';
import EventEmitter from "react-native-eventemitter";
import LinearGradient from 'react-native-linear-gradient'



const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default class NavigationBarComponent extends Component<{}> {

  _branding = Branding.sharedInstance

  constructor(prop){
    super(prop)
    this.state = {
      isHorizontal: UIUtils.isDeviceInHorizontal(),
      newNoti:0,
      profileImage:this.initProfileImage()
    }
    this.changeProfileImage = this.changeProfileImage.bind(this);
  }

  //initalise profile image
  initProfileImage(){
    var branding = Branding.sharedInstance
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    if(branding.getProfileImage() === ""){
      return ""
    }
    return filePath
  }

  changeProfileImage(e){
    this.setState({
      profileImage:this.initProfileImage()
    })
  }

  tappedOnBack(){
    this.props.goBackCallback();
  }

  tappedOnHome(){
    //EventEmitter.emit(EVENT.GO_HOME);
    DeviceEventEmitter.emit(EVENT.GO_HOME)
  }

  tappedOnProfile(){
    this.props.navigate("Profile");
  }


  componentWillMount(){
    EventEmitter.on(EVENT.UPDATE_UNREAD_NOTI, (newNoti) =>  this.setState({newNoti: newNoti}))

    //profile image event
    EventEmitter.addListener(EVENT.PROFILE_PIC_UPDATED, this.changeProfileImage)
  }


  componentWillUnmount(){
    EventEmitter.removeAllListeners(EVENT.UPDATE_UNREAD_NOTI);
    EventEmitter.removeListener(EVENT.PROFILE_PIC_UPDATED, this.changeProfileImage)
  }

  renderLeftGroups(){
    var leftGroups = [];
    if(this.state.isHorizontal&&UIUtils.isIPhoneX()){
      leftGroups.push(<View key="leftUnsafeArea" style={styles.unsafeArea}></View>);
    }

    if(this.props.showProfile){
      let randomString = Math.random().toString(36).slice(2)
      var defaultImage = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE + "?" + randomString

      if(this.state.profileImage !== ""){
        console.log("image is not empty")
        leftGroups.push(
          <TouchableHighlight key="profile" underlayColor="transparent" onPress={() => this.tappedOnProfile()}>
          <Image style={styles.profileImg} source={{uri:'file://'+defaultImage}}></Image>
          </TouchableHighlight>
        )
      }else{
        console.log("image is empty")
        leftGroups.push(
          <TouchableHighlight key="profile" underlayColor="transparent" onPress={() => this.tappedOnProfile()}>
          <Image style={styles.profileImg} source={{uri:require('../../assets/img/base64/default_profile_base64.json')}}></Image>
          </TouchableHighlight>
        )
      }
    }

    if(this.props.showHome){
      leftGroups.push(
        <TouchableHighlight key="home" underlayColor="transparent" onPress={() => this.tappedOnHome()}>
        <Image style={styles.backButton} source={{uri:require("../../assets/img/base64/jcu_home_btn_logo.json")}}></Image>
        </TouchableHighlight>
      );
    }

    if(this.props.showBack){
      leftGroups.push(
        <TouchableHighlight key="back" underlayColor="transparent" onPress={() => this.tappedOnBack()}>
        <Image style={styles.backButton} source={require("../../assets/img/ic_navbar_back.png")}></Image>
        </TouchableHighlight>
      );

    }
    return leftGroups;
  }

  tappedOnAlert(){
    //EventEmitter.emit(EVENT.GO_NOTIFICATIONS);
    DeviceEventEmitter.emit(EVENT.GO_NOTIFICATIONS)
  }

  tappedOnMenu(){
    //alert("tappedOnMenu");
    DeviceEventEmitter.emit(EVENT.OPEN_DRAWER, {status:true});
  }

  tappedOnEmail(){
    alert("tappedOnEmail");
  }

  renderNotiNumber(){
    if(this.state.newNoti!=null&&this.state.newNoti>0){
      if(this.state.newNoti>99){
        return(
          <View style={styles.moreNewNotiWrapper}><Text style={styles.newNotiText}>{this.state.newNoti}</Text></View>
        )
      }else{
        return(
          <View style={styles.newNotiWrapper}><Text style={styles.newNotiText}>{this.state.newNoti}</Text></View>
        )
      }
    }else{
      return(
        <View></View>
      )
    }
  }

  renderRightButtonGroup(){
    var rightButtonGroup = [];
    if(this.props.showEmail){
      rightButtonGroup.push(
        <TouchableHighlight key="email" underlayColor="transparent" onPress={() => this.tappedOnEmail()}>
        <Image style={styles.rightButton} source={require("../../assets/img/ic_navbar_email.png")}>
        </Image>
        </TouchableHighlight>
      );
    }

    if(this.props.showAlert){
      rightButtonGroup.push(
        <TouchableHighlight key="alert" underlayColor="transparent" onPress={() => this.tappedOnAlert()}>
        <View>
        <Image style={styles.rightButton} source={require("../../assets/img/ic_navbar_alert.png")}>
        </Image>
        {this.renderNotiNumber()}
        </View>
        </TouchableHighlight>
      );
    }

    if(this.props.showMenu){
      rightButtonGroup.push(
        <TouchableHighlight key="menu" underlayColor="transparent" onPress={() => this.tappedOnMenu()}>
        <Image style={styles.rightButton} source={require("../../assets/img/ic_navbar_menu.png")}  ></Image>
        </TouchableHighlight>
      );
    }


    if(this.state.isHorizontal&&UIUtils.isIPhoneX()){
      rightButtonGroup.push(<View key="rightUnsafeArea" style={styles.unsafeArea}></View>);
    }
    return rightButtonGroup;
  }

  //get status bar height based on orientation
  getStatusBarheight(){
    if(Platform.OS === 'ios'){
      if(this.state.isHorizontal){
        return 0
      }
      return UIUtils.getStatusBarHeight()
    }
    return 0
  }

  renderStatusBar(){

    if(Platform.OS === 'android'){
      return (
        <View></View>
      );
    }else{
      if(UIUtils.isIPadSize()){
        return (
          <View style={styles.statusBar}><Text> </Text></View>
        );
      }else{
        if(this.state.isHorizontal){
          return(
            <View></View>
          );
        }else {
          return (
            <View style={styles.statusBar}></View>
          );
        }
      }
    }
  }

  renderIPhoneXTopSpace(){
    if(UIUtils.isIPhoneX()){
      return (<View style={styles.iphoneXTopSpace}></View>);
    }
  }


  renterCenterView(){
    var logoImageUri = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
    if(this.props.showLogo){
      return (<Image style={styles.logo} source={{uri:'file://'+logoImageUri}}  ></Image>);
    }else{
      return (<Text style={[styles.navigationBarTitle, {color:this._branding.getNavigationFontColor()}, {fontWeight:this._branding.getNavigationFontWeight()}]}>{this.props.title}</Text>);
    }
  }

  onLayout() {

    var isInHorizontal = UIUtils.isDeviceInHorizontal();
    this.setState({isHorizontal: isInHorizontal});
  }



  render() {
    return (
        <View>
          {this.renderStatusBar()}
          <LinearGradient colors={this._branding.getNavigationColors()} style={[styles.navigatorBarWrapper, {height:UIUtils.size(44)}]} onLayout={()=>this.onLayout()}>
            <View style={styles.leftButtonGroup}>
              {this.renderLeftGroups()}
            </View>
            {this.renterCenterView()}
            <View style={styles.rightButtonGroup}>
              {this.renderRightButtonGroup()}
            </View>
          </LinearGradient>

        </View>

    );
  }
}

const styles = StyleSheet.create({
  statusBar:{
    width:20,
    height:20//status bar height will always be 22 on different devices
  },
  iphoneXTopSpace:{
    height:UIUtils.size(10)
  },
  navigatorBarWrapper:{
    alignItems:"flex-end",
    flexDirection:'row',
    justifyContent:'space-between',
  },

  navigationBarTitle:{
    marginBottom:10,
    fontSize:UIUtils.size(18),
  },

  profileImg:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
    resizeMode:'cover',
    width:UIUtils.size(38),
    height:UIUtils.size(38),
    borderRadius: UIUtils.size(17.5),
    borderColor:COLOR.LINE_GREY,
    borderWidth:UIUtils.size(0.5)
  },

  logo:{
    resizeMode:'contain',
    height:UIUtils.size(40),
    width:UIUtils.size(100)
  },

  leftButtonGroup:{
    width:UIUtils.size(65),
    height:UIUtils.size(44),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:"flex-start"
  },

  rightButtonGroup:{
    width:UIUtils.size(65),
    height:UIUtils.size(44),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:"flex-end"
  },

  backButton:{
    width:UIUtils.size(LAYOUT_SPACE.NAVBAR_ICON_HEIGHT),
    height:UIUtils.size(LAYOUT_SPACE.NAVBAR_ICON_HEIGHT),
    resizeMode:'contain',
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL)
  },

  rightButton:{
    width:UIUtils.size(LAYOUT_SPACE.NAVBAR_ICON_HEIGHT),
    height:UIUtils.size(LAYOUT_SPACE.NAVBAR_ICON_HEIGHT),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL)
  },

  moreNewNotiWrapper:{
    backgroundColor:'red',
    width:UIUtils.size(24),
    height:UIUtils.size(20),
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    top:UIUtils.size(-8),
    right:UIUtils.size(1),
    borderRadius: UIUtils.size(10)
  },

  newNotiWrapper:{
    backgroundColor:'red',
    width:UIUtils.size(20),
    height:UIUtils.size(20),
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    top:UIUtils.size(-8),
    right:UIUtils.size(1),
    borderRadius: UIUtils.size(10)
  },
  newNotiText:{
    fontSize:UIUtils.size(11),
    color:'white',
    fontWeight:'bold'
  },
  unsafeArea:{
    width:44,
    height:1
  }
});
