import React, { Component } from 'react';
import {StyleSheet, Modal, Text, TouchableHighlight, View,Image } from 'react-native';
import UIUtils from '../Utils/UIUtils';
import {COLOR} from '../Constants/Constants';

export default class LoadingViewComponent extends Component {


    constructor(props) {
        super(props);
        this.state = {modalVisible: false};
    }

    setVisible(visible) {
        this.setState({modalVisible: visible});
    }


    render() {
        return (
            <View style={styles.container}>

                <Modal
                        animationType={"fade"}
                        transparent={true}
                        visible={this.state.modalVisible}
                       onRequestClose={() => {
                           // alert("Modal has been closed.")
                        }
                       }
                >
                    <View style={styles.modalWrapper}>
                        <Image style={styles.loadingIcon} source={require('../../assets/img/ic_loading.gif')}></Image>
                    </View>

                </Modal>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    modalWrapper:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#00000055'
    },
    loadingIcon:{
        width:UIUtils.size(40),
        height:UIUtils.size(40),
        resizeMode:'contain'

    }
});