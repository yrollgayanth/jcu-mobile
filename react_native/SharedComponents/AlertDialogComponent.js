import React, { Component } from 'react';
import {StyleSheet, Modal, Text, TouchableHighlight, View } from 'react-native';
import UIUtils from '../Utils/UIUtils';
import {COLOR} from '../Constants/Constants';

export default class AlertDialogComponent extends Component {


    /*
    popup:  this.refs.alertDialog.setVisible(true);


     With ok/cancel button

    *<AlertDialogComponent ref='alertDialog'
     title={"Popup title"}
     message={"!!!!Message from My Study page,a this message is veeeeeeeeeeery  lonnnnnnnnnnnnnnnnnnnnnnnng."}
     leftButtonLabel={"OK"}
     rightButtonLabel={"Cancel"}
     leftButtonCallback={()=>{}}
     rightButtonCallback={()=>{}}
     ></AlertDialogComponent>


      Single ok button

     <AlertDialogComponent ref='alertDialog'
     message={"!!!!Message from My Study page,a this message is veeeeeeeeeeery  lonnnnnnnnnnnnnnnnnnnnnnnng."}
     isSingleButton={true}
     okButtonCallback={()=>{}}
     ></AlertDialogComponent>
    *
    *
    *
    * */

    constructor(props) {
        super(props);
        this.state = {modalVisible: false};
        this.leftButtonTapped = this.leftButtonTapped.bind(this);
        this.rightButtonTapped = this.rightButtonTapped.bind(this);
        this.okButtonTapped = this.okButtonTapped.bind(this);
        this.renderTitle = this.renderTitle.bind(this);
    }

    setVisible(visible) {
        this.setState({modalVisible: visible});
    }

    leftButtonTapped(){
        this.setVisible(!this.state.modalVisible);
        this.props.leftButtonCallback();
    }
    rightButtonTapped(){
        this.setVisible(!this.state.modalVisible);
        this.props.rightButtonCallback();
    }

    okButtonTapped(){
        this.setVisible(!this.state.modalVisible);
        this.props.singleButtonCallback();

    }

    renderButtonBar(){

        if(this.props.isSingleButton==null||this.props.isSingleButton===false){
            return (
                <View style={styles.buttonBar}>
                    <TouchableHighlight style={styles.leftButton}
                                        onPress={this.leftButtonTapped}>
                        <Text style={styles.buttonText}>{this.props.leftButtonLabel}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight  style={styles.rightButton}
                                         onPress={this.rightButtonTapped}>
                        <Text style={styles.buttonText}>{this.props.rightButtonLabel}</Text>
                    </TouchableHighlight>
                </View>

            );
        }else if(this.props.isSingleButton===true){
            return(
                <View style={styles.singleButtonBar}>
                    <TouchableHighlight style={styles.okButton}
                                        onPress={this.okButtonTapped}>
                        <Text style={styles.buttonText}>{this.props.singleButtonLabel}</Text>
                    </TouchableHighlight>
                </View>

            );

        }

    }

    renderTitle(){

        if(this.props.title==null||this.props.title===""){
            return (
                <View></View>
            )
        }else{
            return(
                <Text style={styles.title}>
                    {this.props.title}
                </Text>
            );

        }
    }

    render() {
        return (
            <View style={styles.container}>

                <Modal
                        animationType={"fade"}
                        transparent={true}
                        visible={this.state.modalVisible}
                       onRequestClose={() => {
                           // alert("Modal has been closed.")
                        }
                       }
                >
                    <View style={styles.modalWrapper}>
                        <View style={styles.modalContent}>
                            {/*<View style={styles.testHeight}></View>*/}
                            {this.renderTitle()}
                            <Text style={styles.messageContent}>
                                {this.props.message}
                            </Text>
                            {this.renderButtonBar()}

                        </View>

                    </View>

                </Modal>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    modalWrapper:{
        flex:1,
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:'#00000055'
    },
    modalContent: {
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'space-between',
        minHeight: UIUtils.size(120),
        width: UIUtils.size(300),
        borderRadius: UIUtils.size(6)

    },
    title:{
        color:COLOR.APP_THEME_BLUE,
        fontWeight:"bold",
        paddingLeft:UIUtils.size(20),
        paddingRight:UIUtils.size(20),
        paddingTop:UIUtils.size(20),
        fontSize:UIUtils.size(15),
        flexWrap:'wrap',
        textAlign:'center'
    },
    messageContent:{
        padding:UIUtils.size(20),
        flexWrap:'wrap',
        fontSize:UIUtils.size(15)
    },
    buttonBar:{
        padding:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    singleButtonBar:{
        padding:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around'
    },
    button:{
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:COLOR.APP_THEME_BLUE,
        width:UIUtils.size(80),
        height:UIUtils.size(35),
        borderRadius: UIUtils.size(3)
    },
    leftButton:{
        marginLeft:UIUtils.size(50),
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:COLOR.APP_THEME_BLUE,
        width:UIUtils.size(80),
        height:UIUtils.size(35),
        borderRadius: UIUtils.size(6)
    },
    rightButton:{
        marginRight:UIUtils.size(50),
        alignItems:'center',
        justifyContent:'space-around',
        backgroundColor:COLOR.APP_THEME_BLUE,
        width:UIUtils.size(80),
        height:UIUtils.size(35),
        borderRadius: UIUtils.size(6)
    },
    okButton:{
            alignItems:'center',
            justifyContent:'space-around',
            backgroundColor:COLOR.APP_THEME_BLUE,
            width:UIUtils.size(80),
            height:UIUtils.size(35),
            borderRadius: UIUtils.size(6)
    },
    buttonText: {
        fontSize:UIUtils.size(14),
        color:'white',
        fontWeight:'bold'
    }

});