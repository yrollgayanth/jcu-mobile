/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

import UIUtils from "../../../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE} from "../../../Constants/Constants";
import GreyDividerComponent from '../../GreyDividerComponent';

export default class MenuItemComponent extends Component<{}> {

    constructor(prop){
        super(prop);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.menuContainer}>
                    <Image style={styles.menuOptionIcon} source={this.props.menuOptionModel.menuOptionIconName}  ></Image>
                    <Text style={styles.menuOptionTitle} >{this.props.menuOptionModel.menuOptionName}</Text>
                    <Image style={styles.arrowIcon} source={require('../../../../assets/img/ic_arrow_right_grey.png')} ></Image>
                </View>
                <GreyDividerComponent></GreyDividerComponent>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
    menuContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        flexDirection:'row',
        height:UIUtils.size(LAYOUT_SPACE.MENU_ITEM_HEIGHT),
    },
    menuOptionIcon:{
        marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
        width:UIUtils.size(LAYOUT_SPACE.STANDARD_ICON_SIZE_MID),
        height:UIUtils.size(LAYOUT_SPACE.STANDARD_ICON_SIZE_MID),
        resizeMode:'contain'
    },
    menuOptionTitle:{
        marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
        fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    },
    arrowIcon:{
        position: 'absolute',
        right: UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
        width:UIUtils.size(LAYOUT_SPACE.STANDARD_ARROR_SIZE_WIDTH),
        height:UIUtils.size(LAYOUT_SPACE.STANDARD_ARROR_SIZE_HEIGHT)
    }
});
