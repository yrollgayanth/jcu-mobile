/**
* Created by Sylvia on 29/11/17.
*/
export default class MenuOptionModel{
  menuOptionName;
  menuOptionIconName;

  constructor(menuOptionName,menuOptionIconName){
    this.menuOptionIconName = menuOptionIconName;
    this.menuOptionName = menuOptionName;
  }
}
