/**
* Created by Sylvia on 29/11/17.
*/
export default class MenuOptionAsyncIconModel{
  menuOptionName;
  menuOptionIconUrl;

  constructor(menuOptionName,menuOptionIconUrl){
    this.menuOptionIconUrl = menuOptionIconUrl;
    this.menuOptionName = menuOptionName;
  }
}
