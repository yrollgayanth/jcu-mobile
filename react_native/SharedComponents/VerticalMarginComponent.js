/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';

import UIUtils from '../Utils/UIUtils';

export default class VerticalMarginComponent extends Component<> {

    constructor(prop){
        super(prop);
    }

    render() {
        return (
            <View style={{height:UIUtils.size(this.props.height)}}></View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
