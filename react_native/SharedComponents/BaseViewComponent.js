
import React, { Component } from 'react';
import { StyleSheet, View, BackAndroid} from 'react-native';
import AlertDialogComponent from "./AlertDialogComponent";
import LoadingViewComponent from "./LoadingViewComponent";
import PickerComponent from './PickerComponent';
import NavigationBarComponent from './NavigationBarComponent';
import ActionSheetComponent from './ActionSheetComponent';
import UIUtils from '../Utils/UIUtils';

export default class BaseViewComponent extends Component<{}> {

    /*
     * A component with build-in NavigationBar LoadingView, AlertDialog, Picker
     *
     * How to use:
     *
     * 1. make your component extends BaseViewComponentRefined
     * 2. overwrite renderView(), remove the code from render() to this method
     * 3. enable component you need in this.state and set config data for particular component
     *
     *      e.g.
     *
     *      this.state={

     enableLoadingView:true,
     enableSingleButtonDialog:true,
     enableTwoButtonDialog:true,

     singleButtonDialogConfig:{
     title:"TestSharedComponent",
     message:"This is the message content from TestSharedComponent",
     buttonLabel:"Ok",
     callback:this.singleButtonDialogCallback
     },

     twoButtonDialogConfig:{
     title:"",
     message:"Two button message from TestSharedComponent",
     leftButtonLabel:"Ok",
     rightButtonLabel:"Cancel",
     leftButtonCallback:this.leftButtonDialogCallback,
     rightButtonCallback:this.rightButtonDialogCallback
     }
     };

     *
     * 3. call methods to show/hide component,
     *       e.g.
     *
     *           this.showLoadingView();
     *
     *    or extend the functionality via overwriting the methods
     *       e.g.
     *
     *       showLoadingView(){
     super.showLoadingView();
     // TODO: extend functionality
     }
     *
     *
     * */

     constructor(props){
       super(props);
       this.state = {

         isHorizontal:UIUtils.isDeviceInHorizontal(),

         //enable detection for top/bottom safe area of iPhoneX in portrait mode
         topBottomSafeArea:true,
         //enable detection for left/right safe area of iPhoneX in landscape mode
         leftRightSafeArea:true,
         enableNavigationBar:false,
         enableLoadingView:false,
         enableSingleButtonDialog:false,
         enableTwoButtonDialog:false,
         enablePicker:false,
         enableActionSheet:false,

         actionSheetConfig:{},
         navigationBarConfig:{},
         singleButtonDialogConfig:{},
         twoButtonDialogConfig:{},
         pickerConfig:{},
       }
     }

    showActionSheet(){
      if(this.state.enableActionSheet){
        this.refs.actionSheet.setVisible(true);
      }
    }

    showLoadingView(){
      if(this.state.enableLoadingView){
        this.refs.loadingView.setVisible(true);
      }
    }

    hideLoadingView(){
      if(this.state.enableLoadingView){
        this.refs.loadingView.setVisible(false);
      }
    }

    popSingleButtonAlertDialog(){

      if(this.state.enableSingleButtonDialog){
        this.refs.singleButtonDialog.setVisible(true);
      }
    }

    popTwoButtonAlertDialog(){
      if(this.state.enableTwoButtonDialog){
        this.refs.twoButtonDialog.setVisible(true);
      }
    }

    showPickerComponent() {
        if (this.state.enablePicker) {
            this.refs.pickerComponent.setVisible(true);
        }
    }

    //handle go back for iOS and Android
    goBackCall(){
      this.props.navigation.goBack()
    }

    /*
     overwrite this method in your component: child  of this component
     in your child component, MOVE the return() block from render() into this method
    */
    renderView(){}

    renderNavigationBar(){

        if(this.state.enableNavigationBar){
            var title = (this.state.navigationBarConfig.title==null||this.state.navigationBarConfig.title=="")?
                "Please config the navbar":this.state.navigationBarConfig.title;
            var showHome = (this.state.navigationBarConfig.showHome==null)?false:this.state.navigationBarConfig.showHome;
            var showBack = (this.state.navigationBarConfig.showBack==null)?false:this.state.navigationBarConfig.showBack;
            var showAlert = (this.state.navigationBarConfig.showAlert==null)?false:this.state.navigationBarConfig.showAlert;
            var showMenu = (this.state.navigationBarConfig.showMenu==null)?false:this.state.navigationBarConfig.showMenu;
            var showLogo = (this.state.navigationBarConfig.showLogo==null)?false:this.state.navigationBarConfig.showLogo;
            var showProfile = (this.state.navigationBarConfig.showProfile==null)?false:this.state.navigationBarConfig.showProfile;
            var showEmail = (this.state.navigationBarConfig.showEmail==null)?false:this.state.navigationBarConfig.showEmail;


            return (
                <NavigationBarComponent
                    navigate={this.props.navigate}
                    title={title}
                    showHome={showHome}
                    showBack={showBack}
                    showAlert={showAlert}
                    showEmail={showEmail}
                    showMenu={showMenu}
                    showLogo={showLogo}
                    showProfile={showProfile}
                    goBackCallback={()=>{this.goBackCall()}}
                ></NavigationBarComponent>
            );
        }
    }


    renderPickerComponent(){
        if(this.state.enablePicker){
            return(
                <PickerComponent ref="pickerComponent"
                                 title={this.state.pickerConfig.title}
                                items={this.state.pickerConfig.items}
                                selectedItem={this.state.pickerConfig.selectedItem}
                                 doneCallback={(item)=>this.state.pickerConfig.doneCallback(item)}
                ></PickerComponent>
            );
        }
    }

    renderLoadingView(){
      if(this.state.enableLoadingView){
        return(
          <LoadingViewComponent ref="loadingView"></LoadingViewComponent>
        );
      }
    }

    renderActionSheet(){
      if(this.state.enableActionSheet){
        return(
          <ActionSheetComponent ref="actionSheet"
          title={this.state.actionSheetConfig.title}
          options={this.state.actionSheetConfig.options}
          optionCallback={(option)=>this.state.actionSheetConfig.optionCallback(option)}
          ></ActionSheetComponent>
        );
      }
    }

    renderSingleButtonDialog(){
      if(this.state.enableSingleButtonDialog){
        return(
          <AlertDialogComponent ref='singleButtonDialog'
          title={this.state.singleButtonDialogConfig.title}
          message={this.state.singleButtonDialogConfig.message}
          isSingleButton={true}
          singleButtonLabel={this.state.singleButtonDialogConfig.buttonLabel}
          singleButtonCallback={()=>this.state.singleButtonDialogConfig.callback()}
          ></AlertDialogComponent>
        );
      }
    }

    renderTwoButtonDialog() {
      if(this.state.enableTwoButtonDialog){
        return (
          <AlertDialogComponent ref='twoButtonDialog'
          title={this.state.twoButtonDialogConfig.title}
          message={this.state.twoButtonDialogConfig.message}
          leftButtonLabel={this.state.twoButtonDialogConfig.leftButtonLabel}
          rightButtonLabel={this.state.twoButtonDialogConfig.rightButtonLabel}
          leftButtonCallback={()=>this.state.twoButtonDialogConfig.leftButtonCallback()}
          rightButtonCallback={()=>this.state.twoButtonDialogConfig.rightButtonCallback()}
          ></AlertDialogComponent>
        );
      }
    }

    renderIPhoneXTopSpace(){
      if(this.state.topBottomSafeArea == null||this.state.topBottomSafeArea == true){
          if(UIUtils.isIPhoneX()&&!this.state.isHorizontal){
              return (<View style={styles.iphoneXTopSpace}></View>);
          }
      }
    }

    renderiPhoneXBottomSpace(){

        if((this.state.topBottomSafeArea == null||this.state.topBottomSafeArea == true)&&UIUtils.isIPhoneX()){
            if(!this.state.isHorizontal){
                return (<View style={styles.iphoneXBottomSpace}></View>);
            }else{
                return (<View style={styles.iphoneXBottomSpace}></View>);
            }
        }
    }

    onLayout(){
        var isInHorizontal = UIUtils.isDeviceInHorizontal();
        this.setState({isHorizontal: isInHorizontal});
    }

    renderLandscapeUnsafeArea(){
        if(this.state.leftRightSafeArea==null||this.state.leftRightSafeArea==true){
            if(this.state.isHorizontal&&UIUtils.isIPhoneX()){
                return(<View style={styles.unsafeArea}></View>);
            }
        }
    }
    //
    // renderRightUnsafeArea(){
    //
    //     if(this.state.leftRightSafeArea==null||this.state.leftRightSafeArea==true){
    //         if(this.state.isHorizontal&&UIUtils.isIPhoneX()){
    //             return(<View style={styles.unsafeArea}></View>);
    //         }
    //     }
    // }
    //

    renderContentWithSafeArea() {
        return(
            <View style={styles.safeAreaContent}>
                {this.renderLandscapeUnsafeArea()}
                {this.renderView()}
                {this.renderLandscapeUnsafeArea()}
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container} onLayout={()=>this.onLayout()}>
                {/*{this.renderIPhoneXTopSpace()}*/}
                {this.renderNavigationBar()}
                {this.renderContentWithSafeArea()}
                {/*{this.renderView()}*/}
                {this.renderiPhoneXBottomSpace()}
                {this.renderSingleButtonDialog()}
                {this.renderTwoButtonDialog()}
                {this.renderLoadingView()}
                {this.renderPickerComponent()}
                {this.renderActionSheet()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    iphoneXTopSpace:{
      height:UIUtils.size(12)
    },
    iphoneXBottomSpace:{
        backgroundColor:'white',
        height:UIUtils.size(20)
    },
    safeAreaContent:{
        flexDirection:'row',
        flex:1
    },
    unsafeArea:{
        width:44,
        height:60
    }
});
