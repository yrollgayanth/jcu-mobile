/**
* Created by Sylvia on 29/11/17.
*/


import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from 'react-native';

import UIUtils from "../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../Constants/Constants";
import LinearGradient from 'react-native-linear-gradient';

export default class ExpandableMenuSectionComponent extends Component<{}> {

  constructor(prop){
    super(prop);
  }


  renderArrow(){

    if(this.props.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }
  }

  render() {
    return (
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {alert("tapped")}}>
      <LinearGradient colors={[COLOR.GRADIENT_DARK_BLUE, COLOR.GRADIENT_LIGHT_BLUE]}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={styles.sectionTitle}>{this.props.title}</Text>
      {this.renderArrow()}
      </LinearGradient>

      </TouchableHighlight>
    );
  }

}

const styles = StyleSheet.create({
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  }
});
