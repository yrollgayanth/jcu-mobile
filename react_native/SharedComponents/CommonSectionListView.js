/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    SectionList
} from 'react-native';
import UIUtils from "../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../Constants/Constants";


export default class CommonSectionListView extends Component<{}> {

    /*
    * <CommonSectionListView
             models={this.state.menuOptionModels}
             renderItem={this.renderRow}
             renderSectionHeader={this.renderSectionHeader}
     ></CommonSectionListView>
    * */


    constructor(prop){
        super(prop);
    }



    render() {
        return (
            <View style={styles.container}>
                <SectionList
                    stickySectionHeadersEnabled={false}
                    sections={this.props.models}
                    renderItem={({item})=>this.props.renderItem(item)}
                    renderSectionHeader={({section}) => this.props.renderSectionHeader(section)}
                />
            </View>

        );
    }




}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});