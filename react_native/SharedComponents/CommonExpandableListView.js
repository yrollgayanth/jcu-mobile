/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import {COLOR,LAYOUT_SPACE} from '../Constants/Constants';
import UIUtils from '../Utils/UIUtils';
import ExpanableList from 'react-native-expandable-section-flatlist';


export default class CommonExpandableListView extends Component<{}> {


    /*

     <CommonExpandableListView
         dataSource={this.state.optionData}
         renderRow={this.renderRow}
         renderSection={this.renderSection}
     ></CommonExpandableListView>


    * Example dataSource structure
    *
    * keys required: header/member/header.isOpen
    *
    *[
         {
            header: {
                sectionTitle:'Section A',
                isOpen:false
            },
            member: [
                {
                    menuOptionName: 'Option A-1',
                    content: 'content',
                    ...
                },
                 {
                    menuOptionName: 'Option A-2',
                    content: 'content',
                    ....
                 },
             ]
        },
         {
            header: {
                 sectionTitle:'Section B',
                 isOpen:false
             },
            member: [
                 {
                     menuOptionName: 'Option B-1',
                     content: 'content',
                     ...
                 },
                {
                     menuOptionName: 'Option B-2',
                     content: 'content',
                     ....
                 }
             ]
         }
    ]
    *
    *
    * */

    // state = {dataSource:[]};


    constructor(prop){
        super(prop);

        this.headerOnPress = this.headerOnPress.bind(this);
        this.state = {dataSource:[]};

    }

    componentDidMount(){

        this.setState({dataSource:this.props.dataSource});
    }


    headerOnPress(sectionId,isOpen){

        if(isOpen){
            this.state.dataSource[sectionId].header.isOpen = false;

        }else{
            this.state.dataSource[sectionId].header.isOpen = true;
        }

    }


    render() {
        return (
            <View style={styles.container}>
                <ExpanableList
                    dataSource={this.state.dataSource}
                    headerKey="header"
                    memberKey="member"
                    renderRow={this.props.renderRow}
                    renderSectionHeaderX={this.props.renderSection}
                    openOptions={[]}
                    headerOnPress={this.headerOnPress}
                />
            </View>

        );
    }




}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});