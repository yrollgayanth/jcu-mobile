import React, { Component } from 'react';
import {Platform,StyleSheet, Modal, Text, TouchableHighlight, View, Picker} from 'react-native';
import UIUtils from '../Utils/UIUtils';
import {COLOR} from '../Constants/Constants';

export default class PickerComponent extends Component {



    constructor(props) {
        super(props);

        var selectedItem = (this.props.selectedItem==null||this.props.selectedItem=="")?"":this.props.selectedItem;

        this.state = {
            selectedItem:selectedItem,
            modalVisible: false,
            focuseditem:selectedItem
        };

        this.setVisible = this.setVisible.bind(this);
        this.tappedOnCancel = this.tappedOnCancel.bind(this);
        this.tappedOnDone = this.tappedOnDone.bind(this);

    }

    setVisible(visible) {
        this.setState({modalVisible: visible});
    }


    tappedOnCancel(){
        this.setVisible(false);
    }

    tappedOnDone(){
        console.log("tappedonDone:" + this.state.selectedItem);
        this.props.doneCallback(this.state.selectedItem);
        this.setVisible(false);
    }

    renderPickerItems(){

        var pickerItems = [];
        for(var i=0;i<this.props.items.length;i++){
            var item = this.props.items[i];
            pickerItems.push(<Picker.Item key={item} label={item} value={item} />);
        }
        return pickerItems;
    }

    render() {

        var modalWrapperStyle;
        var modalContentStyle;

        if(Platform.OS === 'android'){
            modalContentStyle = styles.modalContentAndroid;
            modalWrapperStyle = styles.modalWrapperAndroid;
        }else{
            modalContentStyle = styles.modalContentIOS;
            modalWrapperStyle = styles.modalWrapperIOS;
        }

        return (
            <View style={styles.container}>

                <Modal
                        animationType={"fade"}
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {}}
                >
                    <View style={modalWrapperStyle}>
                        <View style={modalContentStyle}>
                            <View style={styles.topButtonBar}>
                                <TouchableHighlight underlayColor="transparent"
                                                    onPress={this.tappedOnCancel}>
                                    <Text style={styles.button}>Cancel</Text>
                                </TouchableHighlight>
                                <Text style={styles.title}>{this.props.title}</Text>
                                <TouchableHighlight underlayColor="transparent"
                                                    onPress={this.tappedOnDone}>
                                    <Text style={styles.button}>Done</Text>
                                </TouchableHighlight>
                            </View>
                            <Picker
                                    selectedValue={this.state.selectedItem}
                                    onValueChange={(item) => this.setState({selectedItem: item})}>
                                {this.renderPickerItems()}
                            </Picker>
                        </View>

                    </View>

                </Modal>

            </View>
        );
    }
}

const styles = StyleSheet.create({

    modalWrapperAndroid:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#00000055'
    },
    modalContentAndroid: {
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'space-between',
        width:UIUtils.getScreenWidth()*0.8
    },
    modalWrapperIOS:{
        flex:1,
        alignItems:'center',
        justifyContent:'flex-end',
        backgroundColor:'#00000055'
    },
    modalContentIOS: {
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'space-between',
        width:UIUtils.getScreenWidth()
    },
    title:{
        fontSize:UIUtils.size(16),
        fontWeight:'bold'
    },
    button:{
        fontSize:UIUtils.size(16),
        color:'blue',
        paddingLeft:UIUtils.size(10),
        paddingRight:UIUtils.size(10)
    },
    topButtonBar:{
        height:UIUtils.size(40),
        justifyContent:'space-between',
        flexDirection:"row",
        alignItems:'center'
    }

});