import React, { Component } from 'react';
import {ScrollView, StyleSheet, Modal, Text, TouchableHighlight, View } from 'react-native';
import UIUtils from '../Utils/UIUtils';
import {COLOR} from '../Constants/Constants';

export default class ActionSheetComponent extends Component {


    constructor(props) {
        super(props);
        this.state = {modalVisible: false};
        // this.tappedOnOption = this.tappedOnOption.bind(this);
    }

    setVisible(visible) {
        this.setState({modalVisible: visible});
    }

    tappedOnOption(option){
        this.setState({modalVisible: false});
        this.props.optionCallback(option);
    }


    renderOptions(){
        var optionViews = [];
        for(var i=0;i<this.props.options.length;i++){
            var optionLabel = this.props.options[i];
            optionViews.push(
                <View key={i}>
                    <View style={styles.divider}></View>
                    <TouchableHighlight
                        underlayColor="transparent"
                        onPress={()=>{this.tappedOnOption(optionLabel)}}
                        style={styles.cancelOption}>
                        <Text style={styles.optionTitle}>{optionLabel}</Text>
                    </TouchableHighlight>
                </View>

            );
        }

        return optionViews;

    }

    renderTitle(){
        if(this.props.title==null||this.props.title===""){
            return(<View></View>);
        }else{
            return(<View style={styles.titleWrapper}>
                <Text style={styles.title}>{this.props.title}</Text>
            </View>);
        }
    }

    render() {

        //calculate height of scrollView
        var scrollViewHeight;
        var screenHeight = UIUtils.getScreenHeight();
        scrollViewHeight = screenHeight - UIUtils.size(40) -UIUtils.size(40);
        if(this.props.title!=null&&this.props.title!=""){
            scrollViewHeight -= UIUtils.size(40);
        }


        return (
            <View style={styles.container}>
                <Modal
                        animationType={"none"}
                        transparent={true}
                        visible={this.state.modalVisible}
                       onRequestClose={() => {
                           // alert("Modal has been closed.")
                        }
                       }
                >
                    <View style={styles.modalWrapper}>
                        <View style={styles.modalContent}>
                            {this.renderTitle()}
                            <ScrollView style={{maxHeight:scrollViewHeight}}
                                        showsHorizontalScrollIndicator={false}>
                                {this.renderOptions()}
                            </ScrollView>
                        </View>
                        <TouchableHighlight
                            underlayColor="transparent"
                            onPress={()=>{this.setState({modalVisible:false})}}
                            style={styles.cancelOption}>
                            <Text style={styles.optionTitle}>Cancel</Text>
                        </TouchableHighlight>

                    </View>

                </Modal>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    modalWrapper:{
        flex:1,
        justifyContent:'flex-end',
        backgroundColor:'#00000055'
    },
    modalContent: {
        // height:300,
        margin:UIUtils.size(5),
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'space-between',
        borderRadius: UIUtils.size(6)

    },
    scrollView:{
        // flex:1
    },
    option:{
        margin:UIUtils.size(5),
        marginTop:0,
        height:UIUtils.size(40),
        alignItems:'center',
        justifyContent:'center'
    },
    cancelOption:{
        margin:UIUtils.size(5),
        marginTop:0,
        height:UIUtils.size(40),
        backgroundColor:'white',
        borderRadius: UIUtils.size(6),
        alignItems:'center',
        justifyContent:'center'
    },
    divider:{
        marginLeft:UIUtils.size(8),
        marginRight:UIUtils.size(8),
        height:UIUtils.size(1),
        backgroundColor:"#eeeeee"
    },
    titleWrapper:{
        height:UIUtils.size(40),
        justifyContent:'center',
        alignItems:'center',
        padding:UIUtils.size(3),
    },
    title:{
        color:'grey',
        fontSize:UIUtils.size(14),
        textAlign:'center'
    },

    optionTitle:{
        color:'#1a82fc',
        fontSize:UIUtils.size(18),
        textAlign:'center',
        justifyContent:'center'
    }
});