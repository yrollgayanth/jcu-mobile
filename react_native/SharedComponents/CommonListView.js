/**
* Created by Sylvia on 29/11/17.
*/
import React, { Component } from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import UIUtils from "../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../Constants/Constants";

export default class CommonListView extends Component<{}> {

  /*
  *
  * <CommonListView
  *       models={this.state.optionModels}
  *       renderItem={this.renderItem}>
  * </CommonListView>
  *
  *
  * */

  constructor(prop){
    super(prop);
  }

  render() {
    return (
      <View style={styles.container}>
      <FlatList
        data={this.props.models}
        renderItem={({item}) => this.props.renderItem(item)}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
