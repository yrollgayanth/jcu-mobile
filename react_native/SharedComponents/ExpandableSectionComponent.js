/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

import UIUtils from "../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../Constants/Constants";
import GreyDividerComponent from './GreyDividerComponent';

export default class ExpandableSectionComponent extends Component<{}> {

    constructor(prop){
        super(prop);
    }

    renderGreyArrow(){
        if(this.props.sectionModel.isOpen){
            return (
                <Image style={styles.arrowIcon} source={require('../../assets/img/ic_arrow_up_white.png')} ></Image>
            );
        }else{
            return (
                <Image style={styles.arrowIcon} source={require('../../assets/img/ic_arrow_down_white.png')} ></Image>
            )

        }

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.menuContainer}>
                    <Text style={styles.menuOptionTitle} >{this.props.sectionModel.sectionTitle}</Text>
                    {this.renderGreyArrow()}
                </View>
                <GreyDividerComponent></GreyDividerComponent>
            </View>
        );
    }

}

const styles = StyleSheet.create({

    container: {
      flex:1
    },
    menuContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: COLOR.SECTION_DARK_GREY,
        flexDirection:'row',
        height:UIUtils.size(LAYOUT_SPACE.MENU_ITEM_HEIGHT)
    },
    menuOptionTitle:{
        color:'white',
        marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
        fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
        fontWeight: 'bold'
    },
    arrowIcon:{
        position: 'absolute',
        right: UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
        width:UIUtils.size(LAYOUT_SPACE.STANDARD_ARROR_SIZE_HEIGHT),
        height:UIUtils.size(LAYOUT_SPACE.STANDARD_ARROR_SIZE_HEIGHT),
        resizeMode:'contain'
    }

});
