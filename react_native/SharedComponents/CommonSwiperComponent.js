

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  TouchableHighlight,
  ScrollView
} from 'react-native';

import UIUtils from "../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../Constants/Constants";
import MenuSectionComponent from './MenuSectionComponent';


export default class CommonSwiperComponent extends Component<{}> {


  /*
  *
  * <CommonSwiperComponent
  title={"Blackboard"}
  countItems={this.state.dummyNewsModel.length}
  renderItems={this.renderNewsItems}
  ></CommonSwiperComponent>
  *
  * */

  constructor(prop){
    super(prop);

    this.state = {
      dotCount:this.props.countItems,
      activeDot:0,
      isHorizontal:UIUtils.isDeviceInHorizontal()
    }
    this.onScrollEnd = this.onScrollEnd.bind(this);
    this.onLayout = this.onLayout.bind(this);
  }

  renderDotBar(){
    var dots = [];
    for(var i=0;i<this.state.dotCount;i++){
      if(i==this.state.activeDot){
        dots.push(
          <View style={styles.dotWrapper} key={i}>
          <Image style={styles.dotImage} source={require('../../assets/img/dot_active.png')}></Image>
          </View>
        );
      }else{
        dots.push(
          <View style={styles.dotWrapper} key={i}>
          <Image style={styles.dotImage} source={require('../../assets/img/dot_inactive.png')}></Image>
          </View>
        );
      }
    }
    return dots;
  }

  onScrollEnd(event){

    var width = UIUtils.getScreenWidth();
    var xOffset = event.nativeEvent.contentOffset.x;
    var scrolledToIndex = xOffset/width;
    this.setState({activeDot:scrolledToIndex});
  }

  onLayout(){
    if(this.state.isHorizontal!=UIUtils.isDeviceInHorizontal()){
      this.setState({isHorizontal:UIUtils.isDeviceInHorizontal()});
      this.refs.scrollView.scrollTo({x:0, y:0, animated:false});
      this.setState({activeDot:0});
    }
  }

  render() {
    return (
      <View style={styles.container} onLayout={this.onLayout}>

      <View style={styles.scrollViewWrapper}>
      <MenuSectionComponent title={this.props.title}></MenuSectionComponent>
      <ScrollView ref="scrollView"
      style={styles.scrollView}
      onMomentumScrollEnd={this.onScrollEnd}
      showsHorizontalScrollIndicator={false}
      pagingEnabled={true}
      horizontal={true}>
      {this.props.renderItems()}
      </ScrollView>
      </View>
      <View style={styles.dotBar}>
      <View style={styles.dotsWrapper}>{this.renderDotBar()}</View>
      </View>
      <TouchableHighlight  style={styles.seeAllButton}
      onPress={this.rightButtonTapped}>
      <Text style={styles.buttonText}>SEE ALL</Text>
      </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection:'column',
  },
  scrollViewWrapper:{
    // height:100
  },
  scrollView:{
    backgroundColor:'white'
  },
  dotBar:{
    flexDirection:'row',
    height:UIUtils.size(35),
    backgroundColor:COLOR.NAV_GREY,
    justifyContent:"space-around",
    alignItems:'center'
  },
  dotsWrapper:{
    flexDirection:'row',
  },
  dotWrapper:{
    // backgroundColor:'white',
    alignItems:'center',
    justifyContent:'space-around',
    flexDirection:'row',
    width:UIUtils.size(15),
    height:UIUtils.size(15),
  },
  dotImage:{
    width:UIUtils.size(8),
    height:UIUtils.size(8),
    resizeMode:'contain'
  },
  seeAllButton:{
    backgroundColor:'white',
    width:UIUtils.size(70),
    height:UIUtils.size(30),
    borderRadius: UIUtils.size(6),
    borderColor:COLOR.APP_THEME_BLUE,
    borderWidth:UIUtils.size(1),
    alignItems:'center',
    justifyContent:'space-around',
    position:'absolute',
    right:UIUtils.size(10),
    bottom:UIUtils.size(2.5),
  },
  buttonText:{
    fontSize:UIUtils.size(14),
    color:COLOR.APP_THEME_BLUE,
    fontWeight:'bold'
  }
});
