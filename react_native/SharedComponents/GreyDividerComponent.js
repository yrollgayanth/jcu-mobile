/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';

import UIUtils from "../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../Constants/Constants";

export default class GreyDividerComponent extends Component<> {

    constructor(prop){
        super(prop);
    }

    render() {
        return (
            <View style={styles.divider}></View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    divider:{
    height:1,
        backgroundColor:COLOR.LINE_GREY
    }
});
