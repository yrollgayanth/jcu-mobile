/**
* Created by Yrol on 31/01/18.
*/
import React, { Component } from 'react';
import EventEmitter from "react-native-eventemitter";
import { View, ImageBackground, Image, Text, TextInput, TouchableOpacity,StyleSheet,
        Alert, DeviceEventEmitter, KeyboardAvoidingView } from 'react-native';
import {EVENT} from '../../Constants/Constants';
import  UIUtils from '../../Utils/UIUtils';
import { DEFAULT_FILES } from '../../Constants/Constants';

export default class CurrentStudentComponent extends Component<{}> {

  _device_width = UIUtils.getScreenWidth()
  _header_height = UIUtils.navBarheight() + 5

  constructor(props) {
    super(props);
  }

  currentStudentView(){

    var navigate = this.props.navigation
    const { data } = this.props;
    var backgroundImageUri = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE
    var logoImageUri = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
    return(
      <ImageBackground
        source={{uri:'file://'+backgroundImageUri}}
        style={styles.imageBackground}>

        <View style={[{marginTop:this._header_height}]}>
          <Image style={styles.logoImage} source={{uri:'file://'+logoImageUri}}/>
        </View>

        <KeyboardAvoidingView style = {styles.container}>

          <TextInput style = {styles.input}
          autoCapitalize="none"
          onSubmitEditing={() => this.passwordInput.focus()}
          autoCorrect={false}
          returnKeyType="next"
          placeholder='Student ID'
          placeholderTextColor='white'/>

          <TextInput style = {styles.input}
          returnKeyType="go"
          autoCorrect={false}
          ref={(input)=> this.passwordInput = input}
          placeholder='Password'
          placeholderTextColor='white'
          secureTextEntry/>

          <TouchableOpacity style={styles.buttonContainer} onPress={()=>onSignInButtonPress(navigate)}>
            <Text style={styles.buttonText}>Sign In</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ImageBackground>
    )

  }

  render() {
    return this.currentStudentView()
  }
}

onSignInButtonPress = (navigateComponent) => {
  //EventEmitter.emit(EVENT.LOGIN_STATUS, true);
  DeviceEventEmitter.emit(EVENT.LOGIN_STATUS, {status:true});
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    //alignItems: 'center'
  },
  imageBackground:{
    width: '100%',
    height: '100%'
  },
  logoImage:{
    width: this._device_width,
    height: 180,
    margin: 10,
  },
  container: {
    justifyContent: 'center',
    padding: 20
  },
  input:{
    height: 40,
    backgroundColor: '#00ACED',
    marginBottom: 10,
    padding: 10,
    color: '#fff'
  },
  buttonContainer:{
    backgroundColor: '#2980b6',
    paddingVertical: 15
  },
  buttonText:{
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700'
  }
})
