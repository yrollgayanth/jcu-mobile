/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react'
import EventEmitter from "react-native-eventemitter"
import {DRAWER_MENU,EVENT, COLOR, SUPPORT_EVENTS, TAB_MANAGER_PREFS} from '../../Constants/Constants'
import UIUtils from "../../Utils/UIUtils"
import BrandingEngine from '../../Utils/BrandingEngine'
import Branding from "../../Utils/Branding"
import WebEngine from "../../Utils/WebEngine"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils'
import {Platform, StyleSheet, Text, View, ActivityIndicator, RefreshControl,  Image, ScrollView, Button, DeviceEventEmitter } from 'react-native'
import BaseViewComponent from '../../SharedComponents/BaseViewComponent'
import SupportListComponent from '../../Widgets/SupportListComponent/SupportListComponent'
import QuickLInksWidget from '../../Widgets/QuickLinks/QuickLInksWidget'
import BrowserUtils from '../../Utils/BrowserUtils';

export default class SupportComponent extends BaseViewComponent {

  _mounted = true

  constructor(props){
    super(props);

    this.state= {
      topBottomSafeArea:false,
      enableNavigationBar: true,
      loaded:false,
      loadingAttempted:false,
      supportData:null,
      isRefreshing:false,
      navigationBarConfig: {
        title: "Support",
        showHome: true,
        showEmail:false,
        showAlert: true,
        showMenu: true
      }
    }
    this.openExternal = this.openExternal.bind(this)
    this.openInternal = this.openInternal.bind(this)

    //on refresh method
    this.onRefresh = this.onRefresh.bind(this)
  }

  async componentWillMount(){
    this._mounted = true
    this.initiateSupportFetch()

    //get analytics
    UIUtils.sendAnalytics("Support")

    EventEmitter.addListener(SUPPORT_EVENTS.OPEN_INTERNAL, this.openInternal);
    EventEmitter.addListener(SUPPORT_EVENTS.OPEN_EXTERNAL,this.openExternal);
  }

  componentWillUnmount(){
    this._mounted = false
    EventEmitter.removeListener(SUPPORT_EVENTS.OPEN_EXTERNAL,this.openExternal);
    EventEmitter.removeListener(SUPPORT_EVENTS.OPEN_INTERNAL,this.openInternal);
  }

  openExternal(e){
    BrowserUtils.openBrowser(e.pageTitle,e.webLink);
    // if(Platform.OS =='ios'){
    //       BrowserUtils.openBrowser(e.pageTitle,e.webLink);
    //     }else{
    //       this.props.navigate('InAppWeb', {webLink: e.webLink, pageTitle:e.pageTitle})
    //     }
  }

  openInternal(e){}

  onRefresh(){
    this.setState({isRefreshing: true})
    this.initiateSupportFetch()
  }

  //initiate suport data
  async initiateSupportFetch(){
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""
    supportData = []

    this.setState({loaded:false, loadingAttempted:false})

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    //verify if the token needs to be updated
    if(shouldUpdateToken){
        await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
          if(values[0] === false){
            this.setFetchStatus(false, true, supportData, false)
          }else{
            newUserToken = values[0]
            LocalStorageManager.saveAuthTokens(newUserToken,()=>{
              LocalStorageManager.getSavedAuthTokens((savedValues)=>{
                ConfigUtils.id_token = savedValues.id_token
                this.fetchSupportData()
              },(error)=>{
                this.setFetchStatus(false, true, supportData, false)
              })
            }),(error)=>{
              this.setFetchStatus(false, true, supportData, false)
            }
          }
        })
    }else{
      this.fetchSupportData()
    }

  }

  async fetchSupportData(){
    supportData = []
    this.setState({loaded:false, loadingAttempted:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.fetchSupportData2()]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setFetchStatus(false, true, supportData, false)
      }else{
        supportData = jsonContent
        this.setFetchStatus(true, false, supportData, false)
      }
    })
  }

  //set fetch status
  setFetchStatus(loadedStatus, attemptedStatus, supportData, refreshStatus){
    if(this._mounted){
      this.setState({
        loaded     : loadedStatus,
        loadingAttempted : attemptedStatus,
        supportData: supportData,
        isRefreshing:refreshStatus
      })
    }
  }

  componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.SUPPORT_ITEM});
  }

  renderTopUnsafeArea(){
    return (<View style={styles.iphoneXTopSpace}></View>);
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  //failed view
  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.initiateSupportFetch()}
      title="Try again">
      </Button>
      </View>
    )
  }

  renderSupportList(){

    if( typeof this.state.supportData === 'undefined' || this.state.supportData === null || this.state.supportData.length <=0 ){
      return(
        <View style={styles.loadFailedContainer}>
        <Text>No support data found</Text>
        <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
        <Button onPress={() => this.initiateSupportFetch()}
        title="Try again">
        </Button>
        </View>
      )
    }

    return(
      <View style={[{flex:1}]}>
      <ScrollView
        refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
        }>
      {this.state.supportData.map((prop, key) => {
          return (
            <SupportListComponent supportItems={this.state.supportData[key]} sectionIndex={key} key={key}/>
          );
      })}
      </ScrollView>
      </View>
    )
  }

  renderView() {
    //load failed
    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    //success load
    return (
      this.renderSupportList()
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorImage:{
      height:150,
      width:150
  }
});
