import React, { Component } from 'react';
import {COLOR} from '../../Constants/Constants';
import UIUtil from '../../Utils/UIUtils';
import Branding from '../../Utils/Branding';
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


export default class TabbarItemComponent extends Component<{}> {

  _branding = Branding.sharedInstance

  constructor(prop){
    super(prop);
  }

  render() {

    if(!this.props.itemModel.selected){
      return (
          <LinearGradient colors={this._branding.getTabBarNormalGradients()}
                          style={styles.highlightBg} >
            <Image style={styles.icon} source={this.props.itemModel.icon}></Image>
            <Text style={[styles.titleSelected, {fontWeight:this._branding.getTabBarFontWeight()}, {color:this._branding.getTabBarNormalFontColor()} ]}>{this.props.itemModel.title}</Text>
          </LinearGradient>
      );
    }else{//tapped
      return (
          <LinearGradient colors={this._branding.getTabBarTappedGradients()} style={styles.container}>
            <Image style={styles.icon} source={this.props.itemModel.selectedIcon}></Image>
            <Text style={[styles.title, {fontWeight:this._branding.getTabBarFontWeight()}, {color:this._branding.getTabBarTappedFontColor()} ]}>{this.props.itemModel.title}</Text>
          </LinearGradient>
      );
    }
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection:'column',
    alignItems:'center'
  },
  highlightBg:{
    flex: 1,
    flexDirection:'column',
    alignItems:'center'
  },
  containerSelected: {
    flex: 1,
    backgroundColor: 'grey',
    flexDirection:'column',
    alignItems:'center'
  },
  icon:{
    resizeMode:'contain',
    marginTop:UIUtil.size(5),
    height:UIUtil.size(25),
    width:UIUtil.size(25)
  },
  title:{
    marginTop:UIUtil.size(3),
    fontSize:UIUtil.size(10)
  },
  titleSelected:{
    color:'white',
    marginTop:UIUtil.size(3),
    fontSize:UIUtil.size(10)
  }

});
