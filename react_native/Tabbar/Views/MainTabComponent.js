/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, TouchableHighlight, DeviceEventEmitter, Alert } from 'react-native';
import EventEmitter from "react-native-eventemitter";
import UIUtils from '../../Utils/UIUtils';
import { COLOR,EVENT,DRAWER_MENU } from '../../Constants/Constants';
import TabbarItemComponent from './TabbarItemComponent';
import MyStudyComponent from '../../MyStudy/Views/MyStudyComponent';
import MyToolsComponent from '../../MyTools/Views/MyToolsComponent';
import SupportComponent from '../../Support/Views/SupportComponent';
import UniLifeComponent from '../../UniLife/Views/UniLifeComponent';
import StepProcessComponent from '../../StepProcess/Views/StepProcessComponent';
import HomeComponent from '../../Home/Views/HomeComponent';
import NotificationsComponent from '../../Notifications/Views/NotificationsComponent';
import AppFeecbackComponent from '../../AppFeedback/Views/AppFeecbackComponent'
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import EnrolmentStepDetailsComponent from '../../Enrolment/Views/EnrolmentStepDetailsComponent';
import EnrolmentListComponent from '../../Enrolment/Views/EnrolmentListComponent';

var tabOptions = require('../Models/TabOptions.json');

export default class MainTabComponent extends BaseViewComponent {

  static navigationOptions = {

  };

  constructor(props){
    super(props);
    this.state = {
      leftRightSafeArea:false,
      selectedTab: this.checkFirstPage(),
      selectedCourseID:UIUtils.COURSE_ID
    };

    this.goHome = this.goHome.bind(this)
    this.checkFirstPage = this.checkFirstPage.bind(this)
    this.openEnrolmentFromInternalLink = this.openEnrolmentFromInternalLink.bind(this)
  }

  checkFirstPage(){

    if(UIUtils.GO_HOME_FIRST){
      return tabOptions.HOME
    }else{
      if(UIUtils.SINGLE_OFFER){
        return tabOptions.STEP_PROCESS
      }else{
        return tabOptions.ENROLMENT_LIST
      }
    }

  }

  goHome(){
    this.setState({selectedTab: tabOptions.HOME});
  };

  //go to enrolment
  goEnrolmentProgress(courseID){
    this.setState({selectedCourseID:courseID})
    this.setState({selectedTab:tabOptions.STEP_PROCESS})
  }


  openEnrolmentFromInternalLink(){
    if(UIUtils.SINGLE_OFFER){
      this.setState({selectedTab:tabOptions.STEP_PROCESS})

    }else{
      this.setState({selectedTab:tabOptions.ENROLMENT_LIST})
    }
  }

  componentWillMount(){

    EventEmitter.on(EVENT.OPEN_ENROLMENT, this.openEnrolmentFromInternalLink);

    //EventEmitter to go to Home
    //EventEmitter.on(EVENT.GO_HOME, () =>  this.setState({selectedTab: tabOptions.HOME}));
    DeviceEventEmitter.addListener(EVENT.GO_HOME, (e) => {
      this.setState({selectedTab: tabOptions.HOME})
    })

    //EventEmitter to go to step process
    EventEmitter.on(EVENT.GO_STEP_PROCESS, () =>  this.setState({selectedTab: tabOptions.STEP_PROCESS}));


    //EventEmitter to go notification page
    //EventEmitter.on(EVENT.GO_NOTIFICATIONS, () =>  this.setState({selectedTab: tabOptions.NOTIFICATIONS}));
    DeviceEventEmitter.addListener(EVENT.GO_NOTIFICATIONS, (e) => {
      this.setState({selectedTab: tabOptions.NOTIFICATIONS})
    })

    EventEmitter.on(EVENT.GO_FROM_SIDE_MENU, (eventData) =>  {
      console.log(eventData.toOpen)

      if(eventData.toOpen===DRAWER_MENU.ENROLL_ITEM){
        this.setState({selectedTab: tabOptions.STEP_PROCESS});
      }else if(eventData.toOpen===DRAWER_MENU.HOME_ITEM){
        this.setState({selectedTab: tabOptions.HOME});
      }else if(eventData.toOpen===DRAWER_MENU.MY_STUDY_ITEM){
        this.setState({selectedTab: tabOptions.MY_STUDY});
      }else if(eventData.toOpen===DRAWER_MENU.MY_TOOLS_ITEM){
        this.setState({selectedTab: tabOptions.MY_TOOLS});
      }else if(eventData.toOpen===DRAWER_MENU.UNI_LIFE_ITEM){
        this.setState({selectedTab: tabOptions.UNI_LIFE});
      }else if(eventData.toOpen===DRAWER_MENU.SUPPORT_ITEM){
        this.setState({selectedTab: tabOptions.SUPPORT});
      }else if(eventData.toOpen===DRAWER_MENU.NOTIFICATION_ITEM){
        this.setState({selectedTab: tabOptions.NOTIFICATIONS});
      }else if(eventData.toOpen===DRAWER_MENU.FEEDBACK_ITEM){
        this.setState({selectedTab: tabOptions.APP_FEEDBACK})
      }else if(eventData.toOpen===DRAWER_MENU.OPEN_DRAWER_PROFILE){
        this.props.navigation.navigate("Profile")
      }
    });
  }

  componentWillUnmount(){
    EventEmitter.removeAllListeners(EVENT.GO_HOME);
    EventEmitter.removeAllListeners(EVENT.GO_STEP_PROCESS);
    EventEmitter.removeAllListeners(EVENT.GO_NOTIFICATIONS);
    EventEmitter.removeListener(EVENT.OPEN_ENROLMENT,this.openEnrolmentFromInternalLink)
  }

  renderContent(){

    if(this.state.selectedTab === tabOptions.MY_STUDY){
      return (
        <MyStudyComponent navigate={this.props.navigation.navigate}></MyStudyComponent>
      );
    }else if(this.state.selectedTab === tabOptions.MY_TOOLS){
      return (
        <MyToolsComponent navigate={this.props.navigation.navigate}></MyToolsComponent>
      );
    }else if(this.state.selectedTab === tabOptions.UNI_LIFE){
      return (
        <UniLifeComponent navigate={this.props.navigation.navigate}></UniLifeComponent>
      );

    }else if(this.state.selectedTab === tabOptions.SUPPORT){
      return (
        <SupportComponent navigate={this.props.navigation.navigate}></SupportComponent>
      );
    }else if(this.state.selectedTab === tabOptions.HOME) {
      return (
        <HomeComponent navigate={this.props.navigation.navigate} goEnrolmentProgress={this.goEnrolmentProgress.bind(this)}></HomeComponent>
      );
    }else if(this.state.selectedTab === tabOptions.ENROLMENT_LIST){

      return(<EnrolmentListComponent navigate={this.props.navigation.navigate} goEnrolmentProgress={this.goEnrolmentProgress.bind(this)}></EnrolmentListComponent>);

    }else if(this.state.selectedTab === tabOptions.STEP_PROCESS){
      if(this.state.selectedCourseID==null||this.state.selectedCourseID==""){
          return
      }
      return (
        <EnrolmentStepDetailsComponent navigate={this.props.navigation.navigate} courseID={this.state.selectedCourseID}></EnrolmentStepDetailsComponent>
      );

    }else if(this.state.selectedTab === tabOptions.NOTIFICATIONS){
      return (
          <NotificationsComponent navigate={this.props.navigation.navigate}></NotificationsComponent>
      );

    }else if(this.state.selectedTab === tabOptions.APP_FEEDBACK){
      return(
        <AppFeecbackComponent navigate={this.props.navigation.navigate}></AppFeecbackComponent>
      )
    }
  }

  renderTabbarItems(){

    var tabTitles = [tabOptions.MY_STUDY,tabOptions.UNI_LIFE,tabOptions.SUPPORT,tabOptions.MY_TOOLS];
      var tabIcons = [
        require('../../../assets/img/tab_icon_my_study_new_grey.png'),
        require('../../../assets/img/tab_icon_uni_life_grey.png'),
        require('../../../assets/img/tab_icon_support_new_grey.png'),
        require('../../../assets/img/tab_icon_my_tools_grey.png')
      ];
      var tabSelectedIcons = [
        require('../../../assets/img/tab_icon_my_study_new_white.png'),
        require('../../../assets/img/tab_icon_uni_life_white.png'),
        require('../../../assets/img/tab_icon_support_new_white.png'),
        require('../../../assets/img/tab_icon_my_tools_white.png')
      ];

      var tabItems = [];
      for(var i=0;i<tabTitles.length;i++){

        var selected = tabTitles[i]===this.state.selectedTab?true:false;

        var itemModel = {title:tabTitles[i],
          icon:tabIcons[i],
          selectedIcon:tabSelectedIcons[i],
          selected:selected
        };
        tabItems.push(this.renderTabbarItem(itemModel));
      }
      return tabItems;
    }

    renderTabbarItem(itemModel){

      return (
        <TouchableHighlight key={itemModel.title} style={styles.tabbarItemWrapper}
                            underlayColor="transparent"
                            onPress={() =>  this.setState({selectedTab: itemModel.title})}
        >
          <TabbarItemComponent
            itemModel={itemModel}>
          </TabbarItemComponent>
        </TouchableHighlight>
      );

    }

    renderBottomTabbar(){

      if(this.state.selectedTab == tabOptions.STEP_PROCESS||this.state.selectedTab == tabOptions.ENROLMENT_LIST){
        return
      }

      return(<View style={styles.bottomTabbarWrapper}>
        {this.renderTabbarItems()}
      </View>)

    }


    renderView(){
      return (
          <View style={styles.mainContainer}>
            {this.renderContent()}
            {this.renderBottomTabbar()}
          </View>
      );
    }


    // render() {
    //   return (
    //     <View style={styles.mainContainer}>
    //     {this.renderContent()}
    //     <View style={styles.bottomTabbarWrapper}>
    //     {this.renderTabbarItems()}
    //     </View>
    //     </View>
    //   );
    // }
  }


const styles = StyleSheet.create({

    mainContainer: {
      flex: 1
      // height:300
    },
    contentContainer:{
      flex:1
    },
    bottomTabbarWrapper:{
      flexDirection:'row',
      borderTopColor:COLOR.LINE_GREY,
      // borderWidth:1,
      height:UIUtils.size(50),
      backgroundColor:'white',
    },
    tabbarItemWrapper:{
      flex:1
    }
});
