/**
* Created by Yrol on 17/03/18.
*/
import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, Button, TouchableHighlight, ActivityIndicator, ScrollView } from 'react-native'
import GreyDividerComponent from '../../SharedComponents/GreyDividerComponent'
import VerticalMarginComponent from '../../SharedComponents/VerticalMarginComponent'
import BrandingEngine from '../../Utils/BrandingEngine'
import Branding from "../../Utils/Branding"
import WebEngine from "../../Utils/WebEngine"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils'
import BrowserUtils from '../../Utils/BrowserUtils'
import UIUtils from '../../Utils/UIUtils'
import Moment from 'moment'

export default class SeeAllNewsComponent extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      loaded:false,
      loadingAttempted:false,
      myNewsData:null,
    }
  }

  componentWillMount(){
    this.initiateFetchAllNews()
  }

  componentWillUnmount(){}


  async initiateFetchAllNews(){
    var myNewsArr = []
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""

    this.setState({loaded:false, loadingAttempted:false})

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    if(shouldUpdateToken){
      await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
        if(values[0] === false){
          this.setFetchStatus(false, true, supportData, false)
        }else{
          newUserToken = values[0]
          LocalStorageManager.saveAuthTokens(newUserToken,()=>{
            LocalStorageManager.getSavedAuthTokens((savedValues)=>{
              ConfigUtils.id_token = savedValues.id_token
              this.fetchAllNews()
            },(error)=>{
              this.setFetchStatus(false, true, myNewsArr)
            })
          }),(error)=>{
            this.setFetchStatus(false, true, myNewsArr)
          }
        }
      })
    }else{
      this.fetchAllNews()
    }
  }

  async fetchAllNews(){
    var myNewsArr = []
    this.setState({loaded:false, loadingAttempted:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.fetchAllNews()]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setFetchStatus(false, true, myNewsArr)
      }else{
        myNewsArr = jsonContent
        this.setFetchStatus(true, false, myNewsArr)
      }
    })
  }

  setFetchStatus(loadedStatus, attemptedStatus, newsData){
    this.setState({
      loaded     : loadedStatus,
      loadingAttempted : attemptedStatus,
      myNewsData: newsData
    })
  }

  openNewslink(title, webLink){
    BrowserUtils.openBrowser(title, webLink)
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  //render news data
  renderNewsData(){
    if( typeof this.state.myNewsData === 'undefined' || this.state.myNewsData === null || this.state.myNewsData.length <=0 ){
      this.renderFailedView("No news data found")
    }
    var blocks = []
    let newsData = this.state.myNewsData
    for(var i =0; i<newsData.length; i++){
      let currentIndex = i
      blocks.push(
        <TouchableHighlight onPress={()=>this.openNewslink(newsData[currentIndex].title, newsData[currentIndex].link)} key={currentIndex} style={{height:UIUtils.size(90),    justifyContent: 'center', alignItems: 'center', width:UIUtils.getScreenWidth()}}>
          <View style={{flexDirection: 'row'}}>
            <Image source={{uri: newsData[currentIndex].img_url}} style={styles.imageBlock} />
            <View style={{flex: 1}}>
              <Text style={styles.mainInfo}>{newsData[currentIndex].title}</Text>
              <Text style={styles.subInfo}></Text>
              <Text style={styles.subInfo}>{Moment(newsData.date).format('ddd, DD MMM YYYY')}</Text>
            </View>
            <GreyDividerComponent></GreyDividerComponent>
          </View>
        </TouchableHighlight>
      )
    }
    return blocks
  }

  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.initiateFetchAllNews()}
      title="Try again">
      </Button>
      </View>
    )
  }

  render(){
    //load failed
    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    //success load
    return (
      <ScrollView>
      {this.renderNewsData()}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  imageBlock: {
    width: 60,
    height: 60,
    marginLeft:10,
    resizeMode:'cover',
    backgroundColor: 'gray',
    justifyContent: 'flex-end',
  },
  mainInfo:{
    fontWeight: 'bold',
    paddingLeft: 15,
    paddingRight: 20
  },
  subInfo:{
    color: 'gray',
    paddingLeft: 15,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorImage:{
      height:150,
      width:150
  },
  newsContainer:{
    width:"100%",
    padding:10,
    height:40,
    backgroundColor:"white"
  },
});
