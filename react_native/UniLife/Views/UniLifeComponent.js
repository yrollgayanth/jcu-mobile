/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import {Platform,StyleSheet, Text, View, ActivityIndicator, RefreshControl, ScrollView, Image, Button} from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import EventEmitter from "react-native-eventemitter";
import {DRAWER_MENU,EVENT, UNI_LIFE_EVENTS} from '../../Constants/Constants';
import UIUtils from "../../Utils/UIUtils"
import BrandingEngine from '../../Utils/BrandingEngine'
import Branding from "../../Utils/Branding"
import WebEngine from "../../Utils/WebEngine"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils'
import UniLifeListComponent from '../../Widgets/UniLifeListComponent/UniLifeListComponent'
import BrowserUtils from '../../Utils/BrowserUtils';

export default class UniLifeComponent extends BaseViewComponent {

  _mounted = true

  constructor(props){
    super(props);

    this.state= {
      loaded:false,
      loadingAttempted:false,
      uniLifeData:null,
      topBottomSafeArea:false,
      enableNavigationBar: true,
      isRefreshing:false,
      navigationBarConfig: {
        title: "Uni Life",
        showEmail:false,
        showHome: true,
        showAlert: true,
        showMenu: true
      }
    }
    //open external and internal links
    this.openExternal = this.openExternal.bind(this)
    this.openInternal = this.openInternal.bind(this)

    //on refresh method
    this.onRefresh = this.onRefresh.bind(this)
  }

  async componentWillMount(){
    this._mounted = true
    this.initiateUniLifeFetch()

    //get analytics
    UIUtils.sendAnalytics("Uni Life")

    EventEmitter.addListener(UNI_LIFE_EVENTS.OPEN_INTERNAL, this.openInternal);
    EventEmitter.addListener(UNI_LIFE_EVENTS.OPEN_EXTERNAL,this.openExternal);
  }

  openExternal(e){

    BrowserUtils.openBrowser(e.pageTitle,e.webLink);
        // if(Platform.OS =='ios'){
        //   BrowserUtils.openBrowser(e.pageTitle,e.webLink);
        // }else{
        //   this.props.navigate('InAppWeb', {webLink: e.webLink, pageTitle:e.pageTitle})
        // }

//        this.props.navigate('InAppWeb', {webLink: e.webLink, pageTitle:e.pageTitle})
  }

  openInternal(e){

  }



  componentWillUnmount(){
    this._mounted = false

    EventEmitter.removeListener(UNI_LIFE_EVENTS.OPEN_EXTERNAL,this.openExternal);
    EventEmitter.removeListener(UNI_LIFE_EVENTS.OPEN_INTERNAL,this.openInternal);
  }

  componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.UNI_LIFE_ITEM});
  }


  onRefresh(){
    this.setState({isRefreshing: true})
    this.initiateUniLifeFetch()
  }

  //initiate unilife fetch
  async initiateUniLifeFetch(){
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""
    var uniLifeDataArr = []

    this.setState({loaded:false, loadingAttempted:false})

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    //verify if the token needs to be updated
    if(shouldUpdateToken){
        await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
          if(values[0] === false){
            this.setFetchStatus(false, true, uniLifeDataArr, false)
          }else{
            newUserToken = values[0]
            LocalStorageManager.saveAuthTokens(newUserToken,()=>{
              LocalStorageManager.getSavedAuthTokens((savedValues)=>{
                ConfigUtils.id_token = savedValues.id_token
                this.fetchUniLifeData()
              },(error)=>{
                this.setFetchStatus(false, true, uniLifeDataArr, false)
              })
            }),(error)=>{
              this.setFetchStatus(false, true, uniLifeDataArr, false)
            }
          }
        })
    }else{
      this.fetchUniLifeData()
    }
  }

  async fetchUniLifeData(){
    var uniLifeDataArr = []
    this.setState({loaded:false, loadingAttempted:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.fetchUniLife()]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setFetchStatus(false, true, uniLifeDataArr, false)
      }else{
        uniLifeDataArr = jsonContent
        this.setFetchStatus(true, false, uniLifeDataArr, false)
      }
    })
  }

  setFetchStatus(loadedStatus, attemptedStatus, uniLifeData, refreshStatus){
    if(this._mounted){
      this.setState({
        loaded     : loadedStatus,
        loadingAttempted : attemptedStatus,
        uniLifeData: uniLifeData,
        isRefreshing:refreshStatus
      })
    }
  }


  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.initiateUniLifeFetch()}
      title="Try again">
      </Button>
      </View>
    )
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  renderUniLifeList(){
    if( typeof this.state.uniLifeData === 'undefined' || this.state.uniLifeData === null || this.state.uniLifeData.length <=0 ){
      return(
        <View style={styles.loadFailedContainer}>
        <Text>No study data found</Text>
        <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
        <Button onPress={() => this.initiateUniLifeFetch()}
        title="Try again">
        </Button>
        </View>
      )
    }

    return(
      <View style={[{flex:1}]}>
      <ScrollView
        refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
        }>
      {this.state.uniLifeData.map((prop, key) => {
          return (
            <UniLifeListComponent uniLifeItems={this.state.uniLifeData[key]} sectionIndex={key} key={key}/>
          );
      })}
      </ScrollView>
      </View>
    )
  }

  renderView() {

    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    return (
      this.renderUniLifeList()
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorImage:{
      height:150,
      width:150
  }
});
