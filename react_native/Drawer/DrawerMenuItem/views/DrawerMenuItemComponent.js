/**
 * Created by Sylvia on 29/11/17.
 */


import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,Image } from 'react-native';
import UIUtils from "../../../Utils/UIUtils";
import {FONT_SIZE,LAYOUT_SPACE,COLOR} from "../../../Constants/Constants";
import GreyDividerComponent from '../../../SharedComponents/GreyDividerComponent';

export default class DrawerMenuItemComponent extends Component<{}> {

    constructor(prop){
        super(prop);
    }

    render() {
        if(this.props.highlighted){
            return (
              <View style={styles.container}>
                  <View style={styles.menuContainerHighlighted}>
                      <Text style={styles.menuOptionTitleHighlighted} >{this.props.menuOptionModel.menuOptionName}</Text>
                  </View>
                  <GreyDividerComponent></GreyDividerComponent>
              </View>
            );
        }else{
            return (
                <View style={styles.container}>
                    <View style={styles.menuContainer}>
                        <Text style={styles.menuOptionTitle} >{this.props.menuOptionModel.menuOptionName}</Text>
                    </View>
                    <GreyDividerComponent></GreyDividerComponent>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
      flex:1
    },
    menuContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        flexDirection:'row',
        height:UIUtils.size(LAYOUT_SPACE.MENU_ITEM_HEIGHT),
    },
    menuContainerHighlighted: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: COLOR.NAV_GREY,
        flexDirection:'row',
        height:UIUtils.size(LAYOUT_SPACE.MENU_ITEM_HEIGHT),
    },
    menuOptionIcon:{
        marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
        width:UIUtils.size(LAYOUT_SPACE.STANDARD_ICON_SIZE_MID),
        height:UIUtils.size(LAYOUT_SPACE.STANDARD_ICON_SIZE_MID),
        resizeMode:'contain'
    },
    menuOptionTitle:{
        marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
        fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
        color:COLOR.TEXT_DESC_GREY,
        fontWeight:'bold'
    },
    menuOptionTitleHighlighted:{
        marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_SMALL),
        fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
        color:COLOR.APP_THEME_BLUE,
        fontWeight:'bold'
    }
});
