/**
* Created by Yrol on 05/02/18.
*/
import React, { Component } from 'react'
import { StyleSheet, Text, View, Button, Card, ScrollView, TouchableHighlight, Alert, DeviceEventEmitter,Image, AsyncStorage } from 'react-native';
import { LAYOUT_SPACE, DRAWER_MENU, EVENT } from '../Constants/Constants'
import VerticalMarginComponent from '../SharedComponents/VerticalMarginComponent'
import MenuItemComponent from '../SharedComponents/MenuItem/views/MenuItemComponent'
import CommonListView from '../SharedComponents/CommonListView'
import MenuItemModel from '../SharedComponents/MenuItem/models/MenuItemModel'
import EventEmitter from "react-native-eventemitter"
import {COLOR, DEFAULT_FILES, SAVED_FILES} from '../Constants/Constants'
import Branding from '../Utils/Branding'
import UIUtils from '../Utils/UIUtils'
import DrawerMenuItemModel from './DrawerMenuItem/models/DrawerMenuItemModel'
import DrawerMenuItemComponent from './DrawerMenuItem/views/DrawerMenuItemComponent'
import ConfigUtils from '../Utils/ConfigUtils'

const MenuItems = [
  {"MenuOptionName":DRAWER_MENU.ENROLL_ITEM},
  {"MenuOptionName":DRAWER_MENU.FEEDBACK_ITEM},
  {"MenuOptionName":DRAWER_MENU.ABOUT_US_ITEM},
  {"MenuOptionName":DRAWER_MENU.SETTINGS_ITEM},
  {"MenuOptionName":DRAWER_MENU.TERMS_ITEM}
]

export default class DrawerViewComponent extends Component<{}> {
  constructor(props){
    super(props);
    this.state = {
      isHorizontal:UIUtils.isDeviceInHorizontal(),
      // menuOptionsList:this.initMenuOptions(),//yrol
      // profileImage:'https://d3l077x3t9qdee.cloudfront.net/images/UAT/mobile/connexus/support/ic_gettting_started_at_uni.png',
      firstName:this.initFirstname(),
      lastName:this.initLastname(),
      profileImage:this.initProfileImage(),
      currentPosition:DRAWER_MENU.MY_STUDY_ITEM,
      menuOptionModels:this.initMenuOptionModels()//sylvia
    }
    this.renderMenuItem = this.renderMenuItem.bind(this);
    this.changeProfileImage = this.changeProfileImage.bind(this);
  }

  componentWillMount(){
    EventEmitter.on(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, (eventData) =>  {
      this.setState({currentPosition: eventData.nowAt});
      this.setState({menuOptionModels:this.initMenuOptionModels()});
    });

    //profile image event
    EventEmitter.addListener(EVENT.PROFILE_PIC_UPDATED, this.changeProfileImage)
  }

  //initialize profile image
  initProfileImage(){
    var branding = Branding.sharedInstance
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    if(branding.getProfileImage() === ""){
      return ""
    }
    return filePath
  }

  initFirstname(){
    var branding = Branding.sharedInstance
    return branding.getUserFirstname()
  }

  initLastname(){
    var branding = Branding.sharedInstance
    return branding.getUserLastname()
  }

  //changing profile picture
  changeProfileImage(e){
    var branding = Branding.sharedInstance
    this.setState({
      firstName:branding.getUserFirstname(),
      lastName:branding.getUserLastname(),
      profileImage:this.initProfileImage()
    })
  }

  componentWillUnmount(){
    EventEmitter.removeAllListeners(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU)
    EventEmitter.removeListener(EVENT.PROFILE_PIC_UPDATED, this.changeProfileImage)
  }

  onLayout(){
    this.setState({isHorizontal: UIUtils.isDeviceInHorizontal()});
  }

  initMenuOptionModels(){
    var menuOptionModels = [];
    var menuOptionNames = [
      DRAWER_MENU.HOME_ITEM,
      DRAWER_MENU.MY_STUDY_ITEM,
      DRAWER_MENU.UNI_LIFE_ITEM,
      DRAWER_MENU.SUPPORT_ITEM,
      DRAWER_MENU.MY_TOOLS_ITEM,
      DRAWER_MENU.NOTIFICATION_ITEM,
      DRAWER_MENU.FEEDBACK_ITEM,
      DRAWER_MENU.LOGOUT_ITEM
    ];

    var menuOptionIcons = [
    ];

    var menuOptionIconsHighlight = []

    for(var i=0;i<menuOptionNames.length;i++){

      var menuItemModel = new DrawerMenuItemModel(menuOptionNames[i]);
      menuItemModel.key = i;
      menuOptionModels.push(menuItemModel);
    }
    return menuOptionModels;
  }

  initMenuOptions(){
    var menuOptionIcomNames =
    [
      require("../../assets/img/ic_navbar_home.png"),
      require("../../assets/img/ic_navbar_home.png"),
      require("../../assets/img/ic_navbar_home.png"),
      require("../../assets/img/ic_navbar_home.png"),
    ]

    var menuOptions = [];
    for(var i=0;i<MenuItems.length;i++){
      var menuOption = MenuItems[i];
      var menuOptionModel = new MenuItemModel(menuOption.MenuOptionName,menuOptionIcomNames[i]);
      menuOptionModel.key = i;
      menuOptions.push(menuOptionModel);
    }
    return menuOptions;
  }

  //when the menu item is selected
  menuItemSelected(selectedItem){
    // alert(selectedItem);
  }


  //yrol's
  // renderSupportMenu(optionModel){
  //
  //   return (
  //     <TouchableHighlight underlayColor="transparent" onPress={()=>{DeviceEventEmitter.emit(EVENT.CLOSE_DRAWER, {selected:optionModel.menuOptionName});}}>
  //       <View>
  //         <VerticalMarginComponent></VerticalMarginComponent>
  //         <MenuItemComponent style={styles.menuItem} menuOptionModel={optionModel}></MenuItemComponent>
  //       </View>
  //     </TouchableHighlight>
  //   )
  //
  // }

  renderTopUnsafeArea(){

    if(this.state.topBottomSafeArea == null||this.state.topBottomSafeArea == true){
      if(UIUtils.isIPhoneX()&&!this.state.isHorizontal){
        return (<View style={styles.iphoneXTopSpace}></View>);
      }
    }
  }

  renderBottomUnsafeArea(){
    if((this.state.topBottomSafeArea == null||this.state.topBottomSafeArea == true)&&UIUtils.isIPhoneX()){
      if(!this.state.isHorizontal){
        return (<View style={styles.iphoneXBottomSpace}></View>);
      }else{
        return (<View style={styles.iphoneXBottomSpace}></View>);
      }
    }
  }

  renderLandscapeUnsafeArea(){
    if(this.state.leftRightSafeArea==null||this.state.leftRightSafeArea==true){
      if(this.state.isHorizontal&&UIUtils.isIPhoneX()){
        return(<View style={styles.unsafeArea}></View>);
      }
    }
  }

  renderProfileImage(){
    let randomString = Math.random().toString(36).slice(2)
    var defaultImage = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE + "?" + randomString
    var defaultImageAssets = require("../../assets/img/base64/default_profile_base64.json")
    if(this.state.profileImage !== ""){
      return(<Image style={styles.profileImage} source={{uri:'file://'+defaultImage}}></Image>);
    }else{
      return(<Image style={styles.profileImage} source={{uri:defaultImageAssets}}></Image>);
    }
  }

  renderTopProfilePanel(){
    return(
        <View style={styles.profilePanel}>
            <TouchableHighlight underlayColor="transparent" onPress={()=>{DeviceEventEmitter.emit(EVENT.CLOSE_DRAWER, {selected:DRAWER_MENU.OPEN_DRAWER_PROFILE})}}>{this.renderProfileImage()}</TouchableHighlight>
            <View style={styles.namePanel}>
              <Text style={styles.firtNameText}>{this.state.firstName}</Text>
              <Text style={styles.firtNameText}>{this.state.lastName}</Text>
            </View>
        </View>
    );
  }

  //yrol's
  // renderDummyMenuList(){
  //   return (
  //       <CommonListView models={this.state.menuOptionsList} renderItem={this.renderSupportMenu}></CommonListView>
  //   );
  // }


  renderMenuItem(menuOptionModel) {
    return (
        <TouchableHighlight underlayColor="transparent" onPress={()=>{DeviceEventEmitter.emit(EVENT.CLOSE_DRAWER, {selected:menuOptionModel.menuOptionName});}}>
          <View>
            <DrawerMenuItemComponent highlighted={this.state.currentPosition===menuOptionModel.menuOptionName} menuOptionModel={menuOptionModel}></DrawerMenuItemComponent>
          </View>
        </TouchableHighlight>
    );
  }


  renderMenuContent(){
    return(
        <ScrollView style={styles.scrollView} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
          {this.renderTopProfilePanel()}
          {/*{this.renderDummyMenuList()}*/}
          {this.renderMenuOptions()}
          {this.renderAppVersion()}
        </ScrollView>

    )
  }

  renderMenuOptions(){
    return (
      <CommonListView models={this.state.menuOptionModels} renderItem={this.renderMenuItem}></CommonListView>
    );
  }

  renderContentInSafeArea(){
    return(
        <View style={styles.contentInSafeArea}>
          {this.renderLandscapeUnsafeArea()}
          {this.renderMenuContent()}
        </View>
    );
  }


  renderAppVersion(){
    return(<View><Text style={styles.appVersionText}>App Version: {ConfigUtils.APP_VERSION}</Text></View>)
  }


  render(){
    return(
      <View style = {styles.mainContainer} onLayout={()=>this.onLayout()}>
        {this.renderTopUnsafeArea()}
        {this.renderContentInSafeArea()}
        {this.renderBottomUnsafeArea()}
      </View>
    );
  }
}

const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    // backgroundColor:'orange'
  },
  scrollView:{
    // paddingLeft:UIUtils.size(10),
    // paddingRight:UIUtils.size(10),
  },
  contentInSafeArea:{
    flexDirection:'row',
    flex:1
  },
  unsafeArea:{
    width:44,
    height:60
  },
  iphoneXTopSpace:{
    backgroundColor:COLOR.APP_THEME_BLUE,
    height:UIUtils.size(40)
  },
  iphoneXBottomSpace:{
    backgroundColor:'white',
    height:UIUtils.size(20)
  },
  profilePanel:{
    justifyContent:'center',
    alignItems:'center',
    height:UIUtils.size(180),
    backgroundColor:COLOR.APP_THEME_BLUE
  },
  profileImage:{
    resizeMode:'cover',
    borderRadius: UIUtils.size(50),
    borderColor:COLOR.LINE_GREY,
    borderWidth:UIUtils.size(0.5),
    height:UIUtils.size(100),
    width:UIUtils.size(100)
  },
  namePanel:{
    marginTop:UIUtils.size(15),
    flexDirection:'row'
  },
  firtNameText:{
    color:'white',
    marginRight:UIUtils.size(5),
    fontSize:UIUtils.size(16),
    fontWeight:'bold'
  },
  lastNameText:{
    color:'white',
    marginLeft:UIUtils.size(5),
    fontSize:UIUtils.size(16),
    fontWeight:'bold'
  },
  appVersionText:{
    flex:1,
    marginTop:UIUtils.size(5),
    textAlign:'center',
    fontStyle:'italic',
    color:'grey',
    fontSize:UIUtils.defaultMenuTextFontSize()
  }
});
