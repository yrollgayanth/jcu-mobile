/**
* Created by Yrol on 06/02/18.
*/

import React, { Component } from 'react';
import EventEmitter from "react-native-eventemitter";
import { View, Text, TextInput, TouchableOpacity,StyleSheet, Alert, DeviceEventEmitter, WebView, Platform, ActivityIndicator } from 'react-native';
import {EVENT} from '../../Constants/Constants'


export default class InAppWebComponent extends Component<{}> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.pageTitle,
      headerTitleStyle: { color: "#000", alignSelf: "center" }
    };
  };

  constructor(props) {
    super(props);
  }

  ActivityIndicatorLoadingView() {
    return (
      <ActivityIndicator
      color='#009688'
      size='large'
      style={styles.ActivityIndicatorStyle}
      />
    );
  }

  render() {
    const webLink = this.props.navigation.state.params.webLink
    return (
      <WebView
      style={styles.WebViewStyle}
      source={{uri: webLink.trim()}}
      javaScriptEnabled={true}
      domStorageEnabled={true}
      scalesPageToFit={true}
      renderLoading={this.ActivityIndicatorLoadingView}
      startInLoadingState={true}
      />
    );
  }
}

const styles = StyleSheet.create({
  WebViewStyle:{
    justifyContent: 'center',
    alignItems: 'center',
    flex:1,
  },

  ActivityIndicatorStyle:{
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'

  }
});
