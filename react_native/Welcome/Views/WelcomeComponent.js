/**
* Created by Yrol on 01/02/18.
*/
import React, { Component } from 'react'
import { NativeEventEmitter, NativeModules,Platform, StyleSheet, ImageBackground, Text, View, Button, Card, Image, TouchableOpacity, ScrollView } from 'react-native';
import  UIUtils from '../../Utils/UIUtils'
import ConfigUtils from '../../Utils/ConfigUtils'
import Branding from '../../Utils/Branding'
import { DEFAULT_FILES,EVENT } from '../../Constants/Constants'
import RNFS from'react-native-fs';
import Orientation from 'react-native-orientation'
import NativeBridge from '../../Utils/NativeBridge'
import BatchedBridge from "react-native/Libraries/BatchedBridge/BatchedBridge"
import BrowserUtils from '../../Utils/BrowserUtils'
import EventEmitter from "react-native-eventemitter"


var THIS

export default class WelcomeComponent extends Component<{}> {

  _device_width = UIUtils.getScreenWidth()
  _header_height = UIUtils.navBarheight()

  iosEventSubscription;

  constructor(props){
    super(props)

    THIS = this

    this.state={
      isHorizontal:false
    }
    const nativeBridge = new NativeBridge();
    BatchedBridge.registerCallableModule("JavaScriptVisibleToJava",nativeBridge);

    if(Platform.OS=='ios'){
      iosEventSubscription = new NativeEventEmitter(NativeModules.IOSEventManager)
    }

    this.checkQtacQuery = this.checkQtacQuery.bind(this)

  }

  //device orientation
  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      this.setState({isHorizontal: true})
    }else{
      this.setState({isHorizontal: false})
    }
  }

  componentWillMount(){
    //get initial orientation
    const initial = Orientation.getInitialOrientation()
    if (initial === 'PORTRAIT') {
      this.state.isHorizontal = false
    } else {
      this.state.isHorizontal = true
    }
  }

  getStartedFromQtac(queryData){
    // alert(queryData.query)
    console.log("getStartedFromQtac")
    console.log("query: ",queryData)

    // if(!UIUtils.NEW_STUDENT_OPENED){
    //   UIUtils.NEW_STUDENT_OPENED = true
    //   this.props.navigation.navigate('NewStudent', { goToLogin: this.goToLogin, query:queryData.query})
    // }

    setTimeout(function(){
      if(!UIUtils.NEW_STUDENT_OPENED){
        UIUtils.NEW_STUDENT_OPENED = true
        this.props.navigation.navigate('NewStudent', { goToLogin: this.goToLogin, query:queryData.query})
      }

    }, 200)
  }

  componentWillUnmount(){

    Orientation.removeOrientationListener(this._orientationDidChange);
    EventEmitter.removeListener(EVENT.FINISHED_GET_STARTED,this.goToLogin);

    if(Platform.OS=='ios'){
      iosEventSubscription.removeListener('OnRetrieveQtacParams',this.getStartedFromQtac);
    }else{
      EventEmitter.removeListener(EVENT.ON_RETRIEVE_QTAC_PARAMS,this.getStartedFromQtac);
    }

  }

  async checkQtacQuery(){

    if(Platform.OS =='ios'){
      try {

        var IOSQtacQueryManager = NativeModules.QtacQueryManager;
        var qtacQuery = await IOSQtacQueryManager.checkQtacQuery();
        console.log("raw qtac query:",qtacQuery)

        if(qtacQuery!=null && qtacQuery!=""){
          console.log("qtacQuery: ",qtacQuery)

          setTimeout(function(){
            if(!UIUtils.NEW_STUDENT_OPENED){
              UIUtils.NEW_STUDENT_OPENED = true
              THIS.props.navigation.navigate('NewStudent', { goToLogin: this.goToLogin, query:qtacQuery})
            }

          }, 200)



        }else{
          console.log("no qtac query")
        }

      } catch (e) {
        console.error(e);
      }

    }else{

      var AndroidQtacQueryManager = NativeModules.AndroidQtacQueryManager;

      AndroidQtacQueryManager.checkQtacQuery(
          (errorMsg) => {
            console.log("errorMsg: ",errorMsg)

          },
          (qtacQuery) => {
            if(qtacQuery!=null && qtacQuery!=""){
              // alert("qtacQuery: ",qtacQuery)
              console.log("qtacQuery: ",qtacQuery)
              // alert(qtacQuery)
              setTimeout(function(){
                if(!UIUtils.NEW_STUDENT_OPENED){
                  UIUtils.NEW_STUDENT_OPENED = true
                  THIS.props.navigation.navigate('NewStudent', { goToLogin: this.goToLogin, query:qtacQuery})
                }

              }, 200)

            }else{
              // alert("no qtac query")
              console.log("no qtac query")
            }
          }
      );

    }

  }



  componentDidMount(){

    //remove orientation listener
    Orientation.addOrientationListener(this._orientationDidChange);
    EventEmitter.addListener(EVENT.FINISHED_GET_STARTED, this.goToLogin);

    if(Platform.OS=='ios'){
      iosEventSubscription.addListener('OnRetrieveQtacParams', (queryData) => this.getStartedFromQtac(queryData))
    }else{
      EventEmitter.addListener(EVENT.ON_RETRIEVE_QTAC_PARAMS,  (queryData) => this.getStartedFromQtac(queryData));
    }


  }


  tappedOnCurrentStudent(){

    if(Platform.OS == "ios"){

      // this.props.navigation.navigate('SSOLogin', {name: 'Lucy'})
      var iosSSOManager = NativeModules.IOSSSOManager;
      iosSSOManager.openSSOPage(ConfigUtils.SSO_URL);

    }else{
      console.log("ConfigUtils.SSO_URL",ConfigUtils.SSO_URL);
      var androidSSOManager = NativeModules.AndroidSS0Manager;
      androidSSOManager.openSSOPage(ConfigUtils.SSO_URL);
    }

  }

  //go to login page on success "get started" - callback function
  goToLogin = data => {
    console.log("goToLogin")
    if(Platform.OS == "ios"){
      console.log(Platform.OS == "ios")
      // this.props.navigation.navigate('SSOLogin', {name: 'Lucy'})
      var iosSSOManager = NativeModules.IOSSSOManager;
      iosSSOManager.openSSOPage(ConfigUtils.SSO_URL);

    }else{
      console.log("ConfigUtils.SSO_URL",ConfigUtils.SSO_URL);
      var androidSSOManager = NativeModules.AndroidSS0Manager;
      androidSSOManager.openSSOPage(ConfigUtils.SSO_URL);
    }
  }



  tappedOnNewStudent(){
    //BrowserUtils.openBrowser("Future Student","https://jcu-dev.connexus.online/get-started/");
    if(!UIUtils.NEW_STUDENT_OPENED){
      UIUtils.NEW_STUDENT_OPENED = true
      this.props.navigation.navigate('NewStudent', { goToLogin: this.goToLogin})
    }
  }

  tappedOnFeatureStudent(){
    BrowserUtils.openBrowser("Future Student","https://www.jcu.edu.au/");
  }

  renderFormContainer(){
    var paddingTopPercentage = "0%"
    if(!this.state.isHorizontal){
      paddingTopPercentage = "30%"
    }else{
      paddingTopPercentage = "0%"
    }

    var logoImageUri = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
    var jcuLogoWhite = require('../../../assets/img/base64/jsu-logo-shield.json')
    return(
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop:paddingTopPercentage
      }}>
        <View style={{width: "70%", height: 100, marginBottom:20, backgroundColor: 'rgba(52, 52, 52, 0)'}}>
          <Image style={[styles.logoImage, {height:"100%"}]} source={{uri: jcuLogoWhite}}/>
        </View>

        <TouchableOpacity style={{width: "70%", flexDirection: 'column', padding:10,  height: 70, marginBottom:10, backgroundColor: 'rgba(255,255,255, 0.9)'}} onPress={() => this.tappedOnNewStudent()}>
          <View style={{flexDirection: 'row', flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text style={styles.buttonMainHeader}>{"Your Offer"}</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity style={{width: "70%", flexDirection: 'column', padding:10,  height: 70, marginBottom:10, backgroundColor: 'rgba(255,255,255, 0.9)'}} onPress={()=>this.tappedOnCurrentStudent()}>
          <View style={{flexDirection: 'row', flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text style={styles.buttonMainHeader}>{"Current Student"}</Text>
          </View>
        </TouchableOpacity>


        <TouchableOpacity style={{width: "70%", flexDirection: 'column', padding:10,  height: 70, marginBottom:10, backgroundColor: 'rgba(255,255,255, 0.9)'}} onPress={()=>this.tappedOnFeatureStudent()}>
            <View style={{flexDirection: 'row', flex:1, justifyContent:'center', alignItems:'center'}}>
              <Text style={styles.buttonMainHeader}>{"Future Student"}</Text>
            </View>
        </TouchableOpacity>


      </View>
    )
  }


  onLayout(){
    // var isInHorizontal = UIUtils.isDeviceInHorizontal();
    // this.setState({isHorizontal: isInHorizontal});
  }

  render() {
    this.checkQtacQuery()
    const {navigate} = this.props.navigation
    //var backgroundImageUri = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE
    var backgroundImageUri = require('../../../assets/img/base64/welcome_image.json')
    var logoImageUri = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
    return (

            <ImageBackground onLayout={()=>this.onLayout()}
            source={{uri: backgroundImageUri}}
            style={styles.imageBackground}>
              <ScrollView style={styles.mainContainer}>
              {this.renderFormContainer()}
              </ScrollView>
            </ImageBackground>

    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },

  buttonMainHeader:{
    fontSize:20,
    fontWeight:"bold",
    color:'#000'
  },

  buttonSubHeader:{
    color:'white'
  },

  imageBackground:{
    width: '100%',
    height: '100%'
  },
  logoImage:{
    width: this._device_width,
    resizeMode:'contain'
  },
});
