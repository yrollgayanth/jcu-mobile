/**
* Created by Yrol on 01/02/18.
*/
import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableHighlight } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import CommonListView from '../../SharedComponents/CommonListView';
import MenuItemModel from '../../SharedComponents/MenuItem/models/MenuItemModel';
import { LAYOUT_SPACE } from '../../Constants/Constants';
import VerticalMarginComponent from '../../SharedComponents/VerticalMarginComponent';
import MenuItemComponent from '../../SharedComponents/MenuItem/views/MenuItemComponent';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import EventEmitter from "react-native-eventemitter";
import {DRAWER_MENU,EVENT} from '../../Constants/Constants';

//const labels = ["Cart","Delivery Address","Order Summary","Payment Method","Track",];
const labels = ["Cart cart","Cart cart","Cart cart","Cart cart","Cart cart","Cart cart","Cart cart","Cart cart","Cart cart","Cart cart"];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: 'transparent',
  labelSize: 13,
  currentStepLabelColor: 'transparent'
}

const SupportOptions = [
  {"MenuOptionName":"My Uni emails"},
  {"MenuOptionName":"News/replace Student id card"},
  {"MenuOptionName":"LMS"},
  {"MenuOptionName":"Campus maps"},
  {"MenuOptionName":"Parking &public transport"},
  {"MenuOptionName":"Academic calendar"},
  {"MenuOptionName":"My Profile"},
  {"MenuOptionName":"Terms & conditions"},
  {"MenuOptionName":"Parking &public transport"},
  {"MenuOptionName":"Academic calendar"},
  {"MenuOptionName":"My Profile"},
  {"MenuOptionName":"Terms & conditions"},
  {"MenuOptionName":"My Uni emails"},
  {"MenuOptionName":"News/replace Student id card"},
  {"MenuOptionName":"LMS"},
  {"MenuOptionName":"Campus maps"},
];


export default class StepProcessComponent extends BaseViewComponent<{}> {

  constructor(props){
    super(props);
    this.state = {
      topBottomSafeArea:false,
      currentPosition: 0,
      supportOptionsList:this.initSupportOptions(),

      enableNavigationBar: true,
      navigationBarConfig: {
        title: "Enrolment",
        showHome: true,
        showAlert: true,
        showMenu: true
      }
    }
  }


  componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.ENROLL_ITEM});
  }

  //each listitem
  renderSupportItem(optionModel){
    return (
      <TouchableHighlight underlayColor="transparent" onPress={() => {}}>
      <View>
      <VerticalMarginComponent></VerticalMarginComponent>
      <MenuItemComponent style={styles.menuItem} menuOptionModel={optionModel}></MenuItemComponent>
      </View>
      </TouchableHighlight>
    )
  }

  initSupportOptions(){
    var menuOptionIcomNames =
    [
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png"),
      require("../../../assets/img/ic_navbar_home.png")
    ];

    var menuOptions = [];
    for(var i=0;i<SupportOptions.length;i++){
      var menuOption = SupportOptions[i];
      var menuOptionModel = new MenuItemModel(menuOption.MenuOptionName,menuOptionIcomNames[i]);
      menuOptionModel.key = i;
      menuOptions.push(menuOptionModel);
    }
    return menuOptions;
  }

  renderView() {
    return (
      <View style = {styles.mainContainer}>
        <View style={{height:'auto'}}>
          <ScrollView showsHorizontalScrollIndicator={false} horizontal={true} contentContainerStyle={{flexGrow : 1, justifyContent : 'center'}}>
              <StepIndicator
              customStyles={customStyles}
              currentPosition={this.state.currentPosition}
              stepCount={labels.length}
              labels={labels}
              />
          </ScrollView>
        </View>
        <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
          <CommonListView models={this.state.supportOptionsList} renderItem={this.renderSupportItem}></CommonListView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    padding:10,
  }
});
