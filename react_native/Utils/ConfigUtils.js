/**
* Created by Sylvia on 29/11/17.
*/
import { NativeModules, Platform } from 'react-native';

export default class ConfigUtils{

  static APP_ENV = "PROD";
  static BASE_URL = "prod_initiate";
  static SSO_URL = "";
  static NEW_STUDENT_URL = "";
  static APP_VERSION = "";
  static NON_QTAC_PARAMS = "fuseaction=reset"

  static id_token = "";


  static async checkPlatformInfo(){

    this.checkENV()
    this.checkAppVersion()

  }

  static async checkAppVersion(){

    if(Platform.OS =='ios'){
      try {

        var IOSBuildTargetManager = NativeModules.BuildTargetManager;
        var app_version = await IOSBuildTargetManager.checkAPPVersion();

        if(app_version){
          console.log("app_version",app_version)
          ConfigUtils.APP_VERSION = app_version
        }else{
          ConfigUtils.APP_VERSION = "Unavailable"
        }

      } catch (e) {
        console.error(e);
      }

    }else{

      var AndroidBuildTargetManager = NativeModules.AndroidBuildTargetManager

      AndroidBuildTargetManager.checkAppVersion(
          (errorMsg) => {
            ConfigUtils.APP_VERSION = "Unavailable"
          },
          (app_version) => {
            ConfigUtils.APP_VERSION = app_version
            // console.log("RN app_version:",app_version)
          }
      );

    }

  }



  //check env == UAT/DEV/RPOD
  static async checkENV() {

    if(Platform.OS =='ios'){
      try {

        var IOSBuildTargetManager = NativeModules.BuildTargetManager
        var env = await IOSBuildTargetManager.checkENV()
        if(env){
          ConfigUtils.APP_ENV = env;
          // console.log(ConfigUtils.APP_ENV);
          if(env=='PROD'){
            ConfigUtils.BASE_URL = "https://jcu-api.connexus.online/jcu-api/"
            //ConfigUtils.SSO_URL = "https://jcu-uatapi.connexus.online/ssologin"
            ConfigUtils.SSO_URL = "https://jcu.api.connexus.online/ssologin"
            ConfigUtils.NEW_STUDENT_URL = "https://secure.jcu.edu.au/app/gsportal/"
            //ConfigUtils.NEW_STUDENT_URL = "https://test.secure.jcu.edu.au/app/gsportal/?fuseaction=reset"
          }else if(env=='UAT'){
            ConfigUtils.BASE_URL = "https://jcu-uatapi.connexus.online/jcu-api/"
            ConfigUtils.SSO_URL = "https://jcu-uatapi.connexus.online/ssologin"
            ConfigUtils.NEW_STUDENT_URL = "https://test.secure.jcu.edu.au/app/gsportal/?"
            //ConfigUtils.NEW_STUDENT_URL = "https://test.secure.jcu.edu.au/app/gsportal/?fuseaction=reset"
          }else if(env=='DEV'){
            ConfigUtils.BASE_URL = "https://jcu-devapi.connexus.online/jcu-api/"
            ConfigUtils.SSO_URL = "https://jcu-devapi.connexus.online/ssologin"
            //ConfigUtils.NEW_STUDENT_URL = "https://jcu-dev.connexus.online/get-started"
            ConfigUtils.NEW_STUDENT_URL = "https://secure.jcu.edu.au/app/gsportal/"

          }
        }else{
          alert("Failed to get env")
        }
      } catch (e) {
        console.error(e)
      }
    }else{

      var AndroidBuildTargetManager = NativeModules.AndroidBuildTargetManager

      AndroidBuildTargetManager.checkENV(
          (errorMsg) => {
            alert("Failed to get env")
          },
          (env) => {
            ConfigUtils.APP_ENV = env
            // console.log(ConfigUtils.APP_ENV);
            if(env=='PROD'){
              ConfigUtils.BASE_URL = "https://jcu-uatapi.connexus.online/jcu-api/"
              //ConfigUtils.SSO_URL = "https://jcu-uatapi.connexus.online/ssologin"
              ConfigUtils.SSO_URL = "http://jcu.api.connexus.online/ssologin"
              ConfigUtils.NEW_STUDENT_URL = "https://secure.jcu.edu.au/app/gsportal/"
            }else if(env=='UAT'){
              ConfigUtils.BASE_URL = "https://jcu-uatapi.connexus.online/jcu-api/"
              ConfigUtils.SSO_URL = "https://jcu-uatapi.connexus.online/ssologin"
              ConfigUtils.NEW_STUDENT_URL = "https://test.secure.jcu.edu.au/app/gsportal/?"
            }else if(env=='DEV'){
              ConfigUtils.BASE_URL = "https://jcu-devapi.connexus.online/jcu-api/"
              ConfigUtils.SSO_URL = "https://jcu-devapi.connexus.online/ssologin"
              // ConfigUtils.NEW_STUDENT_URL = "https://jcu-dev.connexus.online/get-started"
              ConfigUtils.NEW_STUDENT_URL = "https://test.secure.jcu.edu.au/app/gsportal/?"
            }
          }
      );
    }
  }
}
