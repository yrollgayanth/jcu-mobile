/**
* Created by Sylvia on 29/11/17.
*/
import {AsyncStorage} from 'react-native';
import AuthTokens from '../Objects/AuthTokens';

export default class LocalStorageManager{


  static saveKeyValue(key,value,success,failure){
      AsyncStorage.setItem(key,value,(error)=>{
          if(error==null){
              success()
          }
          else{
              failure(error)
          }
      })
  }

  static getValueForKey(key,success,failure){
      AsyncStorage.getItem(key,(error,result)=>{
          if(error==null){
              success(result)
          }else{
              failure(error)
          }
      })

  }


  static saveAuthTokens(authToken,success,failure){

      AsyncStorage.setItem(AuthTokens.TOKEN_STORAGE_KEY,JSON.stringify(authToken),(error)=>{
          if(error==null){
              success()
          }
          else{
              failure(error)
          }
      })
  }

  static getSavedAuthTokens(success,failure){

      AsyncStorage.getItem(AuthTokens.TOKEN_STORAGE_KEY,(error,result)=>{
          if(error==null){
              if(result!=null){
                  result = new AuthTokens(JSON.parse(result));
              }
              success(result)
          }else{
              failure(error)
          }
      })
  }

  static deleteSavedAuthTokens(success,failure){
      AsyncStorage.removeItem(AuthTokens.TOKEN_STORAGE_KEY,(error)=>{
          if(error==null){
              success()
          }else{
              failure(error)
          }
      })

  }




}
