/**
* Created by Sylvia on 29/11/17.
*/
import {Alert, Dimensions, Platform, NativeModules, StatusBar} from 'react-native'
import EventEmitter from "react-native-eventemitter"
import { EVENT, PLATFORM, DEFAULT_FILES} from '../Constants/Constants'
import RNFetchBlob from 'react-native-fetch-blob'
import RNFS from 'react-native-fs'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import { Header } from 'react-navigation'
import LocalStorageManager from './LocalStorageManager'
import DeviceInfo from 'react-native-device-info'
import ConfigUtils from './ConfigUtils'
import Moment from 'moment'
import Branding from './Branding'
import 'moment-timezone'
import { Analytics, Hits as GAHits, Experiment as GAExperiment } from 'react-native-google-analytics'

export default class UIUtils{

  static GO_HOME_FIRST = true
  static SINGLE_OFFER = false
  static COURSE_ID = ""

  static SSO_OPENED = false
  static NEW_STUDENT_OPENED = false

  static checkDeviceModelNeedsExtraConfigForText(){

    if(DeviceInfo.getManufacturer()==='OPPO'){
        let oppoModels = ['A77','A57'];

      if(oppoModels.indexOf(DeviceInfo.getModel())!=-1){
        return true
      }else{
        return false
      }

    }else{
      return false
    }

  }

  static isIPhoneX(){
    if(Platform.OS=== 'ios'&&(UIUtils.getScreenHeight()==812||UIUtils.getScreenWidth()==812)){
      return true;
    }else{
      return false;
    }
  }

  static isDeviceInHorizontal(){
    if(UIUtils.getScreenWidth()>UIUtils.getScreenHeight()){
      return true;
    }else{
      return false;
    }
  }

  //get the status bar height
  static getStatusBarHeight(){
    return Platform.OS === "ios" ? getStatusBarHeight() : StatusBar.currentHeight
  }

  static navBarheight(){
    return Platform.OS === "ios" ? 64 : 56
  }

  static getScreenWidth(){
    return Dimensions.get('window').width;
  }


  static getScreenHeight(){
    return Dimensions.get('window').height;
  }

  static isIPadSize(){
    var width= UIUtils.getScreenWidth();
    var height = UIUtils.getScreenHeight();
    if(width>480&&height>480){
      return true;
    }else{
      return false;
    }
  }

  static size(size){
    if(UIUtils.isIPadSize()){
      return size*1.5;
    }else{
      return size;
    }
  }

  static defaultMenuTextFontSize(){
    return Platform.OS === "ios" ? UIUtils.size(17) : 14;
  }


  //get the document path
  static getDocumentPath(){
    return RNFS.DocumentDirectoryPath
  }

  static checkPlatform(){
    if (Platform.OS === PLATFORM.IOS ){
      return PLATFORM.IOS
    }
    return PLATFORM.ANDROID
  }

  static updateUnreadNoti(newNoti){
    EventEmitter.emit(EVENT.UPDATE_UNREAD_NOTI,newNoti);
  }

  //show generic alert
  static showGenericAlert(msg){
    Alert.alert('',
    msg,
      [
        {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ]
    )
  }

  //check expiry for token refresh
  static checkTokenExpiryForRefresh(){
    LocalStorageManager.getSavedAuthTokens((value)=>{
      if(typeof value !== 'undefined' && value !== null && value !== ''){
        let now =  Moment(new Date()).utc(Moment.tz.guess()).format()
        let formattedTokenExpiry =   Moment.unix(value.expires_at).utc(Moment.tz.guess()).format()
        if(now > formattedTokenExpiry){
          console.log("reached here")
          return false
        }
        return true
      }
      console.log("reached here 2")
      return false
    },(error)=>{//failed to check expiry
      console.log("reached here 3")
      return false
    })
  }

  //check token expiry
  static checkTokenExpiry(){
    LocalStorageManager.getSavedAuthTokens((value)=>{
      if(typeof value !== 'undefined' && value !== null && value !== ''){
        let now =  Moment(new Date()).utc(Moment.tz.guess()).format()
        let formattedTokenExpiry =   Moment.unix(value.expires_at).utc(Moment.tz.guess()).format()
        if(now > formattedTokenExpiry){
          EventEmitter.emit(EVENT.TOKEN_EXPIRE_LOGOUT)
        }
      }else{
        EventEmitter.emit(EVENT.TOKEN_EXPIRE_LOGOUT)
      }
    },(error)=>{//failed to check expiry
      EventEmitter.emit(EVENT.TOKEN_EXPIRE_LOGOUT)
    })
  }

  //check refresh token
  static checkRefreshTokenExpiry(value){
    if(typeof value !== 'undefined' && value !== null && value !== ''){
      let now =  Moment(new Date()).utc(Moment.tz.guess()).format()
      let formattedTokenExpiry =   Moment.unix(value.expires_at).utc(Moment.tz.guess()).format()
      if(now > formattedTokenExpiry){
        return true
      }
      return false
    }
    return false
  }

  //check whether the token need to refresh or not
  static isTokenNeedToBeRefreshed(tokenValue){
    let now =  Moment(new Date()).utc(Moment.tz.guess()).format()
    let formattedTokenExpiry =   Moment.unix(tokenValue.expires_at).utc(Moment.tz.guess()).format()
    if(now > formattedTokenExpiry){
      return true
    }
    return false
  }

  //bg color for section header
  static getSectionHeaderProperties(){
    var jsonFile = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BRANDING_JSON
    RNFS.readFile(jsonFile, 'utf8')
    .then((contents) => {
      var jsonContent = JSON.parse(contents);
      var sectionHeaderParams = jsonContent.brandSetting.header
      return sectionHeaderParams
    })
  }

  //handle google anlytics
  static sendAnalytics(screenName){
    if(ConfigUtils.APP_ENV === "PROD"){
      console.log("analytics sent")
      let branding = Branding.sharedInstance
      let clientId = DeviceInfo.getUniqueID();
      var ga = new Analytics('UA-118029631-1', clientId, 1, DeviceInfo.getUserAgent())
      let screenView = new GAHits.ScreenView(
        DeviceInfo.getApplicationName(),
        screenName,
        ConfigUtils.APP_VERSION,
        DeviceInfo.getBundleId()
      )
      ga.addDimension(1, branding.getStudentID())
      ga.send(screenView)
    }
  }
}
