/**
* Created by Yrol on 05/02/18.
*/

import UserDetailsObject from '../Objects/UserDetailsObject'


//in-memory data manager
export default class DataManager {

  //shared instance
  static sharedInstance = this.sharedInstance == null ? new DataManager() : this.sharedInstance

  userObject:UserDetailsObject;

  saveUserData(userData){
    this.userObject = userData;
  }


  getUserData(){

    return this.userObject;

  }




}
