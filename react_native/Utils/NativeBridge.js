import BatchedBridge from "react-native/Libraries/BatchedBridge/BatchedBridge";
import {EVENT} from '../Constants/Constants';
import LocalStorageManager from '../Utils/LocalStorageManager';
import AuthTokens from "../Objects/AuthTokens";
import EventEmitter from 'react-native-eventemitter';
import {DeviceEventEmitter} from 'react-native';


export default class NativeBridge{

  // onAndroidSSOSuccess(cookie){
  //
  //   var cookieData = decodeURIComponent(cookie).split(';');
  //   // console.log(cookie);
  //   var jcu_token = cookieData[0].split('=')[1];
  //   console.log("jcu_token",jcu_token);
  //
  //   // var jsonToken = JSON.parse(jcu_token);
  //   // console.log("jsonToken",jsonToken);
  //
  //
  //   // var authTokens = new AuthTokens(jsonToken);
  //   // console.log("authTokens",authTokens);
  //   //
  //   //
  //   //
  //   // LocalStorageManager.saveAuthTokens(authTokens,()=>{
  //   //
  //   //   DeviceEventEmitter.emit(EVENT.LOGIN_STATUS, {status:true});
  //   // },(error)=>{
  //   //   alert("failed to save");
  //   // })
  //
  // }


  onQuitWebpage(){
    EventEmitter.emit(EVENT.QUIT_WEBPAGE);
  }

  onAndroidSSOSuccess(jsonStr){

    // var cookieData = decodeURIComponent(cookie).split(';');
    // // console.log(cookie);
    // var jcu_token = cookieData[0].split('=')[1];
    // console.log("jcu_token",jcu_token);

    var jsonToken = JSON.parse(jsonStr);
    console.log("jsonToken",jsonToken);


    var authTokens = new AuthTokens(jsonToken);
    console.log("authTokens",authTokens);


    LocalStorageManager.saveAuthTokens(authTokens,()=>{

      DeviceEventEmitter.emit(EVENT.LOGIN_STATUS, {status:true});
    },(error)=>{
      alert("failed to save");
    })

  }

  onRetrieveQtacParams(query){

    EventEmitter.emit(EVENT.ON_RETRIEVE_QTAC_PARAMS, {query:query});

  }






}


const nativeBridge = new NativeBridge();
BatchedBridge.registerCallableModule("JavaScriptVisibleToJava",nativeBridge);