/**
* Created by Yrol on 12/02/18.
*/
import UIUtils from './UIUtils';
import { DEFAULT_FILES, COLOR } from '../Constants/Constants';

export default class Branding{

  //shared instance
  static sharedInstance = this.sharedInstance == null ? new Branding() : this.sharedInstance

  //header default
  _header_gradient = [COLOR.GRADIENT_DARK_BLUE, COLOR.GRADIENT_LIGHT_BLUE]
  _header_font_weight = "normal"
  _header_font_color = "#FFFFFF"

  //navigation defualt
  _navigation_gradient = [COLOR.NAV_GREY]
  _navigation_font_weight = "normal"
  _navigation_font_color = COLOR.APP_THEME_BLUE

  //tab bar tapped
  _tab_tapped_gradient = [COLOR.GRADIENT_DARK_BLUE, COLOR.GRADIENT_LIGHT_BLUE]
  _tab_tapped_font_color = "#FFFFFF"

  //tabbar common properties
  _tab_font_weight = "normal"

  //tabbar normal
  _tab_normal_gradient = [COLOR.WHITE]
  _tab_normal_font_color = "#FFFFFF"

  //profile image location
  _profileImage = ""

  //user first name
  _userFirstName = ""

  //user last name
  _userLastName = ""

  //student id
  _student_id = ""

  //quicklinks edit mode
  _quicklinks_edit_mode = false

  //*********** SET, GET, REMOVE HEADER colors ***********
  setHeaderColors(headerColor){
    this._header_gradient.push(headerColor)
  }

  getHeaderGradient(){
    return this._header_gradient
  }

  clearHeaderGradient(){
    this._header_gradient = []
  }

  //*********** SET, GET, REMOVE HEADER font weight ***********
  setHeaderFontWeight(fontWeight){
    this._header_font_weight = fontWeight
  }

  getHeaderFontWeight(){
    return this._header_font_weight
  }

  //*********** SET, GET, REMOVE HEADER font color ***********
  setHeaderFontColor(fontColor){
    this._header_font_color = fontColor
  }

  getHeaderFontColor(){
    return this._header_font_color
  }


  //*********** SET, GET, REMOVE NAVIGATION colors ***********
  setNavigationColors(color){
    this._navigation_gradient.push(color)
  }

  getNavigationColors(){
    return this._navigation_gradient
  }

  clearNavigationGradients(){
    this._navigation_gradient = []
  }

  //*********** set and get NAVIGATION font weight ***********
  setNavigationFontWeight(fontWeight){
    this._navigation_font_weight = fontWeight
  }

  getNavigationFontWeight(){
    return this._navigation_font_weight
  }

  //*********** set and get NAVIGATION font weight ***********
  setNavigationFontColor(fontColor){
    this._navigation_font_color = fontColor
  }

  getNavigationFontColor(){
    return this._navigation_font_color
  }

  //*********** set and get TABBAR Tapped font color ***********
  setTabBarTappedFontColor(fontColor){
    this._tab_tapped_font_color = fontColor
  }

  getTabBarTappedFontColor(){
    return this._tab_tapped_font_color
  }

  //*********** set, get and clear TABBAR Tapped background color ***********
  clearTabBarTappedGradients(){
    this._tab_tapped_gradient = []
  }

  setTabBarTappedGradients(color){
    this._tab_tapped_gradient.push(color)
  }

  getTabBarTappedGradients(){
    return this._tab_tapped_gradient
  }

  //*********** set and get TABBAR Tapped and untapped font weight ***********
  setTabBarFontWeight(fontWeight){
    this._tab_font_weight = fontWeight
  }

  getTabBarFontWeight(){
    return this._tab_font_weight
  }

  //*********** set and get TABBAR Normal font color ***********
  setTabBarNormalFontColor(fontColor){
    this._tab_normal_font_color = fontColor
  }

  getTabBarNormalFontColor(){
    return this._tab_normal_font_color
  }

  //*********** set, get and clear TABBAR normal background color ***********
  clearTabBarNormalGradients(){
    this._tab_normal_gradient = []
  }

  setTabBarNormalGradients(color){
    this._tab_normal_gradient.push(color)
  }

  getTabBarNormalGradients(){
    return this._tab_normal_gradient
  }

  //set and get profile image
  setProfileImage(imagePath){
    this._profileImage = imagePath
  }

  getProfileImage(){
    return this._profileImage
  }

  clearProfileImage(){
    this._profileImage = ""
  }

  //user firstname
  setUserFirstname(fname){
    this._userFirstName = fname
  }

  getUserFirstname(){
    return this._userFirstName
  }

  //user lastname
  setUserLastname(lname){
    this._userLastName = lname
  }

  getUserLastname(){
    return this._userLastName
  }

  //set and get student ID
  setStudentID(sid){
    this._student_id = sid
  }

  getStudentID(){
    return this._student_id
  }

  //set quicklinks  edit mode
  setQuicklinksEditMode(mode){
    this._quicklinks_edit_mode = mode
  }

  getQuicklinksEditMode(){
    return this._quicklinks_edit_mode
  }
}
