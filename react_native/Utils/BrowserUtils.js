/**
* Created by Sylvia on 29/11/17.
*/
import { NativeModules, Platform } from 'react-native';

export default class BrowserUtils{


  //check env == UAT/DEV/RPOD
  static openBrowser(pageTitle, url) {

    url = url.trim();
    if(Platform.OS =='ios'){

        var IOSBrowserManager = NativeModules.IOSBrowserManager;
        // IOSBrowserManager.openBrowser(pageTitle,url);

        IOSBrowserManager.openInAppBrowser(pageTitle,url);

    }else{
        //var AndroidBuildTargetManager = NativeModules.AndroidBuildTargetManager;
        console.log(url);
        var AndroidBrowserManager = NativeModules.AndroidBrowserManager;
        AndroidBrowserManager.openBrowser(pageTitle,url);
    }

  }

}
