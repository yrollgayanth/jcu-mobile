/**
* Created by Yrol on 05/02/18.
*/

import EventEmitter from "react-native-eventemitter";
import Parser from './Parser'
import Branding from './Branding'
import { WEB_REQUEST_STATUS } from '../Constants/Constants';
import ConfigUtils from './ConfigUtils';

export default class WebEngine{
  //shared instance
  static sharedInstance = this.sharedInstance == null ? new WebEngine() : this.sharedInstance
  _branding = Branding.sharedInstance

  async updateEnrolmentStep(bodyData){

    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/updateEnrolStepById"

    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify(bodyData)
    })
        .then((response) =>response.json())
        .catch((error) => false);
  }

  async fetchEnrolmentList(student_id){

    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getEnrolmentDetails"

    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify({"student_id":student_id})
    })
        .then((response) =>response.json())
        .catch((error) => false);
  }

  async fetchEnrolmentStepDetails(student_id,courseId){

    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getEnrolmentStepDetails"

    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify({"student_id":student_id,"courseId":courseId})
    })
        .then((response) =>response.json())
        .catch((error) => false);
  }

  //fetch all news
  async fetchAllNews(){
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getNews"
    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
    })
    .then((response) =>response.json())
    .catch((error) => false);
  }

  //SUPPORT DATA
  async fetchSupportData(url, token, studenID, pageTitle) {

    let parser = Parser.sharedInstance
    return fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify({
        student_id: studenID,
        page_title: pageTitle,
      })
    })
    .then((response) => response.json())
    .then((responseData) => parser.parseSupportJson(responseData))
    .catch((error) => false)
  }

  //submit app feedback
  async submitFeedback(feedbackJson){
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/feedback/"

    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: feedbackJson
    })
    .then((response) => response.json())
    .catch((error) => false);
  }

  async fetchSupportData2(){
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getPageLinksById"
    return fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'page_title':'Support',
        'student_id':this._branding.getStudentID(),
        "token":ConfigUtils.id_token
      }
    }).then((response) => response.json())
    .catch((error) => false)
  }

  //fetch branding data
  async fetchBrandingData(){
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    //var url = "https://jcu-devapi.connexus.online/jcu-api/getBrandingScheme"
    let url = base_url + "getBrandingScheme"
    console.log(base_url)
    return fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'token':ConfigUtils.id_token
      },
      body: JSON.stringify({
        client: "jcu",
      })
    })
    .then((response) => response.json())
    .catch((error) => false)
  }

  //fetching home content
  async fetchHomeContent(){
    //get base url
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let parser = Parser.sharedInstance
    //var url = "https://jcu-devapi.connexus.online/jcu-api/user/getHomeContent"
    var url = base_url + "user/getHomeContent"
    return fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'student_id':this._branding.getStudentID(),
        "token":ConfigUtils.id_token
      },
    }).then((response) => response.json())
    .then((responseData) => parser.parseHomeContent(responseData))
    .catch((error) => false)
  }

  //fetching the new refresh token
  async fetchNewRefreshToken(existingToken){
    this.getBaseUrl()

    var base_url = "https://jcu-devapi.connexus.online/"
    
    if(ConfigUtils.APP_ENV === 'UAT'){
      base_url = "https://jcu-uatapi.connexus.online/"
    }else if(ConfigUtils.APP_ENV === 'PROD'){
      base_url = "https://jcu-api.connexus.online/"
    }

    let url = base_url + "ssologin/refresh"
    var stringifiedTokenData = JSON.stringify({ token: existingToken })
    console.log('stringfied:' + stringifiedTokenData)
    console.log("refresh url:" + url)
    return fetch(url,{
      method:'POST',
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },body: stringifiedTokenData
    }).then((response) => response.json())
    .catch((error) => false)
  }


  //fetch my study objects
  async fetchMyStudy(){
    this.getBaseUrl()
    let parser = Parser.sharedInstance
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getPageLinksById"
    return fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'page_title':'My Study',
        'student_id':this._branding.getStudentID(),
        "token":ConfigUtils.id_token
      }
    }).then((response) => response.json())
    .catch((error) => false)
  }

  //fetch uni life
  async fetchUniLife(){
    this.getBaseUrl()
    let parser = Parser.sharedInstance
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getPageLinksById"
    return fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'page_title':'Uni Life',
        'student_id':this._branding.getStudentID(),
        "token":ConfigUtils.id_token
      }
    }).then((response) => response.json())
    .catch((error) => false)
  }

  //clean token on logout
  async logoutTokenClean(){
    this.getBaseUrl()
    var base_url = "https://jcu-devapi.connexus.online/"

    if(ConfigUtils.APP_ENV === 'UAT'){
      base_url = "https://jcu-uatapi.connexus.online/"
    }else if(ConfigUtils.APP_ENV === 'PROD'){
      base_url = "https://jcu-api.connexus.online/"
    }

    let url = base_url + "ssologin/logout?s=" + ConfigUtils.id_token
    return fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json'
      }
    }).then((response) => response.json())
    .catch((error) => false)
  }

  //fetch settings
  async fetchSettings(){
    this.getBaseUrl()
    let parser = Parser.sharedInstance
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getPageLinksById"
    return fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type':'application/json',
        'page_title':'Settings',
        'student_id':this._branding.getStudentID(),
        "token":ConfigUtils.id_token
      }
    }).then((response) => response.json())
    .catch((error) => false)
  }

  //upload profile image
  async uploadProfileImage(imageData){
    this.getBaseUrl()
    let parser = Parser.sharedInstance
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/updateUserProfile/"
    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: imageData
    })
    .then((response) => true)
    .catch((error) => false);
  }

  //fetch profile data
  async fetchProfileData(studentID){
    var studentData = {
      'student_id':studentID
    }
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getUserDetails"
    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify(studentData)
    })
    .then((response) => response.json())
    .catch((error) => false);
  }

  //fetch quick links
  async fetchQuicklinks(){
    var studentData = {
      'student_id':this._branding.getStudentID()
    }
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getQuickLinks"
    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify(studentData)
    })
    .then((response) => response.json())
    .catch((error) => false);
  }


  //re-order quicklink
  async reorderAddDelEditQuicklink(reorderedData){
    console.log("reorder data: " + JSON.stringify(reorderedData.quick_links))
    this.getBaseUrl()
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/addQuickLinks"
    return fetch(url, {
      method: "POST",
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        "token":ConfigUtils.id_token
      },
      body: JSON.stringify(reorderedData)
    })
    .then((response) => response.json())
    .catch((error) => false);
  }

  //getting the base URL depending upon build (DEV, PROD or UAT)
  async getBaseUrl(){
    await Promise.all([ConfigUtils.checkENV()]).then(values => {})
  }
}
