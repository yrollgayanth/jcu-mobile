/**
* Created by Yrol on 05/02/18.
*/

import EventObject from '../Objects/EventObject'

export default class Parser {

  //shared instance
  static sharedInstance = this.sharedInstance == null ? new Parser() : this.sharedInstance

  parseSupportJson(jsonContent) {
    var sectionContent = jsonContent, length = sectionContent.length, dataBlob = {}, sectionIDs = [],
    rowIDs = [], organization, items, itemLength, item, i, j, responseDataDump = {};

    for (i = 0; i < length; i++) {
      section = sectionContent[i];
      sectionIDs.push(section.section_id);
      dataBlob[section.section_id] = section.section;

      items = section.items;
      itemLength = items.length;

      rowIDs[i] = [];

      for(j = 0; j < itemLength; j++) {
        item = items[j]
        rowIDs[i].push(item.id);
        dataBlob[section.section_id + ':' + item.id] = item;
      }
    }
    responseDataDump["dataBlob"] = dataBlob
    responseDataDump["sectionIDs"] = sectionIDs
    responseDataDump["rowIDs"] = rowIDs
    return responseDataDump;
  }
  
  //parsing home content
  parseHomeContent(jsonContent){
    var responseDataDump = {}
    var announcementLength = jsonContent.news.length
    var announcemenObjArr = []
    var enrolmentData = []
    var quicklinkData = []

    //announcements
    if(announcementLength > 0){
      var announcementArr = jsonContent.news

      for(i = 0; i<announcementLength; i++){
        var announcementID = announcementArr[i]._id
        var announcementTitle = announcementArr[i].title
        var announcementText = ""
        var announcementUrl = announcementArr[i].link
        var announcementCreationDate = announcementArr[i].date
        var announcementStartDate = announcementArr[i].sortable_date
        var announcementExpiryDate = announcementArr[i].sortable_date
        var announcementImage = announcementArr[i].img_url
        var eventObj = new EventObject(announcementID, announcementTitle, announcementText, announcementUrl,
          announcementCreationDate, announcementStartDate, announcementExpiryDate, announcementImage)
        announcemenObjArr.push(eventObj)
      }
    }

    //undefine pass empty arrays above else pass the following and handle zero items from the app

    //enrolment data
    enrolmentData = jsonContent.enrolment_details
    quicklinkData = jsonContent.quick_links

    //dumping the above created data objects
    responseDataDump["brandingVersion"] = jsonContent.version //define branding version as zero if undefined
    responseDataDump["profileImage"] = jsonContent.profilepic
    responseDataDump["announcements"] = announcemenObjArr
    responseDataDump["enrolment"] = enrolmentData
    responseDataDump['quicklinks'] = quicklinkData
    responseDataDump['campusSecurityTel'] = jsonContent.campus_safety_number
    return responseDataDump
  }
}
