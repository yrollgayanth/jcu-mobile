/**
* Created by Yrol on 21/02/18.
*/

import {AsyncStorage, Number } from 'react-native'
import RNFS from'react-native-fs'
import { STORAGE_KEY, DEFAULT_FILES } from '../Constants/Constants'
import WebEngine from "./WebEngine"
import Branding from "./Branding"
import UIUtils from "./UIUtils"
import AuthTokens from '../Objects/AuthTokens';

export default class BrandingEngine{

  async updateOnlineBranding(latestBrndingVersion){
    var shouldUpdate = false
    try{
      const currentBranding = await AsyncStorage.getItem(STORAGE_KEY.CURRENT_BRANDING_VERSION)
      if(parseInt(currentBranding, 10) < parseInt(latestBrndingVersion, 10)){
        shouldUpdate = true
      }
    }catch(error){
      console.log(error.message)
      return false
    }

    //start updating data
    if(shouldUpdate){
      let webEngine = WebEngine.sharedInstance
      let jsonContent = null
      let logoImageUrl = null
      let backgroundImageUrl = null
      await Promise.all([webEngine.fetchBrandingData()]).then(values => {
        jsonContent = values[0]
        backgroundImageUrl = jsonContent.brandSetting.defaultSection.backgroundImage
        logoImageUrl = jsonContent.brandSetting.navigationSection.backgroundImage
      })

      if(jsonContent !== null && logoImageUrl !== null && backgroundImageUrl !== null){
        await Promise.all([
          this.saveOnlineImages(DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE, backgroundImageUrl),
          this.saveOnlineImages(DEFAULT_FILES.DEFAULT_LOGO_IMAGE, logoImageUrl),
          this.saveDefaultOnlineJsons(jsonContent),
          this.reInitiateBranding()
        ]).then(values => {
          if(values.indexOf(false) == -1){ //check if false returned by above operations
            console.log("save success")

            //save the branding version
            try{
              AsyncStorage.setItem(STORAGE_KEY.CURRENT_BRANDING_VERSION, latestBrndingVersion.toString());
            }catch(error){
              console.log(error.message)
            }
          }
        })
      }
    }
  }

  //update the shared instance file
  async reInitiateBranding(){
    var branding = Branding.sharedInstance
    var jsonFile = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BRANDING_JSON
    console.log(jsonFile)
      return RNFS.readFile(jsonFile, 'utf8')
      .then((contents) => {
        var jsonContent = JSON.parse(contents)

        //branding for headers in sections
        var sectionHeaderParams = jsonContent.brandSetting.headerBannerSection
        var bgLength = sectionHeaderParams.backgroundColour.length
        if(bgLength > 0){
          branding.clearHeaderGradient()
          for(var i = 0; i<bgLength; i++){
            branding.setHeaderColors(sectionHeaderParams.backgroundColour[i])
          }
        }

        //header text weight
        var headerFontWeight = sectionHeaderParams.fontWeight
        branding.setHeaderFontWeight(headerFontWeight)

        //header text color
        var headerFontColor = sectionHeaderParams.fontColour
        branding.setHeaderFontColor(headerFontColor)


        //navigation colors
        var navigationSection = jsonContent.brandSetting.navigationSection
        var navBgColorLength = navigationSection.backgroundColourMobile.length

        if(navBgColorLength > 0){
          branding.clearNavigationGradients()
          for(var i = 0; i<navBgColorLength; i++){
            branding.setNavigationColors(navigationSection.backgroundColourMobile[i])
          }
        }

        //navigation text weight
        var navigationFontWeight =  navigationSection.fontWeight
        branding.setNavigationFontWeight(navigationFontWeight)

        //navigation text color
        var navigationFontColor = navigationSection.fontColour
        branding.setNavigationFontColor(navigationFontColor)

        //tab bar
        var tabBarTapped = jsonContent.brandSetting.tabBarButtonSection.tapped
        var tabBarDefault = jsonContent.brandSetting.tabBarButtonSection.default
        var tabBarFontWeight = jsonContent.brandSetting.tabBarButtonSection.fontWeight

        //tab bar tapped font color
        var tabBarTappedFontColor = tabBarTapped.fontColour
        branding.setTabBarTappedFontColor(tabBarTappedFontColor)

        //tab bar tapped gradient
        var tabBarTappedGradient = tabBarTapped.backgroundColour
        var tabBarTappedGradientLength = tabBarTappedGradient.length
        if(tabBarTappedGradientLength > 0){
          branding.clearTabBarTappedGradients()
          for(var i = 0; i<tabBarTappedGradientLength; i++){
            branding.setTabBarTappedGradients(tabBarTappedGradient[i])
          }
        }

        //tab bar tapped font weight
        branding.setTabBarFontWeight(tabBarFontWeight)

        var tabBarNormalFontColor = tabBarDefault.fontColour
        branding.setTabBarNormalFontColor(tabBarNormalFontColor)


        //tab bar normal gradient
        var tabBarNormalGradient = tabBarDefault.backgroundColour
        var tabBarNormalGradientLength = tabBarNormalGradient.length
        if(tabBarNormalGradientLength > 0){
          branding.clearTabBarNormalGradients()
          for(var i = 0; i<tabBarNormalGradientLength; i++){
            branding.setTabBarNormalGradients(tabBarNormalGradient[i])
          }
        }

      })
  }

  //save online branding JSONS
  async saveDefaultOnlineJsons(jsonContent){
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BRANDING_JSON
    return RNFS.writeFile(filePath, JSON.stringify(jsonContent), 'utf8')
    .then((success) => true)
    .catch((err) => false)
  }

  //saveing current image
  async saveOnlineImages(image, url){
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE
    if(image === DEFAULT_FILES.DEFAULT_LOGO_IMAGE){
      filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
    }
    return RNFS.downloadFile({fromUrl:url, toFile: filePath}).promise
    .then(res => true)
    .catch(false)
  }

  async tokenExpiryForRefresh(){
    let expiryTokenDetails = await AsyncStorage.getItem(AuthTokens.TOKEN_STORAGE_KEY)
    let expiryToken = JSON.parse(expiryTokenDetails)
    return expiryToken
  }
}
