/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import EventEmitter from "react-native-eventemitter";
import {DRAWER_MENU,EVENT, SETTINGS_EVENTS} from '../../Constants/Constants';
import {Platform, StyleSheet, RefreshControl, Text, View, ActivityIndicator, Image, ScrollView, Button } from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import UIUtils from "../../Utils/UIUtils"
import BrandingEngine from '../../Utils/BrandingEngine'
import Branding from "../../Utils/Branding"
import WebEngine from "../../Utils/WebEngine"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils'
import SettingsListComponent from '../../Widgets/SettingsListComponent/SettingsListComponent'
import BrowserUtils from '../../Utils/BrowserUtils';
export default class MyToolsComponent extends BaseViewComponent {

  _mounted = true

  constructor(props){
    super(props);

    this.state= {
      topBottomSafeArea:false,
      enableNavigationBar: true,
      loaded:false,
      loadingAttempted:false,
      settingsData:null,
      isRefreshing:false,
      navigationBarConfig: {
        title: "Settings",
        showHome: true,
        showAlert: true,
        showMenu: true,
        showEmail:false
      }
    }
    this.openExternal = this.openExternal.bind(this)
    this.openInternal = this.openInternal.bind(this)

    this.onRefresh = this.onRefresh.bind(this)
  }

  async componentWillMount(){
    EventEmitter.addListener(SETTINGS_EVENTS.OPEN_INTERNAL, this.openInternal);
    EventEmitter.addListener(SETTINGS_EVENTS.OPEN_EXTERNAL,this.openExternal);
    this._mounted = true
    this.initiateSettingsFetch()
  }

  componentWillUnmount(){
    this._mounted = false

    //get analytics
    UIUtils.sendAnalytics("Settings")

    EventEmitter.removeListener(SETTINGS_EVENTS.OPEN_EXTERNAL,this.openExternal);
    EventEmitter.removeListener(SETTINGS_EVENTS.OPEN_INTERNAL,this.openInternal);
  }

  openInternal(e){
    if(e.link === "My Profile"){
      this.props.navigate("Profile");
    }
  }

  openExternal(e){
    BrowserUtils.openBrowser(e.title,e.link);
//      this.props.navigate('InAppWeb', {webLink: e.link, pageTitle:e.title})
//       if(Platform.OS =='ios'){
//         BrowserUtils.openBrowser(e.title,e.link);
//       }else{
//             this.props.navigate('InAppWeb', {webLink: e.link, pageTitle:e.title})
//       }

  }

  onRefresh(){
    this.setState({isRefreshing: true})
    this.initiateSettingsFetch()
  }

  //initiate setting fetch
  async initiateSettingsFetch(){
    var settingsDataArr = []
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""

    this.setState({loaded:false, loadingAttempted:false})

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    //verify if the token needs to be updated
    if(shouldUpdateToken){
        await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
          if(values[0] === false){
            this.setFetchStatus(false, true, settingsDataArr, false)
          }else{
            newUserToken = values[0]
            LocalStorageManager.saveAuthTokens(newUserToken,()=>{
              LocalStorageManager.getSavedAuthTokens((savedValues)=>{
                ConfigUtils.id_token = savedValues.id_token
                this.fetchSettingsData()
              },(error)=>{
                this.setFetchStatus(false, true, settingsDataArr, false)
              })
            }),(error)=>{
              this.setFetchStatus(false, true, settingsDataArr, false)
            }
          }
        })
    }else{
      this.fetchSettingsData()
    }
  }

  //fetch setting data
  async fetchSettingsData(){
    var settingsDataArr = []
    this.setState({loaded:false, loadingAttempted:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.fetchSettings()]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setFetchStatus(false, true, settingsDataArr, false)
      }else{
        settingsDataArr = jsonContent
        this.setFetchStatus(true, false, settingsDataArr, false)
      }
    })
  }

  setFetchStatus(loadedStatus, attemptedStatus, settingsData, refreshStatus){
    if(this._mounted){
      this.setState({
        loaded     : loadedStatus,
        loadingAttempted : attemptedStatus,
        settingsData: settingsData,
        isRefreshing: refreshStatus
      })
    }
  }

  componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.MY_TOOLS_ITEM});
  }

  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.initiateSettingsFetch()}
      title="Try again">
      </Button>
      </View>
    )
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  renderView() {

    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    return (
      <View style={[{flex:1}]}>
      <ScrollView
        refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
        }
      >
      {this.state.settingsData.map((prop, key) => {
          return (
            <SettingsListComponent settingItems={this.state.settingsData[key]} sectionIndex={key} key={key}/>
          );
      })}
      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorImage:{
      height:150,
      width:150
  }
});
