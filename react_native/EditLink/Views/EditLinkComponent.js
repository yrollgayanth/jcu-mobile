/**
* Created by Yrol on 19/03/18.
*/

import React, { Component } from 'react'
import WebEngine from '../../Utils/WebEngine'
import UIUtils from "../../Utils/UIUtils"
import Branding from '../../Utils/Branding'
import {COLOR} from '../../Constants/Constants'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, Label, TouchableOpacity, TextInput, ActivityIndicator, KeyboardAvoidingView } from 'react-native'
import { NavigationActions } from 'react-navigation'

export default class EditLinkComponent extends Component<{}>{

  _device_width = UIUtils.getScreenWidth()
  _header_height = UIUtils.navBarheight() + 5
  _branding = Branding.sharedInstance

  constructor(props){
    super(props)
    this.state={
      loaded:true,
      title:this.props.navigation.state.params.title,
      url:this.props.navigation.state.params.url,
      editID:this.props.navigation.state.params.editID,
      allLinks:this.props.navigation.state.params.allLinks
    }
  }

  componentWillMount(){
    console.log("Edit row passed:" + this.state.editID)
  }

  async addLink(){
    let webEngine = WebEngine.sharedInstance
  }

  async submitLink(){
    let webEngine = WebEngine.sharedInstance
    let editID = this.state.editID
    var allLinks = this.state.allLinks
    const {title} = this.state
    const {url} = this.state

    if(title == '' || url ==''){
      alert("Title and url must be filled")
    }else{

      Object.keys(allLinks).map(function (key){
        if(Number(allLinks[key].id) === Number(editID)){
          allLinks[key].title = title
          allLinks[key].url = url
        }
      })

      //do the submit
      let webSubmitObject = {student_id:this._branding.getStudentID(), quick_links:allLinks}
      this.setState({loaded:false})
      const { navigation } = this.props;
      await Promise.all([webEngine.reorderAddDelEditQuicklink(webSubmitObject)]).then(values => {
        jsonOutput = values[0]
        if(jsonOutput === false){
          alert("An error occurred. Please try again")
          this.setFetchStatus(false)
        }else{
          this.setFetchStatus(true)
          navigation.dispatch(NavigationActions.back())
          navigation.state.params.refreshOnQuickLinkAdd();
        }
      })
    }
  }

  setFetchStatus(loadedStatus){
    if(loadedStatus){
      this.setState({loaded:loadedStatus, title:"", url:""})
    }
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  render(){
    if(!this.state.loaded){
      return(
        this.renderLoaderView()
      )
    }
    return(
      <KeyboardAvoidingView style={styles.container}>
        <TextInput style = {styles.input}
        autoCapitalize="none"
        autoCorrect={false}
        returnKeyType="next"
        placeholder='Display text'
        value={this.state.title}
        onChangeText={(title) => this.setState({title})}
        placeholderTextColor='lightgrey'/>

        <TextInput style = {styles.input}
        autoCapitalize="none"
        onSubmitEditing={() => this.passwordInput.focus()}
        autoCorrect={false}
        returnKeyType="next"
        value={this.state.url}
        placeholder='Url'
        onChangeText={(url) => this.setState({url})}
        placeholderTextColor='lightgrey'/>

        <TouchableOpacity style={styles.buttonContainer} onPress={()=>this.submitLink()}>
          <Text style={styles.buttonText}>Save</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    padding: 20
  },
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  input:{
    width:"90%",
    height: 40,
    backgroundColor: 'white',
    color:'black',
    borderWidth: 2,
    borderColor: '#969696',
    marginBottom: 10,
    padding: 10
  },
  buttonContainer:{
    backgroundColor: "#969696",
    paddingVertical: 15,
    width:"90%",
    padding:10
  },
  buttonText:{
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700'
  }
})
