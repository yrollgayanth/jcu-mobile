/**
* Created by Sylvia on 30/1/18.
*/
import MainTabComponent from "./Tabbar/Views/MainTabComponent";
import { StackNavigator } from 'react-navigation';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
import TestSharedComponent from './TestSharedComponent';
import ProfileComponent from './Profile/Views/ProfileComponent'
import EditQuickLinksComponent from './EditQuicklinks/Views/EditQuicklinksComponent'
import SeeAllNewsComponent from "./AllNews/Views/SeeAllNewsComponent"
import ClubPreferencesComponent from './Profile/ClubPreferences/views/ClubPreferencesComponent';
import NotiPreferencesComponent from './Profile/NotiPreferences/views/NotiPreferencesComponent';
//import EditQuickLinksComponent from './Profile/EditQuickLinks/views/EditQuickLinksComponent';
export const AppNavigator = StackNavigator(

    {
        // TestSharedComponent:{screen:TestSharedComponent},
        MainTab:{screen:MainTabComponent},
        Profile:{screen:ProfileComponent},
        ClubPreference:{screen:ClubPreferencesComponent},
        NotiPreference:{screen:NotiPreferencesComponent},
        EditQuickLinks:{screen:EditQuickLinksComponent}
    },
    {
        transitionConfig: () => ({
            screenInterpolator: CardStackStyleInterpolator.forHorizontal,
        }),
        headerMode:'none'
    }

);
