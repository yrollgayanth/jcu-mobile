/**
* Created by Yrol on 19/03/18.
*/

import React, { Component } from 'react'
import WebEngine from '../../Utils/WebEngine'
import UIUtils from "../../Utils/UIUtils"
import {COLOR} from '../../Constants/Constants'
import Branding from '../../Utils/Branding'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, Label, TouchableOpacity, TextInput, ActivityIndicator, KeyboardAvoidingView } from 'react-native'
import { NavigationActions } from 'react-navigation'

export default class AddLinkComponent extends Component<{}>{

  _device_width = UIUtils.getScreenWidth()
  _header_height = UIUtils.navBarheight() + 5
  _branding = Branding.sharedInstance

  constructor(props){
    super(props)
    this.state={
      loaded:true,
      title:'',
      url:'',
      allQuicklinks:this.props.navigation.state.params.allLinks,
    }
  }

  async addLink(){
    const {title} = this.state
    const {url} = this.state

    let validatedUrl = this.isValidUrl(url)
    if(title == '' || validatedUrl === false){
      //alert("Title cannot be empty and the URL must be valid")
      UIUtils.showSingleAlert("Title cannot be empty and the URL must be valid")
    }else{
      var allLinksObj = this.state.allQuicklinks
      let lastObject = Object.keys(allLinksObj).pop()
      allLinksObj[Number(lastObject) + 1] = {title:this.state.title, url:validatedUrl}
      let webSubmitObject = {student_id:this._branding.getStudentID(), quick_links:allLinksObj}

      const { navigation } = this.props;
      let webEngine = WebEngine.sharedInstance
      this.setState({loaded:false})
      await Promise.all([webEngine.reorderAddDelEditQuicklink(webSubmitObject)]).then(values => {
        jsonOutput = values[0]
        if(jsonOutput === false){
          //alert("An error occurred. Please try again")
          UIUtils.showSingleAlert("An error occurred. Please try again")
          this.setFetchStatus(false)
        }else{
          this.setFetchStatus(true)
          navigation.dispatch(NavigationActions.back())
          navigation.state.params.refreshOnQuickLinkAdd();
        }
      });
    }
  }

  //validate URL
  isValidUrl(url){
    var urlVal = url

    //check if the url is empty
    if(typeof urlVal === 'undefined' || urlVal === null || urlVal === ''){
      return false
    }

    //append http if need
    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
      urlVal = 'http://' + urlVal
    }

    var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;

    if(urlregex.test(urlVal) === true){
      return urlVal
    }

    return false
  }

  setFetchStatus(loadedStatus){
    if(loadedStatus){
      this.setState({loaded:loadedStatus, title:"", url:""})
    }
  }

  //loadingView
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  render(){
    if(!this.state.loaded){
      return(
        this.renderLoaderView()
      )
    }
    return(
      <KeyboardAvoidingView style={styles.container}>
        <TextInput style = {styles.input}
        autoCapitalize="none"
        onChangeText={title => this.setState({title})}
        autoCorrect={false}
        returnKeyType="next"
        placeholder='Display text'
        value={this.state.title}
        onChangeText={(title) => this.setState({title})}
        placeholderTextColor='lightgrey'/>

        <TextInput style = {styles.input}
        autoCapitalize="none"
        onSubmitEditing={() => this.passwordInput.focus()}
        onChangeText={url => this.setState({url})}
        autoCorrect={false}
        returnKeyType="next"
        value={this.state.url}
        placeholder='Url'
        onChangeText={(url) => this.setState({url})}
        placeholderTextColor='lightgrey'/>

        <TouchableOpacity style={styles.buttonContainer} onPress={()=>this.addLink()}>
          <Text style={styles.buttonText}>Save</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    padding: 20
  },
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  input:{
    width:"90%",
    height: 40,
    backgroundColor: 'white',
    color:'black',
    borderWidth: 2,
    borderColor: '#969696',
    marginBottom: 10,
    padding: 10
  },
  buttonContainer:{
    backgroundColor: "#969696",
    paddingVertical: 15,
    width:"90%",
    padding:10
  },
  buttonText:{
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700'
  }
})
