/**
* Created by Yrol on 01/02/18.
*/
import React, { Component } from 'react'
import EventEmitter from "react-native-eventemitter"
import { Platform, NativeModules, StyleSheet, Text, View, Button, TextInput, TouchableOpacity, TouchableHighlight, Alert, ImageBackground, Image, WebView, ActivityIndicator } from 'react-native';
import  UIUtils from '../../Utils/UIUtils'
import {EVENT, QTAC_ERROR_CODES, QTAC_ERROR_MESSAGE } from '../../Constants/Constants'
import ConfigUtils from '../../Utils/ConfigUtils'
import BaseViewComponent from '../../SharedComponents/BaseViewComponent'
import AdvancedPopupWidget from '../../Widgets/AdvancedPopupWidget/AdvancedPopupWidget';
import { NavigationActions } from 'react-navigation'

export default class NewStudentComponent extends BaseViewComponent{

  _device_width = UIUtils.getScreenWidth()
  _header_height = UIUtils.navBarheight() + 5

  constructor(props) {
    super(props);
    this.webView = null;
    this.hideSpinner = this.hideSpinner.bind(this)
    this.onMessage = this.onMessage.bind(this)

    this.state = {
      enableNavigationBar: true,
      navigationBarConfig: {
        title:"New Student",
        showBack:true,
        showAlert: false,
        showEmail:false,
        showMenu: false
      },
        errorMessage:""
    }

  }

  componentWillMount(){}

  componentDidMount(){

  }


  componentWillUnmount(){
      this.setOpenedToFalse()
  }


  setOpenedToFalse(){

      console.log("UIUtils.NEW_STUDENT_OPENED = false")
      UIUtils.NEW_STUDENT_OPENED = false
  }


  showAlertForErrorCode(errorCode){

      console.log("showAlertForErrorCode: ",errorCode)

      if(errorCode === QTAC_ERROR_CODES.ACCOUNT_NOT_FOUND){
          this.setState({errorMessage:QTAC_ERROR_MESSAGE.ACCOUNT_NOT_FOUND})
      }else if(errorCode === QTAC_ERROR_CODES.PASSWORD_SET_FAIL){
          this.setState({errorMessage:QTAC_ERROR_MESSAGE.PASSWORD_SET_FAIL})
      }else if(errorCode === QTAC_ERROR_CODES.QTAC_STUDENT_NOT_FOUND){
          this.setState({errorMessage:QTAC_ERROR_MESSAGE.QTAC_STUDENT_NOT_FOUND})
      }else if(errorCode === QTAC_ERROR_CODES.QTAC_VERIFY_FAIL){
          this.setState({errorMessage:QTAC_ERROR_MESSAGE.QTAC_VERIFY_FAIL})
      }

      this.refs.alertDialog.setVisible(true)

  }


  ActivityIndicatorLoadingView() {
    return (
      <ActivityIndicator
      color='#009688'
      size='large'
      style={styles.ActivityIndicatorStyle}
      />
    );
  }

  hideSpinner(){}

  onMessage(event) {

    console.log("event.nativeEvent.data",event.nativeEvent.data)
      let jsonObj = JSON.parse(event.nativeEvent.data)
      console.log("jsonObj:",jsonObj)

      //TODO CHECK IF SUCCESS

      if(jsonObj.status ==="error"){
        this.showAlertForErrorCode(jsonObj.code)
      }else if(jsonObj.status ==="complete") {
          //Succeeded: go to SSO login page
          this.setOpenedToFalse()
          const { navigation } = this.props;
          navigation.dispatch(NavigationActions.back())
          EventEmitter.emit(EVENT.FINISHED_GET_STARTED,{})

      }

  }




  tappedOnOkButton(){

      setTimeout(function(){

          if(Platform.OS == "ios"){
              var iosSSOManager = NativeModules.IOSSSOManager;
              iosSSOManager.openSSOPage(ConfigUtils.SSO_URL);
          }else{
              var androidSSOManager = NativeModules.AndroidSS0Manager;
              androidSSOManager.openSSOPage(ConfigUtils.SSO_URL);
          }

      }, 200)

      this.setOpenedToFalse()
      const { navigation } = this.props;
      navigation.dispatch(NavigationActions.back())

  }


  renderWebView(){

      var fullLink = ConfigUtils.NEW_STUDENT_URL
      let qtac_params = this.props.navigation.state.params.query
      // console.log("qtac_params:",qtac_params)
      if(qtac_params!=null&&qtac_params!=""){
          fullLink += qtac_params
      }else{
          fullLink += ConfigUtils.NON_QTAC_PARAMS
      }


      if(Platform.OS == "ios"){
          var IOSQtacQueryManager = NativeModules.QtacQueryManager;
          IOSQtacQueryManager.clearQtacQuery();
      }else{
          var AndroidQtacQueryManager = NativeModules.AndroidQtacQueryManager;
          AndroidQtacQueryManager.clearQtacQuery();
      }



    return(
        <View style={{flex:1}}>
          <WebView
              style={styles.WebViewStyle}
              //onLoad={() => this.hideSpinner()}
              source={{uri: fullLink}}
              //injectedJavaScript="window.postMessage = String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');"
              //injectedJavaScript={patchPostMessageJsCode}
              javaScriptEnabled={true}
              ref={webView => (this.webView = webView)}
              onMessage={this.onMessage}
              domStorageEnabled={true}
              mixedContentMode={'always'}
              scalesPageToFit={true}
              renderLoading={this.ActivityIndicatorLoadingView}
              startInLoadingState={true}
          />
          <AdvancedPopupWidget ref='alertDialog'
                               message={this.state.errorMessage}
                               singleButtonLabel={"Ok"}
                               singleButtonCallback={this.tappedOnOkButton.bind(this)}
          ></AdvancedPopupWidget>
        </View>

    )
  }


  renderView(){

    return this.renderWebView()

  }



}


const styles = StyleSheet.create({

    WebViewStyle:{
      justifyContent: 'center',
      alignItems: 'center',
      flex:1,
    },

    ActivityIndicatorStyle:{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'

    }
  });
