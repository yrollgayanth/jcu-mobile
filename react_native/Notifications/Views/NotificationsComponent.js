/**
 * Created by Sylvia on 30/11/17.
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import EventEmitter from "react-native-eventemitter";
import {DRAWER_MENU,EVENT} from '../../Constants/Constants';

export default class NotificationsComponent extends BaseViewComponent {


    constructor(props){
        super(props);

        this.state= {
            topBottomSafeArea:false,
            enableNavigationBar: true,
            navigationBarConfig: {
                title: "Notifications",
                showHome: true,
                showAlert: true,
                showMenu: true
            }
        }

    }

    componentDidMount(){
        EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.NOTIFICATION_ITEM});
    }


    renderView() {
        return (
          <View style={{padding:20}}>
                <Text>{"You have no notifications at this time. Don’t forget to check your email for other important information."}</Text>
          </View>
        );
    }


}

const styles = StyleSheet.create({
    mainContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    }
});
