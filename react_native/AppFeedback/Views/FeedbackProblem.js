/**
* Created by Yrol on 02/03/18.
*/
import React, { Component } from 'react'
import { StyleSheet, Text, View, Alert, ActivityIndicator, Image, ScrollView, Button, TouchableHighlight, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import {DRAWER_MENU,EVENT, LAYOUT_SPACE, FONT_SIZE, COLOR} from '../../Constants/Constants'
import UIUtils from "../../Utils/UIUtils"
import Branding from "../../Utils/Branding"
import EventEmitter from "react-native-eventemitter"
import LinearGradient from 'react-native-linear-gradient'

export default class FeedbackProblem extends Component{

  _branding = Branding.sharedInstance
  _current_radio_option = "I can't find something"

  constructor(props){
    super(props);
    this.state = {
      expanded:true,
      problem:""
    }
  }

  renderExpandableSectionProblem(){
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {this.setState({expanded:!this.state.expanded})}}
      >

      <LinearGradient colors={this._branding.getHeaderGradient()}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={[styles.sectionTitle, {color:this._branding.getHeaderFontColor()}]}>Report a problem</Text>
      {this.renderArrow()}
      </LinearGradient>
      </TouchableHighlight>
    );
  }

  onSelect(index, value){
    this._current_radio_option = value
  }

  submitProblem(){
    EventEmitter.emit(EVENT.FEEDBACK_PROBLEM, {type:this._current_radio_option, desciption:this.state.problem})
  }

  renderContent(){
    if(this.state.expanded){
      return(
        <View style={styles.container}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'flex-start',
            alignItems: 'center'
          }}>
            <View style={{width: "90%", height:"auto", marginBottom:20, marginTop:10}}>
              <Text style={styles.subSectionTitle}>{"Type of Problem"}</Text>
            </View>

            <View style={{width: "90%", height:"auto", marginBottom:20, marginTop:0}}>
            <RadioGroup
              size={20}
              thickness={2}
              color='#000000'
              selectedIndex={0}
              onSelect = {(index, value) => this.onSelect(index, value)}
            >
              <RadioButton value={"I can't find something"} >
                <Text>{"I can't find something"}</Text>
              </RadioButton>

              <RadioButton value={"There is a bug in the app"}>
                <Text>{"There is a bug in the app"}</Text>
              </RadioButton>

              <RadioButton value={'Other'}>
                <Text>{"Other"}</Text>
              </RadioButton>
            </RadioGroup>
            </View>

            <View style={{width: "90%", height:"auto", backgroundColor:COLOR.BG_GREY, marginBottom:20, marginTop:0}}>
              <TextInput
              onChangeText={problem => this.setState({problem})}
              multiline={true}
              style={{ height: 100, textAlignVertical: "top"}}
              />
            </View>
            <TouchableOpacity style={styles.submitButton} onPress={() => this.submitProblem()}>
              <Text style={styles.buttonText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }

  renderArrow(){
    if(this.state.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }

  render(){
    return(
      <View>
      {this.renderExpandableSectionProblem()}
      {this.renderContent()}
      </View>
    )
  }

}


const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
    // alignItems:'center',
    // flexDirection:'row',
  },
  submitButton:{
    justifyContent:'center',
    alignItems:'center',
    width:"90%",
    flexDirection:'column',
    padding:10,
    height:40,
    marginBottom:10,
    backgroundColor:'#006aba'
  },
  buttonText:{
    color:"#fff",
    fontWeight:"normal",
    fontSize:16
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  subSectionTitle:{
    color:"#000000",
    fontWeight:"bold",
    fontSize:15
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  editButton:{
    fontSize:UIUtils.size(10),
    color:'white',
    borderColor:'white',
    borderWidth:1,
    borderRadius:UIUtils.size(3),
    padding:UIUtils.size(4),
    paddingLeft:UIUtils.size(8),
    paddingRight:UIUtils.size(8)
  },
  editButtonWrapper:{
    position:'absolute',
    left:UIUtils.size(110),
  }
});
