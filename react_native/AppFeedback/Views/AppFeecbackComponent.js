/**
* Created by Yrol on 02/03/18.
*/

import React, { Component } from 'react'
import BaseViewComponent from '../../SharedComponents/BaseViewComponent'
import { StyleSheet, Text, View, ActivityIndicator, Image, ScrollView, Button, TouchableHighlight, TextInput, TouchableOpacity, KeyboardAvoidingView, Platform} from 'react-native'
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import {DRAWER_MENU,EVENT, LAYOUT_SPACE, FONT_SIZE, COLOR} from '../../Constants/Constants'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import UIUtils from "../../Utils/UIUtils"
import EventEmitter from "react-native-eventemitter"
import BrandingEngine from '../../Utils/BrandingEngine'
import Branding from "../../Utils/Branding"
import WebEngine from "../../Utils/WebEngine"
import LocalStorageManager from '../../Utils/LocalStorageManager'
import ConfigUtils from '../../Utils/ConfigUtils'
import LinearGradient from 'react-native-linear-gradient'
import FeedbackProblem from './FeedbackProblem'
import FeedbackSuggestion from './FeedbackSuggestion'
import DeviceInfo from 'react-native-device-info'

export default class AppFeecbackComponent extends BaseViewComponent {

  _branding = Branding.sharedInstance

  constructor(props){
    super(props);
    this.state = {
      expanded:true,
      topBottomSafeArea:false,
      currentPosition: 0,
      enableNavigationBar: true,
      showForm:true,
      navigationBarConfig: {
        title: "App Feedback",
        showEmail:false,
        showHome: true,
        showAlert: true,
        showMenu: true
      }
    }
    //event listeners
    this.receiveProblem = this.receiveProblem.bind(this)
    this.receiveSuggestion = this.receiveSuggestion.bind(this)
  }

  componentDidMount(){
    EventEmitter.emit(EVENT.UPDATE_HIGHLIGHT_ON_SIDE_MENU, {nowAt:DRAWER_MENU.FEEDBACK_ITEM});
  }

  componentWillMount(){

    //feedback and problem listener
    EventEmitter.addListener(EVENT.FEEDBACK_PROBLEM,this.receiveProblem)
    EventEmitter.addListener(EVENT.FEEDBACK_SUGGESTION,this.receiveSuggestion)
  }

  componentWillUnmount(){
    EventEmitter.removeListener(EVENT.FEEDBACK_PROBLEM,this.receiveProblem)
    EventEmitter.removeListener(EVENT.FEEDBACK_SUGGESTION, this.receiveSuggestion)
  }

  //problem
  async receiveProblem(e){
    if(typeof e.type === 'undefined' || e.type === null || e.type === '' ||
        typeof e.desciption === 'undefined' || e.desciption === null || e.desciption === ''){
          UIUtils.showGenericAlert("Problem desciption and the error type must be specified")
    }else{
      let webEngine = WebEngine.sharedInstance
      var branding = new BrandingEngine()
      var shouldUpdateToken = false
      var existingUserToken = ""
      var newUserToken = ""

      await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
        existingUserToken = values[0]
        if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
          shouldUpdateToken = true
        }
      })

      if(shouldUpdateToken){
        await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
          if(values[0] === false){
            this.setState({showForm:false})
          }else{
            newUserToken = values[0]
            LocalStorageManager.saveAuthTokens(newUserToken,()=>{
              LocalStorageManager.getSavedAuthTokens((savedValues)=>{
                ConfigUtils.id_token = savedValues.id_token
                this.submitProblem(e)
              },(error)=>{
                this.setState({showForm:true})
                UIUtils.showGenericAlert("An error occurred. Please try again.")
              })
            }),(error)=>{
              this.setState({showForm:true})
              UIUtils.showGenericAlert("An error occurred. Please try again.")
            }
          }
        })
      }else{
        this.submitProblem(e)
      }
    }
  }

  async submitProblem(e){
    var jsonFeedback = {
      "student_id" : this._branding.getStudentID(),
      "primary_type" : "problem",
      "secondary_type" : e.type,
      "details" : e.desciption ,
      "device_type" : DeviceInfo.getModel(),
      "os_name" : DeviceInfo.getSystemName(),
      "os_version" : DeviceInfo.getSystemVersion(),
      "app_version" : "1.0"
    }
    this.setState({showForm:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.submitFeedback(JSON.stringify(jsonFeedback))]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setState({showForm:true})
        UIUtils.showGenericAlert("An error occurred. Please try again.")
      }else{
        this.setState({showForm:true})
        if(jsonContent.success === false){
          UIUtils.showGenericAlert("an error occurred while submitting your request. Please try again")
        }else{
          UIUtils.showGenericAlert("Submitted successfully")
        }
      }
    })
  }

  //submit suggestion
  async submitSuggestion(e){
    var jsonFeedback = {
        "student_id" : this._branding.getStudentID(),
        "primary_type" : "suggestion",
        "details" : e.desciption,
        "device_type" : DeviceInfo.getModel(),
        "os_name" : DeviceInfo.getSystemName(),
        "os_version" : DeviceInfo.getSystemVersion(),
        "app_version" : "1.0"
    }

    this.setState({showForm:false})
    let webEngine = WebEngine.sharedInstance
    await Promise.all([webEngine.submitFeedback(JSON.stringify(jsonFeedback))]).then(values => {
      var jsonContent = values[0]
      if(jsonContent === false){
        this.setState({showForm:true})
        UIUtils.showGenericAlert("An error occurred. Please try again.")
      }else{
        this.setState({showForm:true})
        if(jsonContent.success === false){
          UIUtils.showGenericAlert("an error occurred while submitting your request. Please try again")
        }else{
          UIUtils.showGenericAlert("Submitted successfully")
        }
      }
    })
  }

  //suggestion
  async receiveSuggestion(e){
    if(typeof e.suggestion === 'undefined' || e.suggestion === null || e.suggestion === ''){
      UIUtils.showGenericAlert("Please describe your suggestion")
    }else{
      let webEngine = WebEngine.sharedInstance
      var branding = new BrandingEngine()
      var shouldUpdateToken = false
      var existingUserToken = ""
      var newUserToken = ""

      await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
        existingUserToken = values[0]
        if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
          shouldUpdateToken = true
        }
      })

      if(shouldUpdateToken){
        await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
          if(values[0] === false){
            this.setState({showForm:false})
          }else{
            newUserToken = values[0]
            LocalStorageManager.saveAuthTokens(newUserToken,()=>{
              LocalStorageManager.getSavedAuthTokens((savedValues)=>{
                ConfigUtils.id_token = savedValues.id_token
                this.submitSuggestion(e)
              },(error)=>{
                this.setState({showForm:true})
                UIUtils.showGenericAlert("An error occurred. Please try again.")
              })
            }),(error)=>{
              this.setState({showForm:true})
              UIUtils.showGenericAlert("An error occurred. Please try again.")
            }
          }
        })
      }else{
        this.submitSuggestion(e)
      }
    }
  }

  renderExpandableSectionProblem(){
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {this.setState({expanded:!this.state.expanded})}}
      >

      <LinearGradient colors={this._branding.getHeaderGradient()}
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 1 }}
      style={styles.sectionItem} >
      <Text style={[styles.sectionTitle, {color:this._branding.getHeaderFontColor()}]}>Report a problem</Text>
      {this.renderArrow()}
      </LinearGradient>
      </TouchableHighlight>
    );
  }

  renderArrow(){
    if(this.state.expanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }

  //render submitting version
  renderFormView(){
    return(
      <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
      <FeedbackProblem />
      <FeedbackSuggestion/>
      </KeyboardAwareScrollView>
    )
  }

  //render loading view
  showLoadingView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.showForm} size="large" color="#0000ff" />
      </View>
    )
  }

  renderView() {
    if(this.state.showForm){
      return this.renderFormView()
    }else{
      return this.showLoadingView()
    }
  }
}

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#ffffff',
    // alignItems:'center',
    // flexDirection:'row',
  },
  mainContainer: {
    flex: 1,
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  submitButton:{
    justifyContent:'center',
    alignItems:'center',
    width:"90%",
    flexDirection:'column',
    padding:10,
    height:40,
    marginBottom:10,
    backgroundColor:'#006aba'
  },
  buttonText:{
    color:"#fff",
    fontWeight:"normal",
    fontSize:16
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  subSectionTitle:{
    color:COLOR.GRADIENT_DARK_BLUE,
    fontWeight:"bold",
    fontSize:15
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  editButton:{
    fontSize:UIUtils.size(10),
    color:'white',
    borderColor:'white',
    borderWidth:1,
    borderRadius:UIUtils.size(3),
    padding:UIUtils.size(4),
    paddingLeft:UIUtils.size(8),
    paddingRight:UIUtils.size(8)
  },
  editButtonWrapper:{
    position:'absolute',
    left:UIUtils.size(110),
  }
});
