/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import {Platform, StyleSheet,Text,View,ScrollView,Image,TouchableHighlight, ActivityIndicator, AsyncStorage } from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import {COLOR, STORAGE_KEY, DEFAULT_FILES, EVENT} from '../../Constants/Constants';
import WebEngine from '../../Utils/WebEngine'
import BrowserUtils from '../../Utils/BrowserUtils';
import EventEmitter from 'react-native-eventemitter'
import { NativeEventEmitter, NativeModules } from 'react-native';
import AuthTokens from '../../Objects/AuthTokens';
import LocalStorageManager from '../../Utils/LocalStorageManager';
import ButtonModel from '../Models/ButtonModel';
import EnrolmentStatusWidget from '../../Widgets/EnrolmentStatus/EnrolmentStatusWidget';
import HTMLView from 'react-native-htmlview';



const ButtonType = {HELP:"helpInfo",ACTION:"actionButton",HIGHLIGHT:""};
const ButtonFunc = {EXTERNAL_URL:"externalUrl"};
const PLAN_YOUR_STUDY = "PLAN-YOUR-STUDY";

export default class EnrolmentStepDetailsComponent extends BaseViewComponent {

    iosEventSubscription;

    _mounted = true
    constructor(props){

        super(props);
        this.state= {

            enableSingleButtonDialog:true,
            singleButtonDialogConfig:{
                message:"This step is not available at the moment.",
                buttonLabel:"Ok",
                callback:()=>{}
            },


          enableNavigationBar: true,
          navigationBarConfig: {
            title: "Enrolment",
            showHome: true,
            showAlert:true,
              showMenu:true
          },

            isLoading:true,
            needsReload:false,

            //step content data
            courseId:this.props.courseID,
            studentID:"",
            selectedStep:"",
            courseTitle:"",
            steps:[],

            singleButtonDialogConfig:{
                message:"Network request failed. Please try again.",
                buttonLabel:"Retry",
                callback:this.retryRequest.bind(this)
            },

        }

        if(Platform.OS=='ios'){
            iosEventSubscription = new NativeEventEmitter(NativeModules.IOSEventManager)
        }

        this.quitEnrolmentWebpage = this.quitEnrolmentWebpage.bind(this)

    }

    retryRequest(){

        this.fetchEnrolmentDetails()
    }

    performNextStep(){

      console.log("performNextStep")
      this.setState({isLoading:true})

      var bodyData = {
        "student_id":this.state.studentID,
        "courseId":this.state.courseId,
        "step_name":this.state.selectedStep
      };

      let webEngine = WebEngine.sharedInstance

      Promise.all([webEngine.updateEnrolmentStep(bodyData)]).then(res => {

          if(res === false){

              this.popSingleButtonAlertDialog()

          }else{

              console.log("courseSteps EnrolmentStepDetailsComponent13Apri",res[0])
              if(res[0].steps.length==0||res[0].steps==0){
                  console.log("No step data")
                  return
              }

              this.setState({courseTitle:res[0].course_name})
              this.setState({steps:res[0].steps})
              for(var stepID in this.state.steps){
                  if(this.state.steps[stepID].current_step){
                      this.setState({selectedStep:stepID})
                      break
                  }
              }
              this.setState({isLoading:false})

          }

      })
    }


    async fetchEnrolmentDetails(){

        this.setState({isLoading:true})


        LocalStorageManager.getSavedAuthTokens((result)=>{

            var authTokens = result;
            var studentId = result.getStudentID()
            this.setState({studentID:studentId})
            let webEngine = WebEngine.sharedInstance

            Promise.all([webEngine.fetchEnrolmentStepDetails(studentId,this.state.courseId)]).then(res => {

                this.setState({isLoading:false})

                if(res === false){

                    this.popSingleButtonAlertDialog()

                }else{

                    if(res[0].steps.length==0||res[0].steps==0){
                        // console.log("No step data")
                        return
                    }

                    this.setState({courseTitle:res[0].course_name})
                    this.setState({steps:res[0].steps})
                    for(var stepID in this.state.steps){
                        if(this.state.steps[stepID].current_step){
                            this.setState({selectedStep:stepID})
                            break
                        }
                    }

                    this.setState({isLoading:false})

                }

            })

        },error =>{
            console.log("error:",error)
        })

    }



    quitEnrolmentWebpage(){

        console.log("quitEnrolmentWebpage");
        if(this.state.needsReload===true){
            this.setState({needsReload:false})
            this.fetchEnrolmentDetails();
        }
    }



    async componentWillMount(){

        this._mounted = true
        if(Platform.OS=='ios'){
            iosEventSubscription.addListener('QuitIOSBrowser', () => this.quitEnrolmentWebpage())
        }else{
            EventEmitter.addListener(EVENT.QUIT_WEBPAGE, this.quitEnrolmentWebpage);
        }
        this.fetchEnrolmentDetails()

    }


    componentWillUnmount(){
        this._mounted = false

        if(Platform.OS=='ios'){
            iosEventSubscription.removeListener('QuitIOSBrowser',this.quitEnrolmentWebpage);
        }else{
            EventEmitter.removeListener(EVENT.QUIT_WEBPAGE,this.quitEnrolmentWebpage);
        }
    }



    tappedOnButton(button){
        console.log(button);

        //plan your study action button will be always be next
        var selectedStepData = this.state.steps[this.state.selectedStep]
        if(button.buttonType === ButtonType.ACTION&&selectedStepData.content.id===PLAN_YOUR_STUDY){
            this.performNextStep()
        }else{

            if(button.buttonFunc===ButtonFunc.EXTERNAL_URL){

                if(button.buttonType === ButtonType.ACTION){
                    this.setState({needsReload:true})
                }
                BrowserUtils.openBrowser(button.buttonName,button.buttonDesc)
                // var url = "https://secure.jcu.edu.au/eStudentDev/SM/OfferDtls10.aspx?f=%23EST.OFFRDTLS.01.WEB&IsSSO=true&Config=Dev03+direct"
                // BrowserUtils.openBrowser(button.buttonName,url)

            }

        }




    }

    populateButton(button) {

        if(button.buttonType===ButtonType.HELP){
            return(
                <TouchableHighlight key={button.buttonName} style={styles.helpButton} underlayColor="transparent"
                                    onPress={() => this.tappedOnButton(button)}
                >
                    <View style={styles.helpBtnContent}>
                        <Image style={styles.helpBtnIcon} source={require("../../../assets/img/ic_help_btn.png")}></Image>
                        <Text style={styles.buttonText}>{button.buttonName}</Text>
                    </View>
                </TouchableHighlight>
            )
        }else if(button.buttonType===ButtonType.ACTION){
            return(
                <TouchableHighlight key={button.buttonName} style={styles.actionButton} underlayColor="transparent"
                                    onPress={() => this.tappedOnButton(button)}
                >
                    <View style={styles.helpBtnContent}>
                        <Text style={styles.buttonText}>{button.buttonName}</Text>
                    </View>
                </TouchableHighlight>
            )
        }else{


        }

    }


    populateLeftButtons(leftButtons){

        var leftButtonViews = [];
        for(var leftButton of leftButtons){
            var button = new ButtonModel(leftButton.leftButtonName,leftButton.leftButtonType,leftButton.leftButtonFunction,leftButton.leftButtonDescription)
            leftButtonViews.push(this.populateButton(button))
        }

        return (

            <View style={styles.leftButtonPanel}>
                {leftButtonViews}
            </View>
        )
    }


    populateRightButtons(rightButtons){

        var selectedStepData = this.state.steps[this.state.selectedStep]

        var rightButtonViews = [];
        for(var rightButton of rightButtons){
            var button = new ButtonModel(rightButton.rightButtonName,rightButton.rightButtonType,rightButton.rightButtonFunction,rightButton.rightButtonDescription)
            if(rightButton.rightButtonName==='Next'){
                if(selectedStepData.show_next_button===true){
                    rightButtonViews.push(this.populateButton(button))
                }

            }

        }

        return(
            <View style={styles.rightButtonPanel}>
                {rightButtonViews}
            </View>
        )

    }


    poopulateTappedOnNext(selectedStepData){
        console.log(this.state.studentID+","+this.state.courseId+","+selectedStepData.content.id);
        this.performNextStep();

    }



    populateNextButton(selectedStepData){

        var selectedStepData = this.state.steps[this.state.selectedStep]

        // var showNextButton = selectedStepData.show_next_button===true
        // console.log("showNext:",showNextButton)


        if(PLAN_YOUR_STUDY===selectedStepData.content.id){
            return
        }

        if(selectedStepData.show_next_button===true){
            return(
                <View style={styles.middleButtonPanel}>
                    <TouchableHighlight style={styles.nextButton} underlayColor="transparent"
                                        onPress={() => this.poopulateTappedOnNext(selectedStepData)}
                    >
                        <View style={styles.helpBtnContent}>
                            <Text style={styles.buttonText}>Next</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            )
        }

        {/*if(PLAN_YOUR_STUDY===selectedStepData.content.id){*/}
        //     return
        // }
        //
        // if(!selectedStepData.is_mandatory&&selectedStepData.current_step){
        //     return(
        //         <View style={styles.middleButtonPanel}>
        //
        //             <TouchableHighlight style={styles.nextButton} underlayColor="transparent"
        //                                 onPress={() => this.poopulateTappedOnNext(selectedStepData)}
        //             >
        //                 <View style={styles.helpBtnContent}>
        //                     <Text style={styles.buttonText}>Next</Text>
        //                 </View>
        //             </TouchableHighlight>
        //         </View>
        //     )
        // }


    }


    renderLoaderView(){
        return(
            <View style={styles.mainContainer}>
                <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
            </View>

        )
    }


    tapCallback(data){


        let tappedStep = this.state.steps[data.stepKey];
        if(tappedStep.available===true){
            this.setState({selectedStep:data.stepKey})
        }else{
            this.popSingleButtonAlertDialog();
        }


    }

    renderStatusBar(){

        return(<EnrolmentStatusWidget
            hideSection={true}
            tapCallback={this.tapCallback.bind(this)} showCourseTitle={false} enrolmentData={[{
            "course_code": this.state.courseId,
            "steps": this.state.steps}]} isEnrolmentComponent={true}>
        </EnrolmentStatusWidget>)
    }


    openLinkInDescription(url){
        BrowserUtils.openBrowser("",url)
    }


    renderDescription() {

        var selectedStepData = this.state.steps[this.state.selectedStep]
        if(selectedStepData==null){
            return
        }

        if (selectedStepData.is_mandatory || selectedStepData.content.id === PLAN_YOUR_STUDY) {
            const htmlContent = '<div>'+selectedStepData.content.description+'</div>';
            return (
                <View style={styles.fullDescription}>
                    <HTMLView
                        stylesheet={htmlViewStyles}
                        value={htmlContent}
                        onLinkPress={(url) => this.openLinkInDescription(url)}
                    />
                </View>
            )

        } else {
            return (<Text style={styles.fullDescription}>{selectedStepData.static_text}</Text>)
        }
    }



    renderButtons(){

        var selectedStepData = this.state.steps[this.state.selectedStep]
        if(selectedStepData==null){
            return
        }
        return(<View style={styles.buttonPanel}>
            {this.populateLeftButtons(selectedStepData.content.leftButtons)}
            {this.populateNextButton(selectedStepData)}
            {this.populateRightButtons(selectedStepData.content.rightButtons)}
        </View>)

    }

    renderStepInfo(){

        var selectedStepData = this.state.steps[this.state.selectedStep]
        if(selectedStepData==null){
            return
        }else{
            return(
                <View>
                    {/*<Text style={styles.stepLabel}>{selectedStepData.content.name}</Text>*/}
                    <Text style={styles.shortDesc}>{selectedStepData.content.shortDescription}</Text>
                </View>
            )

        }



    }

    renderView() {

      if(this.state.isLoading === true){
        return this.renderLoaderView()
      }


      return (

        <View style={styles.mainContainer}>
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                <Text style={styles.courseTitle}>{this.state.courseId + " - " + this.state.courseTitle}</Text>

                {this.renderStepInfo()}
                {this.renderStatusBar()}
                {this.renderDescription()}

            </ScrollView>
            {this.renderButtons()}



        </View>
      )

    }
  }



const htmlViewStyles = StyleSheet.create({
    div: {
        marginTop:UIUtils.size(5),
        paddingTop:UIUtils.size(5),
        fontSize:UIUtils.size(15)
    },
    p: {
        fontSize:UIUtils.size(15),
        lineHeight:UIUtils.size(16)
    },
    a:{
        color:'blue',
        fontSize:UIUtils.size(15),
        lineHeight:UIUtils.size(12)
    },
    ul:{
        lineHeight:UIUtils.size(12)
    }
});


  const styles = StyleSheet.create({
    mainContainer: {
      flex: 1,
      justifyContent: 'center'
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    },


      courseTitle:{

        margin:UIUtils.size(15),
          marginTop:UIUtils.size(20),
          fontSize:UIUtils.size(17),
          fontWeight:'bold',
          textAlign:'center'
    },
      stepLabel:{
        marginTop:UIUtils.size(15),
          marginLeft:UIUtils.size(20),
          fontSize:UIUtils.size(15),
          fontWeight:'bold'
      },
      shortDesc:{
          marginTop:UIUtils.size(15),
          marginBottom:UIUtils.size(15),
          marginLeft:UIUtils.size(20),
          marginRight:UIUtils.size(20),
          fontSize:UIUtils.size(15)
      },
      statusBar:{
          height:100,
          width:300,
          backgroundColor:'grey'
      },
      fullDescription:{
          marginTop:UIUtils.size(15),
          marginBottom:UIUtils.size(15),
          marginLeft:UIUtils.size(20),
          marginRight:UIUtils.size(20),
          flex:1
      },
      buttonPanel:{
          flexDirection:'row'
      },
      leftButtonPanel:{
          flex:1,
          padding:UIUtils.size(5),
          paddingLeft:UIUtils.size(10),
          alignItems:'flex-start'
      },
      helpBtnIcon:{
          width:UIUtils.size(15),
          height:UIUtils.size(15),
          marginRight:UIUtils.size(5),
          marginTop:UIUtils.size(1)
      },
      rightButtonPanel:{
          flex:1,
          padding:UIUtils.size(5),
          paddingRight:UIUtils.size(10),
          alignItems:'flex-end'
      },
      middleButtonPanel:{
          flex:0.4,
          padding:UIUtils.size(5),
          alignItems:'center'
      },
      nextButton:{
          justifyContent:'center',
          alignItems:'center',
          flexDirection:'row',
          backgroundColor:"#0051BD",
          paddingTop:UIUtils.size(5),
          paddingBottom:UIUtils.size(5),
          marginBottom:UIUtils.size(5),
          width:UIUtils.size(60)
      },
      helpButton:{
          flexDirection:'row',
          justifyContent:'center',
          alignItems:'center',
          backgroundColor:"#FF9A02",
          paddingTop:UIUtils.size(5),
          paddingBottom:UIUtils.size(5),
          paddingLeft:UIUtils.size(8),
          paddingRight:UIUtils.size(8),
          marginBottom:UIUtils.size(5)
      },
      actionButton:{
          justifyContent:'center',
          alignItems:'center',
          flexDirection:'row',
          backgroundColor:"#0051BD",
          paddingTop:UIUtils.size(5),
          paddingBottom:UIUtils.size(5),
          paddingLeft:UIUtils.size(8),
          paddingRight:UIUtils.size(8),
          marginBottom:UIUtils.size(15)
      },
      buttonText:{
          color:"white",
          fontSize:UIUtils.size(13),
          fontWeight:'bold'
      },

      helpBtnContent:{
          flexDirection:'row'
      }

  });
