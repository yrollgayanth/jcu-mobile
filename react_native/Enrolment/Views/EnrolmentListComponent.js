/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import {Platform, StyleSheet,Text,View,ScrollView,Image,TouchableHighlight, ActivityIndicator, AsyncStorage } from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import UIUtils from '../../Utils/UIUtils'
import {COLOR, STORAGE_KEY, DEFAULT_FILES, EVENT} from '../../Constants/Constants';
import WebEngine from '../../Utils/WebEngine'
import EventEmitter from 'react-native-eventemitter'
import { NativeEventEmitter, NativeModules } from 'react-native';
import AuthTokens from '../../Objects/AuthTokens';
import LocalStorageManager from '../../Utils/LocalStorageManager';
import EnrolmentStatusWidget from '../../Widgets/EnrolmentStatus/EnrolmentStatusWidget';

export default class EnrolmentListComponent extends BaseViewComponent {

  _mounted = true
  constructor(props){

      super(props);
      this.state= {
        enableNavigationBar: true,
        navigationBarConfig: {
          title: "Enrolment",
          showHome: true,
          showAlert:true,
          showMenu:true
        },
        isLoading:true,
        enrolmentList:[]
      }
  }


  //list of enrolment progress
  async fetchEnrolmentList(){

      this.setState({isLoading:true})

      // var enrolJSON = require('../../SampleJSON/EnrolmentJSON')
      // console.log("enrolJSON",enrolJSON.enrolment_details)

      LocalStorageManager.getSavedAuthTokens((result)=>{
          var authTokens = result;
          var studentId = result.getStudentID()
          let webEngine = WebEngine.sharedInstance
          Promise.all([webEngine.fetchEnrolmentList(studentId)]).then(res => {
              this.setState({enrolmentList:res[0].enrolment_details})
              this.setState({isLoading:false})
          })

      },()=>{
          alert("Failed to get studentID")
      })
  }

  async componentWillMount(){
    this._mounted = true
    this.fetchEnrolmentList()
  }


  componentWillUnmount(){
      this._mounted = false
  }

  renderLoaderView(){
      return(
        <View style={styles.mainContainer}>
            <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
        </View>
      )
  }

  tappedOnCourse(courseID){
      //go to enrolment progress page for course
      this.props.goEnrolmentProgress(courseID);
  }

  tapCallback(data){
      // console.log("tapCallback",data)
      this.props.goEnrolmentProgress(data.courseId);
  }

  renderView() {

    if(this.state.isLoading === true){
      return this.renderLoaderView()
    }

    return (

      <View style={styles.mainContainer}>
          <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
              <EnrolmentStatusWidget tapCallback={this.tapCallback.bind(this)} showCourseTitle={true} enrolmentData={this.state.enrolmentList}></EnrolmentStatusWidget>
          </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});
