/**
* Created by Yrol on 05/02/18.
*/

import React, { Component } from 'react'
import {Platform, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, ScrollView } from 'react-native'
import HTMLView from 'react-native-htmlview'
import UIUtils from '../../Utils/UIUtils'
import {EVENT} from '../../Constants/Constants'
import EventEmitter from "react-native-eventemitter"

const PLAN_YOUR_STUDY = "PLAN-YOUR-STUDY";

export default class EnrolmenntDescription extends Component<{}> {

  constructor(prop){
    super(prop)
    this.state = {
      steps:this.props.stepDescription
    }

    this.openWebPage = this.openWebPage.bind(this)
  }

  componentWillMount(){}

  //open web page
  openWebPage(url){
    EventEmitter.emit(EVENT.OPEN_WEBPAGE, {link:url})
  }

  componentWillReceiveProps(newProps){
    this.setState({steps:newProps.stepDescription})
  }

  render(){
    var selectedStepData = this.state.steps
    if(selectedStepData==null){
        return
    }

    if (selectedStepData.is_mandatory || selectedStepData.content.id === PLAN_YOUR_STUDY) {
        const htmlContent = '<div>'+selectedStepData.content.description+'</div>';
        return (
            <View style={styles.fullDescription}>
                <HTMLView
                    stylesheet={htmlViewStyles}
                    value={htmlContent}
                    onLinkPress={(url) => this.openWebPage(url)}
                />
            </View>
        )


    } else {
        return (<Text style={styles.fullDescription}>{selectedStepData.static_text}</Text>)
    }
  }
}


const styles = StyleSheet.create({
  fullDescription:{
      marginTop:UIUtils.size(15),
      marginBottom:UIUtils.size(15),
      marginLeft:UIUtils.size(20),
      marginRight:UIUtils.size(20),
      flex:1
  }
})

const htmlViewStyles = StyleSheet.create({
  div: {
      marginTop:UIUtils.size(5),
      paddingTop:UIUtils.size(5),
      fontSize:UIUtils.size(15)
  },
  p: {
      fontSize:UIUtils.size(15),
      lineHeight:UIUtils.size(16)
  },
  a:{
      color:'blue',
      fontSize:UIUtils.size(15),
      lineHeight:UIUtils.size(12)
  },
  ul:{
      lineHeight:UIUtils.size(12)
  }
});
