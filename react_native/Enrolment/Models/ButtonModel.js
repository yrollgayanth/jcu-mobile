/**
* Created by Yrol on 14/03/18.
*/

export default class ButtonModel {

  buttonName:string
  buttonType:string
  buttonFunc:string
  buttonDesc:string

  constructor(buttonName, buttonType, buttonFunc, buttonDesc){
    this.buttonName = buttonName
    this.buttonType = buttonType
    this.buttonFunc = buttonFunc
    this.buttonDesc = buttonDesc
  }
}
