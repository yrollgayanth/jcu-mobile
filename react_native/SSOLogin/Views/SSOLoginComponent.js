/**
* Created by Yrol on 01/02/18.
*/
import React, { Component } from 'react';
import EventEmitter from "react-native-eventemitter";
import { DeviceEventEmitter, TouchableHighlight, StyleSheet, Text, View, TextInput, TouchableOpacity, Alert, ImageBackground, Image, WebView, ActivityIndicator } from 'react-native';
import  UIUtils from '../../Utils/UIUtils';
import { EVENT,DEFAULT_FILES } from '../../Constants/Constants';
import LocalStorageManager from '../../Utils/LocalStorageManager';
import AuthTokens from '../../Objects/AuthTokens';
import ConfigUtils from  '../../Utils/ConfigUtils';
var jwtDecode = require('jwt-decode');

export default class SSOLoginComponent extends Component<{}> {

  _device_width = UIUtils.getScreenWidth()
  _header_height = UIUtils.navBarheight() + 5

  constructor( props ) {
      super( props );
      this.webView = null;
  }

  onMessage( event ) {
      console.log("SSOLoginComponent")
      // console.log(event.nativeEvent.data);
      var cookie = decodeURIComponent(event.nativeEvent.data).split(';');
      console.log("cookie",cookie);

      var jcu_token

      for(var param of cookie){
          if(param.substring("jcu_token")!=-1){

              jcu_token = cookie[0].split('=')[1];
              break;
          }
      }

      console.log("jcu_token",jcu_token);

      var jsonToken = JSON.parse(jcu_token);

      console.log("jsonToken",jsonToken);

      var authTokens = new AuthTokens(jsonToken);

      // console.log("authTokens",authTokens);
      // console.log("access_token:",authTokens.access_token);
      // console.log("refresh_token:",authTokens.refresh_token);
      // console.log("id_token:",authTokens.id_token);
      // console.log("expires_at", authTokens.expires_at);

      LocalStorageManager.saveAuthTokens(authTokens,()=>{
          DeviceEventEmitter.emit(EVENT.LOGIN_STATUS, {status:true});
      },(error)=>{
          alert("faled to save");
      })

  }

    injectjs() {

        let jsCode = `
        var myInterval =  setInterval(() => {
            var element = document.getElementById('jcu_token');
            if(element!=null){
              window.postMessage(element.value, "*");
              clearInterval(myInterval);
            }
          }, 500)
    `;

        return jsCode;

    }

    componentDidMount(){

        this.webView.reload();

    }


  render() {
      return (
          <View style={{flex: 1}}>
              <WebView
                  style={{flex: 1}}
                  source={{uri: ConfigUtils.SSO_URL}}
                  ref={( webView ) => {
                      this.webView = webView}}
                  injectedJavaScript={this.injectjs()}
                  javaScriptEnabled={true}
                  onMessage={this.onMessage}
              />
          </View>
      );
  }


}

//on button press login
onSignUpButtonPress = (navigateComponent) => {
  //navigateComponent.navigate('StepProcess', {name: 'Lucy'});
};


const styles = StyleSheet.create({
    WebViewStyle:{
      justifyContent: 'center',
      alignItems: 'center',
      flex:1,
    },

    ActivityIndicatorStyle:{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center'

    }
  });
