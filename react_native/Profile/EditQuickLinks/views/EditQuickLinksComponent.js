/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

import BaseViewComponent from '../../../SharedComponents/BaseViewComponent';

export default class EditQuickLinksComponent extends BaseViewComponent {

  constructor(props){
    super(props);
    this.state = {
      enableNavigationBar: true,
      navigationBarConfig: {
        title:"Quick Links",
        showBack:true,
        showAlert: true,
        showMenu: true,
        showEmail: false
      }
    }
  }

  renderView() {
    return (
      <View style={styles.mainContainer}>
        <Text>edit quick links</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
