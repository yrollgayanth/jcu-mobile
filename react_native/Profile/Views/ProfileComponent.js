/**
* Created by Sylvia on 30/11/17.
*/
import React, { Component } from 'react';
import {StyleSheet, Alert, Text, View, ScrollView, Image, RefreshControl, TouchableHighlight, TouchableOpacity, ActivityIndicator, AsyncStorage, Platform } from 'react-native';
import BaseViewComponent from '../../SharedComponents/BaseViewComponent';
import UIUtils from '../../Utils/UIUtils'
import Branding from '../../Utils/Branding'
import LocalStorageManager from '../../Utils/LocalStorageManager'
import BrandingEngine from '../../Utils/BrandingEngine'
import BrowserUtils from '../../Utils/BrowserUtils'
import {COLOR, STORAGE_KEY, DEFAULT_FILES, EVENT} from '../../Constants/Constants';
import CommonListView from '../../SharedComponents/CommonListView';
import GreyDividerComponent from '../../SharedComponents/GreyDividerComponent';
import MenuItemAsyncIconComponent from '../../SharedComponents/MenuItem/views/MenuItemAsyncIconComponent';
import MenuItemComponent from '../../SharedComponents/MenuItem/views/MenuItemComponent';
import MenuOptionAsyncIconModel from '../../SharedComponents/MenuItem/models/MenuItemAsyncIconModel';
import MenuOptionModel from '../../SharedComponents/MenuItem/models/MenuItemModel';
import VerticalMarginComponent from '../../SharedComponents/VerticalMarginComponent'
import ImagePickerManager from 'react-native-image-picker'
import RNFS from'react-native-fs'
import EventEmitter from "react-native-eventemitter"
import WebEngine from '../../Utils/WebEngine'
import RNFetchBlob from 'react-native-fetch-blob'
import ImageResizer from 'react-native-image-resizer'
import ConfigUtils from '../../Utils/ConfigUtils'


const mockOptionData = {

  "page_title":"My Profile",
  "items":[
    {
      "id":1,
      "title":"Add/ modify quick links",
      "icon":"https://d3l077x3t9qdee.cloudfront.net/images/UAT/mobile/connexus/support/ic_gettting_started_at_uni.png",
      "link":"http://www.latrobe.edu.au/students/wellbeing/studying-with-a-disability",
      "link_type":"external"
    }
  ]};

  _imageBase64 = null

export default class ProfileComponent extends BaseViewComponent {
  constructor(props){
    super(props);

    this.state= {
      profileImage:this.initProfileImage(),
      loaded:true,
      firstname:"First Name",
      lastname:"Last Name",
      menuOptions:this.initReferenceMenuOptions(),
      menuLocalIconOptions:this.initMenuOptionsWithLocalIcon(),
      isRefreshing:false,
      enableNavigationBar: true,
      navigationBarConfig: {
        title: mockOptionData.page_title,
        showBack: true,
      }
    }
    this.renderMenuItemWithLocalIcon = this.renderMenuItemWithLocalIcon.bind(this)
    this.renderMenuItem = this.renderMenuItem.bind(this)
    this.onRefresh = this.onRefresh.bind(this)
  }

  async componentWillMount(){
    this.initiateProfileFetch()

    //google analytics
    UIUtils.sendAnalytics("Profile")
  }

  onRefresh(){
    this.setState({isRefreshing: false})
    this.initiateProfileFetch()
  }


  //initiate fetch data with token refresh
  async initiateProfileFetch(){
    let webEngine = WebEngine.sharedInstance
    var branding = new BrandingEngine()
    var shouldUpdateToken = false
    var existingUserToken = ""
    var newUserToken = ""

    await Promise.all([branding.tokenExpiryForRefresh()]).then(values => {
      existingUserToken = values[0]
      if(UIUtils.isTokenNeedToBeRefreshed(existingUserToken)){
        shouldUpdateToken = true
      }
    })

    //verify if the token needs to be updated
    if(shouldUpdateToken){
      await Promise.all([webEngine.fetchNewRefreshToken(existingUserToken.id_token)]).then(values => {
        if(values[0] === false){
          this.setState({loaded:false})
          this.showMessage("An error occurred while fetching profile data.")
        }else{
          newUserToken = values[0]
          console.log("profile user token:" + JSON.stringify(newUserToken))
          LocalStorageManager.saveAuthTokens(newUserToken,()=>{
            LocalStorageManager.getSavedAuthTokens((savedValues)=>{
              ConfigUtils.id_token = savedValues.id_token
              this.fetchProfileData()
            },(error)=>{
              this.setState({loaded:false})
              this.showMessage("An error occurred while fetching profile data.")
            })
          }),(error)=>{
            this.setState({loaded:false})
            this.showMessage("An error occurred while fetching profile data.")
          }
        }
      })
    }else{
      this.fetchProfileData()
    }
  }

  async fetchProfileData(){
    var fetchStatus = false
    var firstname = ""
    var lastname = ""
    var base64ProfileImage = ""
    var branding = Branding.sharedInstance
    let webEngine = WebEngine.sharedInstance
    var canProceed = false
    this.setState({loaded:false})
    await Promise.all([webEngine.fetchProfileData(branding.getStudentID())]).then(values => {
      jsonOutput = values[0]
      if(jsonOutput === false){
        this.setState({loaded:false})
        this.showMessage("An error occurred while fetching profile data.")
      }else{
        firstname = jsonOutput.firstName
        lastname = jsonOutput.lastName

        //check for a valid image
        if(typeof jsonOutput.profilepic === 'undefined' || jsonOutput.profilepic === null || jsonOutput.profilepic === '' || jsonOutput.profilepic === '/assets/images/no-image.jpg'){
          base64ProfileImage = ""
        }else{
          base64ProfileImage =  jsonOutput.profilepic
        }
      }
    })

    await Promise.all([this.saveProfileData(firstname, lastname), this.saveProifleImageLocally(base64ProfileImage)]).then(values => {
      this.setState({
        loaded:true,
        firstname:firstname,
        lastname:lastname,
        profileImage:this.initProfileImage(),
        isRefreshing:true
      })

      var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE

      //set image status
      if(base64ProfileImage === ""){
        branding.setProfileImage("")
      }else{
        branding.setProfileImage(filePath)
      }

      if(firstname === ""){
        branding.setUserFirstname("")
      }else{
        branding.setUserFirstname(firstname)
      }

      if(lastname === ""){
        branding.setUserLastname("")
      }else{
        branding.setUserLastname(lastname)
      }

      EventEmitter.emit(EVENT.PROFILE_PIC_UPDATED, {imagePath:filePath})
    })
  }

  isBase64(str) {
    try {
        return btoa(atob(str)) == str
    } catch (err) {
        return false;
    }
  }

  showMessage(msg){
    Alert.alert('',
    msg,
      [
        {text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ]
    )
  }

  //save profile data
  async saveProfileData(firstname, lastname){
    try{
      await AsyncStorage.setItem(STORAGE_KEY.USER_FIRSTNAME, firstname)
      await AsyncStorage.setItem(STORAGE_KEY.USER_LASTNAME, lastname);
    }catch(error){
      console.log(error.message)
    }
  }

  //initialise profile image
  initProfileImage(){
    var branding = Branding.sharedInstance
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    if(branding.getProfileImage() === ""){
      return ""
    }
    return filePath
  }

  initMenuOptionsWithLocalIcon(){

    var menuOptionIcomNames = [
      require("../../../assets/img/localIcons/ic_quick_links.png"),
      require("../../../assets/img/localIcons/ic_noti_preference.png"),
      require("../../../assets/img/localIcons/ic_club_society.png")];

      var menuOptions = [];
      for(var i=0;i<mockOptionData.items.length;i++){

        var rawModel = mockOptionData.items[i];
        var menuOptionModel = new MenuOptionModel(rawModel.title,menuOptionIcomNames[i]);
        menuOptionModel.key = rawModel.title;
        menuOptions.push(menuOptionModel);
      }
      return menuOptions;
    }

    initReferenceMenuOptions(){
      var menuOptions = [];
      for(var i=0;i<mockOptionData.items.length;i++){

        var rawModel = mockOptionData.items[i];
        var menuOptionModel = new MenuOptionAsyncIconModel(rawModel.title,rawModel.icon);
        menuOptionModel.key = rawModel.title;
        menuOptions.push(menuOptionModel);
      }
      return menuOptions;
    }

    async tappedOnProfileImageOrCamera(){
      var options = {
        title: 'Select profile picture',
        customButtons: [],
        storageOptions: {
          skipBackup: true,
          path: 'images'
        }
      };

      ImagePickerManager.showImagePicker(options, (response) => {
        if (response.didCancel) {
          console.log('User cancelled image picker');
        }
        else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        }
        else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        }else {
          //let source = { uri: response.uri };
          // You can also display the image using data:
          //let source = { uri: 'data:image/jpeg;base64,' + response.data }

          //disable navigation back while uploading
          let backButtonStatus = this.state.navigationBarConfig
          backButtonStatus.showBack = false
          this.setState({loaded:false, navigationBarConfig:backButtonStatus})
          var imageUri = response.uri
          var imagePath = response.path

          if(Platform.OS !== "ios"){
            //this.uploadImageAndroidBase64(response.data)
            this.resizeImageAndroid(imagePath)
          }else{
            this.resizeImageiOS(imageUri)
          }
        }
      })
    }

    async resizeImageAndroid(imageUri){
      let imageUriModified = imageUri
      await ImageResizer.createResizedImage(imageUriModified, 800, 600, 'PNG', 80, 0).then((response) => {
        this.processResizedImage(response.uri)
      }).catch((err) => {
        let backButtonStatus = this.state.navigationBarConfig
        backButtonStatus.showBack = true
        this.setState({loaded:true, navigationBarConfig:backButtonStatus})
        this.showMessage(err)
      })
    }

    async resizeImageiOS(imageUri){
      await ImageResizer.createResizedImage(imageUri, 800, 600, 'PNG', 80)
      .then((response) => {
        this.processResizedImage(response.uri)
      }).catch((err) => {
        let backButtonStatus = this.state.navigationBarConfig
        backButtonStatus.showBack = true
        this.setState({loaded:true, navigationBarConfig:backButtonStatus})
        this.showMessage(err)
      })
    }

    //save locally and upload to webserver
    async processResizedImage(imageUri){
      let webEngine = WebEngine.sharedInstance
      let branding = Branding.sharedInstance
      var imageReadSate = false
      var imageBase64Data = ""
      await RNFS.readFile(imageUri, 'base64').then((response) => {
        imageReadSate = true
        imageBase64Data = 'data:image/png;base64,' + response
      }).catch((error) => {
        imageReadSate = false
        imageBase64Data = ""
      })

      //process further
      if(imageReadSate === false || imageBase64Data === ""){
        this.showMessage("An error occurred while processing the image")
      }else{
        var jsonImageObj = {
              "student_id" : branding.getStudentID(),
              "profile_pic" :imageBase64Data
            }
        await Promise.all([
          webEngine.uploadProfileImage(JSON.stringify(jsonImageObj)),
          this.saveProifleImageLocally(imageBase64Data)
        ]).then(values => {
          if(values.indexOf(false) === 0){ //check if false returned by above operations
            this.showMessage("An error occurred while uploading the image")
          }else{
            var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
            branding.setProfileImage(filePath)
            this.setState({
              profileImage:this.initProfileImage()
            })
            EventEmitter.emit(EVENT.PROFILE_PIC_UPDATED, {imagePath:filePath})
            this.showMessage("Profile image updated successfully")
          }
        })

        //set status back to default
        let backButtonStatus = this.state.navigationBarConfig
        backButtonStatus.showBack = true
        this.setState({loaded:true, navigationBarConfig:backButtonStatus})
      }
    }

    //base64 android
    async uploadImageAndroidBase64(imageBase64Data){
      let webEngine = WebEngine.sharedInstance
      let branding = Branding.sharedInstance
      let imageTobase64 = 'data:image/png;base64,' + imageBase64Data

      var jsonImageObj = {
            "student_id" : branding.getStudentID(),
            "profile_pic" :imageTobase64
          }

          await Promise.all([
            webEngine.uploadProfileImage(JSON.stringify(jsonImageObj)),
            this.saveProifleImageLocally(imageTobase64)
          ]).then(values => {
            if(values.indexOf(false) === 0){ //check if false returned by above operations
              this.showMessage("An error occurred while uploading the image")
            }else{
              var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
              branding.setProfileImage(filePath)
              this.setState({
                profileImage:this.initProfileImage()
              })
              EventEmitter.emit(EVENT.PROFILE_PIC_UPDATED, {imagePath:filePath})
              this.showMessage("Profile image updated successfully")
            }
          })

          //set status back to default
          let backButtonStatus = this.state.navigationBarConfig
          backButtonStatus.showBack = true
          this.setState({loaded:true, navigationBarConfig:backButtonStatus})
    }

    async saveProifleImageLocally(imageData){
      var base64ImageData = imageData
      if(typeof imageData === 'undefined' || imageData === null || imageData === '' || imageData === '/assets/images/no-image.jpg'){
        var defaultImageAssets = require("../../../assets/img/base64/default_profile_base64.json")
        defaultImageAssets = defaultImageAssets.split('data:image/png;base64,')
        base64ImageData = defaultImageAssets[1]
      }
      base64ImageData =  base64ImageData.split('data:image/png;base64,')//split base64 string
      if(base64ImageData.length === 1){
        base64ImageData = base64ImageData[0]
      }else{
        base64ImageData = base64ImageData[1]
      }
      var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
      return RNFS.writeFile(filePath, base64ImageData, 'base64')
      .then((success) => true)
      .catch((err) => false)
    }

    // async saveProifleImageLocally(imageData){
    //   var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    //   return RNFS.writeFile(filePath, imageData, 'base64').then((success) => {
    //     this.setState({
    //       profileImage:this.initProfileImage()
    //     })
    //     EventEmitter.emit(EVENT.PROFILE_PIC_UPDATED, {imagePath:filePath})
    //   }).catch((error) => {
    //     console.log("default pic save error: " + error.message);
    //   });
    // }

    renderProfileImage(){
      let randomString = Math.random().toString(36).slice(2)
      var defaultImage = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE + "?" + randomString
      var defaultImageAssets = require("../../../assets/img/base64/default_profile_base64.json")
      if(this.state.profileImage !== ""){
        return(<Image style={styles.profileImage} source={{uri:'file://'+defaultImage}}></Image>);
      }else{
        return(<Image style={styles.profileImage} source={{uri:defaultImageAssets}}></Image>);
      }
    }

    renderLoaderView(){
      return(
        <View style={[styles.mainContainer, styles.horizontal]}>
        <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
        </View>
      )
    }

    renderProfilePanel(){
      return(
        <View style={styles.profileContainer}>
        <GreyDividerComponent></GreyDividerComponent>
        <View style={styles.whitePanel}>
        <View style={styles.namePanel}>
        <Text style={styles.firstName}>{this.state.firstname}</Text>
        <GreyDividerComponent></GreyDividerComponent>
        <Text style={styles.lastName}>{this.state.lastname}</Text>
        </View>
        </View>
        <GreyDividerComponent></GreyDividerComponent>
        <TouchableHighlight style={styles.imageWrapper} underlayColor="transparent" onPress={() => this.tappedOnProfileImageOrCamera()}>
        <View >

        {this.renderProfileImage()}
        <Image style={styles.cameraIcon} source={{uri:require('../../../assets/img/base64/ic_camera_white.json')}}></Image>
        </View>
        </TouchableHighlight>

        </View>
      );
    }

    tappedOnMenuOption(menuOptionModel){
      if(menuOptionModel.menuOptionName === mockOptionData.items[0].title){
        this.props.navigation.navigate("EditQuickLinks", {showCustomNavigation:true});
      }else if(menuOptionModel.menuOptionName === mockOptionData.items[1].title){
        this.props.navigation.navigate("NotiPreference");
      }else if(menuOptionModel.menuOptionName === mockOptionData.items[2].title){
        this.props.navigation.navigate("ClubPreference");
      }
    }

    renderMenuItem(menuOptionModel) {
      return (<View key={menuOptionModel.menuOptionName}>
        <VerticalMarginComponent height={15}></VerticalMarginComponent>
        <TouchableHighlight underlayColor="transparent"
        onPress={() => this.tappedOnMenuOption(menuOptionModel)}>
        <MenuItemAsyncIconComponent menuOptionModel={menuOptionModel}></MenuItemAsyncIconComponent>
        </TouchableHighlight>
        </View>
      );
    }

    renderMenuItemWithLocalIcon(menuOptionModel) {
      return (
        <TouchableHighlight underlayColor="transparent"
        onPress={() => this.tappedOnMenuOption(menuOptionModel)}>
        <View><VerticalMarginComponent height={15}></VerticalMarginComponent>
        <MenuItemComponent menuOptionModel={menuOptionModel}></MenuItemComponent>
        </View>
        </TouchableHighlight>
      );
    }

    openQuicklink(){
      this.props.navigation.navigate("EditQuickLinks", {showCustomNavigation:true});
    }

    renderMenuList(){
      return(
        //<CommonListView models={this.state.menuOptions} renderItem={this.renderMenuItem}></CommonListView>
        //<CommonListView models={this.state.menuLocalIconOptions} renderItem={this.renderMenuItemWithLocalIcon}></CommonListView>
        <TouchableOpacity onPress={() => this.openQuicklink()}  style={{flex: 1, justifyContent:'center', backgroundColor:"white", height:40 }}><Text style={{marginLeft:10}}>{"Add / modify quick links"}</Text></TouchableOpacity>
      );
    }

    renderBottomSpace(){
      return(
        <View style={{height:UIUtils.size(60),width:UIUtils.size(1)}}></View>
      );
    }

    //open name change link
    openNameChange(){
      BrowserUtils.openBrowser("Name change","https://jcu.custhelp.com/app/answers/detail/a_id/563/kw/change%20name")
    }



    renderView() {
      if(this.state.loaded === false){
        return this.renderLoaderView()
      }

      return (
        <View style={styles.mainContainer}>
        <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
        <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={styles.topNote}>{"Name changes can be made "}</Text>
            <TouchableOpacity onPress={() => this.openNameChange()}><Text style={styles.topNoteLink}>{"here "}</Text></TouchableOpacity>
        </View>
        {this.renderProfilePanel()}
        {this.renderMenuList()}
        {this.renderBottomSpace()}

        </ScrollView>
        </View>
      )
    }
  }

  const styles = StyleSheet.create({
    mainContainer: {
      flex: 1,
      justifyContent: 'center'

    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    },
    topNote:{
      marginLeft:UIUtils.size(15),
      marginTop:UIUtils.size(20),
      color: COLOR.TEXT_GREY
    },
    topNoteLink:{
      marginTop:UIUtils.size(20),
      color: COLOR.TEXT_GREY,
      fontWeight:"bold"
    },
    profileContainer:{
      marginBottom:UIUtils.size(30),
      marginTop:UIUtils.size(50)
    },
    whitePanel:{
      flexDirection:'row',
      height:UIUtils.size(100),
      backgroundColor:'white'
    },
    namePanel:{
      marginLeft:UIUtils.size(150),
      flex:1,
      justifyContent: 'center',
    },
    firstName:{
      marginBottom:UIUtils.size(10),
      color:COLOR.TEXT_GREY,
      fontSize:UIUtils.size(18),
      fontWeight: '600'
    },
    lastName:{
      marginTop:UIUtils.size(10),
      color:COLOR.TEXT_GREY,
      fontSize:UIUtils.size(18),
      fontWeight: '600'
    },
    imageWrapper:{
      position:'absolute',
      left:UIUtils.size(20),
      top:UIUtils.size(-20),
      // backgroundColor:'red',
      height:UIUtils.size(100),
      width:UIUtils.size(100)
    },
    profileImage:{
      backgroundColor:'white',
      resizeMode:'cover',
      borderRadius: UIUtils.size(50),
      borderColor:COLOR.LINE_GREY,
      borderWidth:UIUtils.size(0.5),
      height:UIUtils.size(100),
      width:UIUtils.size(100)
    },
    cameraIcon:{
      position:'absolute',
      right:0,
      bottom:0,
      height:UIUtils.size(25),
      width:UIUtils.size(25),
      backgroundColor:"#999999",
      borderRadius: UIUtils.size(12.5)
    }
  });
