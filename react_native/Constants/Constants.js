export default class Constants{
  //    event identity

}

module.exports = {

    QTAC_ERROR_CODES:{
        QTAC_VERIFY_FAIL:'QTAC_VERIFY_FAIL',
        QTAC_STUDENT_NOT_FOUND:'QTAC_STUDENT_NOT_FOUND',
        ACCOUNT_NOT_FOUND:'ACCOUNT_NOT_FOUND',
        PASSWORD_SET_FAIL:'PASSWORD_SET_FAIL'
    },
    QTAC_ERROR_MESSAGE:{
        QTAC_VERIFY_FAIL: '<p>Unable to validate QTAC link.  Please contact ICT Helpdesk:  <a href="mailto:ithelpdesk@jcu.edu.au" target="_top">ithelpdesk@jcu.edu.au</a></p>',
        QTAC_STUDENT_NOT_FOUND: '<p>Congratulations on your offer!<br>We are currently processing your offer and will email you shortly with instructions on how to activate your account details.<br>In the meantime, we encourage you to explore our GetReady4Uni page with lots of useful information to help you get prepared for study at JCU.<br>If you have any questions, or you have already received your email from JCU and are still seeing this message, then please contact <a href="mailto:enquiries@jcu.edu.au" target="_top">enquiries@jcu.edu.au</a> or phone <a href="tel:1800246446" target="_top">1800 246 446</a> so that we can assist you.',
        ACCOUNT_NOT_FOUND: '<p>We are unable to identify you with the information you have entered.Please contact <a href="mailto:ithelpdesk@jcu.edu.au" target="_top">ithelpdesk@jcu.edu.au</a></p>',
        PASSWORD_SET_FAIL: '<p>Unable to set your password.  Please  contact ICT Helpdesk:  <a href="mailto:ithelpdesk@jcu.edu.au" target="_top">ithelpdesk@jcu.edu.au</a></p>'
    },

  API_ENDPOINTS:{
    BASE_API:"https://bayshann.connexus.online:3000/mobile/getPageLinksById"
  },

  STORAGE_KEY:{
    FIRST_LAUNCH:"first_launch",
    PROFILE_PIC_CHANGED:"profile_pic_changed",
    CURRENT_BRANDING_VERSION:"current_branding_version",
    USER_FIRSTNAME:"user_firstname",
    USER_LASTNAME:"user_lastname"
  },

  EVENT:{
      GO_NOTIFICATIONS:"go to notifications",
      GO_HOME:"go home",
      SHOW_LOADING_VIEW:"show loading view",
      HIDE_LOADING_VIEW:"hide loading view",
      POP_SINGLE_BUTTON_DIALOG:"pop single button dialog",
      POP_TWO_BUTTON_DIALOG:"pop two button dialog",
      LOGIN_STATUS:"login_status",
      OPEN_DRAWER:"open_drawer",
      CLOSE_DRAWER:"close_drawer",
      GO_STEP_PROCESS:"go_step_process",
      UPDATE_UNREAD_NOTI:"update unread noti",
      UPDATE_HIGHLIGHT_ON_SIDE_MENU:'update highlight on side menu',
      GO_FROM_SIDE_MENU:'go from side menu',
      FEEDBACK_PROBLEM:"submit_problem",
      FEEDBACK_SUGGESTION:"submit_suggestion",
      PROFILE_PIC_UPDATED:"profile_pic_updated",
      SHOW_ALL_NEWS:"show_all_news",
      HOME_TOP_BAR:"home_top_bar",
      QUIT_WEBPAGE:'quit webpage',
      OPEN_WEBPAGE:'open_webpage',
      OPEN_ENROLMENT:"Open enrolment",//enrolment internal link
      OPEN_NEWS_ITEM:"open_news_item",
      OPEN_ALL_NEWS:"open_all_news",
      OPEN_QUICK_LINK:"open_quick_link",
      OPEN_ALL_QUICK_LINK:"open_all_quick_link",
      QUICKLINK_TOPBAR:"quicklink_top_bar",
      QUICKLINK_ORDER_CHANGED:"quicklink_order_changed",
      SHOW_QUICKLINK_EDIT_BUTTONS:"show_quicklink_edit_buttons",
      QUICKLINK_DELETE_ROW:"quicklink_delete_row",
      QUICKLINK_EDIT_ROW:"quicklink_edit_row",
      QUICKLINK_REFRESH_HOME:"quicklink_refresh_home",
      FINISHED_GET_STARTED:"FINISHED_GET_STARTED",
      ON_RETRIEVE_QTAC_PARAMS:"on retrieve qtac params",
      TOKEN_EXPIRE_LOGOUT:"token_expire_logout"
  },

  TOP_BAR_OPTIONS:{
    QUICKLINK_EDIT:"Edit",
    QUICKLINK_ADD:"Add"
  },

  SUPPORT_EVENTS:{
    OPEN_EXTERNAL:"open_external_link",
    OPEN_INTERNAL:"open_internal_link",
    OPEN_PHONE:"open_phone_support",
    OPEN_WIDGET:"open_widget_support",
    OPEN_TEXT:"open_text_support"
  },

  SETTINGS_EVENTS:{
    OPEN_EXTERNAL:"open_settings_external_link",
    OPEN_INTERNAL:"open__settings_internal_link",
  },


  UNI_LIFE_EVENTS:{
    OPEN_EXTERNAL:"open_external_link",
    OPEN_INTERNAL:"open_internal_link",
    OPEN_PHONE:"open_phone_uni_life",
    OPEN_WIDGET:"open_widget_uni_life",
    OPEN_TEXT:"open_text_uni_life"
  },

  MY_STUDY_EVENTS:{
    OPEN_EXTERNAL:"open_external_link",
    OPEN_INTERNAL:"open_internal_link",
    OPEN_PHONE:"open_phone",
    OPEN_WIDGET:"open_widget",
    OPEN_TEXT:"open_text"
  },

  PLATFORM:{
    IOS:"ios",
    ANDROID:"android"
  },

  DEFAULT_FILES:{
    DEFAULT_BACKGROUND_IMAGE:"default_background_image.png",
    DEFAULT_LOGO_IMAGE:"default_logo_image.png",
    DEFAULT_PROFILE_IMAGE:"default_profile_image.png",
    DEFAULT_BRANDING_JSON:"default_branding_json.json",
    DEFAULT_SUPPORT_JSON:"default_support_json.json"
  },

  COLOR: {
    APP_THEME_BLUE: '#0066b3',
    BG_GREY:'#EFEFEF',
    EMPTY_HEADER_COLOR:"#e9e9ef",
    NAV_GREY:'#F5F8FA',
    SECTION_DARK_GREY:'#414141',
    TEXT_DESC_GREY:'#686868',
    LINE_GREY:'#D3D3D3',
    TEXT_GREY:'#666666',
    WHITE:'#ffffff',
    GRADIENT_DARK_BLUE:'#143f81',
    GRADIENT_LIGHT_BLUE:'#0086c3'
  },

  WEB_REQUEST_STATUS:{
    SUPPORT_SUCCESS:"support_success",
    SUPPORT_FAILURE:"support_failure"
  },

  DRAWER_MENU:{
    ENROLL_ITEM:"Enrolment",
    ABOUT_US_ITEM:"About US",
    SETTINGS_ITEM:"Settings",
    TERMS_ITEM:"Terms and Conditions",
    HOME_ITEM:"Home",
    MY_STUDY_ITEM:"My Study",
    MY_TOOLS_ITEM:"Settings",
    UNI_LIFE_ITEM:"Uni Life",
    SUPPORT_ITEM:"Support",
    NOTIFICATION_ITEM:"Notifications",
    FEEDBACK_ITEM:"App Feedback",
    LOGOUT_ITEM:"Logout",
    OPEN_DRAWER_PROFILE:"Open_drawer_profile"
  },

  LINK_TYPES:{
    EXTERNAL:"external",
    INTERNAL:"internal"
  },

  FONT_SIZE:{
    MENU_ITEM_TEXT_FONT_SIZE:14
  },

  ENCRYPTION:{
    ENCRYPTION_KEY:'b@ysh@nn'
  },

  LAYOUT_SPACE:{
    MENU_ITEM_HEIGHT:44,
    SECTION_HEADER_HEIGHT:35,
    STANDARD_MENU_MARGIN:15,
    STANDARD_MARGIN_MID:20,
    STANDARD_MARGIN_SMALL:10,
    STANDARD_ICON_SIZE_MID:20,
    STANDARD_ARROR_SIZE_HEIGHT:12,
    STANDARD_ARROR_SIZE_WIDTH:8,
    NAVBAR_ICON_HEIGHT:22,
  }
};
