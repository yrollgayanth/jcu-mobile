/**
* Created by Yrol on 14/03/18.
*/

export default class EnrolmentObject {
  course_code:string
  course_name:string
  course_id:string
  steps = []

  constructor(courseCode, courseName, courseID) {
    this.course_code = courseCode
    this.course_name = courseName
    this.course_id = courseID
  }

  addStep(courseStep){
    this.steps.push(courseStep)//adding StepObject
  }

  getSteps(){
    return this.steps
  }
}
