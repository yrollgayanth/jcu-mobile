/**
* Created by Yrol on 20/02/18.
*/


export default class EventObject {
  id: string
  title: string
  text: string
  url: string
  creation_date: string
  start_date: string
  expiry_date: string
  image:string

  constructor(newsID, title, text, url, createDate, startDate, expiryDate, imageBase64) {
    this.id = newsID
    this.title = title
    this.text = text
    this.url = url
    this.creation_date = createDate
    this.start_date = startDate
    this.expiry_date = expiryDate
    this.image = imageBase64
  }
}
