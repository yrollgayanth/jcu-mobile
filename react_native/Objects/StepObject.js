/**
* Created by Yrol on 14/03/18.
*/

export default class StepObject {
  step_key = ""
  step_name = ""
  image_base64 = ""
  complete_status = false
  available_status = false
  current_status = false
  courseCode = ""
  courseName = ""
  courseID = ""
  requires = []

  constructor(stepKey, stepName, completeStatus, availableStatus, requireObjects, imageBase64, currentStep, courseCode, courseName, courseID){
    this.step_key = stepKey,
    this.step_name = stepName,
    this.complete_status = completeStatus
    this.available_status = availableStatus
    this.requires = requireObjects
    this.image_base64 = imageBase64
    this.current_status = currentStep
    this.courseCode = courseCode
    this.courseName = courseName
    this.courseID = courseID
  }
}
