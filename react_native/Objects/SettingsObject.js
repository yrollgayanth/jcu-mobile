/**
* Created by Yrol on 27/02/18.
*/


export default class SettingsObject {

  id:string
  title:string
  button_type:string
  internal_link:string
  text:string
  icon:string
  external_link:string
  phone_number:string
  country_code:string

  constructor(){}

  //widget type
  initiateWidget(settingID, settingTitle, buttonType){
    this.id = settingID
    this.title = settingTitle
    this.button_type = buttonType
  }

  //initiate text type
  initiateText(settingID, settingTitle, buttonType, settingText){
    this.id = settingID
    this.title = settingTitle
    this.button_type = buttonType
    this.text = settingText
  }

  //initiate internal link type
  initiateInternalLink(settingID, settingTitle, buttonType, internalLink, settingIcon){
    this.id = settingID
    this.title = settingTitle
    this.button_type = buttonType
    this.internal_link = internalLink
    this.icon = settingIcon
  }

  //initiate external link
  initiateExternalLink(settingID, settingTitle, buttonType, externalLink, settingIcon){
    this.id = settingID
    this.title = settingTitle
    this.button_type = buttonType
    this.external_link = externalLink
    this.icon = settingIcon
  }


  //initiate phone
  initiatePhone(settingID, settingTitle, buttonType, countryCode, phoneNumber, settingIcon){
    this.id = settingID
    this.title = settingTitle
    this.button_type = buttonType
    this.phone_number = phoneNumber
    this.country_code = countryCode
    this.icon = settingIcon
  }


  //initiate margin
  initiateMargin(settingID, settingTitle, buttonType){
    this.id = settingID
    this.title = settingTitle
    this.button_type = buttonType
  }
}
