/**
* Created by Yrol on 22/02/18.
*/


export default class SupportObject {
  id:string
  title:string
  button_type:string
  internal_link:string
  text:string
  icon:string
  external_link:string
  phone_number:string
  country_code:string

  constructor(){}

  //widget type
  initiateWidget(supportID, supportTitle, buttonType){
    this.id = supportID
    this.title = supportTitle
    this.button_type = buttonType
  }

  //initiate text type
  initiateText(supportID, supportTitle, buttonType, supportText){
    this.id = supportID
    this.title = supportTitle
    this.button_type = buttonType
    this.text = supportText
  }

  //initiate internal link type
  initiateInternalLink(supportID, supportTitle, buttonType, internalLink, supportIcon){
    this.id = supportID
    this.title = supportTitle
    this.button_type = buttonType
    this.internal_link = internalLink
    this.icon = supportIcon
  }

  //initiate external link
  initiateExternalLink(supportID, supportTitle, buttonType, externalLink, supportIcon){
    this.id = supportID
    this.title = supportTitle
    this.button_type = buttonType
    this.external_link = externalLink
    this.icon = supportIcon
  }


  //initiate phone
  initiatePhone(supportID, supportTitle, buttonType, countryCode, phoneNumber, supportIcon){
    this.id = supportID
    this.title = supportTitle
    this.button_type = buttonType
    this.phone_number = phoneNumber
    this.country_code = countryCode
    this.icon = supportIcon
  }

  //initiate margin
  initiateMargin(supportID, supportTitle, buttonType){
    this.id = supportID
    this.title = supportTitle
    this.button_type = buttonType
  }

}
