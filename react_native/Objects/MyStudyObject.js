/**
* Created by Yrol on 26/02/18.
*/


export default class MyStudyObject {
  id:string
  title:string
  button_type:string
  internal_link:string
  text:string
  icon:string
  external_link:string
  phone_number:string
  country_code:string

  constructor(){}

  initiateWidget(myStudyID, myStudyTitle, buttonType){
    this.id = myStudyID
    this.title = myStudyTitle
    this.button_type = buttonType
  }

  //initiate text type
  initiateText(myStudyID, myStudyTitle, buttonType, myStudyText){
    this.id = myStudyID
    this.title = myStudyTitle
    this.button_type = buttonType
    this.text = myStudyText
  }

  //initiate internal link type
  initiateInternalLink(myStudyID, myStudyTitle, buttonType, internalLink, myStudyIcon){
    this.id = myStudyID
    this.title = myStudyTitle
    this.button_type = buttonType
    this.internal_link = myStudyTitle
    this.icon = myStudyIcon
  }

  //initiate external link
  initiateExternalLink(myStudyID, myStudyTitle, buttonType, externalLink, myStudyIcon){
    this.id = myStudyID
    this.title = myStudyTitle
    this.button_type = buttonType
    this.external_link = externalLink
    this.icon = myStudyIcon
  }


  //initiate phone
  initiatePhone(myStudyID, myStudyTitle, buttonType, countryCode, phoneNumber, myStudyIcon){
    this.id = myStudyID
    this.title = myStudyTitle
    this.button_type = buttonType
    this.phone_number = phoneNumber
    this.country_code = countryCode
    this.icon = myStudyIcon
  }

  initiateMargin(myStudyID, myStudyTitle, buttonType){
    this.id = myStudyID
    this.title = myStudyTitle
    this.button_type = buttonType
  }
}
