/**
* Created by Yrol on 20/02/18.
*/


export default class QuicklinkObject {
  title:string
  url:string

  constructor(title, url) {
    this.title = title
    this.url = url
  }
}
