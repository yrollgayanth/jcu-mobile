/**
* Created by Yrol on 20/02/18.
*/
import { ENCRYPTION } from '../Constants/Constants'

var jwtDecode = require('jwt-decode')

export default class AuthTokens {
  static TOKEN_STORAGE_KEY = 'Auth Tokens Key';

  // access_token;
  // refresh_token;
  id_token;
  expires_at;

  constructor(jsonToken){
    this.expires_at = jsonToken.expires_at
    // var decodedIdToken = jwtDecode(jsonToken.id_token);
    this.id_token = jsonToken.id_token
  }


  checkExpiry(){

    if(new Date().getTime()/1000>this.expires_at){
      return true;
    }else{
      return false;
    }
  }

  getStudentID(){
    var studentInfo = jwtDecode(this.id_token);
    // console.log("studentInfo",studentInfo);
    return studentInfo.jcu_student_id;

  }
}
