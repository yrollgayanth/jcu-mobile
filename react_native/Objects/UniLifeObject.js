/**
* Created by Yrol on 20/02/18.
*/

export default class UniLifeObject {
  id:string
  title:string
  button_type:string
  internal_link:string
  text:string
  icon:string
  external_link:string
  phone_number:string
  country_code:string

  constructor(){}

  //widget type
  initiateWidget(uniLifeID, uniLifeTitle, buttonType){
    this.id = uniLifeID
    this.title = uniLifeTitle
    this.button_type = buttonType
  }

  //initiate text type
  initiateText(uniLifeID, uniLifeTitle, buttonType, uniLifeText){
    this.id = uniLifeID
    this.title = uniLifeTitle
    this.button_type = buttonType
    this.text = uniLifeText
  }

  //initiate internal link type
  initiateInternalLink(uniLifeID, uniLifeTitle, buttonType, internalLink, uniLifeIcon){
    this.id = uniLifeID
    this.title = uniLifeTitle
    this.button_type = buttonType
    this.internal_link = internalLink
    this.icon = uniLifeIcon
  }

  //initiate external link
  initiateExternalLink(uniLifeID, uniLifeTitle, buttonType, externalLink, uniLifeIcon){
    this.id = uniLifeID
    this.title = uniLifeTitle
    this.button_type = buttonType
    this.external_link = externalLink
    this.icon = uniLifeIcon
  }


  //initiate phone
  initiatePhone(uniLifeID, uniLifeTitle, buttonType, countryCode, phoneNumber, uniLifeIcon){
    this.id = uniLifeID
    this.title = uniLifeTitle
    this.button_type = buttonType
    this.phone_number = phoneNumber
    this.country_code = countryCode
    this.icon = uniLifeIcon
  }

  //initiate margin
  initiateMargin(uniLifeID, uniLifeTitle, buttonType){
    this.id = uniLifeID
    this.title = uniLifeTitle
    this.button_type = buttonType
  }
}
