import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  AppState,
  Picker,
  TouchableHighlight
} from 'react-native';

import CommonSwiperComponent from './SharedComponents/CommonSwiperComponent';
import BaseViewComponent from './SharedComponents/BaseViewComponent';

import CommonListView from './SharedComponents/CommonListView';
import CommonSectionListView from './SharedComponents/CommonSectionListView';
import CommonExpandableListView from './SharedComponents/CommonExpandableListView';

import MenuItemComponent from './SharedComponents/MenuItem/views/MenuItemComponent';
import MenuSectionComponent from './SharedComponents/MenuSectionComponent';
import VerticalMarginComponent from './SharedComponents/VerticalMarginComponent';
import MenuItemModel from './SharedComponents/MenuItem/models/MenuItemModel';
import ExpandableSectionComponent from './SharedComponents/ExpandableSectionComponent';
import UIUtils from './Utils/UIUtils';
import {LAYOUT_SPACE} from './Constants/Constants';
import AuthTokens from './Objects/AuthTokens';
import DataUtils from './Utils/DataUtils';

import LocalStorageManager from './Utils/LocalStorageManager';

const SupportOptions = [
  {"MenuOptionName":"My Uni emails"
},
{"MenuOptionName":"News/replace Student id card"
},
{"MenuOptionName":"LMS"
},
{"MenuOptionName":"Campus maps"
},
{"MenuOptionName":"Parking &public transport"
},
{"MenuOptionName":"Academic calendar"
},
{"MenuOptionName":"My Profile"
},
{"MenuOptionName":"Terms & conditions"
}
];

const MyStudyOptions = [

  {"SectionName":"My timetable",
  "OptionGroup":[
    {"MenuOptionName":"Allocate+"},
    {"MenuOptionName":"View my timetable"}
  ]
},
{"SectionName":"Study tools",
"OptionGroup":[
  {"MenuOptionName":"LMS"},
  {"MenuOptionName":"Manage my enrolment"},
  {"MenuOptionName":"Handbook"},
  {"MenuOptionName":"My Library account"}
]
},
{"SectionName":"Oversears study opportunities",
"OptionGroup":[
  {"MenuOptionName":"Oversears study opportunities"}
]
},
{"SectionName":"Academic calendar",
"OptionGroup":[
  {"MenuOptionName":"Academic calendar"}
]
}

];

const MockData = [
  {
    header: {sectionTitle:'Section A',
    isOpen:false
  },
  member: [
    {
      menuOptionName: 'Option A-1',
      content: 'content',
    },
    {
      menuOptionName: 'Option A-2',
      content: 'content',
    }]
  },
  {
    header: {sectionTitle:'Section B',
    isOpen:false
  },
  member: [
    {
      menuOptionName: 'Option B-1',
      content: 'content',
    },
    {
      menuOptionName: 'Option B-2',
      content: 'content',
    }]
  },
  {
    header: {sectionTitle:'Section C',
    isOpen:false
  },
  member: [
    {
      menuOptionName: 'Option C-1',
      content: 'content',
    },
    {
      menuOptionName: 'Option C-2',
      content: 'content',
    }]
  },
  {
    header: {sectionTitle:'Section D',
    isOpen:false
  },
  member: [
    {
      menuOptionName: 'Option D-1',
      content: 'content',
    },
    {
      menuOptionName: 'Option D-2',
      content: 'content',
    }]
  },
  {
    header: {
      sectionTitle:'Section E',
      isOpen:false
    },
    member: [
      {
        menuOptionName: 'Option E-1',
        content: 'content',
      },
      {
        menuOptionName: 'Option E-2',
        content: 'content',
      }]
    },

  ];


  export default class TestSharedComponent extends BaseViewComponent<{}> {


    constructor(props){

      super(props)

      var newsA = {title:"News A",order_date:"2018-01-20"};
      var newsB = {title:"News B",order_date:"2018-01-20"};
      var newsC = {title:"News C",order_date:"2018-01-20"};
      var newsD = {title:"News D",order_date:"2018-01-20"};
      var newsE = {title:"News E",order_date:"2018-01-20"};

      var dummyNewsModel = [newsA,newsB,newsC,newsD,newsE];


      var items = ["Option 1","Option 2","Option 3","Option 4","Option 5","Option 6","Option 7"];
      var options = ["Option 1","Option 2","Option 3"];

      this.state={

        authTokens:null,
        localStorageValue:"",

        optionData:MockData,
        menuOptionModels:this.initMenuGroupOptions(),
        supportOptions:this.initSupportOptions(),

        dummyNewsModel:dummyNewsModel,

        enableLoadingView:true,
        enableSingleButtonDialog:true,
        enableTwoButtonDialog:true,
        enablePicker:true,
        enableNavigationBar:true,
        enableActionSheet:true,

        actionSheetConfig:{
          options:options,
          optionCallback:this.optionCallback
        },

        navigationBarConfig:{
          title:"Testing Page",
          showLogo:false,
          showHome:true,
          showBack:false,
          showAlert:true,
          showMenu:true
        },


        pickerConfig:{
          title:"Select an option",
          items:items,
          selectedItem:items[3],
          doneCallback:this.pickerDoneCallback
        },

        singleButtonDialogConfig:{
          title:"TestSharedComponent",
          message:"This is the message content from TestSharedComponent",
          buttonLabel:"Ok",
          callback:this.singleButtonDialogCallback
        },

        twoButtonDialogConfig:{
          title:"",
          message:"Two button message from TestSharedComponent",
          leftButtonLabel:"Ok",
          rightButtonLabel:"Cancel",
          leftButtonCallback:this.leftButtonDialogCallback,
          rightButtonCallback:this.rightButtonDialogCallback
        }

      };

      this.renderNewsItems = this.renderNewsItems.bind(this);

      this.renderSectionListRow = this.renderSectionListRow.bind(this);
    }


    componentDidMount(){

      LocalStorageManager.getSavedAuthTokens((value)=>{
        // alert(value);

        this.setState({authTokens:value});
        console.log("expired? ",value.checkExpiry());
      },(error)=>{
        alert("failed to get value from local storage");
      })

    }

    initSupportOptions(){

      var menuOptionIcomNames = [require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png")];

      var menuOptions = [];
      for(var i=0;i<SupportOptions.length;i++){
        var menuOption = SupportOptions[i];
        var menuOptionModel = new MenuItemModel(menuOption.MenuOptionName,menuOptionIcomNames[i]);
        menuOptionModel.key = i;
        menuOptions.push(menuOptionModel);
      }
      return menuOptions;

    }


    optionCallback(option){
      alert("callback: "+option);
    }

    pickerDoneCallback(selectedItem){
      console.log("pickerDoneCallback() selectedItem: "+ selectedItem);
    }

    leftButtonDialogCallback(){

      console.log("tapped on left button on alert dialog");
    }

    rightButtonDialogCallback(){
      console.log("tapped on right button on alert dialog");
    }

    singleButtonDialogCallback(){

      console.log("tapped on single button on alert dialog");
    }

    renderNewsItems(){

      var newsItems = [];
      for(var i=0;i<this.state.dummyNewsModel.length;i++){
        // newsItems.push(<NewsItem key={i} newsModel={this.state.dummyNewsModel[i]}>
        // </NewsItem>)

        newsItems.push(<View key={i} style={{
          flex: 1,
          flexDirection:'row',
          height:UIUtils.size(90),
          alignItems:"center",
          width:UIUtils.getScreenWidth()
        }}><Text>{this.state.dummyNewsModel[i].title}</Text></View>)
      }
      return newsItems;

    }

    //overwrite
    showLoadingView(){
      super.showLoadingView();
      // alert("after super");

      setTimeout(() => {
        this.hideLoadingView();
      }, 1000);
    }

    renderRow(rowItem, rowId, sectionId){
      return (
        <MenuItemComponent menuOptionModel={rowItem}></MenuItemComponent>
      );
    }

    renderSection(section, sectionId){
      return(
        <ExpandableSectionComponent sectionModel={section}></ExpandableSectionComponent>
      );
    }

    /* Common section list: initMenuGroupOptions, renderSectionListRow, renderSectionHeader */
    initMenuGroupOptions(){
      var rawData = MyStudyOptions;
      var menuOptionGroups = {};

      //my timetable group
      var timetableGroup = rawData[0];
      var studyToolsGroup = rawData[1];
      var oversearsGroup = rawData[2];
      var academicCalendarGoup = rawData[3];

      var timetableGroupIcons = [require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png")];
      var studyToolsGroupIcons = [require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png"),
      require("../assets/img/ic_navbar_home.png")];
      var oversearsGroupIcons = [require("../assets/img/ic_navbar_home.png")];
      var academicCalendarGoupIcons = [require("../assets/img/ic_navbar_home.png")];

      menuOptionGroups[timetableGroup.SectionName] = [];
      menuOptionGroups[studyToolsGroup.SectionName] = [];
      menuOptionGroups[oversearsGroup.SectionName] = [];
      menuOptionGroups[academicCalendarGoup.SectionName] = [];

      for(var i=0;i<timetableGroup.OptionGroup.length;i++){
        var menuModel = timetableGroup.OptionGroup[i];
        var menuViewModel = new MenuItemModel(menuModel.MenuOptionName,timetableGroupIcons[i]);
        menuViewModel.key = menuModel.MenuOptionName;//key required for SectionList
        menuOptionGroups[timetableGroup.SectionName].push(menuViewModel);
      }

      for(var i=0;i<studyToolsGroup.OptionGroup.length;i++){
        var menuModel = studyToolsGroup.OptionGroup[i];
        var menuViewModel = new MenuItemModel(menuModel.MenuOptionName,studyToolsGroupIcons[i]);
        menuViewModel.key = menuModel.MenuOptionName;//key required for SectionList
        menuOptionGroups[studyToolsGroup.SectionName].push(menuViewModel);
      }

      for(var i=0;i<oversearsGroup.OptionGroup.length;i++){
        var menuModel = oversearsGroup.OptionGroup[i];
        var menuViewModel = new MenuItemModel(menuModel.MenuOptionName,oversearsGroupIcons[i]);
        menuViewModel.key = menuModel.MenuOptionName;//key required for SectionList
        menuOptionGroups[oversearsGroup.SectionName].push(menuViewModel);
      }

      for(var i=0;i<academicCalendarGoup.OptionGroup.length;i++){
        var menuModel = academicCalendarGoup.OptionGroup[i];
        var menuViewModel = new MenuItemModel(menuModel.MenuOptionName,academicCalendarGoupIcons[i]);
        menuViewModel.key = menuModel.MenuOptionName;//key required for SectionList
        menuOptionGroups[academicCalendarGoup.SectionName].push(menuViewModel);
      }


      var menuOptionGroupList = [];
      menuOptionGroupList.push({sectionTitle:timetableGroup.SectionName,data:menuOptionGroups[timetableGroup.SectionName]});
      menuOptionGroupList.push({sectionTitle:studyToolsGroup.SectionName,data:menuOptionGroups[studyToolsGroup.SectionName]});
      menuOptionGroupList.push({sectionTitle:oversearsGroup.SectionName,data:menuOptionGroups[oversearsGroup.SectionName]});
      menuOptionGroupList.push({sectionTitle:academicCalendarGoup.SectionName,data:menuOptionGroups[academicCalendarGoup.SectionName]});
      return menuOptionGroupList;
    }

    renderSectionListRow(rowData){
      return (
        <TouchableHighlight underlayColor="transparent" onPress={() => {}}>
        <View>
        <MenuItemComponent menuOptionModel={rowData}></MenuItemComponent>
        </View>
        </TouchableHighlight>
      )
    }

    renderSectionHeader(section) {
      if(section.sectionTitle===MyStudyOptions[2].SectionName||section.sectionTitle===MyStudyOptions[3].SectionName){
        return (
          <View>
          <VerticalMarginComponent height={LAYOUT_SPACE.STANDARD_MENU_MARGIN}></VerticalMarginComponent>
          </View>
        )
      }else{
        return (
          <View >
          <VerticalMarginComponent height={LAYOUT_SPACE.STANDARD_MENU_MARGIN}></VerticalMarginComponent>
          <MenuSectionComponent title={section.sectionTitle}></MenuSectionComponent>
          </View>
        )
      }
    }

    renderSupportItem(optionModel){
      return (
        <TouchableHighlight underlayColor="transparent" onPress={() => {}}>
        <View>
        <VerticalMarginComponent height={LAYOUT_SPACE.STANDARD_MENU_MARGIN}></VerticalMarginComponent>
        <MenuItemComponent style={styles.menuItem} menuOptionModel={optionModel}></MenuItemComponent>
        </View>
        </TouchableHighlight>
      )
    }



    popupMessage(){


    }

    renderView(){
      return (
        <View style={styles.container}>

          <TouchableHighlight underlayColor="transparent" onPress={() => this.popupMessage.bind(this)}>
            <View>
             <Text>Poup Message</Text>
            </View>
          </TouchableHighlight>


        <ScrollView>

        <View style={{height:100}}>
          <Text>Refresh Token: {this.state.authTokens==null?null:this.state.authTokens.refresh_token}</Text>
          <Text>Expired: {this.state.authTokens==null?null:this.state.authTokens.checkExpiry()}</Text>
        </View>

        <CommonListView models={this.state.supportOptions} renderItem={this.renderSupportItem}></CommonListView>

        <CommonSectionListView
        models={this.state.menuOptionModels}
        renderItem={this.renderSectionListRow}
        renderSectionHeader={this.renderSectionHeader}
          ></CommonSectionListView>

        <CommonExpandableListView
        dataSource={this.state.optionData}
        renderRow={this.renderRow}
        renderSection={this.renderSection}
        ></CommonExpandableListView>

        <Button title="show loading view" onPress={()=>this.showLoadingView()}></Button>
        <Button title="Single button dialog" onPress={()=>this.popSingleButtonAlertDialog()}></Button>
        <Button title="Two button dialog" onPress={()=>this.popTwoButtonAlertDialog()}></Button>
        <Button title="Show Picker" onPress={()=>this.showPickerComponent()}></Button>
        <Button title="Show ActionSheet" onPress={()=>this.showActionSheet()}></Button>
        <CommonSwiperComponent
        title={"News"}
        countItems={this.state.dummyNewsModel.length}
        renderItems={this.renderNewsItems}

        ></CommonSwiperComponent>

        <CommonSwiperComponent
        title={"Events"}
        countItems={this.state.dummyNewsModel.length}
        renderItems={this.renderNewsItems}
        ></CommonSwiperComponent>
        <CommonSwiperComponent
        title={"Blackboard"}
        countItems={this.state.dummyNewsModel.length}
        renderItems={this.renderNewsItems}
        ></CommonSwiperComponent>
        </ScrollView>
        </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF'
    },
    textPlaceholder: {
      height: 30,
      textAlign: 'center'
    }
  });
