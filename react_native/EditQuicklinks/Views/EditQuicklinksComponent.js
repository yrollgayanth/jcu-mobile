/**
* Created by Yrol on 17/03/18.
*/
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Platform, Dimensions, StyleSheet, Text, View, Alert, Button, Image, TouchableHighlight, ScrollView, ActivityIndicator } from 'react-native'
import TopMenuBar from '../../Widgets/EditTopMenuBar/EditTopMenuBar'
import EventEmitter from "react-native-eventemitter"
import Branding from '../../Utils/Branding'
import UIUtils from "../../Utils/UIUtils"
import BrowserUtils from '../../Utils/BrowserUtils'
import WebEngine from '../../Utils/WebEngine'
import {EVENT, TOP_BAR_OPTIONS, LAYOUT_SPACE, FONT_SIZE, COLOR} from '../../Constants/Constants'
import LinearGradient from 'react-native-linear-gradient'
import GreyDividerComponent from '../../SharedComponents/GreyDividerComponent'
import Modal from 'react-native-modalbox'
import SortableList from 'react-native-sortable-list'
import Row from '../../Widgets/QuicklinkRow/QuicklinkRowComponent.js'
import Orientation from 'react-native-orientation'
import NavigationBarComponent from '../../SharedComponents/NavigationBarComponent'


export default class EditQuicklinksComponent extends Component<{}> {

  _branding = Branding.sharedInstance
  _globalRerrangedObj = {}
  _screenWidth = Dimensions.get('window').width

  // static navigationOptions = ({ navigation }) => {
  //   header: { visible: false }
  // }

  constructor(prop){
    super(prop);
    this.state = {
      uniLinksExpanded:true,
      customLinksExpanded:true,
      showEditBlock:false,
      loaded:false,
      isHorizontal:false,
      loadingAttempted:false,
      dataChanged:false,
      uniLinks:null,
      customLinks:null,
      selectedLink:null,
      selectedTitle:null,
      isNotDragging:true,
      showCustomNavigation:this.props.navigation.state.params.showCustomNavigation
    }
    this.topMenuClicked = this.topMenuClicked.bind(this)
    this.editRowClicked = this.editRowClicked.bind(this)
    this.deleteRowClicked = this.deleteRowClicked.bind(this)
  }

  //fetch links
  async fetchQuickinks(){
    let webEngine = WebEngine.sharedInstance
    var uniLinksArr = []
    customLinksArr = []
    var customLinksObj = {}
    this.setFetchStatus(false, false, uniLinksArr, customLinksArr)
    await Promise.all([webEngine.fetchQuicklinks()]).then(values => {
      jsonOutput = values[0]
      if(jsonOutput === false){
        this.setFetchStatus(false, true, uniLinksArr, customLinksArr)
      }else{
        let allLinks = jsonOutput.links
        for(var i =0; i<allLinks.length; i++){
          if(allLinks[i].edit_allowed === false){
            uniLinksArr.push(allLinks[i])
          }else{
            //customLinksArr.push(allLinks[i])
            console.log("custom link title: " + allLinks[i].title + " quicklink ID: " + allLinks[i].url)
            customLinksObj[i] = {title:allLinks[i].title, url:allLinks[i].url, id:i}
          }
        }

        this._globalRerrangedObj = customLinksObj
        this.setFetchStatus(true, false, uniLinksArr, customLinksObj)
      }
    });
  }

  //set fetch status
  setFetchStatus(loadStatus, loadingAttempted, uniLinks, customLinks){
    this.setState({
      loaded:loadStatus,
      loadingAttempted:loadingAttempted,
      uniLinks:uniLinks,
      customLinks:customLinks
    })
  }

  deleteRow(deleteID){
    this._globalRerrangedObj = this.state.customLinks
    delete this._globalRerrangedObj[deleteID]
    let webSubmitObject = {student_id:this._branding.getStudentID(), quick_links:this._globalRerrangedObj}
    this.deleteItem(webSubmitObject)
  }

  deleteRowClicked(e){
    Alert.alert('Delete',
    'Are you sure to want to delete ' +  e.deleteTitle + '?',
      [
        {text: 'Yes', onPress: () => this.deleteRow(e.deleteID)},
        {text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
      ]
    )
  }

  editRowClicked(e){
    this.props.navigation.navigate("QiuckLinksEditor", {title:e.editTitle, url:e.editUrl, editID:e.editID, allLinks:this._globalRerrangedObj, refreshOnQuickLinkAdd: this.refreshOnQuickLinkAdd})
  }

  async deleteItem(newObj){
    let webEngine = WebEngine.sharedInstance
    this.setState({
      loaded:false
    })
    await Promise.all([webEngine.reorderAddDelEditQuicklink(newObj)]).then(values => {
      console.log("Delete result:" + values.quick_links)
      this.fetchQuickinks()
    })
  }

  _orientationDidChange = (orientation) => {
    if (orientation === 'LANDSCAPE') {
      this._screenWidth = Dimensions.get('window').width
      this.setState({isHorizontal: true})
    }else{
      this._screenWidth = Dimensions.get('window').width
      this.setState({isHorizontal: false})
    }
  }

  //show custom header
  renderNavigationBar(){
    if(this.state.showCustomNavigation === true){
      return(
        <NavigationBarComponent
            navigate={this.props.navigate}
            title={"Quicklinks"}
            showHome={false}
            showBack={true}
            showAlert={false}
            showEmail={false}
            showMenu={false}
            showLogo={false}
            showProfile={false}
            goBackCallback={()=>{this.goBackCall()}}
        ></NavigationBarComponent>
      )
    }
  }

  goBackCall(){
    this.props.navigation.goBack()
  }

  componentWillMount(){
    this.fetchQuickinks()
    EventEmitter.addListener(EVENT.QUICKLINK_TOPBAR, this.topMenuClicked)
    EventEmitter.addListener(EVENT.QUICKLINK_DELETE_ROW, this.deleteRowClicked)
    EventEmitter.addListener(EVENT.QUICKLINK_EDIT_ROW, this.editRowClicked)

    //get initial orientation
    const initial = Orientation.getInitialOrientation();
    if (initial === 'PORTRAIT') {
      this.state.isHorizontal = false
    } else {
      this.state.isHorizontal = true
    }
  }

  componentWillUnmount(){
    EventEmitter.removeListener(EVENT.QUICKLINK_TOPBAR, this.topMenuClicked)
    EventEmitter.removeListener(EVENT.QUICKLINK_DELETE_ROW, this.deleteRowClicked)
    EventEmitter.removeListener(EVENT.QUICKLINK_EDIT_ROW, this.editRowClicked)
    EventEmitter.emit(EVENT.QUICKLINK_REFRESH_HOME)
    Orientation.removeOrientationListener(this._orientationDidChange)
  }

  componentDidMount(){
    //remove orientation listener
    Orientation.addOrientationListener(this._orientationDidChange);
  }

  //failed view
  renderFailedView(message){
    return(
      <View style={styles.loadFailedContainer}>
      <Text>{message}</Text>
      <Image style={styles.errorImage}  source={require('../../../assets/img/loading_error.png')} />
      <Button onPress={() => this.fetchQuickinks()}
      title="Try again">
      </Button>
      </View>
    )
  }

  viewLinkOntheBrowser(title, url){
    BrowserUtils.openBrowser(title, url)
  }

  openCustomLink(title, url){
    //this.props.navigation.navigate("QuickLinksAdder")
    this.setState({
      selectedLink:url,
      selectedTitle:title
    })
    //this.refs.optionsModal.open()
  }

  closeModal(){
    //this.refs.optionsModal.close()
  }

  openEditLink(url, title){
    this.refs.optionsModal.close()
    this.props.navigation.navigate("QiuckLinksEditor",{url:url, title:title})
  }

  openDeleteLink(url){

  }

  openLinkOntheBrowser(url, title){
    this.refs.optionsModal.close()
    this.viewLinkOntheBrowser(title, url)
  }

  renderUniLinksExpandable(sectionTitle){
    var branding = Branding.sharedInstance
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {this.setState({uniLinksExpanded:!this.state.uniLinksExpanded})}}>
        <LinearGradient colors={branding.getHeaderGradient()}
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 1 }}
        style={styles.sectionItem} >
        <Text style={[styles.sectionTitle, {color:branding.getHeaderFontColor()}]}>{sectionTitle}</Text>
        {this.renderUnilinkArrow()}
        </LinearGradient>
      </TouchableHighlight>
    )
  }

  renderCustomLinksExpandable(sectionTitle){
    var branding = Branding.sharedInstance
    return(
      <TouchableHighlight style={styles.container}
      underlayColor="transparent"
      onPress={() => {this.setState({customLinksExpanded:!this.state.customLinksExpanded})}}>
        <LinearGradient colors={branding.getHeaderGradient()}
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 1 }}
        style={styles.sectionItem} >
        <Text style={[styles.sectionTitle, {color:branding.getHeaderFontColor()}]}>{sectionTitle}</Text>
        {this.renderCustomlinkArrow()}
        </LinearGradient>
      </TouchableHighlight>
    )
  }

  renderUniLinks(){
    if( typeof this.state.uniLinks === 'undefined' || this.state.uniLinks === null || this.state.uniLinks.length <=0 ){
      return(
        <View style={styles.nodataFoundContainer}><Text>{"No Uni links found"}</Text></View>
      )
    }

    if(this.state.uniLinksExpanded){
      var blocks = []
      let uniLinkData = this.state.uniLinks
      for(var i = 0; i<uniLinkData.length; i++){
        let currentIndex = i
        blocks.push(
          <TouchableHighlight onPress={()=>this.viewLinkOntheBrowser(uniLinkData[currentIndex].title, uniLinkData[currentIndex].url)} key={currentIndex}>
            <View style={{flexDirection: 'row'}}>
              <View style={styles.unilinksContainer}><Text>{uniLinkData[currentIndex].title}</Text></View>
              <GreyDividerComponent></GreyDividerComponent>
            </View>
          </TouchableHighlight>
        )
      }
      return blocks
    }
  }

  _renderCustomRow = ({data, active}) => {
    return <Row data={data} active={active} />
  }

  //change data listener
  _changedDataOrder = changedOrder => {
    var studentID = this._branding.getStudentID()
    let webEngine = WebEngine.sharedInstance
    var reArrangedObj = {}
    var webSubmitObject = {}
    var data = this.state.customLinks
    Object.keys(data).map(function (key){
      for(var i=0; i<changedOrder.length; i++){
        if(Number(changedOrder[i]) === Number(key)){
          reArrangedObj[i] = {title:data[key].title, url:data[key].url, id:key}
        }
      }
    })
    //this.setState({customLinks:reArrangedObj})
    webSubmitObject = {student_id:studentID, quick_links:reArrangedObj}
    this._globalRerrangedObj = reArrangedObj
    webEngine.reorderAddDelEditQuicklink(webSubmitObject)
  }

  _onActivateRow = activeRow =>{
    if(Platform.OS === 'android'){
      this.setState({isNotDragging:false})
    }
  }

  _onReleaseRow = releaseRow =>{
    if(Platform.OS === 'android'){
      this.setState({isNotDragging:true})
    }
  }

  renderCustomLinks(){
    if( typeof this.state.customLinks === 'undefined' || this.state.customLinks === null || this.state.customLinks.length <=0 ){
      return(
        <View style={styles.nodataFoundContainer}><Text>{"No Custom links found"}</Text></View>
      )
    }

    if(this.state.customLinksExpanded){
      return(
        <View style={styles.customLinkContainer}>
          <SortableList
            style={styles.list}
            scrollEnabled={false}
            contentContainerStyle={[styles.contentContainer, {width:this._screenWidth}]}
            data={this.state.customLinks}
            onChangeOrder={this._changedDataOrder}
            onActivateRow={this._onActivateRow}
            onReleaseRow={this._onReleaseRow}
            renderRow={this._renderCustomRow} />
        </View>
      )
    }
  }

  renderCustomlinkArrow(){
    if(this.state.customLinksExpanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      )
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      )
    }
  }

  renderUnilinkArrow(){
    if(this.state.uniLinksExpanded){
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_up.png')}></Image>
      );
    }else{
      return(
        <Image style={styles.arrowIcon} source={require('../../../assets/img/ic_arrow_white_down.png')}></Image>
      );
    }
  }

  //loader view
  renderLoaderView(){
    return(
      <View style={[styles.mainContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.loaded} size="large" color="#0000ff" />
      </View>
    )
  }

  renderTopMenuBar(){
    if(this.state.customLinksExpanded){
      return(
        <TopMenuBar></TopMenuBar>
      )
    }
  }

  refreshOnQuickLinkAdd = data => {
    this.fetchQuickinks()
  }

  topMenuClicked(e){
    if(e.selected_tab === TOP_BAR_OPTIONS.QUICKLINK_EDIT){
      this.setState({
        showEditBlock:!this.state.showEditBlock
      })
      this._branding.setQuicklinksEditMode(this.state.showEditBlock)
      EventEmitter.emit(EVENT.SHOW_QUICKLINK_EDIT_BUTTONS, {editStatus:this.state.showEditBlock})
    }else{
      if(Object.keys(this._globalRerrangedObj).length >= 10){
        alert("You can add only up to 10 links")
      }else{
        this.props.navigation.navigate("QuickLinksAdder", {allLinks:this._globalRerrangedObj, refreshOnQuickLinkAdd: this.refreshOnQuickLinkAdd})
      }
    }
  }

  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just openned');
  }

  render(){
    //load failed
    if(this.state.loadingAttempted === true){
      return this.renderFailedView("An error occurred while fetching data.")
    }

    //being loaded
    if(this.state.loaded === false){
      return this.renderLoaderView()
    }

    return(
      <View style={[{flex:1}]}>
        {this.renderNavigationBar()}
        <ScrollView
        scrollEnabled={this.state.isNotDragging}
        >
        <View style={[styles.topNoteMainContainer, {flex: 1, flexDirection: 'row'}]}>
            <Text style={styles.topNote}>{"This page allows you to add, edit and reorder quicklinks. Please note you will not be able to modify the mandatory quicklinks added by the university."}</Text>
        </View>
        {this.renderUniLinks()}
        {this.renderCustomLinksExpandable("Add / modify quicklinks")}
        {this.renderTopMenuBar()}
        <GreyDividerComponent></GreyDividerComponent>
        {this.renderCustomLinks()}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    flexDirection:'row',
  },
  container: {
    flexDirection:'column',
  },
  customLinkContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    ...Platform.select({
      ios: {
        paddingTop: 5,
      },
    }),
  },
  contentContainer: {

    ...Platform.select({
      ios: {
        paddingHorizontal: 5,
      },

      android: {
        paddingHorizontal: 0,
      }
    })
  },
  nodataFoundContainer:{
    padding:10,
    height:40,
    backgroundColor:"white"
  },
  unilinksContainer:{
    padding:10,
    height:40,
    backgroundColor:"#d4d4d4",
    marginBottom:10,
    borderColor:"#969696",
    borderWidth: 2,
    width:"100%"
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  arrowIcon:{
    width:UIUtils.size(15),
    height:UIUtils.size(15),
    resizeMode:'contain',
    marginRight:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID)
  },
  sectionTitle:{
    marginLeft:UIUtils.size(LAYOUT_SPACE.STANDARD_MARGIN_MID),
    color:"white",
    fontSize:UIUtils.size(FONT_SIZE.MENU_ITEM_TEXT_FONT_SIZE),
    fontWeight: 'bold'
  },
  sectionItem:{
    height:UIUtils.size(LAYOUT_SPACE.SECTION_HEADER_HEIGHT),
    alignItems:"center",
    flexDirection:'row',
    justifyContent:'space-between'
  },
  errorImage:{
    height:150,
    width:150
  },
  loadFailedContainer:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  wrapper: {
    paddingTop: 50,
    flex: 1
  },

  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  modal2: {
    height: 230,
    backgroundColor: "#3B5998"
  },

  optionsModal: {
    height: 300,
    width: 300
  },

  modal4: {
    height: 300
  },

  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },
  topNoteMainContainer:{
    marginBottom:UIUtils.size(15)
  },
  topNote:{
    marginLeft:UIUtils.size(15),
    marginTop:UIUtils.size(20),
    color: COLOR.TEXT_GREY
  },
  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  text: {
    color: "black",
    fontSize: 18,
    fontWeight:"bold"
  }

})
