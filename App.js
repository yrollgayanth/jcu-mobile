import React, { Component } from 'react';
import RNFetchBlob from 'react-native-fetch-blob';
import RNFS from'react-native-fs';
import WebEngine from "./react_native/Utils/WebEngine"
import {StyleSheet, Platform, View, Text, Image, ActivityIndicator, Card, Alert, DeviceEventEmitter, AsyncStorage, NetInfo } from 'react-native';
import EventEmitter from "react-native-eventemitter"
import { AppNavigator } from './react_native/AppNavigator'
import { StackNavigator } from "react-navigation"
import { APP_ENV,EVENT, DRAWER_MENU, STORAGE_KEY, DEFAULT_FILES, ENCRYPTION } from './react_native/Constants/Constants'
import  UIUtils from './react_native/Utils/UIUtils'
import  Branding from './react_native/Utils/Branding'
import WelcomeComponent from "./react_native/Welcome/Views/WelcomeComponent"
import CurrentStudentComponent from "./react_native/CurrentStudent/Views/CurrentStudentComponent"
import StepProcessComponent from "./react_native/StepProcess/Views/StepProcessComponent"
import NewStudentComponent from "./react_native/NewStudent/Views/NewStudentComponent"
import EditQuicklinksComponent from './react_native/EditQuicklinks/Views/EditQuicklinksComponent'
import EditLinkComponent from './react_native/EditLink/Views/EditLinkComponent'
import AddLinkComponent from './react_native/AddLink/Views/AddLinkComponent'
import SSOLoginComponent from './react_native/SSOLogin/Views/SSOLoginComponent'
import SeeAllNewsComponent from './react_native/AllNews/Views/SeeAllNewsComponent'
import InAppWebComponent from  "./react_native/InAppWeb/Views/InAppWebComponent"
import Drawer from './react_native/Drawer/Drawer';
import DrawerContent from './react_native/Drawer/DrawerViewComponent'
import { NativeEventEmitter, NativeModules } from 'react-native';
import ConfigUtils from './react_native/Utils/ConfigUtils';
import CookieManager from 'react-native-cookies';
import LocalStorageManager from './react_native/Utils/LocalStorageManager';
import AuthTokens from './react_native/Objects/AuthTokens';
import DeviceInfo from 'react-native-device-info';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
import Moment from 'moment'
import 'moment-timezone'

export default class App extends Component<{}> {

  iosEventSubscription;

  constructor(props) {

    super(props)
    this.state = {
      registeredStatus:false,
      firstLaunchInitiation:false,
      latestBrandingVersion:null,
      isLoading:false
    }

    //check UAT/DEV/PROD/APP version from native projects
    ConfigUtils.checkPlatformInfo()

    if(Platform.OS=='ios'){
      iosEventSubscription = new NativeEventEmitter(NativeModules.IOSEventManager)
    }

    this.tokenExpireLogout =  this.tokenExpireLogout.bind(this)
  }

  async saveProfileData(firstname, lastname){

    var branding = Branding.sharedInstance
    branding.setUserFirstname(firstname)
    branding.setUserLastname(lastname)

    try{
      console.log("firstname",firstname);
      console.log("lastname",lastname);
      await AsyncStorage.setItem(STORAGE_KEY.USER_FIRSTNAME, firstname)
      await AsyncStorage.setItem(STORAGE_KEY.USER_LASTNAME, lastname)
    }catch(error){
      console.log(error.message)
    }

  }

  getUserDetailsAndGoInside(studentId){

    var branding = Branding.sharedInstance
    let base_url = ConfigUtils.BASE_URL
    let url = base_url + "user/getUserDetails"
    var data = {"student_id":studentId}

    this.setState({isLoading:true})

    return new Promise(()=>{
      fetch(url,{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          "token":ConfigUtils.id_token
        },
        body:JSON.stringify(data)
      })
          .then(response=>response.json())
          .then(result=>{
            console.log("Userdetails:");
            console.log(result);
            console.log("student ID:" + result.student_id)
            branding.setStudentID(result.student_id)

            //flags used to determine workflow after login
            UIUtils.GO_HOME_FIRST = result.show_home_page
            UIUtils.SINGLE_OFFER = result.single_offer
            UIUtils.COURSE_ID = result.courseId

            //save user info in AsyncStorage
            this.saveProfileData(result.firstName,result.lastName);
            this.setState({registeredStatus:true});
            this.setState({isLoading:false})

          })
          .catch(error=>{
            this.setState({isLoading:false})
            console.log(error);
          })

    })
  }

  async checkLogin(){
    LocalStorageManager.getSavedAuthTokens((value)=>{
      if(value==null){
        this.setState({registeredStatus:false});
      }else{
        //console.log("login token:" + value.id_token)
        this.proceedToLogin(value) //can login to check if user can login
        // ConfigUtils.id_token = value.id_token
        // this.getUserDetailsAndGoInside(value.getStudentID())
      }
    },(error)=>{
      UIUtils.showGenericAlert("failed to get value from local storage")
    })
  }

  //check if user can login
  async proceedToLogin(value){
    let webEngine = WebEngine.sharedInstance
    var canSaveToken = false
    var tokenSaved = false
    var refreshedAuthToken = ""
    let now =  Moment(new Date()).utc(Moment.tz.guess()).format()
    let formattedTokenExpiry =   Moment.unix(value.expires_at).utc(Moment.tz.guess()).format()

    if(now > formattedTokenExpiry){
      this.setState({registeredStatus:false})
      this.setState({isLoading:true})
      await Promise.all([webEngine.fetchNewRefreshToken(value.id_token)]).then(values => {
        if(values[0] === false){
          canSaveToken = false
        }else{
          refreshedAuthToken = values[0]
          canSaveToken = true
        }
      })

      if(canSaveToken){
        this.setState({registeredStatus:false})
        this.setState({isLoading:false})

        LocalStorageManager.saveAuthTokens(refreshedAuthToken,()=>{
          LocalStorageManager.getSavedAuthTokens((savedValues)=>{
            ConfigUtils.id_token = savedValues.id_token
            this.getUserDetailsAndGoInside(savedValues.getStudentID())
          },(error)=>{
            this.logoutUser()
            this.setState({registeredStatus:false});
            this.setState({isLoading:false})
            UIUtils.showGenericAlert("An error occurred while refreshing the token. Please login again using your credentials.")
          })
        }),(error)=>{
          this.logoutUser()
          this.setState({registeredStatus:false});
          this.setState({isLoading:false})
          UIUtils.showGenericAlert("An error occurred while refreshing the token. Please login again using your credentials.")
        }
      }else{
        //stop the loading stuff
        this.logoutUser()
        this.setState({registeredStatus:false});
        this.setState({isLoading:false})
        UIUtils.showGenericAlert("An error occurred while refreshing the token. Please login again using your credentials.")
      }
    }else{
      LocalStorageManager.getSavedAuthTokens((savedValues)=>{
        ConfigUtils.id_token = savedValues.id_token
        this.getUserDetailsAndGoInside(savedValues.getStudentID())
      },(error)=>{
        this.logoutUser()
        UIUtils.showGenericAlert("An error occurred while refreshing the token. Please login again using your credentials.")
      })
    }
  }

  onIOSSSOSuccess(rawToken){
    //console.log("onIOSSSOSuccess token :" + rawToken['rawToken'])
    //console.log("rawToken",rawToken)

    var cookie = decodeURIComponent(rawToken['rawToken']).split(';')

    var jcu_token

    // for(var param of cookie){
    //   if(param.includes('id_token')){
    //     jcu_token = param.split(':')[1]
    //     break;
    //   }
    // }

    //console.log(rawToken['rawToken'])
    var jsonToken = JSON.parse(cookie[0])
    var authTokens = new AuthTokens(jsonToken)

    LocalStorageManager.saveAuthTokens(authTokens,()=>{
      DeviceEventEmitter.emit(EVENT.LOGIN_STATUS, {status:true})
    },(error)=>{
      alert("faled to save")
    })
  }

  async componentWillMount(){
    console.disableYellowBox = true //disable warnings
    //EventEmitter.on(EVENT.LOGIN_STATUS, this.loginCallback);

    //event emiiter for token expiration
    EventEmitter.addListener(EVENT.TOKEN_EXPIRE_LOGOUT, this.tokenExpireLogout)

    //check token expiry status
    UIUtils.checkTokenExpiry()

    if(Platform.OS=='ios'){
      iosEventSubscription.addListener('onIOSSSOSuccess', (rawToken) => this.onIOSSSOSuccess(rawToken))
    }

    DeviceEventEmitter.addListener(EVENT.LOGIN_STATUS, (e) => {
      if(e.status==true){
        this.setState({isLoading:true})

        this.checkLogin()
        // this.getUserDetailsAndGoInside();
      }else{
        this.state.registeredStatus = e.status
      }
      this.setState({now: Date.now()}) //can also use this.forceUpdate()
    });


    //open drawer
    DeviceEventEmitter.addListener(EVENT.OPEN_DRAWER, (e) => {
      this._drawer.open()
    });

    //close drawer
    DeviceEventEmitter.addListener(EVENT.CLOSE_DRAWER, (e) => {
      this._drawer.close()

      //go to relevant section if selected
      // if(e.selected === DRAWER_MENU.ENROLL_ITEM){
      //   EventEmitter.emit(EVENT.GO_STEP_PROCESS)
      // }

      if(e.selected === DRAWER_MENU.ENROLL_ITEM||
          e.selected === DRAWER_MENU.ENROLL_ITEM||
          e.selected === DRAWER_MENU.HOME_ITEM||
          e.selected === DRAWER_MENU.MY_STUDY_ITEM||
          e.selected === DRAWER_MENU.SUPPORT_ITEM||
          e.selected === DRAWER_MENU.MY_TOOLS_ITEM||
          e.selected === DRAWER_MENU.UNI_LIFE_ITEM||
          e.selected === DRAWER_MENU.FEEDBACK_ITEM||
          e.selected === DRAWER_MENU.OPEN_DRAWER_PROFILE||
          e.selected === DRAWER_MENU.NOTIFICATION_ITEM){
        EventEmitter.emit(EVENT.GO_FROM_SIDE_MENU,{toOpen:e.selected});
      }else if(e.selected === DRAWER_MENU.LOGOUT_ITEM){
        this.logoutUser()
      }
    });

    await NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);

  }

  async componentWillUnmount(){
    EventEmitter.removeListener(EVENT.TOKEN_EXPIRE_LOGOUT, this.tokenExpireLogout)
  }

  //logging out the user
  logoutUser(){
    LocalStorageManager.deleteSavedAuthTokens(()=>{
      this.setState({registeredStatus:false})

      //request to clear token from the server
      let webEngine = WebEngine.sharedInstance
      webEngine.logoutTokenClean()

      //delete coookie
      CookieManager.clearAll()
      var profileImageLocation = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
      this.deleteProfileImage(profileImageLocation)
    },(error)=>{
      alert("Failed to delete saved token");
    })
  }

  tokenExpireLogout(){
    //this.logoutUser()
  }

  deleteProfileImage(path){
    var branding = Branding.sharedInstance
    return RNFS.unlink(path).then(() => {
      branding.setProfileImage("")
    })
    .catch((err) => {
      console.log(err.message);
    })
  }


  handleConnectivityChange = isConnected => {

    // if (isConnected) {//online
    //   this.fetchBrandingFromServer()
    // }else{//offline
    //   console.log("processOfflineBranding")
    //   this.processOfflineBranding()
    // }
    this.processOfflineBranding()
  }

  //if internet available excute online branding
  async fetchBrandingFromServer(){
    try {
      const hasFirstlauchInitiated = await AsyncStorage.getItem(STORAGE_KEY.FIRST_LAUNCH)
      var jsonOutput = null
      var canProceedOnline = true
      if (hasFirstlauchInitiated === null){
        let webEngine = WebEngine.sharedInstance
        await Promise.all([webEngine.fetchBrandingData()]).then(values => {
          jsonOutput = values[0]

          if(jsonOutput === false){
            canProceedOnline = false
            this.processOfflineBranding()
          }
        });

        //do online processing
        if(canProceedOnline){
          var backgroundImageUrl = jsonOutput.brandSetting.defaultSection.backgroundImage
          var logoImageUrl = jsonOutput.brandSetting.navigationSection.backgroundImage
          var imageSaveDone = true

          await Promise.all([
            this.saveDefaultProfileImage(),
            this.saveOnlineImages(DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE, backgroundImageUrl),
            this.saveOnlineImages(DEFAULT_FILES.DEFAULT_LOGO_IMAGE, logoImageUrl),
            this.saveDefaultOnlineJsons(jsonOutput)
          ]).then(values => {
            if(values.indexOf(false) == 0){ //check if false returned by above operations
              imageSaveDone = false
            }
          })

          if(imageSaveDone){
            await Promise.all([
              this.initiateAppData(false),
              this.setfirstLaunchStatus()
            ])
          }else{
            this.processOfflineBranding()//fall back if any of the operations above fails
          }
        }
      }else{
        console.log("start initiate branding")
        this.initiateAppData(true) //initiate if already initiated
      }
    }catch(error){
      console.log(error.message)
      console.log("process offline branding")
      this.processOfflineBranding()//fallback to offline mode if any error occurred
    }
  }

  //process branding offline mode
  async processOfflineBranding(){
    try {
      const hasFirstlauchInitiated = await AsyncStorage.getItem(STORAGE_KEY.FIRST_LAUNCH)
      if (hasFirstlauchInitiated === null){
        console.log("hasFirstlauchInitiated")
        await Promise.all([
          this.saveDefaultProfileImage(),
          this.saveDefualtImages(DEFAULT_FILES.DEFAULT_LOGO_IMAGE),
          this.saveDefualtImages(DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE),
          this.saveDefaultJsons(DEFAULT_FILES.DEFAULT_BRANDING_JSON),
          this.saveDefaultJsons(DEFAULT_FILES.DEFAULT_SUPPORT_JSON),
          this.initiateAppData(false),
          this.setfirstLaunchStatus()
        ])
      }else{
        console.log("! hasFirstlauchInitiated")
        await Promise.all([
          this.initiateAppData(true)
        ])
      }
    } catch (error) {
      console.log(error.message)
    }
  }

  //initiate branding
  async initiateAppData(shouldUpdateLaunchStatus){

    var branding = Branding.sharedInstance
    var jsonFile = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BRANDING_JSON
    return new Promise(resolve => {
      RNFS.readFile(jsonFile, 'utf8')
      .then((contents) => {

        var jsonContent = JSON.parse(contents)

        //branding for headers in sections
        var sectionHeaderParams = jsonContent.brandSetting.headerBannerSection
        var bgLength = sectionHeaderParams.backgroundColour.length
        if(bgLength > 0){
          branding.clearHeaderGradient()
          for(var i = 0; i<bgLength; i++){
            branding.setHeaderColors(sectionHeaderParams.backgroundColour[i])
          }
        }

        //header text weight
        var headerFontWeight = sectionHeaderParams.fontWeight
        branding.setHeaderFontWeight(headerFontWeight)

        //header text color
        var headerFontColor = sectionHeaderParams.fontColour
        branding.setHeaderFontColor(headerFontColor)

        //navigation colors
        var navigationSection = jsonContent.brandSetting.navigationSection
        var navBgColorLength = navigationSection.backgroundColourMobile.length

        if(navBgColorLength > 0){
          branding.clearNavigationGradients()
          for(var i = 0; i<navBgColorLength; i++){
            branding.setNavigationColors(navigationSection.backgroundColourMobile[i])
          }
        }

        //navigation text weight
        var navigationFontWeight =  navigationSection.fontWeight
        branding.setNavigationFontWeight(navigationFontWeight)

        //navigation text color
        var navigationFontColor = navigationSection.fontColour
        branding.setNavigationFontColor(navigationFontColor)

        //tab bar
        var tabBarTapped = jsonContent.brandSetting.tabBarButtonSection.tapped
        var tabBarDefault = jsonContent.brandSetting.tabBarButtonSection.default
        var tabBarFontWeight = jsonContent.brandSetting.tabBarButtonSection.fontWeight

        //tab bar tapped font color
        var tabBarTappedFontColor = tabBarTapped.fontColour
        branding.setTabBarTappedFontColor(tabBarTappedFontColor)

        //tab bar tapped gradient
        var tabBarTappedGradient = tabBarTapped.backgroundColour
        var tabBarTappedGradientLength = tabBarTappedGradient.length
        if(tabBarTappedGradientLength > 0){
          branding.clearTabBarTappedGradients()
          for(var i = 0; i<tabBarTappedGradientLength; i++){
            branding.setTabBarTappedGradients(tabBarTappedGradient[i])
          }
        }

        //tab bar tapped font weight
        branding.setTabBarFontWeight(tabBarFontWeight)

        var tabBarNormalFontColor = tabBarDefault.fontColour
        branding.setTabBarNormalFontColor(tabBarNormalFontColor)

        //tab bar normal gradient
        var tabBarNormalGradient = tabBarDefault.backgroundColour
        var tabBarNormalGradientLength = tabBarNormalGradient.length
        if(tabBarNormalGradientLength > 0){
          branding.clearTabBarNormalGradients()
          for(var i = 0; i<tabBarNormalGradientLength; i++){
            branding.setTabBarNormalGradients(tabBarNormalGradient[i])
          }
        }

        //initiate profile image
        this.initiateProfileImage()

        //check login
        this.checkLogin()



        if(shouldUpdateLaunchStatus === true){
          this.setState({
            firstLaunchInitiation:true
          })
        }
      })
    });
  }

  async saveOnlineImages(image, url){
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE
    if(image === DEFAULT_FILES.DEFAULT_LOGO_IMAGE){
      filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
    }
    return RNFS.downloadFile({fromUrl:url, toFile: filePath}).promise
    .then(res => true)
    .catch(false)
  }

  //save online branding JSONS
  async saveDefaultOnlineJsons(jsonContent){
    this.state.latestBrandingVersion =  jsonContent.version //assign version number
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BRANDING_JSON
    return RNFS.writeFile(filePath, JSON.stringify(jsonContent), 'utf8')
    .then((success) => true)
    .catch((err) => false)
  }

  //save defualt images
  async saveDefualtImages(image){
    return new Promise(resolve => {
      var imageBase64 = require("./assets/img/default_background_base64.json")
      var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BACKGROUND_IMAGE

      if(image === DEFAULT_FILES.DEFAULT_LOGO_IMAGE){
        imageBase64 = require("./assets/img/default_logo_base64.json")
        filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_LOGO_IMAGE
      }

      imageBase64 = imageBase64.split('data:image/png;base64,');
      var imageData = imageBase64[1]

      RNFS.writeFile(filePath, imageData, 'base64').then((success) => {
      }).catch((error) => {
        console.log(error.message);
      });
    });
  }

  //save default jsons
  async saveDefaultJsons(json){
    return new Promise(resolve => {
      var jsonContent = require("./assets/default_jsons/branding.json")
      var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_BRANDING_JSON

      if(json === DEFAULT_FILES.DEFAULT_SUPPORT_JSON){
        jsonContent = require("./assets/default_jsons/support_json.json")
        filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_SUPPORT_JSON
      }else{
        this.state.latestBrandingVersion = jsonContent.version //assign version number
      }

      RNFS.writeFile(filePath, JSON.stringify(jsonContent), 'utf8')
      .then((success) => {
        console.log("default json write success")
      })
      .catch((err) => {
        console.log(err.message);
      });
    })
  }

  //during first launch - called in either offline and online mode depending on the internet connection
  async saveDefaultProfileImage(){
    //var branding = Branding.sharedInstance
    var imageBase64 = require("./assets/img/base64/default_profile_base64.json")
    var filePath = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    imageBase64 = imageBase64.split('data:image/png;base64,')
    var imageData = imageBase64[1]

    return RNFS.writeFile(filePath, imageData, 'base64').then((success) => {
      //branding.setProfileImage(filePath)
      console.log("profile image set success")
    }).catch((error) => {
      //branding.setProfileImage("")
      console.log(error.message);
    })
  }


  //initialise profile image - called every time while assuming "saveDefaultProfileImage" is saved
  async initiateProfileImage(){
    var branding = Branding.sharedInstance
    var profileImageLocation = UIUtils.getDocumentPath() + "/" + DEFAULT_FILES.DEFAULT_PROFILE_IMAGE
    if (await RNFS.exists(profileImageLocation)){
      branding.setProfileImage(profileImageLocation)
    } else {
      branding.setProfileImage("")
    }
  }

  //user first name and last names
  async initUserInformation(){

  }

  async componentDidMount(){

    // RNFS.readDir(RNFS.DocumentDirectoryPath) // On Android, use "RNFS.DocumentDirectoryPath" (MainBundlePath is not defined)
    // .then((result) => {
    //   console.log('GOT RESULT', result[0].path);
    //
    //   // stat the first file
    //   //return Promise.all([RNFS.stat(result[0].path), result[0].path]);
    // })
    // .catch((err) => {
    //   console.log(err.message, err.code);
    // });

  }

  //setting the launch status
  async setfirstLaunchStatus(){
    try {
      await AsyncStorage.setItem(STORAGE_KEY.FIRST_LAUNCH, "true");
      await AsyncStorage.setItem(STORAGE_KEY.CURRENT_BRANDING_VERSION, this.state.latestBrandingVersion.toString());
      this.setState({
        firstLaunchInitiation:true
      })
    } catch (error) {
      console.log(error.message)
    }
  }

  //render unregistered view
  renderUnregisteredView(){

    const Layout = StackNavigator(
        {
      Welcome:{
        screen: WelcomeComponent,
        navigationOptions: {
          gesturesEnabled: false,
          header: null
        }
      },
      CurrentStudent: {
        screen: CurrentStudentComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title: "Current Student Signup",
          headerStyle: {
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
          },
          headerTintColor: 'white',
          headerTitleStyle: { color: 'white' },
          headerBackTitleStyle: { color: 'white'},
        }
      },
      NewStudent: {
        screen: NewStudentComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title: "",
          headerStyle: {
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
          },
          headerTintColor: 'white',
          headerTitleStyle: { color: 'white' },
          headerBackTitleStyle: { color: 'white'},
        }
      },
      SSOLogin: {
        screen: SSOLoginComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title: "",
          headerStyle: {
            backgroundColor: 'transparent',
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            borderBottomWidth: 0
          },
          headerTintColor: 'white',
          headerTitleStyle: { color: 'white' },
          headerBackTitleStyle: { color: 'white'},
        }
      },
      StepProcess: {
        screen: StepProcessComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title: "Step Process",
          headerLeft: null
        }
      }
    },
      {
        transitionConfig: () => ({
          screenInterpolator: CardStackStyleInterpolator.forVertical,
        }),
        headerMode:'none'
      });
    return <Layout/>
  }

  //render registered view
  renderRegisteredView(){
    const Layout = StackNavigator({
      MainTabs:{
        screen: AppNavigator,
        navigationOptions: {
          gesturesEnabled: false,
          header: null
        }
      },
      AllNews:{
        screen: SeeAllNewsComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title:"News"
        }
      },
      EditQuicklinks:{
        screen:EditQuicklinksComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title:"Quicklinks"
        }
      },
      QiuckLinksEditor:{
        screen:EditLinkComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title:"Edit quicklink"
        }
      },
      QuickLinksAdder:{
        screen:AddLinkComponent,
        navigationOptions: {
          gesturesEnabled: false,
          title:"Add quicklink"
        }
      },
      InAppWeb: {
        screen: InAppWebComponent,
        navigationOptions: {
          gesturesEnabled: false,
        }
      }
    });

    return (
      //Material Design Style Drawer. (used to return <AppNavigator/> instead of <Layout/> without navigation)
      <Drawer ref={(ref) => this._drawer = ref} content={<DrawerContent/>}>
        <View style={styles.container}>
          <Layout/>
        </View>
      </Drawer>
    );
  }

  //first launch
  renderInitiationComponent(){
    return(
      <View style={[styles.loaderContainer, styles.horizontal]}>
      <ActivityIndicator animating={!this.state.firstLaunchInitiation} size="large" color="#0000ff" />
      </View>
    )
  }


  renderLoaderView(){
    return(
        <View style={[styles.mainContainer, styles.horizontal]}>
          <ActivityIndicator animating={this.state.isLoading} size="large" color="#0000ff" />
        </View>
    )
  }

  render(){

    if(this.state.isLoading){
      return (this.renderLoaderView())
    }

    if(!this.state.firstLaunchInitiation ){
      return(
        this.renderInitiationComponent()
      )
    }

    if(!this.state.registeredStatus){
      return this.renderUnregisteredView()
    }

    //with additional pages
    return this.renderRegisteredView()
  }
}

const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
  },

  loaderContainer: {
    flex: 1,
  },

  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },

  container: {
    flex:1
  },

  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  defaultFontFamily: {
    // fontFamily: ''
    fontFamily: 'system font',
    color:'red'
  }
});
//fix for oppo text cut off
const oldRender = Text.prototype.render;

Text.prototype.render = function (...args) {
    const origin = oldRender.call(this, ...args);
    if(UIUtils.checkDeviceModelNeedsExtraConfigForText()){
        return React.cloneElement(origin, {
            // style: [styles.defaultFontFamily, origin.props.style]
            style: [origin.props.style, styles.defaultFontFamily]
        });
    }else{
        return React.cloneElement(origin, {
            // style: [styles.defaultFontFamily, origin.props.style]
            style: [origin.props.style]
        });
    }

};
