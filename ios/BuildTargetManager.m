//
//  BuildTargetManager.m
//  JCUMobile
//
//  Created by Yiyin Shen on 1/3/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "BuildTargetManager.h"


@implementation BuildTargetManager

RCT_EXPORT_MODULE();


RCT_REMAP_METHOD(checkENV,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  NSString * env = [[[NSBundle mainBundle] infoDictionary] valueForKey:@"ENV_VARIABLE"];
  if (env) {
    resolve(env);
  } else {
    resolve(env);
  }
}


RCT_REMAP_METHOD(checkAPPVersion,
                 resolve:(RCTPromiseResolveBlock)resolve
                 rejecte:(RCTPromiseRejectBlock)reject)
{
  NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//  NSLog(@"app version: %@",version);
  if (version) {
    resolve(version);
  } else {
    resolve(version);
  }
}





@end
