#import "IOSSSOManager.h"
#import "SSOLoginViewController.h"

@implementation IOSSSOManager

RCT_EXPORT_MODULE();


//page title will be used for google analytics later
RCT_EXPORT_METHOD(openSSOPage:(NSString*) url)
{
  
//  NSLog(@"url: %@",url);
  dispatch_sync(dispatch_get_main_queue(), ^{

    UIViewController* rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
    SSOLoginViewController* ssoVC = [SSOLoginViewController new];
    ssoVC.ssoUrl = url;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:ssoVC];
    
    [rootVC presentViewController:navigationController animated:YES completion:nil];
    
  });
  
  
}

@end
