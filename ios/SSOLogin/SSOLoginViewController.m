//
//  SSOLoginViewController.m
//  JCUMobile
//
//  Created by Yiyin Shen on 26/3/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "SSOLoginViewController.h"
#import "IOSEventManager.h"

@interface SSOLoginViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadingLabelHeight;

@end

@implementation SSOLoginViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    [self setupNavigationBar];
  self.webView.delegate = self;
  [self loadWebView];

  self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(getArgs) userInfo:nil repeats:YES];
  [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];

}

- (void)viewWillDisappear:(BOOL)animated{

  [super viewWillDisappear:animated];
  [self.webView stopLoading];

}


- (void)getArgs{

  NSString *argsString = [self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('jcu_token').value;"];

  if (argsString.length > 0) {

    NSLog(@"%@",argsString);
    [self stopTimer];
    IOSEventManager *notification = [IOSEventManager allocWithZone: nil];
    [notification notiSSOSuccess:argsString];
    [self dismissViewControllerAnimated:YES completion:nil];
  }

}

- (void)stopTimer {
  [self.timer invalidate];
  self.timer = nil;
}

- (void)loadWebView {

  NSURL *url = [NSURL URLWithString:self.ssoUrl];
  NSURLRequest *requestObj = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:0];
  [self.webView loadRequest:requestObj];

}



- (void)setupNavigationBar{

  self.title = @"SSO Login";
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_nav_back_grey"] style:UIBarButtonItemStylePlain target:self action:@selector(backTapped)];
  self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];

  NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [UIColor darkGrayColor],NSForegroundColorAttributeName, nil];
  [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];

}


- (void)backTapped {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{

  self.loadingLabel.hidden = YES;
  self.loadingLabelHeight.constant = 0;

}

@end
