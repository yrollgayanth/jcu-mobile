//
//  SSOLoginViewController.h
//  JCUMobile
//
//  Created by Yiyin Shen on 26/3/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSOLoginViewController : UIViewController

@property (nonatomic,strong) NSString* ssoUrl;

@end
