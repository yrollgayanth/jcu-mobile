//
//  BuildTargetManager.m
//  JCUMobile
//
//  Created by Yiyin Shen on 6/3/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "IOSBrowserManager.h"
#import "PushVCManager.h"
#import "IOSWebViewController.h"

@implementation IOSBrowserManager

RCT_EXPORT_MODULE();


//open safari browser
RCT_EXPORT_METHOD(openBrowser:(NSString *)pageTitle url:(NSString*) url)
{
  
  NSLog(@"openBrowser %@ : %@",pageTitle,url);
  NSLog(@"url: %@",url);
  UIViewController* vc = [UIApplication sharedApplication].delegate.window.rootViewController;
  
  [PushVCManager pushWebVCwithVC:vc andURLString:url andTitle:pageTitle];
  
}


//open built in webview
RCT_EXPORT_METHOD(openInAppBrowser:(NSString *)pageTitle url:(NSString*) url)
{
  
  NSLog(@"openBrowser %@ : %@",pageTitle,url);
  NSLog(@"url: %@",url);
  
  
  dispatch_sync(dispatch_get_main_queue(), ^{
    
    UIViewController* rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
    IOSWebViewController* webView = [IOSWebViewController new];
    webView.url = url;
    webView.pageTitle = pageTitle;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webView];
    
    [rootVC presentViewController:navigationController animated:YES completion:nil];
    
  });
  
//  UIViewController* rootVC = [UIApplication sharedApplication].delegate.window.rootViewController;
//  IOSWebViewController* webView = [IOSWebViewController new];
//  webView.url = url;
//  webView.pageTitle = pageTitle;
//  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:webView];
//
//  [rootVC presentViewController:navigationController animated:YES completion:nil];
  
}


@end
