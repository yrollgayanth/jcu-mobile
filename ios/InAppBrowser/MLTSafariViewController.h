//
//  MLTSafariViewController.h
//  ConnexusPhase2
//
//  Created by Yiyin Shen on 10/02/2017.
//  Copyright © 2017 La Trobe University. All rights reserved.
//

#import <SafariServices/SafariServices.h>

@interface MLTSafariViewController : SFSafariViewController
@property (nonatomic,strong) NSString *pageTitle;
@end
