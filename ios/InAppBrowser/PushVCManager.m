//
//  PushVCManager.m
//  0110
//
//  Created by Yiyin Shen on 11/01/2017.
//  Copyright © 2017 Yiyin Shen. All rights reserved.
//

#import "PushVCManager.h"
#import <SafariServices/SafariServices.h>
#import "MLTSafariViewController.h"

@implementation PushVCManager

+ (void)pushWebVCwithVC:(UIViewController *)vc andURLString:(NSString *)urlString andTitle:(NSString *)title{

    if (![urlString hasPrefix:@"http://"] && ![urlString hasPrefix:@"https://"]) {
        urlString = [@"http://" stringByAppendingString:urlString];
    }
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"+"];

    NSURL *url = [NSURL URLWithString:urlString];
    MLTSafariViewController *mltWebVC = [[MLTSafariViewController alloc] initWithURL:url];
    mltWebVC.pageTitle = title;

    dispatch_sync(dispatch_get_main_queue(), ^{
      [vc presentViewController:mltWebVC animated:YES completion:nil];
    });
}

+ (void)pushWebVCwithVCWithCallback:(UIViewController *)vc andURLString:(NSString *)urlString andTitle:(NSString *)title{
  
  if (![urlString hasPrefix:@"http://"] && ![urlString hasPrefix:@"https://"]) {
    urlString = [@"http://" stringByAppendingString:urlString];
  }
  urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"+"];
  
  NSURL *url = [NSURL URLWithString:urlString];
  MLTSafariViewController *mltWebVC = [[MLTSafariViewController alloc] initWithURL:url];
  mltWebVC.pageTitle = title;
  
  [vc presentViewController:mltWebVC animated:YES completion:nil];
}

@end
