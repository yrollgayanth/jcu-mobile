//
//  PushVCManager.h
//  0110
//
//  Created by Yiyin Shen on 11/01/2017.
//  Copyright © 2017 Yiyin Shen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PushVCManager : NSObject

+ (void)pushWebVCwithVC:(UIViewController*)vc andURLString:(NSString*)urlString andTitle:(NSString*)title;
@end
