//
//  IOSWebViewController.h
//  JCUMobile
//
//  Created by Yiyin Shen on 27/3/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOSWebViewController : UIViewController
@property (nonatomic,strong) NSString* url;
@property (nonatomic,strong) NSString* pageTitle;

@end
