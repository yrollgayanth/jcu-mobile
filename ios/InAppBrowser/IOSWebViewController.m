//
//  IOSWebViewController.m
//  JCUMobile
//
//  Created by Yiyin Shen on 27/3/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "IOSWebViewController.h"
#import "IOSEventManager.h"

@interface IOSWebViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadingLabelHeight;

@end

@implementation IOSWebViewController

- (void)viewDidLoad {
  
  [super viewDidLoad];
  self.webView.delegate = self;
  self.webView.scalesPageToFit = YES;
  [self setupNavigationBar];
  [self loadWebView];
  
}

- (void)viewWillDisappear:(BOOL)animated{
  
  [super viewWillDisappear:animated];
  [self.webView stopLoading];
  IOSEventManager *notification = [IOSEventManager allocWithZone: nil];
  [notification notiQuitBrowser];
  
}

- (void)loadWebView {
  
  NSURL *url = [NSURL URLWithString:self.url];
//  NSURL *url = [NSURL URLWithString:@"https://s3-ap-southeast-2.amazonaws.com/jcu-static/jcu-builds/jcu-builds.html"];
  
  NSURLRequest *requestObj = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:0];
  [self.webView loadRequest:requestObj];
  
}

- (void)setupNavigationBar{
  
  self.title = self.pageTitle;
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_nav_back_grey"] style:UIBarButtonItemStylePlain target:self action:@selector(backTapped)];
  self.navigationController.navigationBar.tintColor = [UIColor darkGrayColor];
  
  NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [UIColor darkGrayColor],NSForegroundColorAttributeName, nil];
  [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
  
}

- (void)backTapped {
  [self dismissViewControllerAnimated:YES completion:nil];
  
}

- (void) webViewDidFinishLoad:(UIWebView *)webView{
  self.loadingLabel.hidden = YES;
  self.loadingLabelHeight.constant = 0;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {

  if ([[[request URL] scheme] isEqual:@"mailto"]) {
    [[UIApplication sharedApplication] openURL:[request URL]];
    return NO;
  }
  
  
  return YES;
}

- (IBAction)replayButtonTapped:(id)sender {
      [self.webView reload];
}
- (IBAction)nextButtonTapped:(id)sender {
  if ([self.webView canGoForward]) {
    [self.webView goForward];
  }
}

- (IBAction)prevButtonTapped:(id)sender {
  if ([self.webView canGoBack]) {
    [self.webView goBack];
  }
}

@end
