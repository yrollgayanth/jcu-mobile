//
//  MLTSafariViewController.m
//  ConnexusPhase2
//
//  Created by Yiyin Shen on 10/02/2017.
//  Copyright © 2017 La Trobe University. All rights reserved.
//

#import "MLTSafariViewController.h"
#import "IOSEventManager.h"

#import <React/RCTBridgeModule.h>
#import "React/RCTEventDispatcher.h"
#import "IOSEventManager.h"


@interface MLTSafariViewController ()

@end

@implementation MLTSafariViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
//    NSString * pageName = [NSString stringWithFormat:@"Web page: %@",self.pageTitle];
  
}

- (void)viewWillDisappear:(BOOL)animated{

  IOSEventManager *notification = [IOSEventManager allocWithZone: nil];
  [notification notiQuitBrowser];

}

@end
