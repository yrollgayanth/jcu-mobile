//
//  QtacQueryManager.m
//  JCUMobile
//
//  Created by Yiyin Shen on 17/4/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "QtacQueryManager.h"

@implementation QtacQueryManager

RCT_EXPORT_MODULE();


+ (void)saveQtacQuery:(NSString*)qtacQuery{
  
  [[NSUserDefaults standardUserDefaults] setValue:qtacQuery forKey:@"QtacQuery"];
  
}

+ (NSString*)getSavedQtacQuery{
  
  //delete qtac query, return qtac
  NSString *qtacQuery = [[[NSUserDefaults standardUserDefaults] stringForKey:@"QtacQuery"] copy];
  NSLog(@"getSavedQtacQuery:%@",qtacQuery);
  return qtacQuery;
}


RCT_REMAP_METHOD(checkQtacQuery,
                 resolveQtacQuery:(RCTPromiseResolveBlock)resolveQtacQuery
                 rejecteQtacQuery:(RCTPromiseRejectBlock)rejecteQtacQuery)
{
  NSString *qtacQuery = [QtacQueryManager getSavedQtacQuery];
  //  NSLog(@"app version: %@",version);
  if (qtacQuery) {
    resolveQtacQuery(qtacQuery);
  } else {
    resolveQtacQuery(qtacQuery);
  }
}

RCT_EXPORT_METHOD(clearQtacQuery)
{
  [QtacQueryManager saveQtacQuery:nil];
}

@end
