#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTLog.h>




@interface QtacQueryManager : NSObject <RCTBridgeModule>

+ (void)saveQtacQuery:(NSString*)jsonStr;
+ (NSString*)getSavedQtacQuery;

@end
