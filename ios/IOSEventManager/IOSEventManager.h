#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface IOSEventManager : RCTEventEmitter <RCTBridgeModule>

- (void)notiQuitBrowser;
- (void)notiSSOSuccess:(NSString*)jsonStr;
- (void)notiQtacParams:(NSString*)query;

@end



