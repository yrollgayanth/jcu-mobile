#import "IOSEventManager.h"
#import <React/RCTEventDispatcher.h>
#import <React/RCTLog.h>



@implementation IOSEventManager

RCT_EXPORT_MODULE();

+ (id)allocWithZone:(NSZone *)zone {
  static IOSEventManager *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [super allocWithZone:zone];
  });
  return sharedInstance;
}


- (NSArray<NSString *> *)supportedEvents {
  return @[@"QuitIOSBrowser",@"onIOSSSOSuccess",@"OnRetrieveQtacParams"];
}

- (void)notiQuitBrowser{
  [self sendEventWithName:@"QuitIOSBrowser" body:nil];
}

- (void)notiSSOSuccess:(NSString*)rawToken {
  NSLog(@"notiSSOSuccess");
  [self sendEventWithName:@"onIOSSSOSuccess" body:@{@"rawToken":rawToken}];
}

- (void)notiQtacParams:(NSString*)query {
  [self sendEventWithName:@"OnRetrieveQtacParams" body:@{@"query":query}];
}

@end

