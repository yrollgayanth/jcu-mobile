package com.jcumobile.InAppBrowser;

import android.content.Intent;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;

/**
 * Created by Sylvia on 2/3/18.
 */

public class AndroidBrowserManager extends ReactContextBaseJavaModule {

    public AndroidBrowserManager(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AndroidBrowserManager";
    }


    @ReactMethod
    public void openBrowser(String pageTitle, String url){

        System.out.println("pageTitle:"+pageTitle);
        System.out.println("url:"+url);

        Intent intent = new Intent(getCurrentActivity(),BrowserActivity.class);
        intent.putExtra("PAGE_TITLE", pageTitle);
        intent.putExtra("URL", url);
        getCurrentActivity().startActivity(intent);


    }

}
