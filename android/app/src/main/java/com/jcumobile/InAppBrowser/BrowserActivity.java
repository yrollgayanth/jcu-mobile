package com.jcumobile.InAppBrowser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.BinderThread;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.ReactContext;
import com.jcumobile.BuildConfig;
import com.jcumobile.MainApplication;
import com.jcumobile.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sylvia on 6/3/18.
 */

public class BrowserActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback{


    @BindView(R.id.webview)
    WebView webview;

    @BindView(R.id.loadingView)
    TextView loadingView;

    @BindView(R.id.btnBack)
    ImageButton btnBack;

    @BindView(R.id.tvTitle)
    TextView tvTitle;


    private DownloadManager.Request request;
    private final int REQUEST_STORAGE = 100;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_browser);
        ButterKnife.bind(this);

        String pageTitle = getIntent().getStringExtra("PAGE_TITLE");
        String url = getIntent().getStringExtra("URL");
//        url = "https://s3-ap-southeast-2.amazonaws.com/jcu-static/jcu-builds/jcu-builds.html";
        tvTitle.setText(pageTitle);
        refreshData(url,pageTitle,"");
    }



    @OnClick(R.id.btnBack)
    public void tappedOnBack(){


        MainApplication application = (MainApplication) getApplication();
        ReactNativeHost reactNativeHost = application.getReactNativeHost();
        ReactInstanceManager reactInstanceManager = reactNativeHost.getReactInstanceManager();
        ReactContext reactContext = reactInstanceManager.getCurrentReactContext();

        if (reactContext != null) {
            CatalystInstance catalystInstance = reactContext.getCatalystInstance();

            catalystInstance.callFunction("JavaScriptVisibleToJava", "onQuitWebpage", null);

        }

        finish();

    }



    public void refreshData(String url, String pageTitle, String pageNameForGoogleAnalytics) {

        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setDatabaseEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webview.getSettings().setDatabasePath("/data/data/" + webview.getContext().getPackageName() + "/databases/");
        }

//        webview.setWebViewClient(new JCUWebViewClient(this));
//        webview.setWebChromeClient(new WebChromeClient());
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(android.webkit.WebView view, final SslErrorHandler handler, SslError error) {
                if (!BuildConfig.FLAVOR.equals("prod")) {
                    handler.proceed();
                    return;
                }

                final AlertDialog.Builder builder = new AlertDialog.Builder(BrowserActivity.this
                );
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.show();
            }


            private Intent newEmailIntent(Context context, String address, String subject, String body, String cc) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
                intent.putExtra(Intent.EXTRA_TEXT, body);
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent.putExtra(Intent.EXTRA_CC, cc);
                intent.setType("message/rfc822");
                return intent;
            }

            @Override
            public boolean shouldOverrideUrlLoading(android.webkit.WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    final Activity activity = BrowserActivity.this;
                    if (activity != null) {
                        MailTo mt = MailTo.parse(url);
                        Intent i = newEmailIntent(activity, mt.getTo(), mt.getSubject(), mt.getBody(), mt.getCc());
                        activity.startActivity(i);
                        view.reload();
                        return true;
                    }
                } else if(url.contains("pdf")){
                    view.getSettings().setJavaScriptEnabled(true);
                    view.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
                } else{
                    view.loadUrl(url);
                }
                return true;
            }

            public void onPageStarted(android.webkit.WebView view, String url, Bitmap favicon) {

            }

            public void onPageFinished(android.webkit.WebView view, String url) {
                // hide the spinner
//                webLoadingView.hide();
                loadingView.setVisibility(View.GONE);
            }
        });


        /*handling download*/
        webview.setDownloadListener(new DownloadListener() {

            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {


                request = new DownloadManager.Request(Uri.parse(url));
                request.setMimeType(mimeType);
                //------------------------COOKIE!!------------------------
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie", cookies);
                //------------------------COOKIE!!------------------------
                request.addRequestHeader("User-Agent", userAgent);
                request.setDescription("Downloading file...");
                request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));

                if (ContextCompat.checkSelfPermission(BrowserActivity.this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startDownloading();
                }else{
                    ActivityCompat.requestPermissions(BrowserActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE);
                }

            }


        });


        if(url.contains("pdf")){
            webview.getSettings().setJavaScriptEnabled(true);
            webview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
        } else{
            webview.loadUrl(url);
        }






    }


    private void startDownloading(){

        DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(request);
        Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startDownloading();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @OnClick(R.id.btnPrevious)
    public void prevButtonTapped(){
        if (webview.canGoBack()) {
            webview.goBack();
        }

    }

    @OnClick(R.id.btnNext)
    public void nextButtonTapped(){
        if (webview.canGoForward()) {
            webview.goForward();
        }

    }

    @OnClick(R.id.btnRefresh)
    public void refreshButtonTapped(){
        webview.reload();
    }






}
