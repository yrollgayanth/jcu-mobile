package com.jcumobile.QtacQueryManager;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.webkit.CookieManager;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;


/**
 * Created by Sylvia on 18/4/18.
 */

public class QtacQueryManager extends ReactContextBaseJavaModule {


    public static final String QTAC_QUERY_KEY = "Qtac Query Key";

    private  ReactApplicationContext reactContext;

    public QtacQueryManager(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "AndroidQtacQueryManager";
    }



    @ReactMethod
    public void checkQtacQuery(Callback errorCallback,
                               Callback successCallback){

        try {
            String qtacQuery = getQtacQuery();
            successCallback.invoke(qtacQuery);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }

    }



    @ReactMethod
    public void clearQtacQuery(){

        System.out.println("clearQtacQuery");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(reactContext);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(QTAC_QUERY_KEY, "");
        editor.commit();


    }


    public void saveQtacQuery(String query){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(reactContext);
        final SharedPreferences.Editor editor = prefs.edit();
        editor.putString(QTAC_QUERY_KEY, query);
        editor.commit();

    }


    public String getQtacQuery(){

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(reactContext);
        try {
            return sharedPrefs.getString(QTAC_QUERY_KEY, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }




}
