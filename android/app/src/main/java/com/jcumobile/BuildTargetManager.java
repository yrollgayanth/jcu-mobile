package com.jcumobile;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.uimanager.IllegalViewOperationException;

/**
 * Created by Sylvia on 2/3/18.
 */

public class BuildTargetManager extends ReactContextBaseJavaModule {

    public BuildTargetManager(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AndroidBuildTargetManager";
    }


    @ReactMethod
    public void checkENV( Callback errorCallback,
                          Callback successCallback){

        try {
            System.out.println("BuildConfig.FLAVOR: "+BuildConfig.FLAVOR);
            String env = BuildConfig.FLAVOR;
            successCallback.invoke(env);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }

    }

    @ReactMethod
    public void checkAppVersion( Callback errorCallback,
                          Callback successCallback){

        try {
            System.out.println("APP_VERSION: "+BuildConfig.VERSION_NAME);
            String appVersion = BuildConfig.VERSION_NAME;
            successCallback.invoke(appVersion);
        } catch (IllegalViewOperationException e) {
            errorCallback.invoke(e.getMessage());
        }

    }

}
