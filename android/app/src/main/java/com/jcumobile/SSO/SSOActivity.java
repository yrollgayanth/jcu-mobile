package com.jcumobile.SSO;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.CatalystInstance;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableNativeArray;
import com.jcumobile.AndroidConstants;
import com.jcumobile.BuildConfig;
import com.jcumobile.MainApplication;
import com.jcumobile.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sylvia on 6/3/18.
 */

public class SSOActivity extends Activity {


    @BindView(R.id.webview)
    WebView webview;

    @BindView(R.id.loadingView)
    TextView loadingView;

    @BindView(R.id.btnBack)
    ImageButton btnBack;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private String sso_url;


    private Timer checkLoginSucceededTimer;

    private class CheckLoginSucceededTimerTask extends TimerTask {


        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @SuppressLint("JavascriptInterface")
                @Override
                public void run() {
                    webview.addJavascriptInterface(SSOActivity.this, "android");
                    webview.loadUrl("javascript:android.checkLoginSucceeded(document.getElementById('jcu_token').value)");
                    //webview.loadUrl("javascript:android.checkLoginSucceeded(function getCookie(){ var retrievedObject = localStorage.getItem('jcu_token'); return retrievedObject;} getCookie())");
//                    webview.loadUrl("javascript:android.checkLoginFailed(document.getElementById('errorText').innerHTML)");
                }
            });
        }

    }

    @JavascriptInterface
    public void checkLoginSucceeded(final String value) {

        System.out.println(value);

        if (checkLoginSucceededTimer != null) {
            checkLoginSucceededTimer.cancel();
            checkLoginSucceededTimer.purge();
            checkLoginSucceededTimer = null;
        }


        MainApplication application = (MainApplication) getApplication();
        ReactNativeHost reactNativeHost = application.getReactNativeHost();
        ReactInstanceManager reactInstanceManager = reactNativeHost.getReactInstanceManager();
        ReactContext reactContext = reactInstanceManager.getCurrentReactContext();

        if (reactContext != null) {
            CatalystInstance catalystInstance = reactContext.getCatalystInstance();
            WritableNativeArray params = new WritableNativeArray();

            /*decode and get json*/
            try {

                String[] cookies = URLDecoder.decode(value, "UTF-8").split(";");
                //System.out.print(cookies[0]);
//                System.out.print(cookies[0]);
//                System.out.print("cookies[1]");
//                System.out.print(cookies[1]);
//

                String decodeCookie = "" ;
//                for(String param:cookies) {
//                    if (param.contains("jcu_token")) {
//                        decodeCookie = param;
//                        break;
//                    }
//                }
                //System.out.println("decodeCookie");
                //System.out.println(decodeCookie);

                //String jcu_token = decodeCookie.split("=")[0];

                //System.out.println("jcu_token");
                //System.out.println(jcu_token);

                JSONObject mainObject = new JSONObject(cookies[0]);
                Log.v("JSON", mainObject.toString());
                //System.out.println("mainObject");
                //System.out.println(mainObject);

                AndroidConstants.SSO_OPENED = false;
                finish();
                params.pushString(mainObject.toString());
                catalystInstance.callFunction("JavaScriptVisibleToJava", "onAndroidSSOSuccess", params);



            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            catch (JSONException e) {
                e.printStackTrace();
            }



        }


    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_sso);
        ButterKnife.bind(this);

        sso_url = getIntent().getStringExtra("SSOURL");
        tvTitle.setText("SSO Login");

        refreshData(sso_url,"");
    }



    @OnClick(R.id.btnBack)
    public void tappedOnBack(){
        AndroidConstants.SSO_OPENED = false;
        finish();

    }


    public void refreshData(String url, String pageNameForGoogleAnalytics) {


        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setDatabaseEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webview.getSettings().setDatabasePath("/data/data/" + webview.getContext().getPackageName() + "/databases/");
        }

        webview.setWebChromeClient(new WebChromeClient());
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                if (!BuildConfig.FLAVOR.equals("prod")) {
                    handler.proceed();
                    return;
                }

                final AlertDialog.Builder builder = new AlertDialog.Builder(SSOActivity.this
                );
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += " Do you want to continue anyway?";

                builder.setTitle("SSL Certificate Error");
                builder.setMessage(message);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {


            }

            public void onPageFinished(WebView view, String url) {
                // hide the spinner
//                webLoadingView.hide();
                loadingView.setVisibility(View.GONE);
            }
        });
        webview.loadUrl(url);

        if(checkLoginSucceededTimer == null){
            checkLoginSucceededTimer = new Timer();
            checkLoginSucceededTimer.schedule(new CheckLoginSucceededTimerTask(),0,100);
        }
    }
}
