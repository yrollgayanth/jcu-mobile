package com.jcumobile.SSO;

import android.content.Intent;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.jcumobile.AndroidConstants;
import com.jcumobile.InAppBrowser.BrowserActivity;

import static android.content.ContentValues.TAG;

/**
 * Created by Sylvia on 2/3/18.
 */

public class AndroidSS0Manager extends ReactContextBaseJavaModule {


    public AndroidSS0Manager(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AndroidSS0Manager";
    }


    @ReactMethod
    public void openSSOPage(String ssoURL){


        if(!AndroidConstants.SSO_OPENED){

            AndroidConstants.SSO_OPENED = true;
            CookieManager.getInstance().removeAllCookie();
            Intent intent = new Intent(getCurrentActivity(),SSOActivity.class);
            intent.putExtra("SSOURL", ssoURL);
            getCurrentActivity().startActivity(intent);

        }


    }

}
