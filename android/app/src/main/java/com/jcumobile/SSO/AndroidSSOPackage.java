package com.jcumobile.SSO;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.jcumobile.InAppBrowser.AndroidBrowserManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Sylvia on 2/3/18.
 */

public class AndroidSSOPackage implements ReactPackage{
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new AndroidSS0Manager(reactContext));

        return modules;
    }


}
